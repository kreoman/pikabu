import { Container } from 'typedi';

import { IndexeddbCacheDbPrefixToken } from '@des-kit/cache';

Container.set(IndexeddbCacheDbPrefixToken, 'des--cache-');
