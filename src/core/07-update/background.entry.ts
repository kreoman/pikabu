import { Container } from 'typedi';

import { ExtensionService } from '@des-kit/extension';

const extensionService = Container.get(ExtensionService);

const runtime = extensionService.runtime;

if (!runtime) {
    throw new Error(`runtime does not exist`);
}

runtime.onUpdateAvailable.addListener((details) => () => {
    runtime.reload();
});
