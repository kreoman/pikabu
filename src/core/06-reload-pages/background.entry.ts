import { Container } from 'typedi';

import { ExtensionService } from '@des-kit/extension';
import { NavigationService } from '@des-kit/extension-bg-navigation';

const navigationService = Container.get(NavigationService);
const extensionService = Container.get(ExtensionService);

const runtime = extensionService.runtime;

runtime?.onInstalled.addListener(() => {
    void navigationService.reloadTabs();
});
