import {
    EXTENSION_CURRENT_RUNTIME_TYPE,
    ExtensionRuntimeTypeEnum,
} from '@des-kit/extension';

const runtime = EXTENSION_CURRENT_RUNTIME_TYPE;

if (runtime !== ExtensionRuntimeTypeEnum.Popup) {
    console.error(`Popup EXTENSION_CURRENT_RUNTIME_TYPE: ${runtime}`);
}
