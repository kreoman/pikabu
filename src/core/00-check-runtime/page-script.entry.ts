import {
    EXTENSION_CURRENT_RUNTIME_TYPE,
    ExtensionRuntimeTypeEnum,
} from '@des-kit/extension';

const runtime = EXTENSION_CURRENT_RUNTIME_TYPE;

if (runtime !== ExtensionRuntimeTypeEnum.PageScript) {
    console.error(`PageScript EXTENSION_CURRENT_RUNTIME_TYPE: ${runtime}`);
}
