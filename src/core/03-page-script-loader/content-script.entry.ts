import { Container } from 'typedi';

import { PageScriptLoaderService } from '@des-kit/extension';

const scriptLoaderService = Container.get(PageScriptLoaderService);

void scriptLoaderService.loadResourceFile('page-script.js');
