import './scale.scss';

import { StatusEnum } from '@des-kit/styles';
import classNames from 'classnames';

export interface ScaleProps {
    readonly position: number;
    readonly leftStatus: StatusEnum;
    readonly rightStatus: StatusEnum;
    readonly accent?: null | 'left' | 'right';
}

export function Scale(props: ScaleProps) {
    return (
        <div className="des--scale">
            <div
                className={classNames('des--scale__left', {
                    'des--scale__left_accent': props.accent === 'left',
                })}
                data-status={props.leftStatus}
                style={{ width: `${props.position * 100}%` }}></div>
            <div
                className={classNames('des--scale__right', {
                    'des--scale__right_accent': props.accent === 'right',
                })}
                data-status={props.rightStatus}></div>
        </div>
    );
}
