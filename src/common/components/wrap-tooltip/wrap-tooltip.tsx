import './wrap-tooltip.scss';

import { ReactElement, ReactNode } from 'react';

import { SizeEnum } from '@des-kit/styles';
import { TooltipWrapper } from '@des-kit/tooltip-wrapper';

export interface WrapTooltipProps {
    readonly tooltip: ReactNode;
    readonly children: ReactElement;
}

export function WrapTooltip({ tooltip, children }: WrapTooltipProps) {
    let tooltipContent: ReactNode;

    if (typeof tooltip === 'string') {
        tooltipContent = (
            <div className="des--wrap-tooltip-text">
                <p>{tooltip}</p>
            </div>
        );
    } else {
        tooltipContent = (
            <div className="des--wrap-tooltip-text">{tooltip}</div>
        );
    }

    return (
        <TooltipWrapper
            tooltipContent={tooltipContent}
            styleOverride={{ size: SizeEnum.Medium }}>
            {children}
        </TooltipWrapper>
    );
}
