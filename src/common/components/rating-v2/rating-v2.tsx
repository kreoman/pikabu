import './rating-v2.scss';

import classNames from 'classnames';

import { isSet } from '@des-kit/core';
import { Label } from '@des-kit/label';

export interface RatingV2Props {
    readonly className?: string;
    readonly plusesCount: number;
    readonly minusesCount: number | null;
    readonly upVoted?: boolean;
    readonly downVoted?: boolean;
}

export function RatingV2(props: RatingV2Props) {
    const className = classNames('des--rating-v2', props.className);

    if (!isSet(props.minusesCount)) {
        return <Label>Плюсов: {props.plusesCount}</Label>;
    }

    const pluses = props.plusesCount + (props.upVoted ? 1 : 0);
    const minuses = props.minusesCount + (props.downVoted ? 1 : 0);
    const sum = pluses + minuses;
    const rate = pluses / sum;
    const percent = `${rate * 100}%`;

    let background = '';

    if (sum) {
        background = `linear-gradient(90deg, var(--dkit-status-success-bg) 0 ${percent}, var(--dkit-status-error-bg) ${percent} 100%)`;
    }

    return (
        <Label
            className={className}
            style={{ background }}>
            <span className="des--rating-v2__pluses">{pluses}</span>
            <span>-</span>
            <span className="des--rating-v2__minuses">{minuses}</span>
        </Label>
    );
}
