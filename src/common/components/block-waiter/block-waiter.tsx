import './block-waiter.scss';

import { SizeEnum } from '@des-kit/styles';
import { Waiter } from '@des-kit/waiter';

export function BlockWaiter() {
    return (
        <div className="des--block-waiter-container">
            <Waiter styleOverride={{ size: SizeEnum.Large }} />
        </div>
    );
}
