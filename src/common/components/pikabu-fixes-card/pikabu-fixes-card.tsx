import './pikabu-fixes-card.scss';

import { ReactNode } from 'react';

import { Card } from '@des-kit/card';
import { Icon } from '@des-kit/icon';

import ExtensionIcon from '../../../icons/pf-icon-monochrome.svg';
import SettingsIcon from '../../../icons/settings.svg';
import { ExtensionNameConst } from '../../consts/extension-name.const';

export interface PikabuFixesCardProps {
    readonly title?: ReactNode;
    readonly onSettingsClick?: () => void;
    readonly children?: ReactNode;
}

export function PikabuFixesCard(props: PikabuFixesCardProps) {
    const header = (
        <>
            {props.title ?? ExtensionNameConst}{' '}
            {props.onSettingsClick && (
                <Icon
                    className="des--pikabu-fixes-card__settings-icon"
                    src={SettingsIcon}
                    onClick={props.onSettingsClick}
                />
            )}
        </>
    );

    return (
        <Card
            className="des--pikabu-fixes-card"
            islandClassName="des--pikabu-fixes-card__island"
            iconSrc={ExtensionIcon}
            title={header}>
            {props.children}
        </Card>
    );
}
