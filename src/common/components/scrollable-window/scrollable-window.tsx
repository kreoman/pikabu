import './scrollable-window.scss';

import { Scroll, ScrollProps } from '@des-kit/scroll';
import { Window, WindowProps } from '@des-kit/window';
import classNames from 'classnames';
import { ReactNode } from 'react';

export interface ScrollableWindowProps {
    readonly title: string;
    readonly children: ReactNode;
    readonly onClose?: () => void;
    readonly blockRadius?: boolean; // default true
    readonly className?: string;
    readonly windowProps?: Partial<WindowProps>;
    readonly scrollProps?: Partial<ScrollProps>;
}

export function ScrollableWindow(props: ScrollableWindowProps) {
    const className = classNames('des--scrollable-window', props.className);
    const blockRadius = props.blockRadius ?? true;
    const scrollRadius = blockRadius ? 'block' : void 0;

    const windowProps: Partial<WindowProps> = {
        ...props.windowProps,
        ...(props.onClose
            ? {
                  onBackdropClick: props.onClose,
                  onCloseClick: props.onClose,
              }
            : {}),
    };

    return (
        <Window
            title={props.title}
            {...windowProps}
            className={className}>
            <Scroll
                {...props.scrollProps}
                radius={scrollRadius}>
                {props.children}
            </Scroll>
        </Window>
    );
}
