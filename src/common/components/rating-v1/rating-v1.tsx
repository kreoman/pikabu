import './rating-v1.scss';

import classNames from 'classnames';
import { ReactNode } from 'react';

import { isSet } from '@des-kit/core';
import { SizeEnum, StatusEnum } from '@des-kit/styles';
import { Waiter } from '@des-kit/waiter';

import { Scale } from '../scale/scale';

export interface RatingV1Props {
    readonly title?: string;
    readonly loading?: boolean;
    readonly errorText?: string;
    readonly ratingCount: number | null;
    readonly plusesCount: number | null;
    readonly minusesCount: number | null;
    readonly upVoted?: boolean;
    readonly downVoted?: boolean;
}

export function RatingV1(props: RatingV1Props) {
    function getStub(text: ReactNode, hint?: string) {
        return (
            <div className="des--rating-v1">
                <div
                    className="des--rating-v1__text hint"
                    aria-label={hint}>
                    {text}
                </div>
                <Scale
                    position={1}
                    leftStatus={StatusEnum.Neutral}
                    rightStatus={StatusEnum.Neutral}
                />
            </div>
        );
    }

    if (props.loading) {
        return getStub(
            <Waiter styleOverride={{ size: SizeEnum.Medium }} />,
            'Загружаем рейтинг',
        );
    }

    if (props.errorText) {
        return getStub(`${props.plusesCount ?? '●'}`, props.errorText);
    }

    if (!isSet(props.plusesCount)) {
        return getStub(`●`, props.title ?? 'Рейтинг скрыт');
    }

    if (!isSet(props.minusesCount)) {
        return getStub(
            `${props.ratingCount ?? '●'}`,
            props.title ?? 'Нет информации о минусах',
        );
    }

    const pluses = props.plusesCount + (props.upVoted ? 1 : 0);
    const minuses = props.minusesCount + (props.downVoted ? 1 : 0);
    const sum = pluses + minuses;
    let rate = pluses / sum;

    const accent = props.upVoted ? 'left' : props.downVoted ? 'right' : null;

    if (isNaN(rate)) {
        rate = 1;
    }

    return (
        <div className="des--rating-v1">
            <div
                className={classNames('des--rating-v1__text', {
                    hint: props.title,
                })}
                aria-label={props.title}>
                <span data-status={StatusEnum.Success}>{pluses}</span>
                <span>-</span>
                <span data-status={StatusEnum.Error}>{minuses}</span>
            </div>
            <Scale
                position={rate}
                leftStatus={StatusEnum.Success}
                rightStatus={StatusEnum.Error}
                accent={accent}
            />
        </div>
    );
}
