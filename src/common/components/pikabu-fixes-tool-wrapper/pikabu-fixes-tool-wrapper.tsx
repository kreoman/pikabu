import './pikabu-fixes-tool-wrapper.scss';

import { SizeContext, SizeEnum } from '@des-kit/styles';
import classNames from 'classnames';
import { ReactNode } from 'react';

import { ExtensionNameConst } from '../../consts/extension-name.const';

export interface PikabuFixesStoryToolbarBlockProps {
    readonly highlight?: boolean;
    readonly className?: string;
    readonly children: ReactNode;
}

export function PikabuFixesToolWrapper(
    props: PikabuFixesStoryToolbarBlockProps,
) {
    const className = classNames(
        'des--pikabu-fixes-tool-wrapper',
        props.className,
        {
            'des--pikabu-fixes-tool-wrapper_highlight': props.highlight,
        },
    );

    return (
        <SizeContext.Provider value={SizeEnum.Small}>
            <div className={className}>
                {props.children}
                <div className={'des--pikabu-fixes-tool-wrapper__label'}>
                    {ExtensionNameConst}
                </div>
            </div>
        </SizeContext.Provider>
    );
}
