export function stringifyError(error: any): string {
    console.log(error);

    if (error instanceof Error) {
        return `${error.stack}`;
    }

    return String(error);
}
