import { ReactNode, useState } from 'react';
import { Container } from 'typedi';

import { NavigationService } from '@des-kit/extension-bg-navigation';
import { Icon } from '@des-kit/icon';

import { PikabuFixesCard } from '../../../../common/components/pikabu-fixes-card/pikabu-fixes-card';
import { ExtensionLinksConst } from '../../../../common/consts/extension-links.const';
import { ExtensionNameConst } from '../../../../common/consts/extension-name.const';
import TelegramIcon from '../../../../icons/telegram.svg';
import { PikabuFixesSettingsWindow } from '../pikabu-fixes-settings-window/pikabu-fixes-settings-window';

export interface PikabuSidebarBlockProps {
    readonly children: ReactNode;
}

export function PikabuSidebarBlock(props: PikabuSidebarBlockProps) {
    function goToTelegram() {
        const navigationService = Container.get(NavigationService);
        void navigationService.openInNewTab(ExtensionLinksConst.TelegramGroup);
    }

    const telegramIcon = (
        <Icon
            src={TelegramIcon}
            onClick={goToTelegram}
        />
    );

    const title = (
        <>
            {ExtensionNameConst} {telegramIcon}
        </>
    );

    const [settingsWindowShown, setSettingsWindowShown] = useState(false);

    function onSettingsClick() {
        setSettingsWindowShown(true);
    }

    function onSettingsCloseClick() {
        setSettingsWindowShown(false);
    }

    return (
        <PikabuFixesCard
            title={title}
            onSettingsClick={onSettingsClick}>
            {props.children}

            {settingsWindowShown && (
                <PikabuFixesSettingsWindow onClose={onSettingsCloseClick} />
            )}
        </PikabuFixesCard>
    );
}
