import { isSet } from '@des-kit/core';
import { useObservable } from '@des-kit/hooks';
import { OverlayZIndexContext } from '@des-kit/overlay';
import { Styles, ThemeContext } from '@des-kit/styles';
import { HTMLAttributes } from 'react';

import { listenTheme } from '../../../pikabu-dom/functions/listen-theme';

export function PikabuTheme(props: HTMLAttributes<any>) {
    const [theme] = useObservable(() => listenTheme(), null, []);

    if (!isSet(theme)) {
        return null;
    }

    return (
        <ThemeContext.Provider value={theme}>
            <OverlayZIndexContext.Provider value={100000}>
                <Styles
                    bg={false}
                    {...props}>
                    {props.children}
                </Styles>
            </OverlayZIndexContext.Provider>
        </ThemeContext.Provider>
    );
}
