import { ScrollableWindow } from '../../../../common/components/scrollable-window/scrollable-window';
import { ExtensionNameConst } from '../../../../common/consts/extension-name.const';
import { PikabuFixesSettings } from '../../../settings/ui/pikabu-fixes-settings/pikabu-fixes-settings';

export interface PikabuFixesSettingsWindowProps {
    onClose?: () => void;
}

export function PikabuFixesSettingsWindow(
    props: PikabuFixesSettingsWindowProps,
) {
    return (
        <ScrollableWindow
            title={`Настройки ${ExtensionNameConst}`}
            onClose={props.onClose}>
            <PikabuFixesSettings />
        </ScrollableWindow>
    );
}
