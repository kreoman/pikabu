import { Observable } from 'rxjs';

import { KeysOfType } from '@des-kit/core';

import { SettingsModel } from '../../settings/models/settings.model';

export interface FeatureModel {
    readonly enabledOn?: KeysOfType<SettingsModel, boolean>;
    readonly featureFactory: () => Observable<any>;
}
