import { BoosterFeature } from '../features/booster.feature';
import { PikabuCommentRatingFeature } from '../features/pikabu-comment-rating.feature';
import { PikabuCommentsExpandAutoFeature } from '../features/pikabu-comments-expand-auto.feature';
import { PikabuCommentsExpanderButtonFeature } from '../features/pikabu-comments-expander-button.feature';
import { PikabuCommentsHighlightFeature } from '../features/pikabu-comments-highlight.feature';
import { PikabuFixesSettingsOnPageFeature } from '../features/pikabu-fixes-settings-on-page.feature';
import { PikabuHideDonateFeature } from '../features/pikabu-hide-donate.feature';
import { PikabuHideStoryShortDesignFeature } from '../features/pikabu-hide-story-short-design.feature';
import { PikabuMobileApiAuthFeature } from '../features/pikabu-mobile-api-auth.feature';
import { PikabuStoryIgnoreButtonFeature } from '../features/pikabu-story-ignore-button.feature';
import { PikabuStoryRatingFeature } from '../features/pikabu-story-rating.feature';
import { PikabuStoryVideoDownloadFeature } from '../features/pikabu-story-video-download.feature';
import { PikabuUserIndicatorFeature } from '../features/pikabu-user-indicator.feature';
import { PikabuUserNoteTagsFeature } from '../features/pikabu-user-note-tags.feature';
import { PikabuUserNotesToolFeature } from '../features/pikabu-user-notes-tool.feature';
import { PikabuUserPopupTagsFeature } from '../features/pikabu-user-popup-tags.feature';
import { PikabuUserRatingFeature } from '../features/pikabu-user-rating.feature';
import { UsageFeature } from '../features/usage.feature';
import { FeatureModel } from '../models/feature.model';

export const PageScriptFeaturesConst: FeatureModel[] = [
    {
        enabledOn: 'showUserRegistrationDateIndicator',
        featureFactory: PikabuUserIndicatorFeature,
    },
    {
        enabledOn: 'showStoryVotes',
        featureFactory: PikabuStoryRatingFeature,
    },
    {
        enabledOn: 'showCommentVotes',
        featureFactory: PikabuCommentRatingFeature,
    },
    {
        enabledOn: 'showUserSummary',
        featureFactory: PikabuUserPopupTagsFeature,
    },
    {
        enabledOn: 'showUserRating',
        featureFactory: PikabuUserRatingFeature,
    },
    {
        enabledOn: 'showUserNoteTags',
        featureFactory: PikabuUserNoteTagsFeature,
    },
    {
        enabledOn: 'addForeverIgnore',
        featureFactory: PikabuStoryIgnoreButtonFeature,
    },
    {
        enabledOn: 'showVideoLinks',
        featureFactory: PikabuStoryVideoDownloadFeature,
    },
    {
        enabledOn: 'hideDonateButton',
        featureFactory: PikabuHideDonateFeature,
    },
    {
        enabledOn: 'hideShortStoryDesign',
        featureFactory: PikabuHideStoryShortDesignFeature,
    },
    {
        enabledOn: 'sendStatistic',
        featureFactory: UsageFeature,
    },
    {
        enabledOn: 'showCommentsExpanderButton',
        featureFactory: PikabuCommentsExpanderButtonFeature,
    },
    {
        enabledOn: 'expandCommentsAuto',
        featureFactory: PikabuCommentsExpandAutoFeature,
    },
    {
        enabledOn: 'highlightVotedComments',
        featureFactory: PikabuCommentsHighlightFeature,
    },
    {
        featureFactory: PikabuFixesSettingsOnPageFeature,
    },
    {
        enabledOn: 'booster',
        featureFactory: BoosterFeature,
    },
    {
        enabledOn: 'userNotesTool',
        featureFactory: PikabuUserNotesToolFeature,
    },
    {
        enabledOn: 'mobileApiAuth',
        featureFactory: PikabuMobileApiAuthFeature,
    },
];
