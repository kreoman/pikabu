import './pikabu-slot-mount.scss';

import { Slot, slotMount } from '@des-kit/slot';
import { ReactNode } from 'react';
import { Observable } from 'rxjs';

import { PikabuTheme } from '../../components/pikabu-theme/pikabu-theme';

export function pikabuSlotMount(
    slot: Slot,
    node: ReactNode,
): Observable<never> {
    return slotMount(slot, <PikabuTheme>{node}</PikabuTheme>, 'des--mount');
}
