const map: Record<string, any> = {
    '&quot;': '"',
};

export function pikabuStoryTitleFormat(mobileTitle: string): string {
    for (const [from, to] of Object.entries(map)) {
        mobileTitle = mobileTitle.replaceAll(from, to);
    }

    return mobileTitle;
}
