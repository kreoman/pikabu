import './pikabu-user-indicator.scss';

import classNames from 'classnames';
import { differenceInDays } from 'date-fns';
import { Container } from 'typedi';

import { usePromise } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { useStylePropsHtmlAttributes } from '@des-kit/styles';
import { Waiter } from '@des-kit/waiter';

import CrossIcon from '../../../../icons/cross.svg';
import { PikabuUserApiService } from '../../../pikabu-api/user/services/pikabu-user-api.service';

export interface PikabuUserIndicatorProps {
    readonly nickname: string;
}

export function PikabuUserIndicator(props: PikabuUserIndicatorProps) {
    const styleProps = useStylePropsHtmlAttributes();
    const pikabuUserApiService = Container.get(PikabuUserApiService);
    const [user, userLoadError] = usePromise(
        () => pikabuUserApiService.getFromPikabuByNameLongCache(props.nickname),
        null,
        [props.nickname],
    );

    if (userLoadError) {
        return <Icon src={CrossIcon} />;
    }

    if (!user) {
        return <Waiter />;
    }

    const regDays = differenceInDays(Date.now(), user.registrationDateMs);

    const colorClass = 'des--color-scheme';
    const baseClass = 'des--pikabu-user-indicator';

    const classes = classNames(baseClass, {
        [`${colorClass}_green`]: regDays >= 356,
        [`${colorClass}_green ${baseClass}_less-of-year`]:
            regDays < 365 && regDays >= 30,
        [`${colorClass}_grey ${baseClass}_less-of-month`]:
            regDays < 30 && regDays >= 7,
        [`${colorClass}_red ${baseClass}_less-of-week`]:
            regDays < 7 && regDays >= 1,
        [`${colorClass}_red ${baseClass}_less-of-day`]: regDays < 1,
    });

    return (
        <div
            className={classes}
            {...styleProps}></div>
    );
}
