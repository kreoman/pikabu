import './pikabu-comments-expand-all-button.scss';

import { useDestroy, useObservable } from '@des-kit/hooks';
import { useState } from 'react';
import { takeUntil } from 'rxjs';

import { PikabuCommentsContainerRef } from '../../../pikabu-dom/element-refs/comments-container/pikabu-comments-container.ref';

export interface PikabuCommentsExpandAllButtonProps {
    readonly commentsContainerRef: PikabuCommentsContainerRef;
}

export function PikabuCommentsExpandAllButton(
    props: PikabuCommentsExpandAllButtonProps,
) {
    const [clicked, setClicked] = useState(false);
    const [expanders] = useObservable(
        () => props.commentsContainerRef.getCommentsExpanders(),
        [],
        [props.commentsContainerRef],
    );
    const destroy$ = useDestroy();

    function onExpandAllButtonClick() {
        setClicked(true);

        props.commentsContainerRef
            .expandAllComments()
            .pipe(takeUntil(destroy$))
            .subscribe();
    }

    if (!expanders.length && !clicked) {
        return null;
    }

    return (
        <button
            className="des--pikabu-comments-expand-all-button"
            disabled={clicked}
            onClick={onExpandAllButtonClick}>
            {!clicked
                ? 'Раскрыть все комментарии'
                : 'Все ветки комментариев раскрыты'}
        </button>
    );
}
