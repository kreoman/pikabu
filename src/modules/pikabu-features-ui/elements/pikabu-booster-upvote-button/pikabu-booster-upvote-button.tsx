import './pikabu-booster-upvote-button.scss';

import classNames from 'classnames';
import { SyntheticEvent, useEffect, useState } from 'react';
import { Observable } from 'rxjs';
import { Container } from 'typedi';

import { useObservable } from '@des-kit/hooks';
import { Label } from '@des-kit/label';
import { SizeEnum, StatusEnum } from '@des-kit/styles';
import { Waiter } from '@des-kit/waiter';

import { BoosterCandidateService } from '../../../booster/services/booster-candidate.service';
import { BoosterCandidateStoredModel } from '../../../booster/services/booster-candidate-storage.service';
import { PikabuStoryRef } from '../../../pikabu-dom/element-refs/story/pikabu-story-ref';

export interface PikabuBoosterUpvoteButtonProps {
    readonly storyRef: PikabuStoryRef;
    readonly candidate$: Observable<BoosterCandidateStoredModel | null>;
}

const CLASSNAME = 'des--pikabu-booster-upvote-button';

export function PikabuBoosterUpvoteButton(
    props: PikabuBoosterUpvoteButtonProps,
) {
    const { storyRef, candidate$ } = props;
    const boosterCandidateService = Container.get(BoosterCandidateService);

    const [upVoted] = useObservable(() => storyRef.upVoted$, null, [storyRef]);
    const [downVoted] = useObservable(() => storyRef.downVoted$, null, [
        storyRef,
    ]);
    const [candidate] = useObservable(() => candidate$, null, [candidate$]);

    const [upvoteClicked, setUpvoteClicked] = useState(false);

    useEffect(() => {
        setUpvoteClicked(false);
    }, [candidate]);

    if (!candidate) {
        return;
    }

    const candidateReadyForUpvote =
        candidate.obsolete === 0 &&
        candidate.success === 0 &&
        candidate.progress === 0 &&
        candidate.voted === 0;

    const storyReadyForUpvote = upVoted === false && downVoted === false;
    const readyForUpvote = candidateReadyForUpvote && storyReadyForUpvote;

    function onBoosterVoteClick(
        event: SyntheticEvent<HTMLElement, MouseEvent>,
    ) {
        if (!candidate) {
            return;
        }

        if (candidate.error) {
            return;
        }

        preventPikabuUpvoteClick(event);

        if (candidate.progress) {
            return;
        }

        setUpvoteClicked(true);

        void boosterCandidateService.upvote(
            candidate.orderUuid,
            storyRef.storyId,
            () => storyRef.upvote(),
        );
    }

    function preventPikabuUpvoteClick(
        event: SyntheticEvent<HTMLElement, MouseEvent>,
    ) {
        event.preventDefault();
        event.stopPropagation();
    }

    const progress = upvoteClicked || !!candidate.progress;

    const className = classNames(CLASSNAME, {
        [CLASSNAME + '_progress']: progress,
        [CLASSNAME + '_success']: !!candidate.success,
        [CLASSNAME + '_error']: !!candidate.error,
        [CLASSNAME + '_voted']: !!candidate.voted,
        [CLASSNAME + '_can-vote']: readyForUpvote,
    });

    return (
        <>
            <div
                className={className}
                onClick={onBoosterVoteClick}>
                {progress && <Waiter />}
                {readyForUpvote && candidate.pointsMultiplier > 1 && (
                    <Label
                        className={CLASSNAME + '__multiplier'}
                        swapColors={true}
                        styleOverride={{
                            size: SizeEnum.XSmall,
                            status: StatusEnum.Success,
                        }}>
                        x{candidate.pointsMultiplier}
                    </Label>
                )}
            </div>
        </>
    );
}
