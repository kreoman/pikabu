import { Observable } from 'rxjs';
import { Container } from 'typedi';

import { isSet } from '@des-kit/core';
import { usePromise } from '@des-kit/hooks';
import { useObservable } from '@des-kit/hooks';

import { RatingV1 } from '../../../../common/components/rating-v1/rating-v1';
import { PikabuStoryApiError } from '../../../pikabu-api/story/errors/pikabu-story-api.error';
import { PikabuStoryApiService } from '../../../pikabu-api/story/services/pikabu-story-api.service';

export interface PikabuMobileCommentRatingProps {
    readonly ratingCount$: Observable<number | null>;
    readonly upVoted$: Observable<boolean>;
    readonly downVoted$: Observable<boolean>;
    readonly storyId: number;
    readonly storyIsAdult?: boolean;
    readonly commentId: number;
}

export function PikabuMobileCommentRating(
    props: PikabuMobileCommentRatingProps,
) {
    const storyApiService = Container.get(PikabuStoryApiService);

    const [ratingCount] = useObservable(() => props.ratingCount$, null);
    const [upVoted] = useObservable(() => props.upVoted$, false);
    const [downVoted] = useObservable(() => props.downVoted$, false);

    const [comment, commentError] = usePromise(
        () =>
            storyApiService.getStoryComment(
                props.storyId,
                props.commentId,
                props.storyIsAdult,
            ),
        null,
        [props.storyId, props.commentId],
    );

    let errorText = '';

    if (commentError) {
        if (commentError instanceof PikabuStoryApiError) {
            errorText = commentError.message;
        } else {
            errorText = 'Не удалось получить минусы через мобильное API';
        }
    }

    const loading = !isSet(comment) && !isSet(commentError);
    const pluses = comment?.comment_pluses ?? null;
    const minuses = comment?.comment_minuses ?? null;

    return (
        <RatingV1
            upVoted={upVoted}
            downVoted={downVoted}
            ratingCount={ratingCount}
            plusesCount={pluses}
            minusesCount={minuses}
            loading={loading}
            errorText={errorText}
        />
    );
}
