import { Fade } from '@des-kit/fade';
import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Label } from '@des-kit/label';
import { Row } from '@des-kit/row';
import {
    SizeContext,
    SizeEnum,
    StatusContext,
    StatusEnum,
} from '@des-kit/styles';
import { Container } from 'typedi';

import { WrapTooltip } from '../../../../common/components/wrap-tooltip/wrap-tooltip';
import WarningIcon from '../../../../icons/warning.svg';
import { BoosterControls } from '../../../booster/ui/booster-controls';
import {
    PikabuMobileApiStatusService,
    PikabuMobileApiWarningEnum,
} from '../../../pikabu-mobile-api/common/pikabu-mobile-api-status.service';
import { SettingsService } from '../../../settings/settings.service';
import { PikabuSidebarBlock } from '../../components/pikabu-sidebar-block/pikabu-sidebar-block';

export function PikabuSidebar() {
    const settingsService = Container.get(SettingsService);
    const pikabuMobileApiStatusService = Container.get(
        PikabuMobileApiStatusService,
    );

    const [settings] = useObservable(() => settingsService.settings$, null);
    const [warning] = useObservable(
        () => pikabuMobileApiStatusService.warning$,
        null,
    );

    const tooManyRequestsWarningTooltip =
        'Мобильное API Пикабу возвращает ошибку по причине слишком большого кол-ва запросов. Получение минусов временно недоступно.';

    const tooManyRequestsWarning =
        warning === PikabuMobileApiWarningEnum.TooManyRequests;

    const tooManyRequestsNode = (
        <Fade show={tooManyRequestsWarning}>
            <Row>
                <StatusContext.Provider value={StatusEnum.Warning}>
                    <WrapTooltip tooltip={tooManyRequestsWarningTooltip}>
                        <Label swapColors={true}>
                            <Icon src={WarningIcon} />
                            <span>[429] Ошибка мобильного API</span>
                        </Label>
                    </WrapTooltip>
                </StatusContext.Provider>
            </Row>
        </Fade>
    );

    return (
        <Fade show={!!settings}>
            <PikabuSidebarBlock>
                <SizeContext.Provider value={SizeEnum.Small}>
                    <BoosterControls />
                    {tooManyRequestsNode}
                </SizeContext.Provider>
            </PikabuSidebarBlock>
        </Fade>
    );
}
