import './pikabu-user-popup-content.scss';

import { Card as Card } from '@des-kit/card';
import { NavigationService } from '@des-kit/extension-bg-navigation';
import { usePromise } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Label } from '@des-kit/label';
import { Row } from '@des-kit/row';
import {
    SizeContext,
    SizeEnum,
    StatusContext,
    StatusEnum,
} from '@des-kit/styles';
import { Container } from 'typedi';

import { BlockWaiter } from '../../../../common/components/block-waiter/block-waiter';
import CircleIcon from '../../../../icons/circle.svg';
import CircleXMarkIcon from '../../../../icons/circle-xmark.svg';
import OutIcon from '../../../../icons/out.svg';
import { MonsterApiService } from '../../../monster-api/monster-api.service';

export interface PikabuUserIndicatorProps {
    readonly nickname: string;
}

export function PikabuUserPopupContent(props: PikabuUserIndicatorProps) {
    const navigationService = Container.get(NavigationService);
    const monsterApiService = Container.get(MonsterApiService);
    const [summary, summaryLoadError] = usePromise(
        () => monsterApiService.getUserSummaryOrFail(props.nickname),
        null,
        [props.nickname],
    );

    if (summaryLoadError) {
        return (
            <Card
                iconSrc={CircleXMarkIcon}
                title={'Топ тегов'}>
                <StatusContext.Provider value={StatusEnum.Error}>
                    <Label>Не удалось загрузить информацию о тегах</Label>
                </StatusContext.Provider>
            </Card>
        );
    }

    if (!summary) {
        return <BlockWaiter />;
    }

    function goToSource() {
        if (!summary) {
            return;
        }

        void navigationService.openInNewTab(
            `${summary.host}user/${summary.username}-summary`,
        );
    }

    function onPostsClick() {
        goToSource();
    }

    function onCommentsClick() {
        goToSource();
    }

    return (
        <div className="des--pikabu-user-popup-content">
            {!!summary.postsTags.length && (
                <Card
                    iconSrc={CircleIcon}
                    title={
                        <>
                            Топ тегов среди постов{' '}
                            <Icon
                                src={OutIcon}
                                onClick={onPostsClick}
                            />
                        </>
                    }>
                    <SizeContext.Provider value={SizeEnum.XSmall}>
                        <Row wrap={true}>
                            {summary.postsTags.map((tag, i) => (
                                <Label key={`post-tag-${i}`}>
                                    {tag.TagRU} ● {tag.Count}
                                </Label>
                            ))}
                        </Row>
                    </SizeContext.Provider>
                </Card>
            )}
            {!!summary.commentsTags.length && (
                <Card
                    iconSrc={CircleIcon}
                    title={
                        <>
                            Топ тегов среди комментариев{' '}
                            <Icon
                                src={OutIcon}
                                onClick={onCommentsClick}
                            />
                        </>
                    }>
                    <SizeContext.Provider value={SizeEnum.XSmall}>
                        <Row wrap={true}>
                            {summary.commentsTags.map((tag, i) => (
                                <Label key={`comment-tag-${i}`}>
                                    {tag.TagRU} ● {tag.Count}
                                </Label>
                            ))}
                        </Row>
                    </SizeContext.Provider>
                </Card>
            )}
        </div>
    );
}
