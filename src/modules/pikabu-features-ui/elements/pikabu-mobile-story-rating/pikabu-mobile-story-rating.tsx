import classNames from 'classnames';
import { useEffect } from 'react';
import { Observable } from 'rxjs';
import { Container } from 'typedi';

import { isSet } from '@des-kit/core';
import { usePromise } from '@des-kit/hooks';
import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Label } from '@des-kit/label';
import { Waiter } from '@des-kit/waiter';

import { RatingV1 } from '../../../../common/components/rating-v1/rating-v1';
import { RatingV2 } from '../../../../common/components/rating-v2/rating-v2';
import { WrapTooltip } from '../../../../common/components/wrap-tooltip/wrap-tooltip';
import LockIcon from '../../../../icons/lock.svg';
import { PikabuStoryApiError } from '../../../pikabu-api/story/errors/pikabu-story-api.error';
import { MobileStoryModel } from '../../../pikabu-api/story/models/mobile-story.model';
import { PikabuStoryApiService } from '../../../pikabu-api/story/services/pikabu-story-api.service';

export interface PikabuMobileStoryRatingProps {
    readonly className?: string;
    readonly version: 1 | 2;
    readonly ratingCount$: Observable<number | null>;
    readonly upVoted$: Observable<boolean>;
    readonly downVoted$: Observable<boolean>;
    readonly storyId: number;
    readonly storyIsAdult: boolean;
    readonly onSuccess?: (story: MobileStoryModel) => void;
    readonly onError?: () => void;
}

export function PikabuMobileStoryRating(props: PikabuMobileStoryRatingProps) {
    const className = classNames('des--pikabu-story-rating', props.className);
    const storyApiService = Container.get(PikabuStoryApiService);

    const [ratingCount] = useObservable(() => props.ratingCount$, null);
    const [upVoted] = useObservable(() => props.upVoted$, false);
    const [downVoted] = useObservable(() => props.downVoted$, false);

    const [story, storyError] = usePromise(
        () => storyApiService.getStory(props.storyId, props.storyIsAdult),
        null,
        [props.storyId],
    );

    useEffect(() => {
        if (story) {
            props.onSuccess?.(story);
        }
    }, [story]);

    useEffect(() => {
        if (storyError) {
            props.onError?.();
        }
    }, [storyError]);

    let errorText = '';

    if (storyError) {
        if (storyError instanceof PikabuStoryApiError) {
            errorText = storyError.message;
        } else {
            errorText = 'Не удалось получить минусы через мобильное API';
        }
    }

    const loading = !isSet(story) && !isSet(storyError);
    const pluses = story?.story_pluses ?? null;
    const minuses = story?.story_minuses ?? null;

    if (props.version === 1) {
        return (
            <RatingV1
                loading={loading}
                errorText={errorText}
                ratingCount={ratingCount}
                plusesCount={pluses}
                minusesCount={minuses}
                upVoted={upVoted}
                downVoted={downVoted}
            />
        );
    }

    if (loading) {
        return (
            <Label className={className}>
                <Waiter />
            </Label>
        );
    }

    if (errorText) {
        return (
            <WrapTooltip tooltip={errorText}>
                <Label className={className}>
                    <Icon src={LockIcon} />
                </Label>
            </WrapTooltip>
        );
    }

    if (!isSet(pluses)) {
        return (
            <WrapTooltip tooltip="Нет информации о рейтинге. Возможно, действует защита автора.">
                <Label className={className}>
                    <Icon src={LockIcon} />
                </Label>
            </WrapTooltip>
        );
    }

    return (
        <RatingV2
            className={className}
            plusesCount={pluses}
            minusesCount={minuses}
            upVoted={upVoted}
            downVoted={downVoted}
        />
    );
}
