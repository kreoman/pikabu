import { Button } from '@des-kit/button';
import { delay, isRecord } from '@des-kit/core';
import { oneOrNull } from '@des-kit/element-ref';
import { FileInput } from '@des-kit/file-input';
import { Icon } from '@des-kit/icon';
import { Label } from '@des-kit/label';
import { Row } from '@des-kit/row';
import { StatusEnum } from '@des-kit/styles';
import { Waiter } from '@des-kit/waiter';
import FileSaver from 'file-saver';
import { useState } from 'react';
import { firstValueFrom } from 'rxjs';
import { Container } from 'typedi';

import { PikabuFixesCard } from '../../../../common/components/pikabu-fixes-card/pikabu-fixes-card';
import SaveIcon from '../../../../icons/download.svg';
import ImportIcon from '../../../../icons/upload.svg';
import { PikabuAjaxResponseError } from '../../../pikabu-ajax-api/models/pikabu-ajax-response-error';
import { PikabuUserNoteApiService } from '../../../pikabu-api/user-note/services/pikabu-user-note-api.service';
import { PikabuCurrentUserRef } from '../../../pikabu-dom/element-refs/current-user/pikabu-current-user.ref';

interface StoredNote {
    readonly userId: number;
    readonly note: string;
}

function isValidNote(note: unknown): note is StoredNote {
    return isRecord(note) && 'userId' in note && 'note' in note;
}

export function PikabuUserNotesTool() {
    const pikabuUserNoteApiService = Container.get(PikabuUserNoteApiService);
    const [exportProgress, setExportProgress] = useState(false);
    const [importProgress, setImportProgress] = useState(false);
    const [progressText, setProgressText] = useState('');
    const [progressStatus, setProgressStatus] = useState(StatusEnum.Info);

    async function onExportClick() {
        const currentUser = await firstValueFrom(
            oneOrNull(PikabuCurrentUserRef),
        );
        const nickname = currentUser?.nickname ?? '[username]';
        setExportProgress(true);
        setProgressText('Экспорт заметок');
        setProgressStatus(StatusEnum.Info);

        try {
            const notes = await pikabuUserNoteApiService.fetchNotes(
                (currentPage, found) => {
                    setProgressText(
                        `Экспорт заметок. Страница ${currentPage} из ${found}`,
                    );
                },
            );

            const obj = {
                notes: [] as StoredNote[],
            };

            for (const [userId, note] of notes) {
                obj.notes.push({
                    userId,
                    note,
                });
            }

            const content = JSON.stringify(obj, null, 2);

            FileSaver.saveAs(new Blob([content]), `Заметки-${nickname}.json`);
            setProgressText('');
        } catch (e) {
            setProgressStatus(StatusEnum.Error);
            setProgressText('При сохранении заметок произошла ошибка');
        } finally {
            setExportProgress(false);
        }
    }

    async function onFilesSelect(files: File[]) {
        const file = files[0];

        if (!file) {
            return;
        }

        setImportProgress(true);
        setProgressText('Читаем файл');
        setProgressStatus(StatusEnum.Info);

        try {
            const text = await file.text();

            const json = JSON.parse(text);

            const notes = (json.notes as StoredNote[]) ?? [];

            const retryLimit = 10;
            let retryCount = 0;
            for (let i = 0; i < notes.length; i++) {
                const noteNum = i + 1;
                const note = notes[i];
                const progress = `${noteNum} из ${notes.length}`;

                if (!isValidNote(note)) {
                    continue;
                }

                setProgressStatus(StatusEnum.Info);
                setProgressText(`[${progress}] Сохранение`);

                try {
                    await pikabuUserNoteApiService.addNote(
                        note.userId,
                        note.note,
                    );

                    await delay(6250);

                    retryCount = 0;
                } catch (e) {
                    retryCount++;

                    if (retryCount >= retryLimit) {
                        throw e;
                    }

                    const delayMin = retryCount * 5;

                    console.log({
                        e,
                        noteNum,
                        retryCount,
                        delayMin,
                    });

                    if (e instanceof PikabuAjaxResponseError) {
                        setProgressStatus(StatusEnum.Warning);
                        setProgressText(
                            `[${progress}] ${e.message}. Повтор ${retryCount} через ${delayMin} мин`,
                        );
                    } else {
                        setProgressStatus(StatusEnum.Warning);
                        setProgressText(
                            `[${progress}] Ошибка. Повтор ${retryCount} через ${delayMin} мин`,
                        );
                    }

                    await delay(delayMin * 1000 * 60);
                    i--;
                }
            }

            document.location.reload();
        } catch (e) {
            setProgressStatus(StatusEnum.Error);
            setProgressText(
                `Превышено кол-во попыток загрузки заметок. Попробуйте позже, обновив страницу`,
            );
        } finally {
            setImportProgress(false);
        }
    }

    const inProgress = exportProgress || importProgress;

    return (
        <PikabuFixesCard title={'Экспорт и импорт всех заметок'}>
            {progressText && (
                <Row>
                    <Label styleOverride={{ status: progressStatus }}>
                        <Waiter />
                        <span>{progressText}</span>
                    </Label>
                </Row>
            )}
            <Row>
                <Button
                    disabled={inProgress}
                    onClick={onExportClick}>
                    <Icon src={SaveIcon} />
                    <span>Сохранить заметки в файл</span>
                </Button>
                <FileInput
                    accept="application/JSON"
                    onSelect={onFilesSelect}>
                    <Button disabled={inProgress}>
                        <Icon src={ImportIcon} />
                        <span>Загрузить заметки из файла</span>
                    </Button>
                </FileInput>
            </Row>
        </PikabuFixesCard>
    );
}
