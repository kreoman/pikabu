import './pikabu-user-rating.scss';

import { Container } from 'typedi';

import { usePromise } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { StatusEnum } from '@des-kit/styles';
import { TooltipWrapper } from '@des-kit/tooltip-wrapper';
import { Waiter } from '@des-kit/waiter';

import { PikabuFixesToolWrapper } from '../../../../common/components/pikabu-fixes-tool-wrapper/pikabu-fixes-tool-wrapper';
import ErrorIcon from '../../../../icons/warning.svg';
import { PikabuUserApiService } from '../../../pikabu-api/user/services/pikabu-user-api.service';

export interface PikabuUserRatingProps {
    readonly nickname: string;
}

const CLASS_NAME = 'des--pikabu-user-rating';

export function PikabuUserRating(props: PikabuUserRatingProps) {
    const pikabuUserApiService = Container.get(PikabuUserApiService);

    const [user, error] = usePromise(
        () => pikabuUserApiService.fetchMobileApiUser(props.nickname),
        null,
        [props.nickname],
    );

    if (error) {
        return (
            <TooltipWrapper
                tooltipContent={
                    'Не удалось загрузить данные пользователя через мобильное API'
                }>
                <Icon
                    src={ErrorIcon}
                    styleOverride={{ status: StatusEnum.Error }}
                />
            </TooltipWrapper>
        );
    }

    if (!user) {
        return <Waiter />;
    }

    return (
        <PikabuFixesToolWrapper className={CLASS_NAME}>
            <span className={CLASS_NAME + '__text'}>{user.rating}</span>
        </PikabuFixesToolWrapper>
    );
}
