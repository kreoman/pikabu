import { isSet } from '@des-kit/core';
import { useObservable } from '@des-kit/hooks';

import { RatingV1 } from '../../../../common/components/rating-v1/rating-v1';
import { PikabuCommentRatingRef } from '../../../pikabu-dom/element-refs/comment/comment-rating/pikabu-comment-rating.ref';
import { PikabuMobileCommentRating } from '../pikabu-mobile-comment-rating/pikabu-mobile-comment-rating';

export interface PikabuCommentRefRatingProps {
    readonly storyId: number;
    readonly storyIsAdult?: boolean;
    readonly commentId: number;
    readonly ratingRef: PikabuCommentRatingRef;
}

export function PikabuCommentRefRating(props: PikabuCommentRefRatingProps) {
    const { ratingRef, storyId, storyIsAdult, commentId } = props;

    const { initialPluses, initialMinuses } = ratingRef;

    const [ratingCount] = useObservable(() => ratingRef.ratingCount$, null);
    const [upVoted] = useObservable(() => ratingRef.upVoted$, false);
    const [downVoted] = useObservable(() => ratingRef.downVoted$, false);

    if (isSet(initialPluses) && isSet(initialMinuses)) {
        return (
            <RatingV1
                upVoted={upVoted}
                downVoted={downVoted}
                ratingCount={ratingCount}
                plusesCount={initialPluses}
                minusesCount={initialMinuses}
            />
        );
    }

    return (
        <PikabuMobileCommentRating
            ratingCount$={ratingRef.ratingCount$}
            upVoted$={ratingRef.upVoted$}
            downVoted$={ratingRef.downVoted$}
            storyId={storyId}
            storyIsAdult={storyIsAdult}
            commentId={commentId}
        />
    );
}
