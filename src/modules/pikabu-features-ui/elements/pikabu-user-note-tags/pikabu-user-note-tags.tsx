import './pikabu-user-note-tags.scss';

import { useState } from 'react';
import { Container } from 'typedi';

import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Label } from '@des-kit/label';
import { SizeContext, SizeEnum } from '@des-kit/styles';
import { Waiter } from '@des-kit/waiter';

import { WrapTooltip } from '../../../../common/components/wrap-tooltip/wrap-tooltip';
import { ExtensionNameConst } from '../../../../common/consts/extension-name.const';
import CrossIcon from '../../../../icons/cross.svg';
import TagIcon from '../../../../icons/tag.svg';
import { PikabuUserNoteTagsService } from '../../../pikabu-api/user-note/services/pikabu-user-note-tags.service';
import { PikabuUserInlineRef } from '../../../pikabu-dom/element-refs/user-inline/pikabu-user-inline.ref';

export interface PikabuUserNoteTagsProps {
    readonly userInlineRef: PikabuUserInlineRef;
}

const CLASS = 'des--pikabu-user-note-tags';

export function PikabuUserNoteTags(props: PikabuUserNoteTagsProps) {
    const { hasNote, username } = props.userInlineRef;
    const pikabuUserNoteTagsService = Container.get(PikabuUserNoteTagsService);

    const [newTag, setNewTag] = useState<string>('');
    const [editable, setEditable] = useState(false);
    const [adding, setAdding] = useState(false);
    const [removingTag, setRemovingTag] = useState('');

    const [tags, tagsError] = useObservable(
        () => pikabuUserNoteTagsService.getTags(username, hasNote),
        null,
        [hasNote, username],
    );

    async function onTagRemoveClick(tag: string) {
        setRemovingTag(tag);

        try {
            await pikabuUserNoteTagsService.removeTag(username, tag);
        } finally {
            setRemovingTag('');
        }
    }

    const tagElements = tags?.map((tag, i) => (
        <Label key={i}>
            <WrapTooltip tooltip={<span>{tag}</span>}>
                <span className={CLASS + '__text'}>{tag}</span>
            </WrapTooltip>
            {removingTag !== tag && (
                <WrapTooltip tooltip={<span>Удалить тег</span>}>
                    <Icon
                        src={CrossIcon}
                        onClick={() => onTagRemoveClick(tag)}
                    />
                </WrapTooltip>
            )}
            {removingTag === tag && (
                <WrapTooltip tooltip="Удаляем тег">
                    <Waiter />
                </WrapTooltip>
            )}
        </Label>
    ));

    function onAddTagClick() {
        setEditable(true);
    }

    const addElement = (
        <>
            <WrapTooltip tooltip={`Добавить тег (${ExtensionNameConst})`}>
                <Icon
                    styleOverride={{ size: SizeEnum.Small }}
                    src={TagIcon}
                    onClick={onAddTagClick}
                />
            </WrapTooltip>
        </>
    );

    const addingElement = (
        <>
            <WrapTooltip tooltip="Сохраняем новый тег">
                <Waiter />
            </WrapTooltip>
        </>
    );

    async function onNewTagBlur() {
        await newTagEditEnd();
    }

    async function onNewTagKeyUp(key: string) {
        if (key === 'Enter') {
            await newTagEditEnd();
        }
    }

    async function newTagEditEnd() {
        if (!editable) {
            return;
        }

        setEditable(false);

        if (!newTag.length) {
            return;
        }

        setAdding(true);

        try {
            await pikabuUserNoteTagsService.addTag(username, newTag);

            setNewTag('');
        } finally {
            setAdding(false);
        }
    }

    const newTagElement = (
        <>
            <Label>
                <input
                    placeholder="тег"
                    className={CLASS + '__input'}
                    autoFocus={true}
                    size={newTag.length || 1}
                    onBlur={onNewTagBlur}
                    onKeyUp={(e) => onNewTagKeyUp(e.key)}
                    onChange={(ch) => setNewTag(ch.target.value)}
                />
            </Label>
        </>
    );

    return (
        <SizeContext.Provider value={SizeEnum.XSmall}>
            <div className={CLASS}>
                {tagElements}
                {!editable && addElement}
                {editable && newTagElement}
                {adding && addingElement}
            </div>
        </SizeContext.Provider>
    );
}
