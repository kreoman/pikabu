import './pikabu-story-ignore-button.scss';

import { Button, ButtonAppearanceEnum } from '@des-kit/button';
import { delay } from '@des-kit/core';
import { Icon } from '@des-kit/icon';
import { Waiter } from '@des-kit/waiter';
import { useState } from 'react';
import { Container } from 'typedi';

import { WrapTooltip } from '../../../../common/components/wrap-tooltip/wrap-tooltip';
import BanIcon from '../../../../icons/ban.svg';
import { PikabuAjaxResponseError } from '../../../pikabu-ajax-api/models/pikabu-ajax-response-error';
import { PikabuIgnoreListApiService } from '../../../pikabu-api/ignore/services/pikabu-ignore-list-api.service';

export interface PikabuStoryIgnoreButtonProps {
    readonly authorId: number;
}

export function PikabuStoryIgnoreButton(props: PikabuStoryIgnoreButtonProps) {
    const ignoreListApiService = Container.get(PikabuIgnoreListApiService);
    const [progress, setProgress] = useState(false);
    const [result, setResult] = useState<boolean | null>(null);
    const [hint, setHint] = useState('Добавить автора в игнор-лист');

    async function onIgnoreClick() {
        setProgress(true);
        setHint('Вносим автора поста в игнор-лист');

        try {
            await delay(2000);
            await ignoreListApiService.addForever(props.authorId);

            setResult(true);
            setHint('Автор поста внесен в игнор-лист');
        } catch (e) {
            setResult(false);

            if (e instanceof PikabuAjaxResponseError) {
                setHint(e.message);
            } else {
                console.log(e);
                setHint('Произошла ошибка');
            }
        } finally {
            setProgress(false);
        }
    }

    return (
        <WrapTooltip tooltip={hint}>
            <Button
                appearance={ButtonAppearanceEnum.Transparent}
                disabled={progress || result === true}
                onClick={onIgnoreClick}>
                {progress ? <Waiter /> : <Icon src={BanIcon} />}
            </Button>
        </WrapTooltip>
    );
}
