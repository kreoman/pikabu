import { isSet } from '@des-kit/core';
import { useObservable } from '@des-kit/hooks';

import { RatingV1 } from '../../../../common/components/rating-v1/rating-v1';
import { RatingV2 } from '../../../../common/components/rating-v2/rating-v2';
import { PikabuStoryRef } from '../../../pikabu-dom/element-refs/story/pikabu-story-ref';
import { PikabuMobileStoryRating } from '../pikabu-mobile-story-rating/pikabu-mobile-story-rating';

export interface PikabuStoryRefRatingProps {
    readonly version: 1 | 2;
    readonly storyRef: PikabuStoryRef;
}

export function PikabuStoryRefRating(props: PikabuStoryRefRatingProps) {
    const { storyRef } = props;

    const { initialPluses, initialMinuses } = storyRef;

    const [ratingCount] = useObservable(() => storyRef.ratingCount$, null);
    const [upVoted] = useObservable(() => storyRef.upVoted$, false);
    const [downVoted] = useObservable(() => storyRef.downVoted$, false);

    if (isSet(initialPluses) && isSet(initialMinuses)) {
        switch (props.version) {
            case 1:
                return (
                    <RatingV1
                        ratingCount={ratingCount}
                        plusesCount={initialPluses}
                        minusesCount={initialMinuses}
                        upVoted={upVoted}
                        downVoted={downVoted}
                    />
                );
            case 2:
                return (
                    <RatingV2
                        plusesCount={initialPluses}
                        minusesCount={initialMinuses}
                        upVoted={upVoted}
                        downVoted={downVoted}
                    />
                );
        }
    }

    return (
        <PikabuMobileStoryRating
            version={props.version}
            ratingCount$={storyRef.ratingCount$}
            upVoted$={storyRef.upVoted$}
            downVoted$={storyRef.downVoted$}
            storyId={storyRef.storyId}
            storyIsAdult={storyRef.isAdult}
        />
    );
}
