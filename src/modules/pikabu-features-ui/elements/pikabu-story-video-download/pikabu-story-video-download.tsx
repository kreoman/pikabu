import './pikabu-story-video-download.scss';

import FileSaver from 'file-saver';
import { useState } from 'react';
import { debounceTime, firstValueFrom } from 'rxjs';
import { Container } from 'typedi';

import { delay } from '@des-kit/core';
import { many } from '@des-kit/element-ref';
import { usePromise } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Label } from '@des-kit/label';
import { Row } from '@des-kit/row';
import { SizeContext, SizeEnum, StatusEnum } from '@des-kit/styles';

import ExportIcon from '../../../../icons/export.svg';
import LinkIcon from '../../../../icons/link.svg';
import { PikabuStoryApiService } from '../../../pikabu-api/story/services/pikabu-story-api.service';
import { PikabuStoryRef } from '../../../pikabu-dom/element-refs/story/pikabu-story-ref';

export interface PikabuStoryVideoDownloadProps {
    readonly storyId: number;
    readonly storyIsAdult: boolean;
    readonly webmUrl: string;
    readonly av1Url: string | null;
}

export function PikabuStoryVideoDownload(props: PikabuStoryVideoDownloadProps) {
    const storyApiService = Container.get(PikabuStoryApiService);

    const [copiedFormat, setCopiedFormat] = useState('');

    const [video, error] = usePromise(
        () =>
            storyApiService.getStoryVideoByWebm(
                props.storyId,
                props.webmUrl,
                props.storyIsAdult,
            ),
        null,
        [props.storyId],
    );

    const links: Record<string, string> = {
        webm: props.webmUrl,
    };

    if (props.av1Url) {
        links['av1'] = props.av1Url;
    }

    if (video?.mp4) {
        links['mp4'] = video?.mp4.url;
    }

    function clearText(text: string): string {
        const reg = /[^A-z0-9А-яё\(\) \.\-]/giu;

        return text.replace(reg, '');
    }

    async function getCurrentStoryName() {
        const stories$ = many(PikabuStoryRef);
        const stories = await firstValueFrom(stories$.pipe(debounceTime(10)));
        const currentStory = stories.find(
            (story) => story.storyId === props.storyId,
        );

        if (!currentStory) {
            return null;
        }

        return clearText(currentStory.title);
    }

    async function onDownloadClick(url: string, format: string) {
        const storyTitle = await getCurrentStoryName();
        const name = storyTitle ?? url;

        FileSaver.saveAs(url, `${name}.${format}`);
    }

    async function onCopyClick(url: string, format: string) {
        await navigator.clipboard.writeText(url);

        setCopiedFormat(format);

        await delay(2000);

        setCopiedFormat('');
    }

    return (
        <Row reverse={true}>
            <SizeContext.Provider value={SizeEnum.Small}>
                {Object.entries(links).map(([format, url]) => (
                    <Label
                        key={format}
                        styleOverride={{ status: StatusEnum.Neutral }}>
                        <Icon
                            title={'Скопировать ссылку'}
                            src={LinkIcon}
                            onClick={() => onCopyClick(url, format)}
                        />
                        <a
                            href={url}
                            target="_blank">
                            {copiedFormat === format
                                ? 'Ссылка скопирована'
                                : format}
                        </a>
                        <Icon
                            title={'Скачать файл'}
                            src={ExportIcon}
                            onClick={() => onDownloadClick(url, format)}
                        />
                    </Label>
                ))}
            </SizeContext.Provider>
        </Row>
    );
}
