import { merge, Observable, skipWhile, switchMap } from 'rxjs';
import { tap } from 'rxjs/operators';

import { many } from '@des-kit/element-ref';
import { switchEvery } from '@des-kit/rxjs';

import { PikabuCommentsContainerRef } from '../../pikabu-dom/element-refs/comments-container/pikabu-comments-container.ref';
import { PikabuVoteEnum } from '../../pikabu-dom/enums/pikabu-vote.enum';

export function PikabuCommentsHighlightFeature(): Observable<any> {
    return merge(enableSetVote(), enableHighlight());
}

function enableSetVote(): Observable<any> {
    return many(PikabuCommentsContainerRef).pipe(
        switchEvery((container) =>
            container.comments$.pipe(
                switchEvery((comment) =>
                    comment.ratingOther$.pipe(
                        switchMap((header) => header.vote$),
                        skipWhile((vote) => vote === PikabuVoteEnum.None),
                        tap((vote) =>
                            container.votes.setVote(
                                comment.authorId,
                                comment.commentId,
                                vote,
                            ),
                        ),
                    ),
                ),
            ),
        ),
    );
}

function enableHighlight(): Observable<any> {
    return many(PikabuCommentsContainerRef).pipe(
        switchEvery((container) =>
            container.votes.signedAuthorIds$.pipe(
                switchEvery((signedAuthorId) =>
                    container
                        .getCommentsByAuthorId(Math.abs(signedAuthorId))
                        .pipe(
                            switchEvery((comment) =>
                                comment.highlightWhileSubscribed(
                                    signedAuthorId > 0,
                                ),
                            ),
                        ),
                ),
            ),
        ),
    );
}
