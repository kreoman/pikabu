import React from 'react';
import { merge, Observable, switchMap } from 'rxjs';
import { Container } from 'typedi';

import { many } from '@des-kit/element-ref';
import { never, switchEvery } from '@des-kit/rxjs';

import { RatingV1 } from '../../../common/components/rating-v1/rating-v1';
import { PikabuCommentRatingRef } from '../../pikabu-dom/element-refs/comment/comment-rating/pikabu-comment-rating.ref';
import { PikabuOwnerCommentRatingRef } from '../../pikabu-dom/element-refs/comment/owner-comment-rating/pikabu-owner-comment-rating.ref';
import { PikabuCommentRef } from '../../pikabu-dom/element-refs/comment/pikabu-comment.ref';
import { PikabuCommentsContainerRef } from '../../pikabu-dom/element-refs/comments-container/pikabu-comments-container.ref';
import { PikabuCommentsPartRef } from '../../pikabu-dom/element-refs/comments-part/pikabu-comments-part.ref';
import { PikabuStoryRef } from '../../pikabu-dom/element-refs/story/pikabu-story-ref';
import { UsageErrorService } from '../../statistic/usage-error.service';
import { PikabuCommentRefRating } from '../elements/pikabu-comment-ref-rating/pikabu-comment-ref-rating';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function PikabuCommentRatingFeature(): Observable<any> {
    return merge(handleRatingInStory(), handleRatingInCommentsPart());
}

function handleRatingInStory(): Observable<never> {
    return many(PikabuStoryRef).pipe(
        switchEvery((story) =>
            story.commentsContainer$.pipe(
                switchMap((commentsContainer) =>
                    handleRatingContainer(commentsContainer, story),
                ),
            ),
        ),
        never(),
    );
}

function handleRatingInCommentsPart(): Observable<never> {
    return many(PikabuCommentsPartRef).pipe(
        switchEvery((story) =>
            story.commentContainers$.pipe(
                switchEvery((commentsContainer) =>
                    handleRatingContainer(commentsContainer),
                ),
            ),
        ),
        never(),
    );
}

function handleRatingContainer(
    container: PikabuCommentsContainerRef,
    story?: PikabuStoryRef,
): Observable<never> {
    const usageErrorService = Container.get(UsageErrorService);

    return container.comments$.pipe(
        switchEvery((comment) =>
            handleCommentRating(comment, story).pipe(
                usageErrorService.catchError(),
            ),
        ),
        never(),
    );
}

function handleCommentRating(
    comment: PikabuCommentRef,
    story?: PikabuStoryRef,
): Observable<never> {
    return merge(
        comment.ratingOther$.pipe(
            switchMap((rating) => handleRatingOther(rating, comment, story)),
        ),
        comment.ratingOwner$.pipe(
            switchMap((rating) => handleRatingOwner(rating)),
        ),
    );
}

function handleRatingOwner(
    header: PikabuOwnerCommentRatingRef,
): Observable<never> {
    return header.slotRating$.pipe(
        switchMap((slot) =>
            pikabuSlotMount(
                slot,
                <RatingV1
                    ratingCount={header.ratingCount}
                    plusesCount={header.initialPluses}
                    minusesCount={header.initialMinuses}
                    title={header.hintText}
                />,
            ),
        ),
    );
}

function handleRatingOther(
    pikabuCommentRatingRef: PikabuCommentRatingRef,
    comment: PikabuCommentRef,
    story?: PikabuStoryRef,
): Observable<never> {
    return pikabuCommentRatingRef.slotRating$.pipe(
        switchMap((slot) =>
            pikabuSlotMount(
                slot,
                <PikabuCommentRefRating
                    storyId={comment.storyId}
                    storyIsAdult={story?.isAdult}
                    commentId={comment.commentId}
                    ratingRef={pikabuCommentRatingRef}
                />,
            ),
        ),
    );
}
