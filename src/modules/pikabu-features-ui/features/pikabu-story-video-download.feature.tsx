import { switchMap } from 'rxjs';
import { Container } from 'typedi';

import { many } from '@des-kit/element-ref';
import { switchEvery } from '@des-kit/rxjs';

import { PikabuStoryRef } from '../../pikabu-dom/element-refs/story/pikabu-story-ref';
import { UsageErrorService } from '../../statistic/usage-error.service';
import { PikabuStoryVideoDownload } from '../elements/pikabu-story-video-download/pikabu-story-video-download';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function PikabuStoryVideoDownloadFeature() {
    const usageErrorService = Container.get(UsageErrorService);

    return many(PikabuStoryRef).pipe(
        switchEvery((story) =>
            story.videos$.pipe(
                switchEvery((video) =>
                    video.slotAfter$.pipe(
                        switchMap((slot) =>
                            pikabuSlotMount(
                                slot,
                                <PikabuStoryVideoDownload
                                    storyId={video.storyId}
                                    storyIsAdult={story.isAdult}
                                    webmUrl={video.webmUrl}
                                    av1Url={video.av1Url}
                                />,
                            ),
                        ),
                        usageErrorService.catchError(),
                    ),
                ),
            ),
        ),
    );
}
