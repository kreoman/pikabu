import { switchMap } from 'rxjs';

import { oneOrNever } from '@des-kit/element-ref';

import { PikabuPageRef } from '../../pikabu-dom/element-refs/page/pikabu-page.ref';

export function PikabuHideStoryShortDesignFeature() {
    return oneOrNever(PikabuPageRef).pipe(
        switchMap((page) => page.hideStoryShortDesign()),
    );
}
