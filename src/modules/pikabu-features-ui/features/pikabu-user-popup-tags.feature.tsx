import { EMPTY, merge, Observable, switchMap } from 'rxjs';

import { oneOrNull } from '@des-kit/element-ref';

import { PikabuUserPopupRef } from '../../pikabu-dom/element-refs/user-popup/pikabu-user-popup.ref';
import { PikabuUserPopupV2Ref } from '../../pikabu-dom/element-refs/user-popup-v2/pikabu-user-popup-v2.ref';
import { PikabuUserPopupContent } from '../elements/pikabu-user-popup-content/pikabu-user-popup-content';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function PikabuUserPopupTagsFeature(): Observable<any> {
    return merge(handlePopupV1(), handlePopupV2());
}

function handlePopupV1(): Observable<any> {
    return oneOrNull(PikabuUserPopupRef).pipe(
        switchMap((userPopup) =>
            userPopup
                ? userPopup.slot$.pipe(
                      switchMap((slot) =>
                          pikabuSlotMount(
                              slot,
                              <PikabuUserPopupContent
                                  nickname={userPopup.username}
                              />,
                          ),
                      ),
                  )
                : EMPTY,
        ),
    );
}

function handlePopupV2(): Observable<any> {
    return oneOrNull(PikabuUserPopupV2Ref).pipe(
        switchMap((userPopup) =>
            userPopup
                ? userPopup.slot$.pipe(
                      switchMap((slot) =>
                          pikabuSlotMount(
                              slot,
                              <PikabuUserPopupContent
                                  nickname={userPopup.username}
                              />,
                          ),
                      ),
                  )
                : EMPTY,
        ),
    );
}
