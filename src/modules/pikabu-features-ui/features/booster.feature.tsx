import {
    distinctUntilChanged,
    EMPTY,
    map,
    merge,
    Observable,
    shareReplay,
    switchMap,
} from 'rxjs';
import { Container } from 'typedi';

import { many } from '@des-kit/element-ref';
import { switchEvery } from '@des-kit/rxjs';

import { BoosterCandidateService } from '../../booster/services/booster-candidate.service';
import { BoosterCandidateStoredModel } from '../../booster/services/booster-candidate-storage.service';
import { BoosterStoryToolbar } from '../../booster/ui/booster-story-toolbar/booster-story-toolbar';
import { PikabuStoryRef } from '../../pikabu-dom/element-refs/story/pikabu-story-ref';
import { PikabuBoosterUpvoteButton } from '../elements/pikabu-booster-upvote-button/pikabu-booster-upvote-button';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function BoosterFeature() {
    return merge(handleBoosterStoryToolbar(), handleBoosterStoryUpvote());
}

function handleBoosterStoryUpvote() {
    const boosterCandidateService = Container.get(BoosterCandidateService);

    return many(PikabuStoryRef).pipe(
        switchEvery((story) =>
            handleStoryUpvoteButtonWithCandidate(
                story,
                boosterCandidateService.listenActualCandidate(story.storyId),
            ),
        ),
    );
}

function handleStoryUpvoteButtonWithCandidate(
    story: PikabuStoryRef,
    candidate$: Observable<BoosterCandidateStoredModel | null>,
) {
    const candidateShare$ = candidate$.pipe(
        shareReplay({
            bufferSize: 1,
            refCount: true,
        }),
    );

    return candidateShare$.pipe(
        map((candidate) => !!candidate),
        distinctUntilChanged(),
        switchMap((hasCandidate) =>
            hasCandidate
                ? story.slotUpvote$.pipe(
                      switchMap((slot) =>
                          pikabuSlotMount(
                              slot,
                              <PikabuBoosterUpvoteButton
                                  storyRef={story}
                                  candidate$={candidateShare$}
                              />,
                          ),
                      ),
                  )
                : EMPTY,
        ),
    );
}

function handleBoosterStoryToolbar() {
    return many(PikabuStoryRef).pipe(
        switchEvery((story) =>
            story.slotToolbarRight$.pipe(
                switchMap((slot) =>
                    pikabuSlotMount(
                        slot,
                        <BoosterStoryToolbar storyRef={story} />,
                    ),
                ),
            ),
        ),
    );
}
