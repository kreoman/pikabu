import { merge, Observable, switchMap, take } from 'rxjs';
import { Container } from 'typedi';

import { oneOrNever } from '@des-kit/element-ref';
import { never } from '@des-kit/rxjs';

import { PikabuCurrentUserRef } from '../../pikabu-dom/element-refs/current-user/pikabu-current-user.ref';
import { CurrentUserLogoutMenuItemRef } from '../../pikabu-dom/element-refs/current-user-logout-menu-item/current-user-logout-menu-item.ref';
import { PikabuLoginFormRef } from '../../pikabu-dom/element-refs/login-form/pikabu-login-form.ref';
import { PikabuPopupWarningRef } from '../../pikabu-dom/element-refs/popup-warning/pikabu-popup-warning.ref';
import { PikabuMobileAuthApiService } from '../../pikabu-mobile-api/auth/services/pikabu-mobile-auth-api.service';

export function PikabuMobileApiAuthFeature(): Observable<any> {
    return merge(handleSidebarLogout(), handleMenuLogout(), handleAuth());
}

function handleSidebarLogout(): Observable<never> {
    return oneOrNever(PikabuCurrentUserRef).pipe(
        switchMap((currentUserRef) => currentUserRef.listenLogoutClick$),
        switchMap(() => oneOrNever(PikabuPopupWarningRef).pipe(take(1))),
        switchMap((popup) => popup.listenConfirmClick$),
        switchMap(() => logout()),
        never(),
    );
}

function handleMenuLogout(): Observable<never> {
    return oneOrNever(CurrentUserLogoutMenuItemRef).pipe(
        switchMap(
            (currentUserMenuRef) => currentUserMenuRef.logoutElementClick$,
        ),
        switchMap(() => logout()),
        never(),
    );
}

function handleAuth(): Observable<never> {
    return oneOrNever(PikabuLoginFormRef).pipe(
        switchMap((loginFormRef) =>
            loginFormRef.listenSubmit$.pipe(
                switchMap(() =>
                    login(loginFormRef.username, loginFormRef.password),
                ),
            ),
        ),
        never(),
    );
}

async function logout(): Promise<void> {
    const pikabuMobileAuthApiService = Container.get(
        PikabuMobileAuthApiService,
    );

    return pikabuMobileAuthApiService.logout();
}

async function login(username: string, password: string): Promise<void> {
    const pikabuMobileAuthApiService = Container.get(
        PikabuMobileAuthApiService,
    );

    await pikabuMobileAuthApiService.auth(username, password);
}
