import { merge, Observable, skip, switchMap } from 'rxjs';
import { combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Container } from 'typedi';

import { many, oneOrNever, oneOrNull } from '@des-kit/element-ref';
import { never } from '@des-kit/rxjs';
import { switchEvery } from '@des-kit/rxjs';

import { PikabuCommentRef } from '../../pikabu-dom/element-refs/comment/pikabu-comment.ref';
import { PikabuCurrentUserRef } from '../../pikabu-dom/element-refs/current-user/pikabu-current-user.ref';
import { PikabuExternalLinkRef } from '../../pikabu-dom/element-refs/external-link/pikabu-external-link.ref';
import { PikabuStoryRef } from '../../pikabu-dom/element-refs/story/pikabu-story-ref';
import { PikabuUserPopupRef } from '../../pikabu-dom/element-refs/user-popup/pikabu-user-popup.ref';
import { PikabuUserPopupV2Ref } from '../../pikabu-dom/element-refs/user-popup-v2/pikabu-user-popup-v2.ref';
import { UsageEventService } from '../../statistic/usage-event.service';
import { UsageInfoService } from '../../statistic/usage-info.service';

export function UsageFeature(): Observable<any> {
    return merge(
        handleNickname(),
        handleStoryVote(),
        handleCommentVote(),
        handleUserPopupShow(),
        handleExternalLinkClick(),
    );
}

function handleNickname(): Observable<any> {
    const usageInfoService = Container.get(UsageInfoService);

    return oneOrNull(PikabuCurrentUserRef).pipe(
        switchMap((user) =>
            usageInfoService.setPikabuUserNickname(user?.nickname ?? null),
        ),
        never(),
    );
}

function handleStoryVote(): Observable<any> {
    const usageEventService = Container.get(UsageEventService);

    return many(PikabuStoryRef).pipe(
        switchEvery((story) =>
            combineLatest([story.upVoted$, story.downVoted$]).pipe(
                skip(1),
                tap(([up, down]) =>
                    usageEventService.register('storyVote', {
                        storyId: story.storyId,
                        vote: {
                            up,
                            down,
                        },
                    }),
                ),
                never(),
            ),
        ),
    );
}

function handleCommentVote(): Observable<any> {
    const usageEventService = Container.get(UsageEventService);

    return many(PikabuCommentRef).pipe(
        switchEvery((comment) =>
            comment.ratingOther$.pipe(
                switchMap((commentHeader) =>
                    combineLatest([
                        commentHeader.upVoted$,
                        commentHeader.downVoted$,
                    ]).pipe(
                        skip(1),
                        tap(([up, down]) =>
                            usageEventService.register('commentVote', {
                                storyId: comment.storyId,
                                commentId: comment.commentId,
                                vote: {
                                    up,
                                    down,
                                },
                            }),
                        ),
                        never(),
                    ),
                ),
            ),
        ),
    );
}

function handleUserPopupShow(): Observable<any> {
    const usageEventService = Container.get(UsageEventService);

    return merge(
        oneOrNever(PikabuUserPopupRef),
        oneOrNever(PikabuUserPopupV2Ref),
    ).pipe(
        tap((userPopup) =>
            usageEventService.register('userPopupShow', {
                nickname: userPopup.username,
            }),
        ),
        never(),
    );
}

function handleExternalLinkClick(): Observable<any> {
    const usageEventService = Container.get(UsageEventService);

    return many(PikabuExternalLinkRef).pipe(
        switchEvery((externalLink) =>
            externalLink.click$.pipe(
                tap(() =>
                    usageEventService.register('externalLinkClick', {
                        href: externalLink.href,
                    }),
                ),
            ),
        ),
        never(),
    );
}
