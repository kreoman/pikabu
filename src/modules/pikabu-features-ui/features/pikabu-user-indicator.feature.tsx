import { switchMap } from 'rxjs';
import { map } from 'rxjs/operators';
import { Container } from 'typedi';

import { many } from '@des-kit/element-ref';
import { switchEvery } from '@des-kit/rxjs';
import { SizeContext, SizeEnum } from '@des-kit/styles';

import { PikabuUserInlineRef } from '../../pikabu-dom/element-refs/user-inline/pikabu-user-inline.ref';
import { UsageErrorService } from '../../statistic/usage-error.service';
import { PikabuUserIndicator } from '../elements/pikabu-user-indicator/pikabu-user-indicator';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function PikabuUserIndicatorFeature() {
    const usageErrorService = Container.get(UsageErrorService);

    return many(PikabuUserInlineRef).pipe(
        map((users) => users.filter((user) => user.username)),
        switchEvery((userInline) =>
            userInline.slotLeft$.pipe(
                switchMap((slot) =>
                    pikabuSlotMount(
                        slot,
                        <SizeContext.Provider value={SizeEnum.XSmall}>
                            <PikabuUserIndicator
                                nickname={userInline.username}
                            />
                        </SizeContext.Provider>,
                    ),
                ),
                usageErrorService.catchError(),
            ),
        ),
    );
}
