import { EMPTY, switchMap } from 'rxjs';

import { oneOrNull } from '@des-kit/element-ref';

import { PikabuSettingsNotesRef } from '../../pikabu-dom/element-refs/settings-notes/pikabu-settings-notes.ref';
import { PikabuUserNotesTool } from '../elements/pikabu-user-notes-tool/pikabu-user-notes-tool';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function PikabuUserNotesToolFeature() {
    return oneOrNull(PikabuSettingsNotesRef).pipe(
        switchMap((sn) =>
            sn
                ? sn.topSlot$.pipe(
                      switchMap((slot) =>
                          pikabuSlotMount(slot, <PikabuUserNotesTool />),
                      ),
                  )
                : EMPTY,
        ),
    );
}
