import { EMPTY, of, switchMap } from 'rxjs';

import { oneOrNull } from '@des-kit/element-ref';

import { PikabuSidebarRef } from '../../pikabu-dom/element-refs/sidebar/pikabu-sidebar.ref';
import { PikabuSidebar } from '../elements/pikabu-sidebar/pikabu-sidebar';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function PikabuFixesSettingsOnPageFeature() {
    return oneOrNull(PikabuSidebarRef).pipe(
        switchMap((sidebar) => sidebar?.beforeUserBlockSlot$ ?? of(null)),
        switchMap((blockSlot) =>
            blockSlot ? pikabuSlotMount(blockSlot, <PikabuSidebar />) : EMPTY,
        ),
    );
}
