import { EMPTY, filter, switchMap } from 'rxjs';

import { oneOrNull } from '@des-kit/element-ref';

import { PikabuUserRatingRef } from '../../pikabu-dom/element-refs/user-rating/pikabu-user-rating.ref';
import { PikabuUserRating } from '../elements/pikabu-user-rating/pikabu-user-rating';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function PikabuUserRatingFeature() {
    return oneOrNull(PikabuUserRatingRef).pipe(
        switchMap(
            (ratingRef) =>
                ratingRef?.slot$.pipe(
                    filter(() => !!ratingRef?.nickname),
                    switchMap((slot) =>
                        pikabuSlotMount(
                            slot,
                            <PikabuUserRating nickname={ratingRef.nickname!} />,
                        ),
                    ),
                ) ?? EMPTY,
        ),
    );
}
