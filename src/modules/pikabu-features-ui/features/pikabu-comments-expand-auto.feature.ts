import { many } from '@des-kit/element-ref';
import { never } from '@des-kit/rxjs';
import { switchEvery } from '@des-kit/rxjs';

import { PikabuCommentsContainerRef } from '../../pikabu-dom/element-refs/comments-container/pikabu-comments-container.ref';

export function PikabuCommentsExpandAutoFeature() {
    return many(PikabuCommentsContainerRef).pipe(
        switchEvery((commentContainer) => commentContainer.expandAllComments()),
        never(),
    );
}
