import { switchMap } from 'rxjs';
import { Container } from 'typedi';

import { many } from '@des-kit/element-ref';
import { switchEvery } from '@des-kit/rxjs';
import { SizeContext, SizeEnum } from '@des-kit/styles';

import { PikabuStoryRef } from '../../pikabu-dom/element-refs/story/pikabu-story-ref';
import { UsageErrorService } from '../../statistic/usage-error.service';
import { PikabuStoryRefRating } from '../elements/pikabu-story-ref-rating/pikabu-story-ref-rating';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function PikabuStoryRatingFeature() {
    const usageErrorService = Container.get(UsageErrorService);

    return many(PikabuStoryRef).pipe(
        switchEvery((storyRef) =>
            storyRef.slotRating$.pipe(
                switchMap((slot) =>
                    pikabuSlotMount(
                        slot,
                        <SizeContext.Provider value={SizeEnum.Small}>
                            <PikabuStoryRefRating
                                version={1}
                                storyRef={storyRef}
                            />
                        </SizeContext.Provider>,
                    ),
                ),
                usageErrorService.catchError(),
            ),
        ),
    );
}
