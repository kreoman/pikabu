import { EMPTY, switchMap } from 'rxjs';

import { many } from '@des-kit/element-ref';
import { never, switchEvery } from '@des-kit/rxjs';

import { PikabuUserInlineRef } from '../../pikabu-dom/element-refs/user-inline/pikabu-user-inline.ref';
import { PikabuUserNoteTags } from '../elements/pikabu-user-note-tags/pikabu-user-note-tags';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function PikabuUserNoteTagsFeature() {
    return many(PikabuUserInlineRef).pipe(
        switchEvery((userInline) =>
            userInline.isOwn || !userInline.username
                ? EMPTY
                : userInline.slotRight$.pipe(
                      switchMap((slot) =>
                          pikabuSlotMount(
                              slot,
                              <PikabuUserNoteTags userInlineRef={userInline} />,
                          ),
                      ),
                  ),
        ),
        never(),
    );
}
