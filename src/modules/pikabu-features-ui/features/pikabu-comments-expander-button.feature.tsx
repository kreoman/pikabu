import { EMPTY, Observable, switchMap } from 'rxjs';
import { Container } from 'typedi';

import { many } from '@des-kit/element-ref';
import { switchEvery } from '@des-kit/rxjs';

import { PikabuCommentsContainerRef } from '../../pikabu-dom/element-refs/comments-container/pikabu-comments-container.ref';
import { SettingsService } from '../../settings/settings.service';
import { PikabuCommentsExpandAllButton } from '../elements/pikabu-comments-expand-all-button/pikabu-comments-expand-all-button';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function PikabuCommentsExpanderButtonFeature(): Observable<any> {
    return showButtonIfAutoDisabled();
}

function showButtonIfAutoDisabled() {
    return Container.get(SettingsService)
        .getKey$('expandCommentsAuto')
        .pipe(
            switchMap((expandCommentsAuto) =>
                expandCommentsAuto ? EMPTY : showButton(),
            ),
        );
}

function showButton() {
    return many(PikabuCommentsContainerRef).pipe(
        switchEvery((commentsContainer) =>
            commentsContainer.expanderSlot$.pipe(
                switchMap((slot) =>
                    pikabuSlotMount(
                        slot,
                        <PikabuCommentsExpandAllButton
                            commentsContainerRef={commentsContainer}
                        />,
                    ),
                ),
            ),
        ),
    );
}
