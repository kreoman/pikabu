import { EMPTY, switchMap } from 'rxjs';
import { Container } from 'typedi';

import { many, oneOrNull } from '@des-kit/element-ref';
import { switchEvery } from '@des-kit/rxjs';

import { PikabuCurrentUserRef } from '../../pikabu-dom/element-refs/current-user/pikabu-current-user.ref';
import { PikabuStoryRef } from '../../pikabu-dom/element-refs/story/pikabu-story-ref';
import { UsageErrorService } from '../../statistic/usage-error.service';
import { PikabuStoryIgnoreButton } from '../elements/pikabu-story-ignore-button/pikabu-story-ignore-button';
import { pikabuSlotMount } from '../functions/pikabu-slot-mount/pikabu-slot-mount';

export function PikabuStoryIgnoreButtonFeature() {
    const usageErrorService = Container.get(UsageErrorService);

    return oneOrNull(PikabuCurrentUserRef).pipe(
        switchMap((user) =>
            user
                ? many(PikabuStoryRef).pipe(
                      switchEvery((story) =>
                          story.slotToolbarLeft$.pipe(
                              switchMap((slot) =>
                                  pikabuSlotMount(
                                      slot,
                                      <PikabuStoryIgnoreButton
                                          authorId={story.authorId}
                                      />,
                                  ),
                              ),
                              usageErrorService.catchError(),
                          ),
                      ),
                  )
                : EMPTY,
        ),
    );
}
