import { Container } from 'typedi';

import { BoosterCandidateService } from '../services/booster-candidate.service';
import { BoosterClientService } from '../services/booster-client.service';
import { BoosterOrderService } from '../services/booster-order.service';
import { BoosterOrderConditionsService } from '../services/booster-order-conditions.service';
import { BoosterPointsTransactionService } from '../services/booster-points-transaction.service';
import { BoosterTransactionService } from '../services/booster-transaction.service';

Container.get(BoosterClientService);
Container.get(BoosterCandidateService);
Container.get(BoosterOrderService);
Container.get(BoosterTransactionService);
Container.get(BoosterOrderConditionsService);
Container.get(BoosterPointsTransactionService);
