import { Container } from 'typedi';

import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Label } from '@des-kit/label';
import { RoundedButton } from '@des-kit/rounded-button';
import { StatusEnum } from '@des-kit/styles';
import { Waiter } from '@des-kit/waiter';

import { WrapTooltip } from '../../../../../../common/components/wrap-tooltip/wrap-tooltip';
import UpIcon from '../../../../../../icons/arrow-up.svg';
import ErrorIcon from '../../../../../../icons/circle-xmark.svg';
import { PikabuStoryRef } from '../../../../../pikabu-dom/element-refs/story/pikabu-story-ref';
import { BoosterClientStatusDto } from '../../../../../pikabu-fixes-api/gen';
import { PointsPluralize } from '../../../../pluralize/points.pluralize';
import { BoosterCandidateService } from '../../../../services/booster-candidate.service';
import { BoosterCandidateStoredModel } from '../../../../services/booster-candidate-storage.service';

export interface PikabuStoryToolsBoosterHasCandidateProps {
    readonly storyRef: PikabuStoryRef;
    readonly candidate: BoosterCandidateStoredModel;
    readonly clientStatus: BoosterClientStatusDto;
}

export function PikabuStoryToolsBoosterHasCandidate({
    candidate: {
        error,
        errorReason,
        obsolete,
        orderUuid,
        pointsMultiplier,
        progress,
        success,
        voted,
    },
    clientStatus: { points },
    storyRef,
}: PikabuStoryToolsBoosterHasCandidateProps) {
    const boosterCandidateService = Container.get(BoosterCandidateService);

    const [upVoted] = useObservable(() => storyRef.upVoted$, null);
    const [downVoted] = useObservable(() => storyRef.downVoted$, null);

    const candidateReadyForUpvote =
        obsolete === 0 && success === 0 && progress === 0 && voted === 0;

    const storyVoted = upVoted === true || downVoted === true;
    const storyReadyForUpvote = upVoted === false && downVoted === false;
    const readyForUpvote = candidateReadyForUpvote && storyReadyForUpvote;

    async function upvote() {
        await boosterCandidateService.upvote(orderUuid, storyRef.storyId, () =>
            storyRef.upvote(),
        );
    }

    async function onVoteClick() {
        await upvote();
    }

    if (candidateReadyForUpvote && storyVoted) {
        void boosterCandidateService.markAsVoted(orderUuid);
    }

    if (error) {
        return (
            <WrapTooltip tooltip={errorReason ?? 'Произошла ошибка'}>
                <Label styleOverride={{ status: StatusEnum.Error }}>
                    <Icon src={ErrorIcon} />
                </Label>
            </WrapTooltip>
        );
    }

    if (progress) {
        return (
            <WrapTooltip tooltip="Выполняем транзакцию">
                <Label>
                    <Waiter />
                </Label>
            </WrapTooltip>
        );
    }

    if (success) {
        const tooltipContent = (
            <>
                <p>
                    После завершения сбора плюсов и проверки зачислится{' '}
                    {pointsMultiplier}&nbsp;
                    {PointsPluralize(pointsMultiplier)}
                </p>
                <p>
                    Текущий баланс: {points}&nbsp;
                    {PointsPluralize(points)}
                </p>
            </>
        );

        return (
            <WrapTooltip tooltip={tooltipContent}>
                <Label
                    swapColors={true}
                    styleOverride={{ status: StatusEnum.Success }}>
                    <Icon src={UpIcon} />
                    {pointsMultiplier}
                </Label>
            </WrapTooltip>
        );
    }

    if (readyForUpvote) {
        const tooltipContent = (
            <>
                <p>
                    За поставленный этому посту плюс зачислится&nbsp;
                    {pointsMultiplier}&nbsp;
                    {PointsPluralize(pointsMultiplier)}. Накопленные баллы можно
                    будет использовать для поднятия рейтинга других постов.
                </p>
                <p>
                    Текущий баланс: {points}&nbsp;
                    {PointsPluralize(points)}
                </p>
            </>
        );
        return (
            <WrapTooltip tooltip={tooltipContent}>
                <RoundedButton
                    styleOverride={{ status: StatusEnum.Success }}
                    onClick={onVoteClick}>
                    <Icon src={UpIcon} />
                    {pointsMultiplier}
                </RoundedButton>
            </WrapTooltip>
        );
    }

    return (
        <WrapTooltip tooltip={'Upvote недоступен'}>
            <RoundedButton disabled={true}>
                <Icon src={UpIcon} />
                {pointsMultiplier}
            </RoundedButton>
        </WrapTooltip>
    );
}
