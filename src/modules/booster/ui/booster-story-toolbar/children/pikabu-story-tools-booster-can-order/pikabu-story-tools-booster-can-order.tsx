import { useState } from 'react';
import { Container } from 'typedi';

import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Label } from '@des-kit/label';
import { RoundedButton } from '@des-kit/rounded-button';
import { Waiter } from '@des-kit/waiter';

import { WrapTooltip } from '../../../../../../common/components/wrap-tooltip/wrap-tooltip';
import Up2Icon from '../../../../../../icons/booster-arrows.svg';
import { BoosterOrderConditionsDto } from '../../../../../pikabu-fixes-api/gen';
import { SettingsService } from '../../../../../settings/settings.service';
import { PlusesPluralize } from '../../../../pluralize/pluses.pluralize';
import { PointsPluralize } from '../../../../pluralize/points.pluralize';
import { UsersPluralize } from '../../../../pluralize/users.pluralize';
import { BoosterClientService } from '../../../../services/booster-client.service';
import { BoosterOrderService } from '../../../../services/booster-order.service';
import {
    OrderRequestWindow,
    PikabuFixesBoosterOrderWindowResult,
} from '../../../order-request-window/order-request-window';

export interface PikabuStoryToolsBoosterCanOrderProps {
    readonly storyId: number;
    readonly orderConditions: BoosterOrderConditionsDto;
}

export function PikabuStoryToolsBoosterCanOrder(
    props: PikabuStoryToolsBoosterCanOrderProps,
) {
    const settingsService = Container.get(SettingsService);
    const boosterOrderService = Container.get(BoosterOrderService);
    const boosterClientService = Container.get(BoosterClientService);

    const [orderCreating, setOrderCreating] = useState(false);
    const [orderWindowShown, setOrderWindowShown] = useState(false);

    const [settings] = useObservable(() => settingsService.settings$, null);

    const [status, statusError] = useObservable(
        () => boosterClientService.status$,
        null,
    );

    if (!status || !settings) {
        return (
            <Label>
                <Waiter />
            </Label>
        );
    }

    if (orderCreating) {
        return (
            <WrapTooltip tooltip="Создаем запрос">
                <Label>
                    <Waiter />
                </Label>
            </WrapTooltip>
        );
    }

    const tooltipTexts: string[] = [];

    tooltipTexts.push(
        `Сейчас в сети ${status.onlineClientsCount} ${UsersPluralize(
            status.onlineClientsCount,
        )}`,
    );

    tooltipTexts.push(
        `Баланс ${status.points} ${PointsPluralize(status.points)}`,
    );

    if (!status.points) {
        tooltipTexts.push(
            `Для получения баллов ищи в ленте «Свежее» посты с пометкой (зеленая обводка у кнопки плюса и блока Booster) и ставь им свой плюс`,
        );
    }

    const maxBoost = Math.min(
        status.points,
        status.onlineClientsCount,
        props.orderConditions.requestPointsLimit ?? Infinity,
    );
    const canBoost = maxBoost > 0;
    const text = canBoost ? `+${maxBoost}` : `0`;

    if (canBoost) {
        tooltipTexts.push(
            `Максимум можно запросить ${maxBoost} ${PlusesPluralize(maxBoost)}`,
        );
    }

    function onBoostClick() {
        setOrderWindowShown(true);
    }

    const tooltipContent = (
        <>
            {tooltipTexts.map((text, i) => (
                <p key={i}>{text}</p>
            ))}
        </>
    );

    function onOrderReject() {
        setOrderWindowShown(false);
    }

    async function onOrderResolve(result: PikabuFixesBoosterOrderWindowResult) {
        setOrderWindowShown(false);

        setOrderCreating(true);

        try {
            await settingsService.patch({
                boosterOrderMinutes: result.minutes,
                boosterOrderPoints: result.points,
                boosterOrderPointsMultiplier: result.pointsMultiplier,
            });

            await boosterOrderService.create(
                props.storyId,
                result.points,
                result.minutes,
                result.pointsMultiplier,
            );
        } finally {
            setOrderCreating(false);
        }
    }

    return (
        <>
            <WrapTooltip tooltip={tooltipContent}>
                <RoundedButton
                    disabled={!canBoost}
                    onClick={onBoostClick}>
                    <Icon src={Up2Icon} />
                    {text}
                </RoundedButton>
            </WrapTooltip>
            {orderWindowShown && (
                <OrderRequestWindow
                    initialMinutes={settings.boosterOrderMinutes}
                    initialPoints={settings.boosterOrderPoints}
                    initialPointsMultiplier={
                        settings.boosterOrderPointsMultiplier
                    }
                    boosterStatus={status}
                    orderConditions={props.orderConditions}
                    onReject={onOrderReject}
                    onResolve={onOrderResolve}
                />
            )}
        </>
    );
}
