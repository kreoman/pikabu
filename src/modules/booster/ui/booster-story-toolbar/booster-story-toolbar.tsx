import './booster-story-toolbar.scss';

import classNames from 'classnames';
import React from 'react';
import { Container } from 'typedi';

import { isSet } from '@des-kit/core';
import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Label } from '@des-kit/label';

import { PikabuFixesToolWrapper } from '../../../../common/components/pikabu-fixes-tool-wrapper/pikabu-fixes-tool-wrapper';
import { WrapTooltip } from '../../../../common/components/wrap-tooltip/wrap-tooltip';
import LockIcon from '../../../../icons/lock.svg';
import { PikabuStoryRef } from '../../../pikabu-dom/element-refs/story/pikabu-story-ref';
import { PikabuStoryRefRating } from '../../../pikabu-features-ui/elements/pikabu-story-ref-rating/pikabu-story-ref-rating';
import { SettingsService } from '../../../settings/settings.service';
import { checkOrderConditions } from '../../functions/check-order-conditions';
import { BoosterCandidateService } from '../../services/booster-candidate.service';
import { BoosterClientService } from '../../services/booster-client.service';
import { BoosterOrderService } from '../../services/booster-order.service';
import { BoosterOrderConditionsService } from '../../services/booster-order-conditions.service';
import { BoosterOrderControl } from '../orders/booster-order-control/booster-order-control';
import { PikabuStoryToolsBoosterCanOrder } from './children/pikabu-story-tools-booster-can-order/pikabu-story-tools-booster-can-order';
import { PikabuStoryToolsBoosterHasCandidate } from './children/pikabu-story-tools-booster-has-candidate/pikabu-story-tools-booster-has-candidate';

interface PikabuStoryToolsProps {
    readonly storyRef: PikabuStoryRef;
}

export function BoosterStoryToolbar({ storyRef }: PikabuStoryToolsProps) {
    const className = classNames('des--pikabu-story-tools-booster');

    const settingsService = Container.get(SettingsService);
    const boosterClientService = Container.get(BoosterClientService);
    const boosterOrderService = Container.get(BoosterOrderService);
    const boosterCandidateService = Container.get(BoosterCandidateService);
    const boosterOrderConditionsService = Container.get(
        BoosterOrderConditionsService,
    );

    const [orderConditions] = useObservable(
        () => boosterOrderConditionsService.conditions$,
        null,
    );
    const [settings] = useObservable(() => settingsService.settings$, null);
    const [client] = useObservable(() => boosterClientService.status$, null);
    const [order] = useObservable(
        () => boosterOrderService.getActualOrderOrNull(storyRef.storyId),
        null,
    );

    const [candidate] = useObservable(
        () => boosterCandidateService.listenActualCandidate(storyRef.storyId),
        null,
    );

    if (!client || !settings || !orderConditions) {
        return null;
    }

    const rating = (
        <div className="des--pikabu-story-tools-booster__rating">
            <PikabuStoryRefRating
                version={2}
                storyRef={storyRef}
            />
        </div>
    );

    if (isSet(order)) {
        return (
            <PikabuFixesToolWrapper className={className}>
                <BoosterOrderControl order={order} />
                {rating}
            </PikabuFixesToolWrapper>
        );
    }

    if (isSet(candidate)) {
        return (
            <PikabuFixesToolWrapper
                className={className}
                highlight={candidate.obsolete === 0}>
                <PikabuStoryToolsBoosterHasCandidate
                    candidate={candidate}
                    clientStatus={client}
                    storyRef={storyRef}
                />
                {rating}
            </PikabuFixesToolWrapper>
        );
    }

    const conditionsError = checkOrderConditions(storyRef, orderConditions);

    if (conditionsError) {
        return (
            <PikabuFixesToolWrapper className={className}>
                <WrapTooltip tooltip={conditionsError}>
                    <Label>
                        <Icon src={LockIcon} />
                    </Label>
                </WrapTooltip>
                {rating}
            </PikabuFixesToolWrapper>
        );
    }

    return (
        <PikabuFixesToolWrapper className={className}>
            <PikabuStoryToolsBoosterCanOrder
                storyId={storyRef.storyId}
                orderConditions={orderConditions}
            />
            {rating}
        </PikabuFixesToolWrapper>
    );
}
