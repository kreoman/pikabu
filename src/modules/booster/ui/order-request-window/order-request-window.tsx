import './order-request-window.scss';

import { add, formatDistanceToNowStrict } from 'date-fns';
import { ru } from 'date-fns/locale';
import { useEffect, useMemo, useState } from 'react';

import { Card } from '@des-kit/card';
import { Column } from '@des-kit/column';
import { Dialog } from '@des-kit/dialog';
import { Island } from '@des-kit/island';
import { NumberRange } from '@des-kit/number-range';
import { Scroll } from '@des-kit/scroll';
import { SizeContext, SizeEnum } from '@des-kit/styles';

import StatusIcon from '../../../../icons/info.svg';
import {
    BoosterClientStatusDto,
    BoosterOrderConditionsDto,
} from '../../../pikabu-fixes-api/gen';

export interface PikabuFixesBoosterOrderWindowResult {
    readonly points: number;
    readonly minutes: number;
    readonly pointsMultiplier: number;
}

export interface PikabuFixesBoosterOrderWindowProps {
    readonly initialPoints: number;
    readonly initialMinutes: number;
    readonly initialPointsMultiplier: number;
    readonly orderConditions: BoosterOrderConditionsDto;
    readonly boosterStatus: BoosterClientStatusDto;
    readonly onReject: () => void;
    readonly onResolve: (result: PikabuFixesBoosterOrderWindowResult) => void;
}

export function OrderRequestWindow({
    boosterStatus: { onlineClientsCount, points },
    initialMinutes,
    initialPoints,
    initialPointsMultiplier,
    onReject,
    onResolve: onResolveResult,
    orderConditions: {
        pointsMultiplierMax,
        requestMinutesLimit: maxMinutes,
        requestPointsLimit,
    },
}: PikabuFixesBoosterOrderWindowProps) {
    const maxPointsMultiplier = pointsMultiplierMax ?? 10;
    const maxPoints = Math.min(
        points,
        onlineClientsCount,
        requestPointsLimit ?? Infinity,
    );

    const [state, setState] = useState<PikabuFixesBoosterOrderWindowResult>({
        points: Math.min(initialPoints, maxPoints),
        minutes: Math.min(initialMinutes, maxMinutes),
        pointsMultiplier: Math.min(
            initialPointsMultiplier,
            maxPointsMultiplier,
        ),
    });

    const [pointsRange, setPointsRange] = useState(
        (state.points - 1) / (maxPoints - 1),
    );
    const [minutesRange, setMinutesRange] = useState(
        (state.minutes - 1) / (maxMinutes - 1),
    );
    const [pointsMultiplierRange, setPointsMultiplierRange] = useState(
        (state.pointsMultiplier - 1) / (maxPointsMultiplier - 1),
    );

    useEffect(() => {
        setState({
            ...state,
            points: numberRangePointsValue(pointsRange),
            minutes: numberRangeMinutesValue(minutesRange),
            pointsMultiplier: numberRangePointsMultiplierValue(
                pointsMultiplierRange,
            ),
        });
    }, [pointsRange, maxPoints, minutesRange, pointsMultiplierRange]);

    const outDateDistance = useMemo(() => {
        const nextDate = add(Date.now(), {
            minutes: state.minutes,
        });

        return formatDistanceToNowStrict(nextDate, {
            locale: ru,
            unit: 'minute',
        });
    }, [state.minutes]);

    const debitPoints = state.points * state.pointsMultiplier;

    const canResolve = points > debitPoints;

    function onResolve() {
        onResolveResult(state);
    }

    function numberRangePointsValue(range: number): number {
        return Math.round((maxPoints - 1) * range) + 1;
    }

    function numberRangeMinutesValue(range: number): number {
        return Math.round((maxMinutes - 1) * range) + 1;
    }

    function numberRangePointsMultiplierValue(range: number): number {
        return Math.round((maxPointsMultiplier - 1) * range) + 1;
    }

    return (
        <SizeContext.Provider value={SizeEnum.Medium}>
            <Dialog
                title="Запрос плюсов"
                onReject={onReject}
                onResolve={onResolve}
                resolveDisabled={!canResolve}>
                <Scroll className="des--order-request-window__content">
                    <Column>
                        <Card
                            title="Статус"
                            iconSrc={StatusIcon}>
                            <p>
                                Пользователей в сети:{' '}
                                <strong>{onlineClientsCount}</strong>
                            </p>
                            <p>
                                Баллы на счету: <strong>{points}</strong>
                            </p>
                        </Card>

                        <Card
                            title="Кол-во плюсов"
                            iconSrc={StatusIcon}>
                            <NumberRange
                                value={pointsRange}
                                valueFormatter={numberRangePointsValue}
                                onChange={setPointsRange}
                            />
                            <p>
                                Будет запрошено Upvote:{' '}
                                <strong>{state.points}</strong>
                            </p>
                        </Card>

                        <Card
                            title="Стоимость одного Upvote"
                            iconSrc={StatusIcon}>
                            <NumberRange
                                value={pointsMultiplierRange}
                                valueFormatter={
                                    numberRangePointsMultiplierValue
                                }
                                onChange={setPointsMultiplierRange}
                            />
                            <p>
                                Будет списано баллов:{' '}
                                <strong>{debitPoints}</strong>
                            </p>
                        </Card>

                        <Card
                            title="Длительность запроса"
                            iconSrc={StatusIcon}>
                            <NumberRange
                                value={minutesRange}
                                valueFormatter={numberRangeMinutesValue}
                                onChange={setMinutesRange}
                            />
                            <p>
                                Запрос завершится через {outDateDistance}. По
                                его завершению нереализованные баллы вернутся на
                                баланс
                            </p>
                        </Card>

                        <Island>
                            <strong>
                                Созданный запрос можно будет завершить досрочно.
                                Изменить его будет нельзя.
                            </strong>
                        </Island>
                    </Column>
                </Scroll>
            </Dialog>
        </SizeContext.Provider>
    );
}
