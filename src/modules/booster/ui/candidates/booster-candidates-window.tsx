import { SizeContext, SizeEnum } from '@des-kit/styles';

import { ScrollableWindow } from '../../../../common/components/scrollable-window/scrollable-window';
import { BoosterCandidatesList } from './booster-candidates-list';

export interface BoosterCandidatesWindowProps {
    readonly onClose?: () => void;
}

export function BoosterCandidatesWindow(props: BoosterCandidatesWindowProps) {
    return (
        <SizeContext.Provider value={SizeEnum.Medium}>
            <ScrollableWindow
                title="Кандидаты на Upvote"
                onClose={props.onClose}>
                <BoosterCandidatesList />
            </ScrollableWindow>
        </SizeContext.Provider>
    );
}
