import { isSet } from '@des-kit/core';
import { Fade } from '@des-kit/fade';
import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { RoundedButton } from '@des-kit/rounded-button';
import { Row } from '@des-kit/row';
import { useState } from 'react';
import { Container } from 'typedi';

import UpIcon from '../../../../icons/arrow-up.svg';
import { BoosterCandidateService } from '../../services/booster-candidate.service';
import { BoosterCandidatesWindow } from './booster-candidates-window';

export function BoosterCandidatesRow() {
    const boosterCandidateService = Container.get(BoosterCandidateService);

    const [candidates] = useObservable(
        () => boosterCandidateService.getCandidatesForUpvote(),
        null,
    );

    const [candidatesWindowShown, setCandidatesWindowShown] = useState(false);

    return (
        <>
            {candidatesWindowShown && (
                <BoosterCandidatesWindow
                    onClose={() => setCandidatesWindowShown(false)}
                />
            )}
            <Fade show={isSet(candidates)}>
                <Row>
                    <RoundedButton
                        onClick={() => setCandidatesWindowShown(true)}>
                        <Icon src={UpIcon} />
                        <span>Кандидаты на Upvote: {candidates?.length}</span>
                    </RoundedButton>
                </Row>
            </Fade>
        </>
    );
}
