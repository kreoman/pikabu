import { Column } from '@des-kit/column';
import { isSet } from '@des-kit/core';
import { useObservable } from '@des-kit/hooks';
import { Label } from '@des-kit/label';
import { Placeholder } from '@des-kit/placeholder';
import { StatusEnum } from '@des-kit/styles';
import { Container } from 'typedi';

import { BlockWaiter } from '../../../../common/components/block-waiter/block-waiter';
import { BoosterCandidateService } from '../../services/booster-candidate.service';
import { BoosterCandidatesListItem } from './booster-candidates-list-item';

export function BoosterCandidatesList() {
    const boosterCandidateService = Container.get(BoosterCandidateService);

    const [candidates, candidatesError] = useObservable(
        () => boosterCandidateService.getCandidatesForUpvote(),
        null,
    );

    if (isSet(candidatesError)) {
        return (
            <Label styleOverride={{ status: StatusEnum.Error }}>
                Не удалось получить
            </Label>
        );
    }

    if (!isSet(candidates)) {
        return <BlockWaiter />;
    }

    if (!candidates.length) {
        return (
            <Placeholder type="block">
                Сейчас нет кандидатов на Upvote
            </Placeholder>
        );
    }

    async function onCandidateReject(orderUuid: string) {
        await boosterCandidateService.markAsRejected(orderUuid);
    }

    return (
        <Column>
            {candidates.map((candidate, index) => (
                <BoosterCandidatesListItem
                    key={candidate.orderUuid}
                    candidate={candidate}
                    onReject={() => onCandidateReject(candidate.orderUuid)}
                />
            ))}
        </Column>
    );
}
