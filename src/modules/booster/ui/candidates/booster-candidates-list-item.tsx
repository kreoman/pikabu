import { useState } from 'react';
import { Container } from 'typedi';

import { Column } from '@des-kit/column';
import { isSet } from '@des-kit/core';
import { usePromise } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Island } from '@des-kit/island';
import { Label } from '@des-kit/label';
import { RoundedButton } from '@des-kit/rounded-button';
import { Row } from '@des-kit/row';
import { SizeContext, SizeEnum, StatusEnum } from '@des-kit/styles';
import { Waiter } from '@des-kit/waiter';

import { BlockWaiter } from '../../../../common/components/block-waiter/block-waiter';
import { RatingV2 } from '../../../../common/components/rating-v2/rating-v2';
import { WrapTooltip } from '../../../../common/components/wrap-tooltip/wrap-tooltip';
import RetryIcon from '../../../../icons/rotate.svg';
import RejectIcon from '../../../../icons/trash.svg';
import { PikabuStoryApiError } from '../../../pikabu-api/story/errors/pikabu-story-api.error';
import { PikabuStoryApiService } from '../../../pikabu-api/story/services/pikabu-story-api.service';
import { pikabuStoryTitleFormat } from '../../../pikabu-features-ui/functions/pikabu-story-title-format/pikabu-story-title-format';
import { BoosterCandidateStoredModel } from '../../services/booster-candidate-storage.service';

export interface BoosterCandidatesListItemProps {
    readonly candidate: BoosterCandidateStoredModel;
    readonly onReject?: () => void;
}

export function BoosterCandidatesListItem({
    candidate: candy,
    onReject,
}: BoosterCandidatesListItemProps) {
    const [retryIndex, setRetryIndex] = useState(0);
    const pikabuStoryApiService = Container.get(PikabuStoryApiService);
    const storyId = candy.initialStoryId;

    const [story, storyError] = usePromise(
        () => pikabuStoryApiService.getStory(storyId),
        null,
        [retryIndex],
    );

    function onRetryClick() {
        setRetryIndex(retryIndex + 1);
    }

    if (isSet(storyError)) {
        let errorText = 'Не удалось получить данные поста';

        if (storyError instanceof PikabuStoryApiError) {
            errorText = storyError.message;
        }

        return (
            <Island>
                <SizeContext.Provider value={SizeEnum.Small}>
                    <Row>
                        <Label
                            styleOverride={{
                                status: StatusEnum.Error,
                            }}>
                            {errorText}
                        </Label>
                        <RoundedButton onClick={onRetryClick}>
                            <Icon src={RetryIcon} />
                        </RoundedButton>
                    </Row>
                </SizeContext.Provider>
            </Island>
        );
    }

    if (!isSet(story)) {
        return (
            <Island>
                <BlockWaiter />
            </Island>
        );
    }

    return (
        <Island>
            <Column>
                <SizeContext.Provider value={SizeEnum.Small}>
                    <Row>
                        <RatingV2
                            plusesCount={story.story_pluses ?? 0}
                            minusesCount={story.story_minuses ?? null}
                        />
                        <a
                            href={story.story_url}
                            target="_blank">
                            {pikabuStoryTitleFormat(story.story_title)}
                        </a>
                        {candy.pointsMultiplier > 1 && (
                            <Label
                                swapColors={true}
                                styleOverride={{
                                    size: SizeEnum.XSmall,
                                    status: StatusEnum.Success,
                                }}>
                                x{candy.pointsMultiplier}
                            </Label>
                        )}
                        {!!candy.progress && (
                            <WrapTooltip tooltip="В процессе Upvote">
                                <Waiter />
                            </WrapTooltip>
                        )}
                        <WrapTooltip tooltip="Кандидат будет отклонен">
                            <RoundedButton
                                onClick={onReject}
                                style={{ marginLeft: 'auto' }}>
                                <Icon src={RejectIcon} />
                            </RoundedButton>
                        </WrapTooltip>
                    </Row>
                </SizeContext.Provider>
                <SizeContext.Provider value={SizeEnum.XSmall}>
                    <Row wrap={true}>
                        {story.is_authors && <Label>[моё]</Label>}
                        {story.tags.map((tag) => (
                            <Label key={tag}>{tag}</Label>
                        ))}
                    </Row>
                </SizeContext.Provider>
            </Column>
        </Island>
    );
}
