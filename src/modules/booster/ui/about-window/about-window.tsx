import { Column } from '@des-kit/column';
import { Island } from '@des-kit/island';

import { ScrollableWindow } from '../../../../common/components/scrollable-window/scrollable-window';
import { ExtensionLinksConst } from '../../../../common/consts/extension-links.const';
import { ExtensionNameConst } from '../../../../common/consts/extension-name.const';

export interface PikabuFixesBoosterInfoWindowProps {
    readonly onClose: () => void;
}

export function AboutWindow(props: PikabuFixesBoosterInfoWindowProps) {
    return (
        <ScrollableWindow
            title="Как работает Booster"
            onClose={props.onClose}>
            <Island>
                <Column>
                    <p>
                        <b>Баллы</b>
                        <br />У каждого пользователя расширения теперь есть
                        баланс в баллах, которые можно потратить на поднятие
                        рейтинга поста. Для этого необходимо создать запрос на
                        Upvote. Баллы хранятся на аккаунте {
                            ExtensionNameConst
                        }{' '}
                        и привязаны к ключу Booster.
                    </p>
                    <p>
                        <b>Как заработать баллы?</b>
                        <br />
                        Для этого ищите в ленте “Свежее” посты с тулбаром{' '}
                        {ExtensionNameConst}, обведенным зеленым цветом. Это
                        означает, что кто-то разместил запрос на Upvote этого
                        поста. За поставленный такому посту плюс будет начислен
                        1 балл.
                    </p>
                    <p>
                        <b>Как создать запрос?</b>
                        <br />
                        Это можно сделать из тулбара поста. Кнопка создания
                        запроса откроет диалог с дополнительной информацией.
                        Кнопка активна при наличии положительного баланса и
                        наличии активных пользователей Booster
                    </p>
                    <p>
                        <b>Что такое ключ?</b>
                        <br />
                        Ключ - это уникальный сгенерированный код для доступа к
                        аккаунту Booster. Ключ не привязан к учетной записи
                        pikabu.
                    </p>
                    <p>
                        <b>
                            Можно ли использовать один ключ в нескольких
                            аккаунтах/браузерах?
                        </b>
                        <br />
                        Да, можно использовать один ключ и общий баланс. Но
                        важно помнить, что передача ключа в чужие руки
                        необратима. Код ключа изменить нельзя.
                    </p>
                    <p>
                        <b>Меня не забанят на pikabu?</b>
                        <br />
                        Администрация pikabu всеми способами старается
                        выпиливать любые упоминания в постах и комментариях о
                        расширении.
                        <br />
                        Очевидно, функционал Booster им тоже не понравится, так
                        как является прямым конкурентом платным инструментам
                        самого сайта.
                        <br />
                        Технически, pikabu может реализовать проверку наличия
                        расширения и применять за это какие-либо санкции
                        пользователю, но на текущий момент о таких случаях
                        неизвестно.
                        <br />
                        Для того чтобы следить за новостями и обсуждать работу
                        расширения, подпишитесь на Telegram{' '}
                        <a
                            target="_blank"
                            href={ExtensionLinksConst.TelegramGroup}>
                            канал
                        </a>{' '}
                        и{' '}
                        <a
                            target="_blank"
                            href={ExtensionLinksConst.TelegramChat}>
                            чат
                        </a>
                        .
                    </p>
                </Column>
            </Island>
        </ScrollableWindow>
    );
}
