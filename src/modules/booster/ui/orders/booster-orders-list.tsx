import { Column } from '@des-kit/column';
import { isSet } from '@des-kit/core';
import { useObservable } from '@des-kit/hooks';
import { Label } from '@des-kit/label';
import { Placeholder } from '@des-kit/placeholder';
import { RoundedButton } from '@des-kit/rounded-button';
import { Row } from '@des-kit/row';
import { StatusEnum } from '@des-kit/styles';
import { Waiter } from '@des-kit/waiter';
import { useState } from 'react';
import { Container } from 'typedi';

import { BlockWaiter } from '../../../../common/components/block-waiter/block-waiter';
import { BoosterOrderService } from '../../services/booster-order.service';
import { BoosterOrdersListItem } from './booster-orders-list-item';

export function BoosterOrdersList() {
    const boosterOrderService = Container.get(BoosterOrderService);
    const [obsoleteRemoving, setObsoleteRemoving] = useState(false);
    const [orders, ordersError] = useObservable(
        () => boosterOrderService.getActualOrders(),
        null,
    );

    if (isSet(ordersError)) {
        return (
            <Label styleOverride={{ status: StatusEnum.Error }}>
                Не удалось получить
            </Label>
        );
    }

    if (!isSet(orders)) {
        return <BlockWaiter />;
    }

    if (!orders.length) {
        return <Placeholder type="block">Список запросов пуст</Placeholder>;
    }

    const hasObsoleteOrders = orders.some((order) => !order.active);

    async function onObsoleteRemoveClick() {
        setObsoleteRemoving(true);

        try {
            await boosterOrderService.deleteObsolete();
        } finally {
            setObsoleteRemoving(false);
        }
    }

    return (
        <Column>
            {hasObsoleteOrders && (
                <Row>
                    <RoundedButton
                        disabled={obsoleteRemoving}
                        onClick={onObsoleteRemoveClick}>
                        {obsoleteRemoving ? (
                            <>
                                Удаляем завершенные
                                <Waiter />
                            </>
                        ) : (
                            'Удалить все завершенные'
                        )}
                    </RoundedButton>
                </Row>
            )}
            {orders.map((order, index) => (
                <BoosterOrdersListItem
                    key={order.uuid}
                    order={order}
                />
            ))}
        </Column>
    );
}
