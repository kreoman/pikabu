import { Column } from '@des-kit/column';
import { Island } from '@des-kit/island';
import { Row } from '@des-kit/row';
import { SizeContext, SizeEnum } from '@des-kit/styles';

import { pikabuStoryTitleFormat } from '../../../pikabu-features-ui/functions/pikabu-story-title-format/pikabu-story-title-format';
import { BoosterOrderStoredModel } from '../../services/booster-order-storage.service';
import { BoosterOrderControl } from './booster-order-control/booster-order-control';

export interface BoosterOrdersListItemProps {
    readonly order: BoosterOrderStoredModel;
}

export function BoosterOrdersListItem({ order }: BoosterOrdersListItemProps) {
    return (
        <Island>
            <Column>
                <Row>
                    <a
                        href={`https://pikabu.ru/story/_${order.storyId}`}
                        target="_blank">
                        {pikabuStoryTitleFormat(order.storyTitle)}
                    </a>
                </Row>
                <Row>
                    <SizeContext.Provider value={SizeEnum.Small}>
                        <BoosterOrderControl order={order} />
                    </SizeContext.Provider>
                </Row>
            </Column>
        </Island>
    );
}
