import { Icon } from '@des-kit/icon';
import { Label } from '@des-kit/label';
import { RoundedButton } from '@des-kit/rounded-button';
import { StatusEnum } from '@des-kit/styles';
import { Waiter } from '@des-kit/waiter';
import { formatDistanceToNow, parseISO } from 'date-fns';
import { ru } from 'date-fns/locale';
import { useState } from 'react';
import { Container } from 'typedi';

import { WrapTooltip } from '../../../../../common/components/wrap-tooltip/wrap-tooltip';
import CheckedIcon from '../../../../../icons/circle-check.svg';
import CancelIcon from '../../../../../icons/circle-xmark.svg';
import { DeliveredPluralize } from '../../../pluralize/delivered.pluralize';
import { PlusesPluralize } from '../../../pluralize/pluses.pluralize';
import { BoosterOrderService } from '../../../services/booster-order.service';
import { BoosterOrderStoredModel } from '../../../services/booster-order-storage.service';

interface PikabuStoryToolsBoosterHasOrderProps {
    readonly order: BoosterOrderStoredModel;
}

export function BoosterOrderControl(
    props: PikabuStoryToolsBoosterHasOrderProps,
) {
    const boosterOrderService = Container.get(BoosterOrderService);
    const requestPluses = props.order.votesLimit;
    const successPluses = props.order.votesSuccess;
    const outDate = parseISO(props.order.outDate);
    const outDateDistance = formatDistanceToNow(outDate, { locale: ru });
    const [deleting, setDeleting] = useState(false);

    async function onCancelClick() {
        setDeleting(true);

        try {
            await boosterOrderService.delete(props.order.uuid);
        } finally {
            setDeleting(false);
        }
    }

    if (deleting) {
        return (
            <WrapTooltip tooltip="Запрос удаляется">
                <Label styleOverride={{ status: StatusEnum.Warning }}>
                    <Waiter />
                </Label>
            </WrapTooltip>
        );
    }

    if (props.order.active === 0) {
        const status =
            props.order.votesRest === 0
                ? StatusEnum.Success
                : StatusEnum.Neutral;

        const tooltipContent = (
            <>
                <p>
                    Собрано {successPluses} из {requestPluses}{' '}
                    {PlusesPluralize(requestPluses)}.
                </p>
                <p>Сбор завершен</p>
            </>
        );

        return (
            <>
                <WrapTooltip tooltip={tooltipContent}>
                    <Label
                        styleOverride={{ status }}
                        swapColors={true}>
                        <Icon src={CheckedIcon} />+{successPluses}/
                        {requestPluses}
                    </Label>
                </WrapTooltip>
                <WrapTooltip tooltip="Удалить запрос">
                    <RoundedButton onClick={onCancelClick}>
                        <Icon src={CancelIcon} />
                    </RoundedButton>
                </WrapTooltip>
            </>
        );
    }

    const tooltipContent = (
        <>
            <p>
                {successPluses} {PlusesPluralize(successPluses)} из{' '}
                {requestPluses} уже {DeliveredPluralize(successPluses)}.
            </p>
            <p>Сбор завершится через {outDateDistance}</p>
        </>
    );

    return (
        <>
            <WrapTooltip tooltip={tooltipContent}>
                <Label styleOverride={{ status: StatusEnum.Success }}>
                    <Waiter />+{successPluses}/{requestPluses}
                </Label>
            </WrapTooltip>
            <WrapTooltip tooltip="Отменить запрос">
                <RoundedButton onClick={onCancelClick}>
                    <Icon src={CancelIcon} />
                </RoundedButton>
            </WrapTooltip>
        </>
    );
}
