import { SizeContext, SizeEnum } from '@des-kit/styles';

import { ScrollableWindow } from '../../../../common/components/scrollable-window/scrollable-window';
import { BoosterOrdersList } from './booster-orders-list';

export interface BoosterOrdersWindowProps {
    readonly onClose?: () => void;
}

export function BoosterOrdersWindow(props: BoosterOrdersWindowProps) {
    return (
        <SizeContext.Provider value={SizeEnum.Medium}>
            <ScrollableWindow
                title="Запросы"
                onClose={props.onClose}>
                <BoosterOrdersList />
            </ScrollableWindow>
        </SizeContext.Provider>
    );
}
