import { isSet } from '@des-kit/core';
import { Fade } from '@des-kit/fade';
import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { RoundedButton } from '@des-kit/rounded-button';
import { Row } from '@des-kit/row';
import { useState } from 'react';
import { Container } from 'typedi';

import UpIcon from '../../../../icons/arrow-up.svg';
import { BoosterOrderService } from '../../services/booster-order.service';
import { BoosterOrdersWindow } from './booster-orders-window';

export function BoosterOrdersRow() {
    const boosterOrderService = Container.get(BoosterOrderService);

    const [orders] = useObservable(
        () => boosterOrderService.getActualOrders(),
        null,
    );

    const [ordersWindowShown, setOrdersWindowShown] = useState(false);

    return (
        <>
            {ordersWindowShown && (
                <BoosterOrdersWindow
                    onClose={() => setOrdersWindowShown(false)}
                />
            )}
            <Fade show={isSet(orders)}>
                <Row>
                    <RoundedButton onClick={() => setOrdersWindowShown(true)}>
                        <Icon src={UpIcon} />
                        <span>Мои запросы: {orders?.length}</span>
                    </RoundedButton>
                </Row>
            </Fade>
        </>
    );
}
