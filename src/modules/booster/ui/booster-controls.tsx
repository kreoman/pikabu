import { Column } from '@des-kit/column';
import { Fade } from '@des-kit/fade';
import { useObservable } from '@des-kit/hooks';
import { Container } from 'typedi';

import { BoosterClientService } from '../services/booster-client.service';
import { BoosterCandidatesRow } from './candidates/booster-candidates-row';
import { BoosterOrdersRow } from './orders/booster-orders-row';
import { BoosterPointsRow } from './points/booster-points-row';

export function BoosterControls() {
    const boosterClientService = Container.get(BoosterClientService);

    const [enabledUuid] = useObservable(
        () => boosterClientService.enabledUuid$,
        null,
    );

    return (
        <>
            <Fade show={!!enabledUuid}>
                <Column>
                    <BoosterPointsRow />
                    <BoosterCandidatesRow />
                    <BoosterOrdersRow />
                </Column>
            </Fade>
        </>
    );
}
