import { pluralize } from '@des-kit/core';
import { Island } from '@des-kit/island';
import { Label } from '@des-kit/label';
import { Row } from '@des-kit/row';
import { StatusEnum } from '@des-kit/styles';
import { format, parseISO } from 'date-fns';
import { ru } from 'date-fns/locale';

import { BoosterPointsTransactionDto } from '../../../pikabu-fixes-api/gen';
import { PointsPluralize } from '../../pluralize/points.pluralize';

const DebitPluralize = pluralize('Зачислен', 'Зачислено', 'Зачислено');
const CreditPluralize = pluralize('Списан', 'Списано', 'Списано');

export interface BoosterPointsTransactionProps {
    readonly transaction: BoosterPointsTransactionDto;
}

export function BoosterPointsTransactionListItem({
    transaction: tx,
}: BoosterPointsTransactionProps) {
    const date = parseISO(tx.date);
    const formattedDate = format(date, 'dd.MM.yyyy HH:mm', {
        locale: ru,
    });

    const isDebit = tx.value > 0;
    const valueStatus: StatusEnum = isDebit
        ? StatusEnum.Success
        : StatusEnum.Warning;
    const abs = Math.abs(tx.value);

    const operationName = isDebit ? DebitPluralize(abs) : CreditPluralize(abs);

    const text = `${operationName} ${abs} ${PointsPluralize(abs)}`;

    return (
        <Island>
            <Row>
                <Label styleOverride={{ status: valueStatus }}>{text}</Label>
                <span>{formattedDate}</span>
            </Row>
            <Row>{tx.comment}</Row>
        </Island>
    );
}
