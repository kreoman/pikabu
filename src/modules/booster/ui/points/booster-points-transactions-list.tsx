import { Column } from '@des-kit/column';
import { isSet } from '@des-kit/core';
import { useObservable } from '@des-kit/hooks';
import { Label } from '@des-kit/label';
import { Placeholder } from '@des-kit/placeholder';
import { StatusEnum } from '@des-kit/styles';
import { Container } from 'typedi';

import { BlockWaiter } from '../../../../common/components/block-waiter/block-waiter';
import { BoosterPointsTransactionService } from '../../services/booster-points-transaction.service';
import { BoosterPointsTransactionListItem } from './booster-points-transaction-list-item';

export function BoosterPointsTransactionsList() {
    const boosterPointsTransactionService = Container.get(
        BoosterPointsTransactionService,
    );

    const [transactions, transactionsError] = useObservable(
        () => boosterPointsTransactionService.transactions$,
        null,
    );

    if (isSet(transactionsError)) {
        return (
            <Label styleOverride={{ status: StatusEnum.Error }}>
                Не удалось получить
            </Label>
        );
    }

    if (!isSet(transactions)) {
        return <BlockWaiter />;
    }

    if (!transactions.length) {
        return <Placeholder type="block">Список транзакций пуст</Placeholder>;
    }

    return (
        <Column>
            {transactions.map((transaction, index) => (
                <BoosterPointsTransactionListItem
                    key={index}
                    transaction={transaction}
                />
            ))}
        </Column>
    );
}
