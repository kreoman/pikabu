import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { RoundedButton } from '@des-kit/rounded-button';
import { Row } from '@des-kit/row';
import { Waiter } from '@des-kit/waiter';
import { useState } from 'react';
import { Container } from 'typedi';

import BoosterIcon from '../../../../icons/booster-arrows.svg';
import RefreshIcon from '../../../../icons/rotate.svg';
import { BoosterClientService } from '../../services/booster-client.service';
import { BoosterPointsTransactionsWindow } from './booster-points-transactions-window';

export function BoosterPointsRow() {
    const boosterClientService = Container.get(BoosterClientService);

    const [client] = useObservable(() => boosterClientService.status$, null);
    const [updating, setUpdating] = useState(false);
    const [transactionsWindowShown, setTransactionsWindowShown] =
        useState(false);

    async function onUpdateClick() {
        setUpdating(true);

        try {
            await boosterClientService.updateStatus();
        } finally {
            setUpdating(false);
        }
    }

    return (
        <>
            {transactionsWindowShown && (
                <BoosterPointsTransactionsWindow
                    onClose={() => setTransactionsWindowShown(false)}
                />
            )}
            <Row>
                <RoundedButton onClick={() => setTransactionsWindowShown(true)}>
                    <Icon src={BoosterIcon} />
                    <span>Баллы: {client?.points ?? <Waiter />}</span>
                </RoundedButton>
                <RoundedButton
                    onClick={onUpdateClick}
                    disabled={updating}>
                    {updating ? <Waiter /> : <Icon src={RefreshIcon} />}
                </RoundedButton>
            </Row>
        </>
    );
}
