import { SizeContext, SizeEnum } from '@des-kit/styles';

import { ScrollableWindow } from '../../../../common/components/scrollable-window/scrollable-window';
import { BoosterPointsTransactionsList } from './booster-points-transactions-list';

export interface BoosterTransactionWindowProps {
    readonly onClose?: () => void;
}

export function BoosterPointsTransactionsWindow(
    props: BoosterTransactionWindowProps,
) {
    return (
        <SizeContext.Provider value={SizeEnum.Medium}>
            <ScrollableWindow
                title="Список транзакций"
                onClose={props.onClose}>
                <BoosterPointsTransactionsList />
            </ScrollableWindow>
        </SizeContext.Provider>
    );
}
