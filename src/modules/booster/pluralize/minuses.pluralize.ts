import { pluralize } from '@des-kit/core';

export const MinusesPluralize = pluralize('минус', 'минуса', 'минусов');
