import { pluralize } from '@des-kit/core';

export const UsersPluralize = pluralize(
    'пользователь',
    'пользователя',
    'пользователей',
);
