import { pluralize } from '@des-kit/core';

export const PlusesPluralize = pluralize('плюс', 'плюса', 'плюсов');
