import { pluralize } from '@des-kit/core';

export const PointsPluralize = pluralize('балл', 'балла', 'баллов');
