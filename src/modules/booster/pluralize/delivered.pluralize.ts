import { pluralize } from '@des-kit/core';

export const DeliveredPluralize = pluralize(
    'поставлен',
    'поставлено',
    'поставлено',
);
