import { isSet } from '@des-kit/core';

import { PikabuStoryRef } from '../../pikabu-dom/element-refs/story/pikabu-story-ref';
import { BoosterOrderConditionsDto } from '../../pikabu-fixes-api/gen';
import { MinusesPluralize } from '../pluralize/minuses.pluralize';
import { PlusesPluralize } from '../pluralize/pluses.pluralize';

export function checkOrderConditions(
    story: PikabuStoryRef,
    conditions: BoosterOrderConditionsDto,
): null | string {
    const storyTags = new Set(story.tags.map((tag) => tag.toLowerCase()));

    if (story.isAdult) {
        return 'Запрос рейтинга невозможен для постов с\xA0тегом\xA0"18+"';
    }

    if (conditions.onlyAuthors && !story.isAuthors) {
        return 'Запрос рейтинга возможен только для постов с\xA0тегом\xA0"моё"';
    }

    if (conditions.requiredAnyTags) {
        const requiredAnyTagFound = conditions.requiredAnyTags.some((tag) =>
            storyTags.has(tag),
        );

        if (!requiredAnyTagFound) {
            return `Запрос рейтинга возможен только для постов, содержащих теги: ${conditions.requiredAnyTags.join(
                ', ',
            )}.`;
        }
    }

    const forbiddenAnyTagFound = conditions.forbiddenAnyTags.find((tag) =>
        storyTags.has(tag),
    );

    if (forbiddenAnyTagFound) {
        return `Запрос рейтинга невозможен для постов с тегом\xA0"${forbiddenAnyTagFound.replace(
            ' ',
            '\xA0',
        )}"`;
    }

    if (isSet(conditions.storyMaxPluses)) {
        if (!isSet(story.initialPluses)) {
            return `Нет данных о плюсах поста`;
        }

        if (story.initialPluses > conditions.storyMaxPluses) {
            return `Запрос рейтинга невозможен для постов, набравших ${
                conditions.storyMaxPluses
            } и более ${PlusesPluralize(conditions.storyMaxPluses)}`;
        }
    }

    if (isSet(conditions.storyMaxMinuses)) {
        if (!isSet(story.initialMinuses)) {
            return `Нет данных о минусах поста`;
        }

        if (story.initialMinuses > conditions.storyMaxMinuses) {
            return `Запрос рейтинга невозможен для постов, набравших ${
                conditions.storyMaxMinuses
            } и более ${MinusesPluralize(conditions.storyMaxMinuses)}`;
        }
    }

    return null;
}
