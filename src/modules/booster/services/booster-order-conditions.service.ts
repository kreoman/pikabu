import {
    BehaviorSubject,
    exhaustMap,
    filter,
    firstValueFrom,
    merge,
    Observable,
    switchMap,
    timer,
} from 'rxjs';
import { Container, Service } from 'typedi';

import { BackgroundObservable } from '@des-kit/extension-bg';
import { deferShareReplay } from '@des-kit/rxjs';
import { never } from '@des-kit/rxjs';

import {
    BoosterOrderApiService,
    BoosterOrderConditionsDto,
} from '../../pikabu-fixes-api/gen';
import { BoosterClientService } from './booster-client.service';

@Service()
export class BoosterOrderConditionsService {
    private readonly boosterClientService = Container.get(BoosterClientService);
    private readonly boosterClientApiService = BoosterOrderApiService;

    private readonly conditionsBg$ =
        new BehaviorSubject<BoosterOrderConditionsDto | null>(null);

    private readonly periodicStatusUpdateOnSubscribe$ = deferShareReplay(() =>
        this.periodicUpdateOnSubscribeBg(),
    );

    readonly conditions$: Observable<BoosterOrderConditionsDto | null> =
        deferShareReplay(() => this.getConditionsShare());

    @BackgroundObservable('BoosterOrderConditionsService.getConditionsShare')
    private getConditionsShare(): Observable<BoosterOrderConditionsDto | null> {
        return merge(this.conditionsBg$, this.periodicStatusUpdateOnSubscribe$);
    }

    @BackgroundObservable(
        'BoosterOrderConditionsService.periodicUpdateOnSubscribeBg',
    )
    private periodicUpdateOnSubscribeBg(): Observable<never> {
        return timer(0, 30000).pipe(
            switchMap(() => this.boosterClientService.enabledUuid$),
            filter(Boolean),
            exhaustMap(() => this.updateStatusBg()),
            never(),
        );
    }

    private async updateStatusBg(): Promise<void> {
        try {
            const uuid = await firstValueFrom(
                this.boosterClientService.enabledUuid$,
            );

            if (!uuid) {
                return;
            }

            const status =
                await this.boosterClientApiService.boosterOrderConditionsControllerGetOrderConditions(
                    uuid,
                );

            this.conditionsBg$.next(status);
        } catch (error) {}
    }
}
