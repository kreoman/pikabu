import { Connection, ITable, IWhereQuery } from 'jsstore';
import { IDataBase } from 'jsstore/dist/ts/common';
import workerInjector from 'jsstore/dist/worker_injector';
import { Service } from 'typedi';

import {
    EXTENSION_CURRENT_RUNTIME_TYPE,
    ExtensionRuntimeTypeEnum,
} from '@des-kit/extension';

import { BoosterOrderDto } from '../../pikabu-fixes-api/gen';

export interface BoosterOrderStoredModel {
    uuid: string;
    storyId: number;
    storyTitle: string;
    votesLimit: number;
    votesRest: number;
    votesSuccess: number;
    outDate: string;
    active: number; // 0/1
    deleted: number; // 0/1
}

const ORDER_TABLE: ITable = {
    name: 'order',
    columns: {
        uuid: {
            primaryKey: true,
            dataType: 'string',
            unique: true,
            notNull: true,
        },
        outDate: { dataType: 'string', notNull: true },
        storyId: { dataType: 'number', notNull: true },
        storyTitle: { dataType: 'string', notNull: true },
        votesLimit: { dataType: 'number', notNull: true },
        votesRest: { dataType: 'number', notNull: true },
        votesSuccess: { dataType: 'number', notNull: true },
        active: { dataType: 'number', notNull: true },
        deleted: { dataType: 'number', notNull: true },
    },
};

const DATABASE: IDataBase = {
    version: 1,
    name: 'des--booster-order',
    tables: [ORDER_TABLE],
};

@Service()
export class BoosterOrderStorageService {
    private _db?: Promise<Connection>;

    private get db(): Promise<Connection> {
        if (!this._db) {
            this._db = this.openDB();
        }

        return this._db;
    }

    async setActualOrders(
        orderDtos: readonly BoosterOrderDto[],
    ): Promise<void> {
        const uuids = orderDtos.map((orderDto) => orderDto.uuid);

        await this.markDeletedExcept(uuids);

        const insertData = orderDtos.map((candidateDto) =>
            BoosterOrderStorageService.toInsertStoredModel(candidateDto),
        );

        await this.upsertCandidates(insertData);
    }

    async getActualOrders(): Promise<BoosterOrderStoredModel[]> {
        const db = await this.db;

        return db.select<BoosterOrderStoredModel>({
            from: 'order',
            where: {
                deleted: 0,
            },
            order: [
                {
                    by: 'outDate',
                    type: 'desc',
                },
                {
                    by: 'active',
                    type: 'desc',
                },
            ],
        });
    }

    async getActiveOrders(): Promise<BoosterOrderStoredModel[]> {
        const db = await this.db;

        return db.select<BoosterOrderStoredModel>({
            from: 'order',
            where: {
                active: 1,
                deleted: 0,
            },
        });
    }

    async getActualOrdersByStoryId(
        storyId: number,
    ): Promise<BoosterOrderStoredModel[]> {
        const db = await this.db;

        return db.select<BoosterOrderStoredModel>({
            from: 'order',
            where: {
                storyId,
                deleted: 0,
            },
        });
    }

    private async openDB(): Promise<Connection> {
        if (
            EXTENSION_CURRENT_RUNTIME_TYPE !==
            ExtensionRuntimeTypeEnum.Background
        ) {
            throw new Error(`wrong runtime`);
        }

        const connection = new Connection();

        connection.addPlugin(workerInjector);

        await connection.initDb(DATABASE);

        return connection;
    }

    private static toInsertStoredModel(
        dto: BoosterOrderDto,
    ): BoosterOrderStoredModel {
        return {
            active: dto.active ? 1 : 0,
            deleted: dto.deleted ? 1 : 0,
            outDate: dto.outDate,
            storyId: dto.storyId,
            storyTitle: dto.storyTitle,
            uuid: dto.uuid,
            votesLimit: dto.votesLimit,
            votesRest: dto.votesRest,
            votesSuccess: dto.votesSuccess,
        };
    }

    private async upsertCandidates(
        insertData: BoosterOrderStoredModel[],
    ): Promise<void> {
        if (!insertData.length) {
            return;
        }

        const db = await this.db;

        await db.insert({
            into: 'order',
            values: insertData,
            upsert: true,
        });
    }

    private async markDeletedExcept(
        orderUuids: readonly string[],
    ): Promise<void> {
        const db = await this.db;

        let obsoleteQueryWhere: IWhereQuery[] | IWhereQuery;

        if (orderUuids.length > 0) {
            obsoleteQueryWhere = orderUuids.map((uuid) => ({
                deleted: 0,
                uuid: {
                    '!=': uuid,
                },
            }));
        } else {
            obsoleteQueryWhere = {
                deleted: 0,
            };
        }

        await db.update({
            in: 'order',
            where: obsoleteQueryWhere,
            set: {
                deleted: 1,
                active: 0,
            },
        });
    }
}
