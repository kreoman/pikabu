import { addMinutes } from 'date-fns';
import {
    distinctUntilChanged,
    exhaustMap,
    filter,
    firstValueFrom,
    merge,
    Observable,
    startWith,
    Subject,
    switchMap,
    timer,
} from 'rxjs';
import { map } from 'rxjs/operators';
import { Container, Service } from 'typedi';

import { isObjectsEqual } from '@des-kit/core';
import { BackgroundObservable, BackgroundPromise } from '@des-kit/extension-bg';
import { deferShareReplay } from '@des-kit/rxjs';
import { never } from '@des-kit/rxjs';

import { BoosterOrderApiService } from '../../pikabu-fixes-api/gen';
import { BoosterCandidateService } from './booster-candidate.service';
import { BoosterClientService } from './booster-client.service';
import {
    BoosterOrderStorageService,
    BoosterOrderStoredModel,
} from './booster-order-storage.service';

@Service()
export class BoosterOrderService {
    private readonly boosterCandidateService = Container.get(
        BoosterCandidateService,
    );
    private readonly boosterOrderStorageService = Container.get(
        BoosterOrderStorageService,
    );
    private readonly boosterOrderApiService = BoosterOrderApiService;
    private readonly clientService = Container.get(BoosterClientService);

    private readonly updatedBg$ = new Subject<void>();
    private readonly updateOrdersOnSubscribeBg$ = deferShareReplay(() =>
        this.periodicUpdateOrdersOnSubscribeBg(),
    );

    @BackgroundObservable('BoosterOrderService.getActualOrders')
    getActualOrders(): Observable<readonly BoosterOrderStoredModel[]> {
        return merge(this.updateOrdersOnSubscribeBg$, this.updatedBg$).pipe(
            startWith(null),
            switchMap(() => this.boosterOrderStorageService.getActualOrders()),
            distinctUntilChanged(isObjectsEqual),
        );
    }

    @BackgroundObservable('BoosterOrderService.getActualOrderOrNull')
    getActualOrderOrNull(
        storyId: number,
    ): Observable<BoosterOrderStoredModel | null> {
        return merge(this.updateOrdersOnSubscribeBg$, this.updatedBg$).pipe(
            startWith(null),
            switchMap(() =>
                this.boosterOrderStorageService.getActualOrdersByStoryId(
                    storyId,
                ),
            ),
            map((orders) => orders[0] ?? null),
            distinctUntilChanged(isObjectsEqual),
        );
    }

    @BackgroundPromise('BoosterOrderService.delete')
    async delete(orderUuid: string): Promise<void> {
        const clientUuid = await firstValueFrom(
            this.clientService.enabledUuid$,
        );

        if (!clientUuid) {
            return;
        }

        await this.boosterOrderApiService.boosterOrderControllerDeleteOrder(
            orderUuid,
            clientUuid,
        );

        await this.updateOrdersBg(clientUuid);

        await this.boosterCandidateService.markAsObsolete(orderUuid);
    }

    @BackgroundPromise('BoosterOrderService.deleteObsolete')
    async deleteObsolete(): Promise<void> {
        const clientUuid = await firstValueFrom(
            this.clientService.enabledUuid$,
        );

        if (!clientUuid) {
            return;
        }

        await this.boosterOrderApiService.boosterOrderControllerDeleteObsolete(
            clientUuid,
        );

        await this.updateOrdersBg(clientUuid);

        await this.boosterCandidateService.updateCandidatesBg(clientUuid);
    }

    @BackgroundPromise('BoosterOrderService.create')
    async create(
        storyId: number,
        points: number,
        minutes: number,
        pointsMultiplier: number,
    ): Promise<void> {
        const outDate = addMinutes(Date.now(), minutes);

        const clientUuid = await firstValueFrom(
            this.clientService.enabledUuid$,
        );

        if (!clientUuid) {
            return Promise.reject(`empty client uuid`);
        }

        await this.boosterOrderApiService.boosterOrderControllerCreateOrderV2(
            {
                storyId,
                votesLimit: points,
                outDate: outDate.toISOString(),
                pointsMultiplier,
            },
            clientUuid,
        );

        await this.updateOrdersBg(clientUuid);
    }

    @BackgroundObservable(
        'BoosterOrderService.periodicUpdateOrdersOnSubscribeBg',
    )
    private periodicUpdateOrdersOnSubscribeBg(): Observable<never> {
        return timer(0, 30000).pipe(
            switchMap(() => this.clientService.enabledUuid$),
            filter(Boolean),
            exhaustMap((clientUuid) => this.updateOrdersBg(clientUuid)),
            never(),
        );
    }

    private async updateOrdersBg(clientUuid: string): Promise<void> {
        try {
            const orders =
                await this.boosterOrderApiService.boosterOrderControllerGetOrders(
                    clientUuid,
                );

            await this.boosterOrderStorageService.setActualOrders(orders);

            this.updatedBg$.next();
        } catch (e) {}
    }
}
