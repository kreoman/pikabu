import { formatDistanceToNow, parseISO } from 'date-fns';
import { firstValueFrom, timer } from 'rxjs';
import { Container, Service } from 'typedi';

import { delay, isSet } from '@des-kit/core';
import { BackgroundPromise } from '@des-kit/extension-bg';
import {
    DefaultMappersConst,
    Serializer,
    TypeMapper,
} from '@des-kit/serializer';

import { stringifyError } from '../../../common/functions/stringify-error';
import { PikabuStoryApiService } from '../../pikabu-api/story/services/pikabu-story-api.service';
import {
    ApiError,
    BoosterRatingDto,
    BoosterTransactionAliveDto,
    BoosterTransactionApiService,
    BoosterTransactionDto,
} from '../../pikabu-fixes-api/gen';
import { BoosterTransactionError } from '../models/booster-transaction.error';
import { BoosterClientService } from './booster-client.service';

const BoosterTransactionErrorMapper: TypeMapper<BoosterTransactionError> = {
    type: BoosterTransactionError,
    prefix: 'BoosterTransactionError',
    parse: function (
        data: string,
        serializer: Serializer,
    ): BoosterTransactionError {
        return new BoosterTransactionError(data);
    },
    stringify: function (
        object: BoosterTransactionError,
        serializer: Serializer,
    ): string {
        return object.message;
    },
};

const MAPPERS = [BoosterTransactionErrorMapper, ...DefaultMappersConst];

@Service()
export class BoosterTransactionService {
    private readonly clientService = Container.get(BoosterClientService);

    async transactionClient(
        orderUuid: string,
        storyId: number,
        upvote: () => Promise<void>,
    ): Promise<void> {
        const clientUuid = await firstValueFrom(
            this.clientService.enabledUuid$,
        );

        if (!clientUuid) {
            throw new BoosterTransactionError(`Booster UUID is null`);
        }

        const transaction = await this.initTransactionBg(
            clientUuid,
            orderUuid,
            storyId,
        );

        try {
            await upvote();

            await this.confirmTransactionBg(
                clientUuid,
                transaction.uuid,
                storyId,
            );
        } catch (error) {
            void this.transactionErrorBg(clientUuid, transaction.uuid, error);

            throw error;
        }
    }

    @BackgroundPromise(
        'BoosterTransactionService.initTransactionBg',
        false,
        MAPPERS,
    )
    private async initTransactionBg(
        clientUuid: string,
        orderUuid: string,
        storyId: number,
        ttl = 5,
    ): Promise<BoosterTransactionDto> {
        for (let i = 0; i < ttl; i++) {
            try {
                const tryInitResult = await this.tryInitTransactionBg(
                    clientUuid,
                    orderUuid,
                    storyId,
                );

                if ('uuid' in tryInitResult) {
                    return tryInitResult;
                }

                const outDate = parseISO(tryInitResult.outDate);
                const distance = formatDistanceToNow(outDate);

                console.log(
                    `Ожидаем завершения другой транзакции (${distance})`,
                );

                await firstValueFrom(timer(outDate));
            } catch (error) {
                if (error instanceof ApiError) {
                    console.log(error.status, error.statusText, error.message);

                    // if (
                    //     error.status === 400 &&
                    //     error.message === 'Different rating'
                    // ) {
                    //     // continue;
                    // }

                    await delay(2000);
                    // break;
                }
            }
        }

        throw new BoosterTransactionError(`Не удалось инициировать транзакцию`);
    }

    @BackgroundPromise(
        'BoosterTransactionService.tryInitTransactionBg',
        false,
        MAPPERS,
    )
    private async tryInitTransactionBg(
        clientUuid: string,
        orderUuid: string,
        storyId: number,
    ): Promise<BoosterTransactionDto | BoosterTransactionAliveDto> {
        const rating = await this.getRealStoryRatingBg(storyId);
        return BoosterTransactionApiService.boosterTransactionControllerInit(
            orderUuid,
            storyId,
            rating,
            clientUuid,
        );
    }

    @BackgroundPromise(
        'BoosterTransactionService.confirmTransactionBg',
        false,
        MAPPERS,
    )
    private async confirmTransactionBg(
        clientUuid: string,
        transactionUuid: string,
        storyId: number,
    ): Promise<void> {
        const rating = await this.getRealStoryRatingBg(storyId);

        await BoosterTransactionApiService.boosterTransactionControllerConfirm(
            transactionUuid,
            rating,
            clientUuid,
        );
    }

    @BackgroundPromise(
        'BoosterTransactionService.transactionErrorBg',
        false,
        MAPPERS,
    )
    private async transactionErrorBg(
        clientUuid: string,
        transactionUuid: string,
        error: any,
    ): Promise<void> {
        let errorText: string;

        if (error instanceof BoosterTransactionError) {
            errorText = error.message;
        } else {
            errorText = stringifyError(error);
        }

        await BoosterTransactionApiService.boosterTransactionControllerError(
            transactionUuid,
            { errorText },
            clientUuid,
        );
    }

    private async getRealStoryRatingBg(
        storyId: number,
    ): Promise<BoosterRatingDto> {
        const storyMobileApiService = Container.get(PikabuStoryApiService);

        const response = await storyMobileApiService.fetchStoryPageWithoutCache(
            storyId,
            1,
        );

        const story = response.story;

        if (!isSet(story.story_pluses) || !isSet(story.story_minuses)) {
            throw new BoosterTransactionError(
                `Данные поста не содержат информацию о рейтинге`,
            );
        }

        return {
            pluses: story.story_pluses,
            minuses: story.story_minuses,
        };
    }
}
