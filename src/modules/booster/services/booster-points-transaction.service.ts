import {
    BehaviorSubject,
    exhaustMap,
    filter,
    firstValueFrom,
    merge,
    Observable,
    switchMap,
    timer,
} from 'rxjs';
import { Container, Service } from 'typedi';

import { BackgroundObservable } from '@des-kit/extension-bg';
import { deferShareReplay } from '@des-kit/rxjs';
import { never } from '@des-kit/rxjs';

import {
    BoosterPointsTransactionApiService,
    BoosterPointsTransactionDto,
} from '../../pikabu-fixes-api/gen';
import { BoosterClientService } from './booster-client.service';

@Service()
export class BoosterPointsTransactionService {
    private readonly boosterClientService = Container.get(BoosterClientService);
    private readonly boosterPointsTransactionApiService =
        BoosterPointsTransactionApiService;

    private readonly transactionsBg$ = new BehaviorSubject<
        readonly BoosterPointsTransactionDto[] | null
    >(null);

    private readonly periodicUpdateOnSubscribe$ = deferShareReplay(() =>
        this.periodicUpdateOnSubscribeBg(),
    );

    readonly transactions$: Observable<
        readonly BoosterPointsTransactionDto[] | null
    > = deferShareReplay(() => this.getTransactionsShare());

    @BackgroundObservable(
        'BoosterPointsTransactionService.getTransactionsShare',
    )
    private getTransactionsShare(): Observable<
        readonly BoosterPointsTransactionDto[] | null
    > {
        return merge(this.transactionsBg$, this.periodicUpdateOnSubscribe$);
    }

    @BackgroundObservable(
        'BoosterPointsTransactionService.periodicUpdateOnSubscribeBg',
    )
    private periodicUpdateOnSubscribeBg(): Observable<never> {
        return timer(0, 30000).pipe(
            switchMap(() => this.boosterClientService.enabledUuid$),
            filter(Boolean),
            exhaustMap(() => this.updateTransactionsBg()),
            never(),
        );
    }

    private async updateTransactionsBg(): Promise<void> {
        try {
            const uuid = await firstValueFrom(
                this.boosterClientService.enabledUuid$,
            );

            if (!uuid) {
                return;
            }

            const status =
                await this.boosterPointsTransactionApiService.boosterPointsTransactionControllerGetHistory(
                    uuid,
                );

            this.transactionsBg$.next(status);
        } catch (error) {}
    }
}
