import {
    BehaviorSubject,
    exhaustMap,
    filter,
    firstValueFrom,
    merge,
    Observable,
    switchMap,
    timer,
} from 'rxjs';
import { map } from 'rxjs/operators';
import { Container, Service } from 'typedi';

import { isDefined } from '@des-kit/core';
import {
    EXTENSION_CURRENT_RUNTIME_TYPE,
    ExtensionRuntimeTypeEnum,
} from '@des-kit/extension';
import { BackgroundObservable, BackgroundPromise } from '@des-kit/extension-bg';
import { deferShareReplay } from '@des-kit/rxjs';
import { never } from '@des-kit/rxjs';

import {
    BoosterClientApiService,
    BoosterClientStatusDto,
} from '../../pikabu-fixes-api/gen';
import { SettingsService } from '../../settings/settings.service';

@Service()
export class BoosterClientService {
    private readonly settingsService = Container.get(SettingsService);
    private readonly boosterClientApiService = BoosterClientApiService;

    private readonly statusBg$ =
        new BehaviorSubject<BoosterClientStatusDto | null>(null);

    private readonly periodicStatusUpdateOnSubscribe$ = deferShareReplay(() =>
        this.periodicStatusUpdateOnSubscribeBg(),
    );

    readonly enabledUuid$: Observable<string | null> =
        this.settingsService.settings$.pipe(
            map(
                (settings) =>
                    (settings.booster && settings.boosterUuid) || null,
            ),
        );

    readonly status$: Observable<BoosterClientStatusDto | null> =
        deferShareReplay(() => this.getStatusShare());

    constructor() {
        void this.firstInitializeBg();
    }

    @BackgroundPromise('BoosterClientService.logout')
    async logout(): Promise<void> {
        await this.settingsService.setKey('boosterUuid', null);

        await this.updateStatus();
    }

    @BackgroundPromise('BoosterClientService.generateUuid')
    async generateUuid(): Promise<void> {
        const response =
            await this.boosterClientApiService.boosterClientControllerRegisterClient();

        if (!response.uuid) {
            throw new Error(`uuid not received`);
        }

        await this.settingsService.setKey('boosterUuid', response.uuid);
        await this.updateStatus();
    }

    @BackgroundPromise('BoosterClientService.generateUuidAuto')
    async generateUuidAuto(instanceUuid: string): Promise<void> {
        const response =
            await this.boosterClientApiService.boosterClientControllerRegisterClientAuto(
                instanceUuid,
            );

        if (!response.uuid) {
            throw new Error(`uuid not received`);
        }

        await this.settingsService.setKey('boosterUuid', response.uuid);
        await this.updateStatus();
    }

    @BackgroundPromise('BoosterClientService.setUuid')
    async setUuid(uuid: string): Promise<boolean> {
        try {
            const response =
                await this.boosterClientApiService.boosterClientControllerGetStatus(
                    uuid,
                );

            const success = isDefined(response.points);

            await this.settingsService.setKey('boosterUuid', uuid);

            return success;
        } catch (e) {
            return false;
        }
    }

    async updateStatus(): Promise<void> {
        await this.updateStatusBg();
    }

    @BackgroundObservable('BoosterClientService.getStatusShare')
    private getStatusShare(): Observable<BoosterClientStatusDto | null> {
        return merge(this.statusBg$, this.periodicStatusUpdateOnSubscribe$);
    }

    private async firstInitializeBg() {
        if (
            EXTENSION_CURRENT_RUNTIME_TYPE !==
            ExtensionRuntimeTypeEnum.Background
        ) {
            return;
        }

        const settings = await this.settingsService.get();

        if (settings.boosterFirstInitialized) {
            return;
        }

        if (settings.boosterUuid) {
            await this.settingsService.setKey('boosterFirstInitialized', true);
            return;
        }

        await this.settingsService.patch({
            booster: true,
            boosterFirstInitialized: true,
        });

        await this.generateUuidAuto(settings.uuid);
    }

    @BackgroundObservable(
        'BoosterClientService.periodicStatusUpdateOnSubscribeBg',
    )
    private periodicStatusUpdateOnSubscribeBg(): Observable<never> {
        return timer(0, 30000).pipe(
            switchMap(() => this.enabledUuid$),
            filter(Boolean),
            exhaustMap(() => this.updateStatusBg()),
            never(),
        );
    }

    private async updateStatusBg(): Promise<void> {
        try {
            const enabledUuid = await firstValueFrom(this.enabledUuid$);

            if (!enabledUuid) {
                return;
            }

            const status =
                await this.boosterClientApiService.boosterClientControllerGetStatus(
                    enabledUuid,
                );

            this.statusBg$.next(status);
        } catch (e) {}
    }
}
