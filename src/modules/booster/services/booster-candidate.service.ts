import {
    combineLatest,
    distinctUntilChanged,
    exhaustMap,
    filter,
    firstValueFrom,
    merge,
    Observable,
    shareReplay,
    skip,
    startWith,
    Subject,
    switchMap,
    timeout,
    timer,
} from 'rxjs';
import { map } from 'rxjs/operators';
import { Container, Service } from 'typedi';

import { isObjectsEqual } from '@des-kit/core';
import { BackgroundObservable, BackgroundPromise } from '@des-kit/extension-bg';
import { deferShareReplay, never } from '@des-kit/rxjs';

import { stringifyError } from '../../../common/functions/stringify-error';
import { BoosterCandidateApiService } from '../../pikabu-fixes-api/gen';
import { BoosterTransactionError } from '../models/booster-transaction.error';
import {
    BoosterCandidateStorageService,
    BoosterCandidateStoredModel,
    BoosterCandidateUpdatableModel,
    CandidateUpvoteProgressEnum,
} from './booster-candidate-storage.service';
import { BoosterClientService } from './booster-client.service';
import { BoosterOrderStorageService } from './booster-order-storage.service';
import { BoosterTransactionService } from './booster-transaction.service';

@Service()
export class BoosterCandidateService {
    private readonly boosterOrderStorageService = Container.get(
        BoosterOrderStorageService,
    );
    private readonly boosterCandidateStorageService = Container.get(
        BoosterCandidateStorageService,
    );
    private readonly boosterCandidateApiService = BoosterCandidateApiService;
    private readonly clientService = Container.get(BoosterClientService);
    private readonly boosterTransactionService = Container.get(
        BoosterTransactionService,
    );

    private readonly updatedBg$ = new Subject<void>();
    private readonly updateOnSubscribeBg$ = deferShareReplay(() =>
        this.periodicUpdateOnSubscribeBg(),
    );

    readonly actualCandidatesMap$ = deferShareReplay(() =>
        this.getActualCandidatesMap(),
    );

    @BackgroundObservable('BoosterCandidateService.getCandidatesForUpvote')
    getCandidatesForUpvote(): Observable<
        readonly BoosterCandidateStoredModel[]
    > {
        return merge(this.updateOnSubscribeBg$, this.updatedBg$).pipe(
            startWith(null),
            switchMap(() =>
                combineLatest({
                    candidates:
                        this.boosterCandidateStorageService.getCandidatesForUpvote(),
                    orders: this.boosterOrderStorageService.getActiveOrders(),
                }),
            ),
            map(({ candidates, orders }) => {
                const orderUuids = new Set(orders.map((order) => order.uuid));

                return (
                    candidates.filter(
                        (candidate) => !orderUuids.has(candidate.orderUuid),
                    ) ?? null
                );
            }),
            distinctUntilChanged(isObjectsEqual),
        );
    }

    listenActualCandidate(storyId: number) {
        return this.actualCandidatesMap$.pipe(
            map((candidatesMap) => candidatesMap.get(storyId) ?? null),
            distinctUntilChanged(isObjectsEqual),
            shareReplay({
                bufferSize: 1,
                refCount: true,
            }),
        );
    }

    @BackgroundObservable('BoosterCandidateService.findCandidate')
    findCandidate(
        storyId: number,
    ): Observable<BoosterCandidateStoredModel | null> {
        return merge(this.updateOnSubscribeBg$, this.updatedBg$).pipe(
            startWith(null),
            switchMap(() =>
                this.boosterCandidateStorageService.getCandidateByStoryId(
                    storyId,
                ),
            ),
            distinctUntilChanged(isObjectsEqual),
        );
    }

    async markAsObsolete(orderUuid: string): Promise<void> {
        await this.updateCandidateBg(orderUuid, {
            obsolete: 1,
        });
    }

    async markAsVoted(orderUuid: string): Promise<void> {
        await this.updateCandidateBg(orderUuid, {
            voted: 1,
        });
    }

    async markAsRejected(orderUuid: string): Promise<void> {
        await this.updateCandidateBg(orderUuid, {
            rejected: 1,
        });
    }

    async upvote(
        orderUuid: string,
        storyId: number,
        upvote: () => Promise<void>,
    ): Promise<void> {
        const candidate$ = this.findCandidate(storyId).pipe(
            shareReplay({
                bufferSize: 1,
                refCount: true,
            }),
        );

        const upvoteInner = async () => {
            await this.updateCandidateBg(orderUuid, {
                progress: CandidateUpvoteProgressEnum.Upvoting,
            });

            await upvote();

            await this.updateCandidateBg(orderUuid, {
                progress: CandidateUpvoteProgressEnum.TransactionCompleting,
            });
        };

        try {
            const updatedPromise = firstValueFrom(
                candidate$.pipe(skip(1), timeout(5000)),
            );
            await firstValueFrom(candidate$);

            await this.updateCandidateBg(orderUuid, {
                progress: CandidateUpvoteProgressEnum.Init,
            });

            await updatedPromise;

            await this.updateCandidateBg(orderUuid, {
                progress: CandidateUpvoteProgressEnum.TransactionStart,
            });

            await this.boosterTransactionService.transactionClient(
                orderUuid,
                storyId,
                upvoteInner,
            );

            await this.updateCandidateBg(orderUuid, {
                progress: 0,
                success: 1,
            });
        } catch (error) {
            let errorReason: string;

            if (error instanceof BoosterTransactionError) {
                errorReason = error.message;
            } else {
                errorReason = stringifyError(error);
            }

            console.log(errorReason);

            await this.updateCandidateBg(orderUuid, {
                progress: CandidateUpvoteProgressEnum.None,
                error: 1,
                errorReason: errorReason,
            });
        }
    }

    private getActualCandidatesMap(): Observable<
        ReadonlyMap<number, BoosterCandidateStoredModel>
    > {
        return this.getActualCandidatesBg().pipe(
            map(
                (candidates) =>
                    new Map(candidates.map((c) => [c.initialStoryId, c])),
            ),
        );
    }

    @BackgroundObservable('BoosterCandidateService.getActualCandidatesBg')
    private getActualCandidatesBg(): Observable<
        readonly BoosterCandidateStoredModel[]
    > {
        return merge(this.updateOnSubscribeBg$, this.updatedBg$).pipe(
            startWith(null),
            switchMap(() =>
                this.boosterCandidateStorageService.getActualCandidates(),
            ),
            distinctUntilChanged(isObjectsEqual),
        );
    }

    @BackgroundObservable('BoosterCandidateService.periodicUpdateOnSubscribeBg')
    private periodicUpdateOnSubscribeBg(): Observable<never> {
        return timer(0, 30000).pipe(
            switchMap(() => this.clientService.enabledUuid$),
            filter(Boolean),
            exhaustMap((clientUuid) => this.updateCandidatesBg(clientUuid)),
            never(),
        );
    }

    @BackgroundPromise('BoosterCandidateService.updateCandidatesBg')
    async updateCandidatesBg(clientUuid: string): Promise<void> {
        try {
            const candidates =
                await this.boosterCandidateApiService.boosterCandidateControllerGetCandidatesV2(
                    clientUuid,
                );

            await this.boosterCandidateStorageService.setActualCandidates(
                candidates,
            );

            this.updatedBg$.next();
        } catch (error) {}
    }

    @BackgroundPromise('BoosterCandidateService.updateCandidateBg')
    private async updateCandidateBg(
        orderUuid: string,
        update: Partial<BoosterCandidateUpdatableModel>,
    ): Promise<void> {
        await this.boosterCandidateStorageService.updateCandidate(
            orderUuid,
            update,
        );

        this.updatedBg$.next();
    }
}
