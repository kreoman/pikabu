import { Connection, ITable, IWhereQuery } from 'jsstore';
import { IDataBase } from 'jsstore/dist/ts/common';
import workerInjector from 'jsstore/dist/worker_injector';
import { Service } from 'typedi';

import {
    EXTENSION_CURRENT_RUNTIME_TYPE,
    ExtensionRuntimeTypeEnum,
} from '@des-kit/extension';

import { BoosterCandidateV2Dto } from '../../pikabu-fixes-api/gen';

export enum CandidateUpvoteProgressEnum {
    None = 0,
    Init,
    TransactionStart,
    Upvoting,
    TransactionCompleting,
}

export interface BoosterCandidateUpdatableModel {
    obsolete: number; // 0/1
    progress: CandidateUpvoteProgressEnum;
    success: number; // 0/1
    error: number; // 0/1
    errorReason: string | null;
    voted: number; // 0/1 - рейтинг изменен до Booster`a
    rejected: number; // 0/1 - отклонено пользователем
}

export interface BoosterCandidateStoredModel
    extends BoosterCandidateUpdatableModel {
    orderUuid: string;
    outDate: string;
    initialStoryId: number;
    pointsMultiplier: number;
}

const CANDIDATE_TABLE: ITable = {
    name: 'candidate',
    columns: {
        orderUuid: {
            primaryKey: true,
            dataType: 'string',
            unique: true,
            notNull: true,
        },
        hash: { dataType: 'string', notNull: true },
        lastDigits: { dataType: 'number', notNull: true },
        outDate: { dataType: 'string', notNull: true },
        initialStoryId: { dataType: 'number', default: 0, notNull: true },
        foundedStoryId: { dataType: 'number', default: 0, notNull: true },
        obsolete: { dataType: 'number', notNull: true, default: 0 },
        progress: { dataType: 'number', notNull: true, default: 0 },
        success: { dataType: 'number', notNull: true, default: 0 },
        error: { dataType: 'number', notNull: true, default: 0 },
    },
    alter: {
        6: {
            add: {
                voted: { dataType: 'number', notNull: true, default: 0 },
            },
        },
        7: {
            add: {
                rejected: { dataType: 'number', notNull: true, default: 0 },
            },
        },
        8: {
            add: {
                errorReason: {
                    dataType: 'string',
                    notNull: false,
                    default: null,
                },
            },
        },
        9: {
            drop: {
                hash: {},
                lastDigits: {},
                foundedStoryId: {},
            },
            add: {
                pointsMultiplier: {
                    dataType: 'number',
                    notNull: true,
                    default: 1,
                },
            },
        },
    },
};

const DATABASE: IDataBase = {
    version: 9,
    name: 'des--booster-candidate',
    tables: [CANDIDATE_TABLE],
};

@Service()
export class BoosterCandidateStorageService {
    private _db?: Promise<Connection>;

    private get db(): Promise<Connection> {
        if (!this._db) {
            this._db = this.openDB();
        }

        return this._db;
    }

    async setActualCandidates(
        candidateDtos: readonly BoosterCandidateV2Dto[],
    ): Promise<void> {
        const uuids = candidateDtos.map(
            (candidateDto) => candidateDto.orderUuid,
        );

        await this.markObsoleteExcept(uuids);

        const insertData = candidateDtos.map((candidateDto) =>
            BoosterCandidateStorageService.toInsertStoredModel(candidateDto),
        );

        await this.insertCandidates(insertData);
    }

    async updateCandidate(
        orderUuid: string,
        update: Partial<BoosterCandidateUpdatableModel>,
    ): Promise<void> {
        const db = await this.db;

        await db.update({
            in: 'candidate',
            where: {
                orderUuid: orderUuid,
            },
            set: update,
        });
    }

    async getCandidateByStoryId(
        storyId: number,
    ): Promise<BoosterCandidateStoredModel | null> {
        const db = await this.db;

        const strictFound = await db.select<BoosterCandidateStoredModel>({
            from: 'candidate',
            where: {
                initialStoryId: storyId,
            },
            limit: 1,
        });

        if (strictFound.length) {
            return strictFound[0];
        }

        return null;
    }

    async getActualCandidates(
        limit?: number,
    ): Promise<BoosterCandidateStoredModel[]> {
        const db = await this.db;

        return db.select<BoosterCandidateStoredModel>({
            from: 'candidate',
            where: {
                obsolete: 0,
                rejected: 0,
            },
            limit,
        });
    }

    async getCandidatesForUpvote(
        limit?: number,
    ): Promise<BoosterCandidateStoredModel[]> {
        const db = await this.db;

        return db.select<BoosterCandidateStoredModel>({
            from: 'candidate',
            where: {
                obsolete: 0,
                success: 0,
                error: 0,
                voted: 0,
                rejected: 0,
            },
            limit,
        });
    }

    private async openDB(): Promise<Connection> {
        if (
            EXTENSION_CURRENT_RUNTIME_TYPE !==
            ExtensionRuntimeTypeEnum.Background
        ) {
            throw new Error(`wrong runtime`);
        }

        const connection = new Connection();

        connection.addPlugin(workerInjector);

        await connection.initDb(DATABASE);

        return connection;
    }

    private static toInsertStoredModel(
        dto: BoosterCandidateV2Dto,
    ): BoosterCandidateStoredModel {
        return {
            orderUuid: dto.orderUuid,
            pointsMultiplier: dto.pointsMultiplier,
            outDate: dto.outDate,
            initialStoryId: dto.storyId,
            error: 0,
            progress: 0,
            success: 0,
            obsolete: 0,
            voted: 0,
            rejected: 0,
            errorReason: null,
        };
    }

    private async markObsoleteExcept(
        orderUuids: readonly string[],
    ): Promise<void> {
        const db = await this.db;

        let obsoleteQueryWhere: IWhereQuery[] | IWhereQuery;

        if (orderUuids.length > 0) {
            obsoleteQueryWhere = orderUuids.map((uuid) => ({
                obsolete: 0,
                orderUuid: {
                    '!=': uuid,
                },
            }));
        } else {
            obsoleteQueryWhere = {
                obsolete: 0,
            };
        }

        await db.update({
            in: 'candidate',
            where: obsoleteQueryWhere,
            set: {
                obsolete: 1,
            },
        });
    }

    private async insertCandidates(
        insertData: BoosterCandidateStoredModel[],
    ): Promise<void> {
        if (!insertData.length) {
            return;
        }

        const db = await this.db;

        await db.insert({
            into: 'candidate',
            values: insertData,
            ignore: true,
        });
    }
}
