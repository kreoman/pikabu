export interface PikabuMobileUserApiResponseModel {
    readonly user_id: string;
    readonly user_name: string;
    readonly avatar: string;
    readonly rating: number;
    readonly subscribers_count: number;
    readonly remember: string;
    readonly is_add_comment_photo_ban: boolean;
    readonly min_rating_to_add_comment_photo: number;
}
