export interface PikabuMobileUserSettingsApiSetRequestModel {
    auto_expand_stories: boolean; // true
    auto_play_story_gif: boolean; // false
    min_rating_stories: number; // -15;
    show_adult_stories: boolean; // true
    show_horror_stories: boolean; // false
    tags_fold_mode: number; // 2
    user_id: number; // 7447432
}
