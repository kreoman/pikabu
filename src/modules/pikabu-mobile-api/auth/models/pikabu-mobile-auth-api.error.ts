import { PikabuMobileApiError } from '../../common/models/pikabu-mobile-api.error';

export class PikabuMobileAuthApiError extends PikabuMobileApiError {}
