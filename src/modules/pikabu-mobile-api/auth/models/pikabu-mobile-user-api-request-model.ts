export interface PikabuMobileUserApiRequestModel {
    readonly user_name: string;
    readonly password: string;
}
