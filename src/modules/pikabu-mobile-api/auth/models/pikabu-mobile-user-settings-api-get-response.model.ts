export interface PikabuMobileUserSettingsApiGetResponseModel {
    email: string; //
    phone: string; // +7********66
    phone_is_change_soon: boolean; // false
    gender: number; // 0
    min_rating_comments: -4;
    min_rating_stories: -15;
    community_min_rating: number; // 0
    stories_on_page: number; // 8
    auto_expand_stories: boolean; // true
    auto_play_story_gif: boolean; // false
    auto_play_comment_gif: boolean; // false
    show_adult_stories: boolean; // true
    show_adult_switcher: boolean; // false
    show_horror_stories: boolean; // false
    hide_large_comments_branches: boolean; // false
    max_comments_branch_depth: number; // 0
    show_scroll_top_button: boolean; // true
    is_empty_password: boolean; // false
    oauth: unknown[];
    subs_rating_filter: boolean; // true
    hide_visited_stories: boolean; // true
    about: string; // \u0420\u0430\u0441\u0448\u0438\u0440\u0435\u043d\u0438\u0435 \u0434\u043b\u044f \u043e\u0442\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043c\u0438\u043d\u0443\u0441\u043e\u0432: https://chromewebstore.google.com/detail/nochogffendelpmbpajbdpaodbibgdmk\n\u041a\u0430\u043d\u0430\u043b \u0440\u0430\u0441\u0448\u0438\u0440\u0435\u043d\u0438\u044f: https://t.me/pikabu_fixes
    show_api_user_guide: boolean; // true
    tags_fold_mode: number; // 2
    top_list_min_rating: number; // 32144
    is_disable_ads_allowed: boolean; // false
    is_ads_disabled: boolean; // false
    default_com_view: number; // 3
    is_username_change_available: boolean; // false
}
