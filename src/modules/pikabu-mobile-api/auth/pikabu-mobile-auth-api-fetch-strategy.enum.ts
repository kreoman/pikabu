export enum PikabuMobileAuthApiFetchStrategyEnum {
    OnlyAuthOrFail = 'OnlyAuthOrFail',
    CommonThenAuth = 'CommonThenAuth',
    AuthThenCommon = 'AuthThenCommon',
}
