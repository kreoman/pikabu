import { combineLatest, firstValueFrom, map } from 'rxjs';
import { Container, Service } from 'typedi';

import { BackgroundPromise } from '@des-kit/extension-bg';

import { SettingsService } from '../../../settings/settings.service';
import { PikabuMobileApiErrorCodeEnum } from '../../common/enums/pikabu-mobile-api-error-code.enum';
import { PikabuMobileApiError } from '../../common/models/pikabu-mobile-api.error';
import { PikabuMobileApiRequestModel } from '../../common/models/pikabu-mobile-api-request.model';
import { PikabuMobileApiService } from '../../common/pikabu-mobile-api.service';
import { PikabuMobileAuthApiError } from '../models/pikabu-mobile-auth-api.error';
import { PikabuMobileUserApiRequestModel } from '../models/pikabu-mobile-user-api-request-model';
import { PikabuMobileUserApiResponseModel } from '../models/pikabu-mobile-user-api-response-model';
import { PikabuMobileUserSettingsApiGetResponseModel } from '../models/pikabu-mobile-user-settings-api-get-response.model';
import { PikabuMobileUserSettingsApiSetRequestModel } from '../models/pikabu-mobile-user-settings-api-set-request.model';
import { PikabuMobileAuthApiFetchStrategyEnum } from '../pikabu-mobile-auth-api-fetch-strategy.enum';

@Service()
export class PikabuMobileAuthApiService {
    private readonly settingsService = Container.get(SettingsService);
    private readonly mobileApiAuth$ =
        this.settingsService.getKey$('mobileApiAuth');
    private readonly mobileApiUserId$ =
        this.settingsService.getKey$('mobileApiUserId');

    private readonly pikabuMobileApiService = Container.get(
        PikabuMobileApiService,
    );

    readonly authEnabled$ = combineLatest([
        this.mobileApiAuth$,
        this.mobileApiUserId$,
    ]).pipe(
        map(
            ([mobileApiAuth, mobileApiUserId]) =>
                mobileApiAuth && !!mobileApiUserId,
        ),
    );

    @BackgroundPromise('PikabuMobileAuthApiService.fetchData')
    async fetchData<T>(
        request: PikabuMobileApiRequestModel,
        strategy = PikabuMobileAuthApiFetchStrategyEnum.OnlyAuthOrFail,
    ): Promise<T> {
        switch (strategy) {
            case PikabuMobileAuthApiFetchStrategyEnum.OnlyAuthOrFail: {
                return this.fetchDataWithAuthOrFail(request);
            }
            case PikabuMobileAuthApiFetchStrategyEnum.AuthThenCommon: {
                try {
                    return await this.fetchDataWithAuthOrFail(request);
                } catch (e) {
                    return this.pikabuMobileApiService.fetchData(request);
                }
            }
            case PikabuMobileAuthApiFetchStrategyEnum.CommonThenAuth: {
                try {
                    return await this.pikabuMobileApiService.fetchData(request);
                } catch (e) {
                    if (e instanceof PikabuMobileApiError) {
                        if (
                            e.pikabuErrorCode ===
                            PikabuMobileApiErrorCodeEnum.TooManyRequests
                        ) {
                            throw e;
                        }
                    }

                    return this.fetchDataWithAuthOrFail(request);
                }
            }
        }
    }

    @BackgroundPromise('PikabuMobileAuthApiService.fetchDataWithAuthOrFail')
    private async fetchDataWithAuthOrFail<T>(
        request: PikabuMobileApiRequestModel,
    ): Promise<T> {
        const mobileApiAuth = await firstValueFrom(this.mobileApiAuth$);
        const userId = await firstValueFrom(this.mobileApiUserId$);

        if (!mobileApiAuth || !userId) {
            throw new PikabuMobileAuthApiError(`Нет данных для авторизации`);
        }

        try {
            return await this.pikabuMobileApiService.fetchData({
                ...request,
                body: {
                    ...request.body,
                    user_id: userId,
                },
            });
        } catch (e) {
            if (e instanceof PikabuMobileApiError) {
                if (
                    e.pikabuErrorCode ===
                    PikabuMobileApiErrorCodeEnum.Unauthorized
                ) {
                    console.log(`pikabuErrorCode === 401`, e.message);
                    await this.logout();
                }
            }

            throw e;
        }
    }

    @BackgroundPromise('PikabuMobileAuthApiService.auth')
    async auth(
        username: string,
        password: string,
    ): Promise<PikabuMobileUserApiResponseModel> {
        console.log(`auth`);

        const auth =
            await this.pikabuMobileApiService.fetchData<PikabuMobileUserApiResponseModel>(
                {
                    controller: 'user.auth',
                    body: {
                        user_name: username,
                        password,
                    } as PikabuMobileUserApiRequestModel,
                },
            );

        const userId = Number(auth.user_id);

        console.log(auth);

        await this.settingsService.patch({
            mobileApiUserId: userId,
            mobileApiUserName: auth.user_name,
        });

        return auth;
    }

    @BackgroundPromise('PikabuMobileAuthApiService.logout')
    async logout(): Promise<void> {
        console.log(`logout`);

        await this.settingsService.patch({
            mobileApiUserId: null,
            mobileApiUserName: null,
        });
    }

    @BackgroundPromise('PikabuMobileAuthApiService.fetchSettings')
    private async fetchSettings(
        userId: string,
    ): Promise<PikabuMobileUserSettingsApiGetResponseModel> {
        return this.pikabuMobileApiService.fetchData<PikabuMobileUserSettingsApiGetResponseModel>(
            {
                controller: 'settings.get',
                body: {
                    user_id: `${userId}`,
                },
            },
        );
    }

    @BackgroundPromise('PikabuMobileAuthApiService.putSettings')
    private async putSettings(
        request: PikabuMobileUserSettingsApiSetRequestModel,
    ): Promise<PikabuMobileUserSettingsApiGetResponseModel> {
        return this.pikabuMobileApiService.fetchData<PikabuMobileUserSettingsApiGetResponseModel>(
            {
                controller: 'settings.set',
                body: request,
            },
        );
    }
}
