import { firstValueFrom } from 'rxjs';
import { Md5 } from 'ts-md5';
import { Container, Service } from 'typedi';

import { HttpError, HttpService } from '@des-kit/extension-bg-http';

import { PikabuMobileApiError } from './models/pikabu-mobile-api.error';
import { PikabuMobileApiRequestModel } from './models/pikabu-mobile-api-request.model';
import { PikabuMobileApiResponseModel } from './models/pikabu-mobile-api-response.model';
import { PikabuMobileApiStatusService } from './pikabu-mobile-api-status.service';

const API_KEY = 'kmq4!2cPl)peZ';

@Service()
export class PikabuMobileApiService {
    private readonly httpService = Container.get(HttpService);
    private readonly pikabuMobileApiStatusService = Container.get(
        PikabuMobileApiStatusService,
    );

    async fetchData<T>(request: PikabuMobileApiRequestModel): Promise<T> {
        const ms = Date.now();

        const requestData = {
            new_sort: 1,
            ...request.body,
        };

        const hash = this.getHash(
            {
                ...request,
                body: requestData,
            },
            ms,
        );

        const fullRequest = {
            ...requestData,
            id: 'iws',
            hash,
            token: ms,
        };

        const query = request.queries ? `?${request.queries}` : '';

        try {
            const responseJson = await firstValueFrom(
                this.httpService.postJson<PikabuMobileApiResponseModel<T>>(
                    `https://api.pikabu.ru/v1/${request.controller}${query}`,
                    fullRequest,
                    {
                        mode: 'same-origin',
                    },
                ),
            );

            if (responseJson.error) {
                const pikabuMobileApiError =
                    PikabuMobileApiError.fromResponseError(responseJson);

                await this.pikabuMobileApiStatusService.error(
                    pikabuMobileApiError,
                );

                return Promise.reject(pikabuMobileApiError);
            }

            await this.pikabuMobileApiStatusService.success();

            return responseJson.response!;
        } catch (error) {
            if (error instanceof HttpError) {
                const pikabuMobileApiError =
                    PikabuMobileApiError.fromHttpError(error);

                await this.pikabuMobileApiStatusService.error(
                    pikabuMobileApiError,
                );

                return Promise.reject(pikabuMobileApiError);
            }

            return Promise.reject(error);
        }
    }

    private getHash(request: PikabuMobileApiRequestModel, ms: number): string {
        const join = Object.values(request.body).sort().join(',');
        const toHash = [API_KEY, request.controller, ms, join].join(',');
        const hashed = Md5.hashStr(toHash);
        return btoa(hashed);
    }
}
