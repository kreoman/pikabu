import { PikabuMobileApiErrorCodeEnum } from '../enums/pikabu-mobile-api-error-code.enum';

export interface PikabuMobileApiResponseErrorModel {
    readonly message: string;
    readonly message_code: PikabuMobileApiErrorCodeEnum;
}
