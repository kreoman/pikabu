import { HttpError } from '@des-kit/extension-bg-http';

import { PikabuMobileApiErrorCodeEnum } from '../enums/pikabu-mobile-api-error-code.enum';
import { PikabuMobileApiResponseModel } from './pikabu-mobile-api-response.model';

export class PikabuMobileApiError extends Error {
    constructor(
        message: string,
        readonly pikabuErrorCode?: PikabuMobileApiErrorCodeEnum,
    ) {
        super(message);
    }

    static fromResponseError(
        response: PikabuMobileApiResponseModel<any>,
    ): PikabuMobileApiError {
        return new PikabuMobileApiError(
            `${response.error?.message}`,
            response.error?.message_code,
        );
    }

    static fromHttpError(error: HttpError): PikabuMobileApiError {
        switch (error.statusCode) {
            case 429:
                return new PikabuMobileApiError(
                    `Слишком много запросов`,
                    PikabuMobileApiErrorCodeEnum.TooManyRequests,
                );
        }

        return new PikabuMobileApiError(
            `Запрос к серверу завершился ошибкой (${error})`,
        );
    }
}
