export interface PikabuMobileApiRequestModel {
    readonly controller: string;
    readonly queries?: string;
    readonly body: Record<string, any>;
}
