import { PikabuMobileApiResponseErrorModel } from './pikabu-mobile-api-response-error.model';

export interface PikabuMobileApiResponseModel<TResponse> {
    readonly response?: TResponse;
    readonly error?: PikabuMobileApiResponseErrorModel;
}
