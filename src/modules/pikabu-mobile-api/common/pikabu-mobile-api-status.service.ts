import {
    BehaviorSubject,
    debounceTime,
    distinctUntilChanged,
    Observable,
} from 'rxjs';
import { Service } from 'typedi';

import { BackgroundObservable, BackgroundPromise } from '@des-kit/extension-bg';
import { deferShareReplay } from '@des-kit/rxjs';

import { PikabuMobileApiErrorCodeEnum } from './enums/pikabu-mobile-api-error-code.enum';
import { PikabuMobileApiError } from './models/pikabu-mobile-api.error';

export enum PikabuMobileApiWarningEnum {
    TooManyRequests = 'tooManyRequests',
}

@Service()
export class PikabuMobileApiStatusService {
    private readonly warningBg$ =
        new BehaviorSubject<PikabuMobileApiWarningEnum | null>(null);

    readonly warning$ = deferShareReplay(() => this.listenWarning());

    @BackgroundPromise('PikabuMobileApiStatusService.error')
    async error(error: PikabuMobileApiError): Promise<void> {
        switch (error.pikabuErrorCode) {
            case PikabuMobileApiErrorCodeEnum.TooManyRequests:
                this.warningBg$.next(
                    PikabuMobileApiWarningEnum.TooManyRequests,
                );
                break;
        }
    }

    @BackgroundPromise('PikabuMobileApiStatusService.success')
    async success(): Promise<void> {
        this.warningBg$.next(null);
    }

    @BackgroundObservable('PikabuMobileApiStatusService.listenWarning')
    private listenWarning(): Observable<PikabuMobileApiWarningEnum | null> {
        return this.warningBg$.pipe(distinctUntilChanged(), debounceTime(500));
    }
}
