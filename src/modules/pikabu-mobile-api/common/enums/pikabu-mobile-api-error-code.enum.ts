export enum PikabuMobileApiErrorCodeEnum {
    AuthenticatedTemporaryError = 20,
    AdultContent = 47,
    Unauthorized = 401,
    TooManyRequests = 429,
}
