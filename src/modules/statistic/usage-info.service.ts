import { firstValueFrom } from 'rxjs';
import { Container, Service } from 'typedi';
import uaParser from 'ua-parser-js';

import { ExtensionService } from '@des-kit/extension';
import { BackgroundPromise } from '@des-kit/extension-bg';

import {
    ExtensionEnvironmentDto,
    ExtensionInstanceDto,
    ExtensionPikabuUserDto,
} from '../pikabu-fixes-api/gen';
import { SettingsService } from '../settings/settings.service';

@Service()
export class UsageInfoService {
    private readonly extensionService = Container.get(ExtensionService);

    pikabuUserNickname: string | null = null;

    constructor(private readonly settingsService: SettingsService) {}

    @BackgroundPromise('UsageInfoService.setPikabuUserNickname')
    async setPikabuUserNickname(nickname: string | null) {
        this.pikabuUserNickname = nickname;
    }

    @BackgroundPromise('UsageInfoService.getPikabuUser')
    async getPikabuUser(): Promise<ExtensionPikabuUserDto> {
        return {
            nickname: this.pikabuUserNickname,
        };
    }

    @BackgroundPromise('UsageInfoService.getInstance')
    async getInstance(): Promise<ExtensionInstanceDto> {
        return {
            uuid: await firstValueFrom(this.settingsService.getKey$('uuid')),
        };
    }

    @BackgroundPromise('UsageInfoService.getEnvironment')
    async getEnvironment(): Promise<ExtensionEnvironmentDto> {
        const manifest = this.extensionService.runtime?.getManifest();
        const extensionInfo = await this.extensionService.management?.getSelf();
        const userAgentInfo = uaParser(navigator.userAgent);

        return {
            installType: extensionInfo?.installType ?? 'other',
            manifestVersion: manifest?.manifest_version ?? 0,
            extensionVersion: extensionInfo?.version ?? '-',
            browserName: userAgentInfo.browser.name ?? '',
            browserVersion: userAgentInfo.browser.version ?? '',
            cpuArch: userAgentInfo.cpu.architecture ?? '',
            osName: userAgentInfo.os.name ?? '',
            osVersion: userAgentInfo.os.version ?? '',
        };
    }
}
