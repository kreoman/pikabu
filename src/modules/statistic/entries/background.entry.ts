import { Container } from 'typedi';

import { StatisticSenderService } from '../statistic-sender.service';
import { UsageErrorService } from '../usage-error.service';
import { UsageEventService } from '../usage-event.service';
import { UsageInfoService } from '../usage-info.service';

Container.get(StatisticSenderService);
Container.get(UsageErrorService);
Container.get(UsageEventService);
Container.get(UsageInfoService);
