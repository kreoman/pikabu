import { config } from 'rxjs';
import { Container } from 'typedi';

import { UsageErrorService } from '../usage-error.service';

config.onUnhandledError = (err) => {
    const usageErrorService = Container.get(UsageErrorService);

    usageErrorService.onUnhandledError(err);
};
