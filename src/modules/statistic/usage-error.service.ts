import { catchError, EMPTY, ObservableInput } from 'rxjs';
import { ObservedValueOf, OperatorFunction } from 'rxjs';
import { Service } from 'typedi';

import {
    EXTENSION_CURRENT_RUNTIME_TYPE,
    ExtensionRuntimeTypeEnum,
} from '@des-kit/extension';
import { BackgroundPromise } from '@des-kit/extension-bg';

import { stringifyError } from '../../common/functions/stringify-error';
import { UsageEventService } from './usage-event.service';

@Service()
export class UsageErrorService {
    constructor(private readonly usageEventService: UsageEventService) {}

    onUnhandledError(error: any): void {
        console.error(error);

        this.saveError(error);
    }

    catchError<T, O extends ObservableInput<any>>(
        value$?: O,
    ): OperatorFunction<T, T | ObservedValueOf<O>> {
        return catchError((error) => {
            this.saveError(error);

            return value$ ?? EMPTY;
        });
    }

    saveError(error: any): void {
        const runtime = EXTENSION_CURRENT_RUNTIME_TYPE;
        const errorStr = stringifyError(error);

        void this.saveErrorBg(errorStr, runtime);
    }

    @BackgroundPromise('UsageErrorService.saveErrorBg')
    private async saveErrorBg(
        errorStr: string,
        runtime: ExtensionRuntimeTypeEnum,
    ) {
        void this.parseAndSaveErrorBg(errorStr, runtime);
    }

    private async parseAndSaveErrorBg(
        errorStr: string,
        runtime: ExtensionRuntimeTypeEnum,
    ): Promise<void> {
        this.usageEventService.register('error', {
            errorStr,
            runtime,
        });
    }
}
