import {
    defer,
    exhaustMap,
    firstValueFrom,
    Observable,
    retry,
    timer,
} from 'rxjs';
import { Service } from 'typedi';

import { IntervalMsEnum } from '@des-kit/core';
import { BackgroundObservable } from '@des-kit/extension-bg';
import { HttpService } from '@des-kit/extension-bg-http';
import { gzip } from '@des-kit/gzip';
import { deferShareReplay } from '@des-kit/rxjs';
import { never } from '@des-kit/rxjs';

import { ExtensionUsageDto, OpenAPI } from '../pikabu-fixes-api/gen';
import { UsageErrorService } from './usage-error.service';
import { UsageEventService } from './usage-event.service';
import { UsageInfoService } from './usage-info.service';

@Service()
export class StatisticSenderService {
    readonly workOnSubscribe$ = deferShareReplay(() =>
        this.workOnSubscribeBg(),
    );

    constructor(
        private readonly httpService: HttpService,
        private readonly usageErrorService: UsageErrorService,
        private readonly usageEventService: UsageEventService,
        private readonly usageInfoService: UsageInfoService,
    ) {}

    @BackgroundObservable('StatisticSenderService.workOnSubscribeBg')
    private workOnSubscribeBg(): Observable<never> {
        const firstSendDelayMs = IntervalMsEnum.Minute;
        const intervalMs = IntervalMsEnum.Minute * 5;

        return timer(firstSendDelayMs, intervalMs).pipe(
            exhaustMap(() => this.tickBg()),
            this.usageErrorService.catchError(),
            never(),
        );
    }

    private async tickBg(): Promise<void> {
        const data: ExtensionUsageDto = {
            pikabuUser: await this.usageInfoService.getPikabuUser(),
            instance: await this.usageInfoService.getInstance(),
            environment: await this.usageInfoService.getEnvironment(),
            metrics: [],
            events: await this.usageEventService.shift(1000),
        };

        const send$ = defer(() => this.sendBg(data)).pipe(
            retry({
                count: 10,
                delay: (error, retryCount) => {
                    const multiplier = (retryCount + 1) ^ 3;

                    const delayMs = IntervalMsEnum.Second * 10 * multiplier;

                    return timer(delayMs);
                },
            }),
        );

        await firstValueFrom(send$);
    }

    private async sendBg(data: ExtensionUsageDto): Promise<void> {
        let body: string | Uint8Array = JSON.stringify(data);

        const headers: Record<string, string> = {
            'Content-Type': 'application/json',
        };

        headers['Content-Encoding'] = 'gzip';
        body = await gzip(body);

        await firstValueFrom(
            this.httpService.request({
                url: `${OpenAPI.BASE}/api/extension-usage`,
                method: 'POST',
                body,
                headers,
            }),
        );
    }
}
