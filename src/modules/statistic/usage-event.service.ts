import { DBSchema, IDBPDatabase, openDB } from 'idb';
import { Service } from 'typedi';

import {
    EXTENSION_CURRENT_RUNTIME_TYPE,
    ExtensionRuntimeTypeEnum,
} from '@des-kit/extension';
import { BackgroundPromise } from '@des-kit/extension-bg';

import { ExtensionEventDto } from '../pikabu-fixes-api/gen';
import { SettingsService } from '../settings/settings.service';

interface UsageDB extends DBSchema {
    event: {
        key: number;
        value: ExtensionEventDto;
    };
}

@Service()
export class UsageEventService {
    private _db?: Promise<IDBPDatabase<UsageDB>>;
    private enabled: boolean | null = null;

    private get db(): Promise<IDBPDatabase<UsageDB>> {
        if (!this._db) {
            this._db = this.openDb();
        }

        return this._db;
    }

    constructor(private readonly settingsService: SettingsService) {
        const inBg =
            EXTENSION_CURRENT_RUNTIME_TYPE ===
            ExtensionRuntimeTypeEnum.Background;

        this.settingsService
            .getKey$('sendStatistic')
            .subscribe((sendStatistic) => {
                this.enabled = sendStatistic;

                if (!sendStatistic && inBg) {
                    void this.cleanBg();
                }
            });
    }

    @BackgroundPromise('UsageEventService.shift')
    async shift(limit: number): Promise<ExtensionEventDto[]> {
        const db = await this.db;
        const eventKeys = await db.getAllKeys('event', null, limit);

        if (!eventKeys.length) {
            return [];
        }

        const lastKey = eventKeys[eventKeys.length - 1];
        const keyRange = IDBKeyRange.upperBound(lastKey);

        const events = await db.getAll('event', keyRange);

        await db.delete('event', keyRange);

        return events;
    }

    register<E extends Omit<ExtensionEventDto, 'timestamp'>, K extends keyof E>(
        event: K,
        payload: E[K],
    ): void {
        if (this.enabled === false) {
            return;
        }

        const timestamp = Math.ceil(Date.now() / 1000);

        const eventDto: ExtensionEventDto = {
            timestamp,
            [event]: payload,
        };

        void this.saveEvent(eventDto);
    }

    @BackgroundPromise('UsageEventService.saveEvent')
    private async saveEvent(event: ExtensionEventDto): Promise<void> {
        const db = await this.db;

        await db.put('event', event);
    }

    private openDb(): Promise<IDBPDatabase<UsageDB>> {
        if (
            EXTENSION_CURRENT_RUNTIME_TYPE !==
            ExtensionRuntimeTypeEnum.Background
        ) {
            throw new Error(`wrong runtime`);
        }

        return openDB<UsageDB>(`des--usage`, 1, {
            upgrade(database: IDBPDatabase<UsageDB>) {
                database.createObjectStore('event', {
                    autoIncrement: true,
                });
            },
        });
    }

    private async cleanBg(): Promise<void> {
        const db = await this.db;

        await db.clear('event');
    }
}
