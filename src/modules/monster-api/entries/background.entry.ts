import { Container } from 'typedi';

import { MonsterApiService } from '../monster-api.service';

Container.get(MonsterApiService);
