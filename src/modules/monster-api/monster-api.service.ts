import { firstValueFrom } from 'rxjs';
import { Container, Service } from 'typedi';

import { Cache } from '@des-kit/cache';
import { IntervalMsEnum } from '@des-kit/core';
import { ExtensionRuntimeTypeEnum } from '@des-kit/extension';
import { BackgroundPromise } from '@des-kit/extension-bg';
import { HttpService } from '@des-kit/extension-bg-http';

import { MonsterUserSummaryModel } from './models/monster-user-summary.model';
import { MonsterUserTagModel } from './models/monster-user-tag.model';

@Service()
export class MonsterApiService {
    private readonly httpService = Container.get(HttpService);
    private readonly root = 'https://isla-de-muerta.com/';

    @Cache(IntervalMsEnum.Hour, ExtensionRuntimeTypeEnum.PageScript)
    @Cache(IntervalMsEnum.Day, ExtensionRuntimeTypeEnum.Background)
    @BackgroundPromise('MonsterApiService.getUserSummaryOrFail')
    async getUserSummaryOrFail(
        username: string,
    ): Promise<MonsterUserSummaryModel> {
        const usernameMonster = username.replaceAll('.', '_');

        const userId = await this.fetchUserIdBg(usernameMonster);

        return {
            host: this.root,
            username: usernameMonster,
            postsTags: await this.fetchTagsBg(userId, 'PostTags'),
            commentsTags: await this.fetchTagsBg(userId, 'CommentTags'),
        };
    }

    @Cache(IntervalMsEnum.Week, ExtensionRuntimeTypeEnum.Background)
    @BackgroundPromise('MonsterApiService.fetchUserIdBg')
    private async fetchUserIdBg(username: string): Promise<number> {
        const url = `${this.root}user/${username}-summary`;

        const html = await firstValueFrom(
            this.httpService.fetchText({
                method: 'GET',
                url,
            }),
        );

        const regexp = new RegExp('/api/(\\d+)-');

        const result = regexp.exec(html);

        if (result) {
            return Number(result[1]);
        } else {
            return Promise.reject();
        }
    }

    @Cache(IntervalMsEnum.Day)
    private async fetchTagsBg(
        userId: number,
        key: string,
    ): Promise<MonsterUserTagModel[]> {
        const url = `${this.root}api/${userId}-${key}`;

        return firstValueFrom(
            this.httpService.fetchJson({
                method: 'GET',
                url,
            }),
        );
    }
}
