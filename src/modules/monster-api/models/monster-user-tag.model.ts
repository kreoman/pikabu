export interface MonsterUserTagModel {
    Count: number;
    Loc: string;
    Tag: string;
    TagRU: string;
}
