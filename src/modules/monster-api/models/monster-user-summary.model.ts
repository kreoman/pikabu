import { MonsterUserTagModel } from './monster-user-tag.model';

export interface MonsterUserSummaryModel {
    readonly host: string;
    readonly username: string;
    readonly postsTags: MonsterUserTagModel[];
    readonly commentsTags: MonsterUserTagModel[];
}
