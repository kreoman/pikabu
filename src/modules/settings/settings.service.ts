import { distinctUntilChanged, filter, firstValueFrom, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Service } from 'typedi';
import { v4 } from 'uuid';

import { isObjectsEqual } from '@des-kit/core';
import { isSet } from '@des-kit/core';
import { BackgroundObservable, BackgroundPromise } from '@des-kit/extension-bg';
import { StorageService } from '@des-kit/extension-bg-storage';
import { deferShareReplay } from '@des-kit/rxjs';
import { selectKey } from '@des-kit/rxjs';

import { DefaultSettingsConst } from './consts/default-settings.const';
import { SettingsModel } from './models/settings.model';

const SettingsStorageKeyConst = 'pikabu-fixes.settings';

@Service()
export class SettingsService {
    readonly settings$ = deferShareReplay(() => this.getSettingsWithCreate());

    constructor(private readonly storageService: StorageService) {}

    getKey$<K extends keyof SettingsModel>(
        key: K,
    ): Observable<SettingsModel[K]> {
        return this.settings$.pipe(selectKey(key));
    }

    async get(): Promise<SettingsModel> {
        return firstValueFrom(this.settings$);
    }

    @BackgroundPromise('SettingsService.set')
    async set(settings: SettingsModel): Promise<void> {
        await this.storageService.set(SettingsStorageKeyConst, settings);
    }

    @BackgroundPromise('SettingsService.patch')
    async patch(
        partial: Partial<Omit<SettingsModel, 'uuid' | 'version'>>,
    ): Promise<void> {
        const settingsInitial = await this.get();

        const settingsUpdated = {
            ...settingsInitial,
            ...partial,
        };

        if (isObjectsEqual(settingsInitial, settingsUpdated)) {
            return;
        }

        await this.set(settingsUpdated);
    }

    @BackgroundPromise('SettingsService.setKey')
    async setKey<K extends keyof SettingsModel>(
        key: K,
        value: SettingsModel[K],
    ): Promise<void> {
        let settings = await this.get();

        if (value === settings[key]) {
            return;
        }

        settings = {
            ...settings,
            [key]: value,
        };

        await this.set(settings);
    }

    @BackgroundObservable('SettingsService.getSettingsWithCreate')
    private getSettingsWithCreate(): Observable<SettingsModel> {
        const settingsOrNull$ = this.storageService.listen<SettingsModel>(
            SettingsStorageKeyConst,
        );

        return settingsOrNull$.pipe(
            tap((settings) => this.checkSettings(settings)),
            filter(isSet),
            distinctUntilChanged(isObjectsEqual),
        );
    }

    private async checkSettings(settings: SettingsModel | null) {
        if (
            !settings ||
            !settings.version ||
            settings.version < DefaultSettingsConst.version
        ) {
            settings = {
                ...DefaultSettingsConst,
                ...settings,
                version: DefaultSettingsConst.version,
                uuid: settings?.uuid ?? v4(),
            };

            await this.set(settings);
        }
    }
}
