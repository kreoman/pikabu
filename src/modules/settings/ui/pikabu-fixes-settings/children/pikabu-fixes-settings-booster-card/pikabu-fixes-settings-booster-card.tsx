import { Card } from '@des-kit/card';
import { Column } from '@des-kit/column';
import { isDefined } from '@des-kit/core';
import { Fade } from '@des-kit/fade';
import { useObservable } from '@des-kit/hooks';
import { SettingsItemWithToggle } from '@des-kit/settings-item-with-toggle';
import { useState } from 'react';
import { Container } from 'typedi';

import { ExtensionNameConst } from '../../../../../../common/consts/extension-name.const';
import BoosterArrowsIcon from '../../../../../../icons/booster-arrows.svg';
import { BoosterClientService } from '../../../../../booster/services/booster-client.service';
import { AboutWindow } from '../../../../../booster/ui/about-window/about-window';
import { SettingsService } from '../../../../settings.service';
import { PikabuFixesSettingsBoosterNoKey } from './children/pikabu-fixes-settings-booster-no-key/pikabu-fixes-settings-booster-no-key';
import { PikabuFixesSettingsBoosterWithKey } from './children/pikabu-fixes-settings-booster-with-key/pikabu-fixes-settings-booster-with-key';

export function PikabuFixesSettingsBoosterCard() {
    const boosterClientService = Container.get(BoosterClientService);
    const settingsService = Container.get(SettingsService);
    const [settings] = useObservable(() => settingsService.settings$, null, [
        settingsService,
    ]);
    const [uuid] = useObservable(
        () => boosterClientService.enabledUuid$,
        undefined,
    );
    const [infoWindowShown, setInfoWindowShown] = useState(false);

    if (!settings || !isDefined(uuid)) {
        return null;
    }

    return (
        <>
            <Card
                iconSrc={BoosterArrowsIcon}
                title="Booster">
                <SettingsItemWithToggle
                    title="Включить"
                    description={
                        <>
                            Booster позволяет делиться рейтингом постов между
                            пользователями расширения {ExtensionNameConst}.{' '}
                            <a onClick={() => setInfoWindowShown(true)}>
                                Подробнее
                            </a>
                        </>
                    }
                    toggleProps={{
                        value: settings.booster,
                        onChange: (value) =>
                            settingsService.setKey('booster', value),
                    }}
                />
                <Fade show={settings.booster}>
                    <Column>
                        {uuid && <PikabuFixesSettingsBoosterWithKey />}
                        {!uuid && <PikabuFixesSettingsBoosterNoKey />}
                    </Column>
                </Fade>
            </Card>
            {infoWindowShown && (
                <AboutWindow onClose={() => setInfoWindowShown(false)} />
            )}
        </>
    );
}
