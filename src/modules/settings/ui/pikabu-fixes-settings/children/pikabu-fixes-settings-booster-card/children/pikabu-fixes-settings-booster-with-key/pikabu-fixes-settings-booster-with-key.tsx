import './pikabu-fixes-settings-booster-with-key.scss';

import { Button, ButtonAppearanceEnum } from '@des-kit/button';
import { Dialog } from '@des-kit/dialog';
import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Input } from '@des-kit/input';
import { Island } from '@des-kit/island';
import { Row } from '@des-kit/row';
import { SizeEnum } from '@des-kit/styles';
import { useState } from 'react';
import { Container } from 'typedi';

import { ScrollableWindow } from '../../../../../../../../common/components/scrollable-window/scrollable-window';
import { WrapTooltip } from '../../../../../../../../common/components/wrap-tooltip/wrap-tooltip';
import CopyIcon from '../../../../../../../../icons/copy.svg';
import { PointsPluralize } from '../../../../../../../booster/pluralize/points.pluralize';
import { BoosterClientService } from '../../../../../../../booster/services/booster-client.service';
import { SettingsService } from '../../../../../../settings.service';

export function PikabuFixesSettingsBoosterWithKey() {
    const boosterClientService = Container.get(BoosterClientService);
    const settingsService = Container.get(SettingsService);
    const [settings] = useObservable(() => settingsService.settings$, null, [
        settingsService,
    ]);
    const [status] = useObservable(() => boosterClientService.status$, null);
    const [enabledUuid] = useObservable(
        () => boosterClientService.enabledUuid$,
        null,
    );
    const [logoutDialogShown, setLogoutDialogShown] = useState(false);
    const [copyKeyWindowShown, setCopyKeyWindowShown] = useState(false);

    if (!status || !settings || !enabledUuid) {
        return null;
    }

    function onCopyClick() {
        if (!enabledUuid) {
            return;
        }

        void navigator.clipboard.writeText(enabledUuid);
    }

    function onConfirmLogout() {
        void boosterClientService.logout();
    }

    return (
        <>
            <Row>
                <Button
                    appearance={ButtonAppearanceEnum.Secondary}
                    onClick={() => setCopyKeyWindowShown(true)}>
                    Показать ключ
                </Button>
                <Button
                    appearance={ButtonAppearanceEnum.Secondary}
                    onClick={() => setLogoutDialogShown(true)}>
                    Удалить ключ
                </Button>
            </Row>
            {logoutDialogShown && (
                <Dialog
                    title={'Точно удалить?'}
                    resolveButtonChildren={'Да, удалить'}
                    onResolve={onConfirmLogout}
                    onReject={() => setLogoutDialogShown(false)}>
                    <Island>
                        <p>
                            На аккаунте под этим ключом имеется {status.points}
                            &nbsp;
                            {PointsPluralize(status.points)}.
                        </p>
                        <p>
                            Убедитесь в том, что ключ сохранён, иначе баллы
                            восстановить не удастся.
                        </p>
                    </Island>
                </Dialog>
            )}
            {copyKeyWindowShown && (
                <ScrollableWindow
                    title={'Приватный ключ Booster'}
                    onClose={() => setCopyKeyWindowShown(false)}>
                    <Island>
                        <Input
                            className={
                                'des--pikabu-fixes-settings-booster-with-key__key-input'
                            }
                            styleOverride={{ size: SizeEnum.Small }}
                            value={enabledUuid}
                            withClearButton={false}
                            disabled={false}
                            innerRightNode={
                                <WrapTooltip tooltip="Ключ будет скопирован в буфер обмена">
                                    <Icon
                                        src={CopyIcon}
                                        onClick={onCopyClick}
                                    />
                                </WrapTooltip>
                            }
                        />
                        <p>
                            Важно помнить, что передача ключа в чужие руки
                            необратима. Код ключа изменить нельзя.
                        </p>
                    </Island>
                </ScrollableWindow>
            )}
        </>
    );
}
