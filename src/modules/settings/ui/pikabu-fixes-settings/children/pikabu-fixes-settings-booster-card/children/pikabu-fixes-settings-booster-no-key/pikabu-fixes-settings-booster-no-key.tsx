import { Button } from '@des-kit/button';
import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Input } from '@des-kit/input';
import { Row } from '@des-kit/row';
import { Waiter } from '@des-kit/waiter';
import { useState } from 'react';
import { Container } from 'typedi';

import CircleCheckIcon from '../../../../../../../../icons/circle-check.svg';
import { BoosterClientService } from '../../../../../../../booster/services/booster-client.service';
import { SettingsService } from '../../../../../../settings.service';

export function PikabuFixesSettingsBoosterNoKey() {
    const boosterClientService = Container.get(BoosterClientService);
    const settingsService = Container.get(SettingsService);
    const [settings] = useObservable(() => settingsService.settings$, null, [
        settingsService,
    ]);
    const [enabledUuid] = useObservable(
        () => boosterClientService.enabledUuid$,
        null,
    );
    const [key, setKey] = useState<string>();
    const [applying, setApplying] = useState(false);
    const [creating, setCreating] = useState(false);

    const inProgress = creating || applying;

    if (enabledUuid || !settings) {
        return null;
    }

    async function onCreateKeyClick() {
        if (inProgress) {
            return;
        }

        setCreating(true);
        try {
            await boosterClientService.generateUuid();
        } finally {
            setCreating(false);
        }
    }

    async function onApplyKeyClick() {
        if (!key || inProgress) {
            return;
        }

        setApplying(true);
        try {
            await boosterClientService.setUuid(key);
        } finally {
            setApplying(false);
        }
    }

    function onKeyChange(value: string) {
        setKey(value);
    }

    return (
        <>
            <Button
                disabled={inProgress}
                onClick={onCreateKeyClick}>
                {creating ? <Waiter /> : 'Создать новый ключ'}
            </Button>
            <Row>
                <Input
                    disabled={inProgress}
                    placeholder={'Существующий ключ'}
                    onChange={onKeyChange}
                />
                <Button
                    disabled={!key || inProgress}
                    onClick={onApplyKeyClick}>
                    {applying ? <Waiter /> : <Icon src={CircleCheckIcon} />}
                </Button>
            </Row>
        </>
    );
}
