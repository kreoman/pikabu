import { Card } from '@des-kit/card';
import { useObservable } from '@des-kit/hooks';
import { SettingsItemWithToggle } from '@des-kit/settings-item-with-toggle';
import { Container } from 'typedi';

import ExportIcon from '../../../../../../icons/export.svg';
import { SettingsService } from '../../../../settings.service';

export function PikabuFixesSettingsExportCard() {
    const settingsService = Container.get(SettingsService);
    const [settings] = useObservable(() => settingsService.settings$, null);

    if (!settings) {
        return null;
    }

    return (
        <Card
            iconSrc={ExportIcon}
            title={'Импорт и экспорт данных'}>
            <SettingsItemWithToggle
                title={'Заметки пользователей'}
                description={
                    <>
                        Добавляет блок импорта и экспорта на странице{' '}
                        <a
                            href="/notes/"
                            target="_blank">
                            заметок
                        </a>
                    </>
                }
                toggleProps={{
                    value: settings.userNotesTool,
                    onChange: (value) =>
                        settingsService.setKey('userNotesTool', value),
                }}
            />
        </Card>
    );
}
