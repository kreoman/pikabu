import { Card } from '@des-kit/card';
import { useObservable } from '@des-kit/hooks';
import { SettingsItemWithToggle } from '@des-kit/settings-item-with-toggle';
import { Container } from 'typedi';

import GemIcon from '../../../../../../icons/gem.svg';
import { SettingsService } from '../../../../settings.service';

export function PikabuFixesSettingsCosmeticCard() {
    const settingsService = Container.get(SettingsService);
    const [settings] = useObservable(() => settingsService.settings$, null);

    if (!settings) {
        return null;
    }

    return (
        <Card
            iconSrc={GemIcon}
            title={'Косметические изменения'}>
            <SettingsItemWithToggle
                title={'Убрать дизайн коротких постов'}
                toggleProps={{
                    value: settings.hideShortStoryDesign,
                    onChange: (value) =>
                        settingsService.setKey('hideShortStoryDesign', value),
                }}
            />
            <SettingsItemWithToggle
                title={'Скрыть кнопку и блок доната'}
                toggleProps={{
                    value: settings.hideDonateButton,
                    onChange: (value) =>
                        settingsService.setKey('hideDonateButton', value),
                }}
            />
            <SettingsItemWithToggle
                title={'Отображать ссылки на скачивание видео'}
                description={'Для встроенного плеера Пикабу'}
                toggleProps={{
                    value: settings.showVideoLinks,
                    onChange: (value) =>
                        settingsService.setKey('showVideoLinks', value),
                }}
            />
        </Card>
    );
}
