import { Card } from '@des-kit/card';
import { useObservable } from '@des-kit/hooks';
import { SettingsItemWithToggle } from '@des-kit/settings-item-with-toggle';
import { Container } from 'typedi';

import ChartIcon from '../../../../../../icons/chart.svg';
import { SettingsService } from '../../../../settings.service';

export function PikabuFixesSettingsStatisticCard() {
    const settingsService = Container.get(SettingsService);
    const [settings] = useObservable(() => settingsService.settings$, null);

    if (!settings) {
        return null;
    }

    return (
        <Card
            iconSrc={ChartIcon}
            title={'Статистика'}>
            <SettingsItemWithToggle
                title={'Разрешить сбор статистики использования'}
                description={
                    'Это необходимо для приоритизации нового функционала. Персональные данные пользователя не собираются'
                }
                toggleProps={{
                    value: settings.sendStatistic,
                    onChange: (value) =>
                        settingsService.setKey('sendStatistic', value),
                }}
            />
        </Card>
    );
}
