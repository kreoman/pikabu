import { Container } from 'typedi';

import { Card } from '@des-kit/card';
import { useObservable } from '@des-kit/hooks';
import { SettingsItemWithToggle } from '@des-kit/settings-item-with-toggle';

import UsersIcon from '../../../../../../icons/users.svg';
import { SettingsService } from '../../../../settings.service';

export function PikabuFixesSettingsUsersCard() {
    const settingsService = Container.get(SettingsService);
    const [settings] = useObservable(() => settingsService.settings$, null);

    if (!settings) {
        return null;
    }
    return (
        <Card
            iconSrc={UsersIcon}
            title={'Пользователи'}>
            <SettingsItemWithToggle
                title={'Теги пользователя около ника'}
                description={
                    'Сохранение и отображение тегов пользователя. Данные хранятся в заметках на пикабу'
                }
                toggleProps={{
                    value: settings.showUserNoteTags,
                    onChange: (value) =>
                        settingsService.setKey('showUserNoteTags', value),
                }}
            />
            <SettingsItemWithToggle
                title={'Индикатор срока регистрации пользователя'}
                description={
                    'Отображается в виде цветной точки справа от никнейма'
                }
                toggleProps={{
                    value: settings.showUserRegistrationDateIndicator,
                    onChange: (value) =>
                        settingsService.setKey(
                            'showUserRegistrationDateIndicator',
                            value,
                        ),
                }}
            />
            <SettingsItemWithToggle
                title={'Числовое значение отрицательного рейтинга'}
                description={
                    'Вместо красной шкалы будет загружен и отображен реальный рейтинг пользователя'
                }
                toggleProps={{
                    value: settings.showUserRating,
                    onChange: (value) =>
                        settingsService.setKey('showUserRating', value),
                }}
            />
            <SettingsItemWithToggle
                title={'Топ тегов пользователя'}
                description={
                    <>
                        Отображается во всплывающем окне при наведении на
                        никнейм. Источник: pikabu.monster (gollum.space,
                        isla-de-muerta.com)
                    </>
                }
                toggleProps={{
                    value: settings.showUserSummary,
                    onChange: (value) =>
                        settingsService.setKey('showUserSummary', value),
                }}
            />
        </Card>
    );
}
