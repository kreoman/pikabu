import { Card } from '@des-kit/card';
import { useObservable } from '@des-kit/hooks';
import { SettingsItemWithToggle } from '@des-kit/settings-item-with-toggle';
import { Container } from 'typedi';

import BanIcon from '../../../../../../icons/ban.svg';
import { SettingsService } from '../../../../settings.service';

export function PikabuFixesSettingsIgnoreCard() {
    const settingsService = Container.get(SettingsService);
    const [settings] = useObservable(() => settingsService.settings$, null);

    if (!settings) {
        return null;
    }

    return (
        <Card
            iconSrc={BanIcon}
            title={'Игнор-лист'}>
            <SettingsItemWithToggle
                title={'Кнопка быстрого игнора автора в тулбаре поста'}
                toggleProps={{
                    value: settings.addForeverIgnore,
                    onChange: (value: boolean) =>
                        settingsService.setKey('addForeverIgnore', value),
                }}
            />
        </Card>
    );
}
