import { Container } from 'typedi';

import { Button, ButtonAppearanceEnum } from '@des-kit/button';
import { Card } from '@des-kit/card';
import { NavigationService } from '@des-kit/extension-bg-navigation';
import { useObservable } from '@des-kit/hooks';
import { Icon } from '@des-kit/icon';
import { Row } from '@des-kit/row';

import { ExtensionLinksConst } from '../../../../../../common/consts/extension-links.const';
import LinkIcon from '../../../../../../icons/link.svg';
import TelegramIcon from '../../../../../../icons/telegram.svg';
import { SettingsService } from '../../../../settings.service';

export function PikabuFixesSettingsContactsCard() {
    const navigationService = Container.get(NavigationService);

    function onTelegramChannelClick() {
        void navigationService.openInNewTab(ExtensionLinksConst.TelegramGroup);
    }

    function onTelegramChatClick() {
        void navigationService.openInNewTab(ExtensionLinksConst.TelegramChat);
    }

    const settingsService = Container.get(SettingsService);
    const [settings] = useObservable(() => settingsService.settings$, null);

    if (!settings) {
        return null;
    }

    return (
        <Card
            iconSrc={LinkIcon}
            title={'Контакты'}>
            <Row>
                <Button
                    appearance={ButtonAppearanceEnum.Secondary}
                    onClick={onTelegramChannelClick}>
                    <Icon src={TelegramIcon} />
                    <span>Телеграм - канал</span>
                </Button>
                <Button
                    appearance={ButtonAppearanceEnum.Secondary}
                    onClick={onTelegramChatClick}>
                    <Icon src={TelegramIcon} />
                    <span>Телеграм - чат</span>
                </Button>
            </Row>
        </Card>
    );
}
