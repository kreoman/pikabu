import { Card } from '@des-kit/card';
import { useObservable } from '@des-kit/hooks';
import { SettingsItemWithToggle } from '@des-kit/settings-item-with-toggle';
import { Container } from 'typedi';

import PlusMinusIcon from '../../../../../../icons/plus-minus.svg';
import { SettingsService } from '../../../../settings.service';

export function PikabuFixesSettingsMinusesCard() {
    const settingsService = Container.get(SettingsService);
    const [settings] = useObservable(() => settingsService.settings$, null);

    if (!settings) {
        return null;
    }

    let comment =
        'Запрос минусов будет происходить через мобильное API без авторизации (будут недоступны данные для постов с NSFW, 18+ и т.д.)';

    if (settings.mobileApiAuth) {
        if (settings.mobileApiUserName) {
            comment = `Успешно авторизован как ${settings.mobileApiUserName}`;
        } else {
            comment = `Для авторизации в мобильном API необходимо повторно авторизоваться на сайте с использованием пары логин/пароль`;
        }
    }

    return (
        <Card
            iconSrc={PlusMinusIcon}
            title={'Загрузка и отображение минусов'}>
            <SettingsItemWithToggle
                title={'Авторизация через мобильное API'}
                description={
                    'Использовать пару логин/пароль при входе в учетную запись для авторизации в мобильном API. Без авторизации рейтинг постов и комментариев будет отображен только для публичных постов (без NSFW, 18+ и т.д.)'
                }
                toggleProps={{
                    value: settings.mobileApiAuth,
                    onChange: (value) =>
                        settingsService.setKey('mobileApiAuth', value),
                }}
            />
            <SettingsItemWithToggle
                title={'Для постов'}
                description={comment}
                toggleProps={{
                    value: settings.showStoryVotes,
                    onChange: (value) =>
                        settingsService.setKey('showStoryVotes', value),
                }}
            />
            <SettingsItemWithToggle
                title={'Для комментариев'}
                description={comment}
                toggleProps={{
                    value: settings.showCommentVotes,
                    onChange: (value) =>
                        settingsService.setKey('showCommentVotes', value),
                }}
            />
        </Card>
    );
}
