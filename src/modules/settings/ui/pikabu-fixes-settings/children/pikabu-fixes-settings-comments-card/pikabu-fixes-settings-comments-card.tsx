import { Card } from '@des-kit/card';
import { useObservable } from '@des-kit/hooks';
import { SettingsItemWithToggle } from '@des-kit/settings-item-with-toggle';
import { Container } from 'typedi';

import CommentsIcon from '../../../../../../icons/comments.svg';
import { SettingsService } from '../../../../settings.service';

export function PikabuFixesSettingsCommentsCard() {
    const settingsService = Container.get(SettingsService);
    const [settings] = useObservable(() => settingsService.settings$, null);

    if (!settings) {
        return null;
    }

    return (
        <Card
            iconSrc={CommentsIcon}
            title={'Комментарии'}>
            <SettingsItemWithToggle
                title={
                    'Подсвечивать ник автора комментариев в цвет поставленных оценок'
                }
                description={
                    'Ник будет подсвечен в том случае, если все оценки комментариев одинаковые'
                }
                toggleProps={{
                    value: settings.highlightVotedComments,
                    onChange: (value) =>
                        settingsService.setKey('highlightVotedComments', value),
                }}
            />
            <SettingsItemWithToggle
                title={'Кнопка раскрытия всех комментариев'}
                description={
                    'Отображается под оригинальной кнопкой раскрытия части комментариев'
                }
                toggleProps={{
                    value: settings.showCommentsExpanderButton,
                    onChange: (value) =>
                        settingsService.setKey(
                            'showCommentsExpanderButton',
                            value,
                        ),
                }}
            />
            <SettingsItemWithToggle
                title={'Автоматически раскрывать комментарии'}
                toggleProps={{
                    value: settings.expandCommentsAuto,
                    onChange: (value) =>
                        settingsService.setKey('expandCommentsAuto', value),
                }}
            />
        </Card>
    );
}
