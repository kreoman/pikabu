import { Column } from '@des-kit/column';

import { PikabuFixesSettingsBoosterCard } from './children/pikabu-fixes-settings-booster-card/pikabu-fixes-settings-booster-card';
import { PikabuFixesSettingsCommentsCard } from './children/pikabu-fixes-settings-comments-card/pikabu-fixes-settings-comments-card';
import { PikabuFixesSettingsContactsCard } from './children/pikabu-fixes-settings-contacts-card/pikabu-fixes-settings-contacts-card';
import { PikabuFixesSettingsCosmeticCard } from './children/pikabu-fixes-settings-cosmetic-card/pikabu-fixes-settings-cosmetic-card';
import { PikabuFixesSettingsExportCard } from './children/pikabu-fixes-settings-export-card/pikabu-fixes-settings-export-card';
import { PikabuFixesSettingsIgnoreCard } from './children/pikabu-fixes-settings-ignore-card/pikabu-fixes-settings-ignore-card';
import { PikabuFixesSettingsMinusesCard } from './children/pikabu-fixes-settings-minuses-card/pikabu-fixes-settings-minuses-card';
import { PikabuFixesSettingsStatisticCard } from './children/pikabu-fixes-settings-statistic-card/pikabu-fixes-settings-statistic-card';
import { PikabuFixesSettingsUsersCard } from './children/pikabu-fixes-settings-users-card/pikabu-fixes-settings-users-card';

export function PikabuFixesSettings() {
    return (
        <Column>
            <PikabuFixesSettingsBoosterCard />
            <PikabuFixesSettingsMinusesCard />
            <PikabuFixesSettingsUsersCard />
            <PikabuFixesSettingsCommentsCard />
            <PikabuFixesSettingsIgnoreCard />
            <PikabuFixesSettingsCosmeticCard />
            <PikabuFixesSettingsExportCard />
            <PikabuFixesSettingsStatisticCard />
            <PikabuFixesSettingsContactsCard />
        </Column>
    );
}
