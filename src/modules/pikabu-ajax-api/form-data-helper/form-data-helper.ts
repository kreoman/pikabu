export abstract class FormDataHelper {
    static createFromObject(record: Record<string, any>): FormData {
        let needToModify = false;

        for (const [key, value] of Object.entries(record)) {
            if (typeof value === 'string' || value instanceof Blob) {
                continue;
            }

            needToModify = true;
        }

        if (!needToModify) {
            return FormDataHelper.createFormData(record);
        }

        const prepared: Record<string, string | Blob> = {};

        for (const [key, value] of Object.entries(record)) {
            if (value instanceof Blob) {
                prepared[key] = value;
                continue;
            }

            prepared[key] = String(value);
        }

        return FormDataHelper.createFormData(prepared);
    }

    static createFormData(record: Record<string, string | Blob>): FormData {
        const fd = new FormData();

        for (const [key, value] of Object.entries(record)) {
            if (value instanceof File) {
                fd.set(key, value, value.name);
                continue;
            }

            fd.set(key, value);
        }

        return fd;
    }
}
