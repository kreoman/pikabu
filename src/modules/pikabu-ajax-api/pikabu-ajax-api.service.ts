import { Observable, of, switchMap, throwError } from 'rxjs';
import { Service } from 'typedi';

import { HttpService } from '@des-kit/extension-bg-http';

import { FormDataHelper } from './form-data-helper/form-data-helper';
import { PikabuAjaxActionRequestModel } from './models/pikabu-ajax-action-request.model';
import { PikabuAjaxActionResponseModel } from './models/pikabu-ajax-action-response.model';
import { PikabuAjaxResponseError } from './models/pikabu-ajax-response-error';

const URL_PREFIX = 'https://pikabu.ru/ajax/';

@Service()
export class PikabuAjaxApiService {
    constructor(private readonly httpService: HttpService) {}

    ajax<T, TPayload extends PikabuAjaxActionRequestModel>(
        action: string,
        payload: TPayload,
    ): Observable<T> {
        return this.ajaxWithFails<T, TPayload>(action, payload).pipe(
            switchMap((result) => {
                if (!result.result) {
                    return throwError(
                        () => new PikabuAjaxResponseError(result.message),
                    );
                }

                return of(result.data);
            }),
        );
    }

    private ajaxWithFails<T, TPayload extends PikabuAjaxActionRequestModel>(
        action: string,
        payload: TPayload,
    ): Observable<PikabuAjaxActionResponseModel<T>> {
        const url = `${URL_PREFIX}${action}`;

        const fd = FormDataHelper.createFromObject(payload);

        return this.httpService.fetchJson<PikabuAjaxActionResponseModel<T>>({
            url,
            method: 'POST',
            body: fd,
            mode: 'no-cors',
        });
    }
}
