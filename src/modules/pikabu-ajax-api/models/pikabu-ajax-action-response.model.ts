export interface PikabuAjaxActionResponseModel<T> {
    readonly data: T;
    readonly message: string; // '';
    readonly message_code: number; // 0;
    readonly result: boolean;
}
