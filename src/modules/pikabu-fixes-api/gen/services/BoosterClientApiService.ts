/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BoosterClientRegisterResponseDto } from '../models/BoosterClientRegisterResponseDto';
import type { BoosterClientStatusDto } from '../models/BoosterClientStatusDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class BoosterClientApiService {

    /**
     * @param xPfClientUuid 
     * @returns BoosterClientRegisterResponseDto 
     * @throws ApiError
     */
    public static boosterClientControllerRegisterClient(
xPfClientUuid?: string,
): CancelablePromise<BoosterClientRegisterResponseDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/booster-client/register',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

    /**
     * @param uuid 
     * @param xPfClientUuid 
     * @returns BoosterClientRegisterResponseDto 
     * @throws ApiError
     */
    public static boosterClientControllerRegisterClientAuto(
uuid: string,
xPfClientUuid?: string,
): CancelablePromise<BoosterClientRegisterResponseDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/booster-client/register-auto',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
            query: {
                'uuid': uuid,
            },
        });
    }

    /**
     * @param xPfClientUuid 
     * @returns BoosterClientStatusDto 
     * @throws ApiError
     */
    public static boosterClientControllerGetStatus(
xPfClientUuid?: string,
): CancelablePromise<BoosterClientStatusDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/booster-client/status',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

}
