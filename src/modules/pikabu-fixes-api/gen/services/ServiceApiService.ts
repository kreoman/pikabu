/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class ServiceApiService {

    /**
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static serviceControllerGetClientInfo(
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/service/client-info',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

}
