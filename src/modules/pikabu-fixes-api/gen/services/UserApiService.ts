/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { UserRegistrationRequestDto } from '../models/UserRegistrationRequestDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class UserApiService {

    /**
     * @param requestBody 
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static userRegistrationControllerRequest(
requestBody: UserRegistrationRequestDto,
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/user/registration/request',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param code 
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static userRegistrationControllerComplete(
code: string,
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/user/registration/complete',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
            query: {
                'code': code,
            },
        });
    }

}
