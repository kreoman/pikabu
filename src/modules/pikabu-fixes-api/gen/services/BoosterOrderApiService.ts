/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BoosterOrderConditionsDto } from '../models/BoosterOrderConditionsDto';
import type { BoosterOrderDto } from '../models/BoosterOrderDto';
import type { BoosterOrderRequestV1Dto } from '../models/BoosterOrderRequestV1Dto';
import type { BoosterOrderRequestV2Dto } from '../models/BoosterOrderRequestV2Dto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class BoosterOrderApiService {

    /**
     * @param xPfClientUuid 
     * @returns BoosterOrderDto 
     * @throws ApiError
     */
    public static boosterOrderControllerGetOrders(
xPfClientUuid?: string,
): CancelablePromise<Array<BoosterOrderDto>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/booster-order',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

    /**
     * @param orderUuid 
     * @param xPfClientUuid 
     * @returns BoosterOrderDto 
     * @throws ApiError
     */
    public static boosterOrderControllerGetOrder(
orderUuid: string,
xPfClientUuid?: string,
): CancelablePromise<BoosterOrderDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/booster-order/{orderUuid}',
            path: {
                'orderUuid': orderUuid,
            },
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

    /**
     * @param orderUuid 
     * @param requestBody 
     * @param xPfClientUuid 
     * @returns BoosterOrderDto 
     * @throws ApiError
     */
    public static boosterOrderControllerUpdateOrder(
orderUuid: string,
requestBody: BoosterOrderRequestV1Dto,
xPfClientUuid?: string,
): CancelablePromise<BoosterOrderDto> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/api/booster-order/{orderUuid}',
            path: {
                'orderUuid': orderUuid,
            },
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param orderUuid 
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static boosterOrderControllerDeleteOrder(
orderUuid: string,
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/api/booster-order/{orderUuid}',
            path: {
                'orderUuid': orderUuid,
            },
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

    /**
     * @param requestBody 
     * @param xPfClientUuid 
     * @returns BoosterOrderDto 
     * @throws ApiError
     */
    public static boosterOrderControllerCreateOrderV2(
requestBody: BoosterOrderRequestV2Dto,
xPfClientUuid?: string,
): CancelablePromise<BoosterOrderDto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/booster-order/v2',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static boosterOrderControllerDeleteObsolete(
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/api/booster-order/obsolete',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

    /**
     * @param xPfClientUuid 
     * @returns BoosterOrderConditionsDto 
     * @throws ApiError
     */
    public static boosterOrderConditionsControllerGetOrderConditions(
xPfClientUuid?: string,
): CancelablePromise<BoosterOrderConditionsDto> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/booster-order-conditions',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

}
