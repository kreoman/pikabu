/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BoosterPointsTransactionDto } from '../models/BoosterPointsTransactionDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class BoosterPointsTransactionApiService {

    /**
     * @param xPfClientUuid 
     * @returns BoosterPointsTransactionDto 
     * @throws ApiError
     */
    public static boosterPointsTransactionControllerGetHistory(
xPfClientUuid?: string,
): CancelablePromise<Array<BoosterPointsTransactionDto>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/booster-points-transaction/history',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

}
