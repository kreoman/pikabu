/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BoosterCandidateV2Dto } from '../models/BoosterCandidateV2Dto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class BoosterCandidateApiService {

    /**
     * @param xPfClientUuid 
     * @returns BoosterCandidateV2Dto 
     * @throws ApiError
     */
    public static boosterCandidateControllerGetCandidatesV2(
xPfClientUuid?: string,
): CancelablePromise<Array<BoosterCandidateV2Dto>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/booster-candidate/v2',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

}
