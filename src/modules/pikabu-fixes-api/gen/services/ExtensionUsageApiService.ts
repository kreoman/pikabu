/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ExtensionUsageDto } from '../models/ExtensionUsageDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class ExtensionUsageApiService {

    /**
     * @param requestBody 
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static extensionUsageControllerWriteUsage(
requestBody: ExtensionUsageDto,
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/extension-usage',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
