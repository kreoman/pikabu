/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class TariffApiService {

    /**
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static tariffSubscriptionControllerGetSubscription(
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/tariff-subscription',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

    /**
     * @param userId 
     * @param tariffId 
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static tariffSubscriptionControllerAssignSubscription(
userId: number,
tariffId: number,
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/tariff-subscription/assign',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
            query: {
                'userId': userId,
                'tariffId': tariffId,
            },
        });
    }

}
