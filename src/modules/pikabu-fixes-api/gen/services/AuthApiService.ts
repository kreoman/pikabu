/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AuthRequestDto } from '../models/AuthRequestDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class AuthApiService {

    /**
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static authControllerCheck(
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/auth/check',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

    /**
     * @param requestBody 
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static authControllerLogin(
requestBody: AuthRequestDto,
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/auth/login',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static authControllerLogout(
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/auth/logout',
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
        });
    }

}
