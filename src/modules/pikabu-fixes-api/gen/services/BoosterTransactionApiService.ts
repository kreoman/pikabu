/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BoosterRatingDto } from '../models/BoosterRatingDto';
import type { BoosterTransactionAliveDto } from '../models/BoosterTransactionAliveDto';
import type { BoosterTransactionDto } from '../models/BoosterTransactionDto';
import type { BoosterTransactionErrorRequestDto } from '../models/BoosterTransactionErrorRequestDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class BoosterTransactionApiService {

    /**
     * @param orderUuid 
     * @param storyId 
     * @param requestBody 
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static boosterTransactionControllerInit(
orderUuid: string,
storyId: number,
requestBody: BoosterRatingDto,
xPfClientUuid?: string,
): CancelablePromise<(BoosterTransactionDto | BoosterTransactionAliveDto)> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/booster-transaction/{orderUuid}/{storyId}',
            path: {
                'orderUuid': orderUuid,
                'storyId': storyId,
            },
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param transactionUuid 
     * @param requestBody 
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static boosterTransactionControllerConfirm(
transactionUuid: string,
requestBody: BoosterRatingDto,
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/api/booster-transaction/{transactionUuid}/confirm',
            path: {
                'transactionUuid': transactionUuid,
            },
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param transactionUuid 
     * @param requestBody 
     * @param xPfClientUuid 
     * @returns any 
     * @throws ApiError
     */
    public static boosterTransactionControllerError(
transactionUuid: string,
requestBody: BoosterTransactionErrorRequestDto,
xPfClientUuid?: string,
): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/api/booster-transaction/{transactionUuid}/error',
            path: {
                'transactionUuid': transactionUuid,
            },
            headers: {
                'X-Pf-Client-Uuid': xPfClientUuid,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
