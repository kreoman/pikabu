/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type BoosterOrderRequestV1Dto = {
    votesLimit: number;
    outDate: string;
    storyId: number;
};
