/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ExtensionEnvironmentDto = {
    installType: string;
    manifestVersion: number;
    extensionVersion: string;
    browserName: string;
    browserVersion: string;
    cpuArch: string;
    osName: string;
    osVersion: string;
};
