/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ExtensionEnvironmentDto } from './ExtensionEnvironmentDto';
import type { ExtensionEventDto } from './ExtensionEventDto';
import type { ExtensionInstanceDto } from './ExtensionInstanceDto';
import type { ExtensionMetricDto } from './ExtensionMetricDto';
import type { ExtensionPikabuUserDto } from './ExtensionPikabuUserDto';

export type ExtensionUsageDto = {
    instance: ExtensionInstanceDto;
    environment: ExtensionEnvironmentDto;
    pikabuUser: ExtensionPikabuUserDto;
    events: Array<ExtensionEventDto>;
    metrics: Array<ExtensionMetricDto>;
};
