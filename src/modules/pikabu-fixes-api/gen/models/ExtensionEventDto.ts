/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ExtensionEventCommentVoteDto } from './ExtensionEventCommentVoteDto';
import type { ExtensionEventErrorDto } from './ExtensionEventErrorDto';
import type { ExtensionEventExternalLinkClickDto } from './ExtensionEventExternalLinkClickDto';
import type { ExtensionEventNoteTagModifyDto } from './ExtensionEventNoteTagModifyDto';
import type { ExtensionEventPopupShowAboutTabDto } from './ExtensionEventPopupShowAboutTabDto';
import type { ExtensionEventPopupShowBoosterTabDto } from './ExtensionEventPopupShowBoosterTabDto';
import type { ExtensionEventPopupShowDto } from './ExtensionEventPopupShowDto';
import type { ExtensionEventStoryVoteDto } from './ExtensionEventStoryVoteDto';
import type { ExtensionEventUserPopupShowDto } from './ExtensionEventUserPopupShowDto';

export type ExtensionEventDto = {
    timestamp: number;
    error?: ExtensionEventErrorDto;
    popupShow?: ExtensionEventPopupShowDto;
    popupShowBoosterTab?: ExtensionEventPopupShowBoosterTabDto;
    popupShowAboutTab?: ExtensionEventPopupShowAboutTabDto;
    storyVote?: ExtensionEventStoryVoteDto;
    commentVote?: ExtensionEventCommentVoteDto;
    userPopupShow?: ExtensionEventUserPopupShowDto;
    externalLinkClick?: ExtensionEventExternalLinkClickDto;
    extensionEventNoteTagModify?: ExtensionEventNoteTagModifyDto;
};
