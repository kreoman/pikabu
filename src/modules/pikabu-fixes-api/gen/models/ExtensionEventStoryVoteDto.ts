/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ExtensionEventVoteDto } from './ExtensionEventVoteDto';

export type ExtensionEventStoryVoteDto = {
    storyId: number;
    vote: ExtensionEventVoteDto;
};
