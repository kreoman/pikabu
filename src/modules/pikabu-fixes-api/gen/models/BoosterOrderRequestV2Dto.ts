/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type BoosterOrderRequestV2Dto = {
    votesLimit: number;
    outDate: string;
    storyId: number;
    pointsMultiplier: number;
};
