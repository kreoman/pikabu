/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type BoosterOrderConditionsDto = {
    forbiddenAnyTags: Array<string>;
    requiredAnyTags: Array<string> | null;
    onlyAuthors: boolean;
    requestPointsLimit: number | null;
    requestMinutesLimit: number;
    storyMaxPluses: number | null;
    storyMaxMinuses: number | null;
    pointsMultiplierMin: number;
    pointsMultiplierMax: number | null;
};
