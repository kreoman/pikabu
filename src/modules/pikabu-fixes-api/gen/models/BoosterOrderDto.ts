/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type BoosterOrderDto = {
    storyId: number;
    uuid: string;
    votesLimit: number;
    votesRest: number;
    votesSuccess: number;
    outDate: string;
    storyTitle: string;
    active: boolean;
    deleted: boolean;
};
