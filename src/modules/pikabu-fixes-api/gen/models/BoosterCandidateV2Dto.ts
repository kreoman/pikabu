/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type BoosterCandidateV2Dto = {
    orderUuid: string;
    outDate: string;
    storyId: number;
    pointsMultiplier: number;
};
