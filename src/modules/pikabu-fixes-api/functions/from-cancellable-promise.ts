import { defer, Observable, tap } from 'rxjs';

import { CancelablePromise } from '../gen';

export function fromCancellablePromise<T>(
    promiseFn: () => CancelablePromise<T>,
): Observable<T> {
    let promise: CancelablePromise<T>;

    return defer(() => {
        promise = promiseFn();

        return promise;
    }).pipe(
        tap({
            unsubscribe: () => promise.cancel(),
        }),
    );
}
