export enum PikabuVoteEnum {
    None = 'none',
    Up = 'up',
    Down = 'down',
}
