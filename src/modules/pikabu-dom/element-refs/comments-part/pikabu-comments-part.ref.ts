import { ElementRef, injectNative, many } from '@des-kit/element-ref';

import { PikabuCommentsContainerRef } from '../comments-container/pikabu-comments-container.ref';

@ElementRef({
    selector: 'section.comments-part',
})
export class PikabuCommentsPartRef {
    readonly element = injectNative();

    readonly commentContainers$ = many(
        PikabuCommentsContainerRef,
        this.element,
    );
}
