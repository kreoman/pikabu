import './pikabu-owner-comment-rating.scss';

import { startWith } from 'rxjs';

import {
    getElementText,
    listenElementHasChildren,
    requireElementAttribute,
    requireElementAttributeNumber,
    requireElementChild,
} from '@des-kit/dom';
import { ElementRef, injectNative } from '@des-kit/element-ref';
import { whileSubscribed } from '@des-kit/rxjs';
import { deferShareReplay } from '@des-kit/rxjs';
import { Slot } from '@des-kit/slot';

const WITH_SLOT_RATING_CLASS =
    'des--pikabu-owner-comment-rating-with-slot-rating';

@ElementRef({
    selector: ':scope > .comment__body',
    validator: (el) =>
        listenElementHasChildren(
            el,
            '.comment__user[data-own="true"]',
            '.comment__rating-count',
        ),
})
export class PikabuOwnerCommentRatingRef {
    readonly element = injectNative();

    get ratingElement() {
        return requireElementChild(this.element, '.comment__rating');
    }

    get ratingCountElement() {
        return requireElementChild(this.element, '.comment__rating-count');
    }

    get hintText() {
        return requireElementAttribute(this.ratingCountElement, 'aria-label');
    }

    get ratingCount() {
        return (this.plusesCount ?? 0) - (this.minusesCount ?? 0);
    }

    get initialPluses() {
        try {
            return requireElementAttributeNumber(
                this.ratingElement,
                'data-pluses',
            );
        } catch (e) {
            return null;
        }
    }

    get initialMinuses() {
        try {
            return requireElementAttributeNumber(
                this.ratingElement,
                'data-minuses',
            );
        } catch (e) {
            return null;
        }
    }

    get plusesCount() {
        const plusesCount = Number.parseInt(
            getElementText(this.ratingCountElement),
        );

        return Number.isNaN(plusesCount) ? null : plusesCount;
    }

    get minusesCount() {
        const segments = this.hintText.split(' / ');

        if (segments.length < 2) {
            return null;
        }

        const minusesSegment = segments[1].trim();

        const minusesNumber = Number.parseInt(minusesSegment);

        return Number.isNaN(minusesNumber) ? null : minusesNumber;
    }

    readonly slotRating$ = deferShareReplay(() => this.createSlotRating());

    private createSlotRating() {
        const slot = new Slot(this.ratingCountElement, 'before');

        return whileSubscribed(() => {
            this.element.classList.add(WITH_SLOT_RATING_CLASS);

            return () => {
                slot.unmountAll();
                this.element.classList.remove(WITH_SLOT_RATING_CLASS);
            };
        }).pipe(startWith(slot));
    }
}
