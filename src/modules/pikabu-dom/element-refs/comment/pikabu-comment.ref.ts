import './pikabu-comment.scss';

import { Observable } from 'rxjs';

import {
    requireElementAttribute,
    requireElementAttributeNumber,
} from '@des-kit/dom';
import { ElementRef, injectNative, oneOrNever } from '@des-kit/element-ref';
import { whileSubscribed } from '@des-kit/rxjs';
import { StatusEnum } from '@des-kit/styles';

import { PikabuCommentRatingRef } from './comment-rating/pikabu-comment-rating.ref';
import { PikabuOwnerCommentRatingRef } from './owner-comment-rating/pikabu-owner-comment-rating.ref';

const CLASSES = {
    highlight: ['des--pikabu-comment', 'des--pikabu-comment_highlight'],
};

const PIKABU_CLASSES = {
    comment: {
        ref: '.comment[data-id][data-meta]',
    },
};

const PIKABU_ATTRIBUTES = {
    commentId: 'data-id',
    meta: 'data-meta',
};

const PIKABU_META = {
    storyId: 'sid',
    authorId: 'aid',
};

@ElementRef({
    selector: PIKABU_CLASSES.comment.ref,
})
export class PikabuCommentRef {
    readonly element = injectNative();

    get storyId(): number {
        return Number(this.meta[PIKABU_META.storyId]);
    }

    get authorId(): number {
        return Number(this.meta[PIKABU_META.authorId]);
    }

    get commentId() {
        return requireElementAttributeNumber(
            this.element,
            PIKABU_ATTRIBUTES.commentId,
        );
    }

    get meta(): Record<string, string> {
        const meta: Record<string, string> = {};
        const metaAttribute = requireElementAttribute(
            this.element,
            PIKABU_ATTRIBUTES.meta,
        );
        const metaItems = metaAttribute.split(';');

        for (const item of metaItems) {
            const [key, value] = item.split('=');
            meta[key] = value;
        }

        return meta;
    }

    readonly ratingOwner$ = oneOrNever(
        PikabuOwnerCommentRatingRef,
        this.element,
    );

    readonly ratingOther$ = oneOrNever(PikabuCommentRatingRef, this.element);

    highlightWhileSubscribed(up: boolean): Observable<never> {
        const className = CLASSES.highlight;
        const scheme: StatusEnum = up ? StatusEnum.Success : StatusEnum.Error;

        return whileSubscribed(() => {
            this.element.classList.add(...className);
            this.element.setAttribute('data-status', scheme);

            return () => {
                this.element.classList.remove(...className);
                this.element.removeAttribute('data-status');
            };
        });
    }
}
