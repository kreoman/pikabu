import './pikabu-comment-rating.scss';

import { Observable, startWith } from 'rxjs';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import {
    listenElementHasChildren,
    listenElementSomeClass,
    listenElementTextNumber,
    requireElementAttributeNumber,
    requireElementChild,
} from '@des-kit/dom';
import { ElementRef, injectNative } from '@des-kit/element-ref';
import { whileSubscribed } from '@des-kit/rxjs';
import { deferShareReplay } from '@des-kit/rxjs';
import { Slot } from '@des-kit/slot';

import { PikabuVoteEnum } from '../../../enums/pikabu-vote.enum';
import { calcVote } from '../../../functions/calc-vote';

const WITH_SLOT_RATING_CLASS = 'des--pikabu-comment-rating-with-slot-rating';

@ElementRef({
    selector: ':scope > .comment__body',
    validator: (el) =>
        listenElementHasChildren(
            el,
            '.comment__user:not([data-own])',
            '.comment__rating-up',
            '.comment__rating-count',
            '.comment__rating-down',
        ),
})
export class PikabuCommentRatingRef {
    readonly element = injectNative();

    get ratingElement() {
        return requireElementChild(this.element, '.comment__rating');
    }

    get ratingUpButtonElement() {
        return requireElementChild(this.ratingElement, '.comment__rating-up');
    }

    get ratingCountElement() {
        return requireElementChild(
            this.ratingElement,
            '.comment__rating-count',
        );
    }

    get ratingDownButtonElement() {
        return requireElementChild(this.ratingElement, '.comment__rating-down');
    }

    get initialPluses() {
        try {
            return requireElementAttributeNumber(
                this.ratingElement,
                'data-pluses',
            );
        } catch (e) {
            return null;
        }
    }

    get initialMinuses() {
        try {
            return requireElementAttributeNumber(
                this.ratingElement,
                'data-minuses',
            );
        } catch (e) {
            return null;
        }
    }

    get ratingCount$() {
        return listenElementTextNumber(this.ratingCountElement);
    }

    get upVoted$(): Observable<boolean> {
        return listenElementSomeClass(
            this.ratingUpButtonElement,
            'comment__rating-up_active',
        );
    }

    get downVoted$(): Observable<boolean> {
        return listenElementSomeClass(
            this.ratingDownButtonElement,
            'comment__rating-down_active',
        );
    }

    get vote$(): Observable<PikabuVoteEnum> {
        return combineLatest([this.upVoted$, this.downVoted$]).pipe(
            map(([upVoted, downVoted]) => calcVote(upVoted, downVoted)),
        );
    }

    readonly slotRating$ = deferShareReplay(() => this.createSlotRating());

    private createSlotRating() {
        const slot = new Slot(this.ratingUpButtonElement, 'after');

        return whileSubscribed(() => {
            this.element.classList.add(WITH_SLOT_RATING_CLASS);

            return () => {
                slot.unmountAll();
                this.element.classList.remove(WITH_SLOT_RATING_CLASS);
            };
        }).pipe(startWith(slot));
    }
}
