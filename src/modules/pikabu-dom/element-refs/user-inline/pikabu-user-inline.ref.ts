import './pikabu-user-inline.scss';

import { Observable, startWith } from 'rxjs';

import { isSet } from '@des-kit/core';
import { ElementRef, injectNative } from '@des-kit/element-ref';
import { whileSubscribed } from '@des-kit/rxjs';
import { deferShareReplay } from '@des-kit/rxjs';
import { Slot } from '@des-kit/slot';

const inStoryMobile =
    'html.mv .story__main .story__author-header .story__user-link';
const inStoryCommMobile =
    'html.mv .story__author-panel .story__user-info .story__user-link';
const inStoryDesktop = 'html.dv .story__user-info .story__user-link';
const inStoryCommDesktop =
    'html.dv .story__author-panel .story__user-info .story__user-link';
const inComment = '.comment__user';

const HAS_NOTE_SELECTOR = '.avatar__note';

const WITH_SLOT_APPEND_CLASS = 'des--pikabu-user-inline-with-slot-append';
const WITH_SLOT_AFTER_CLASS = 'des--pikabu-user-inline-with-slot-after';

@ElementRef({
    selector: [
        inStoryCommMobile,
        inStoryDesktop,
        inStoryMobile,
        inStoryCommDesktop,
        inComment,
    ].join(', '),
})
export class PikabuUserInlineRef {
    readonly element = injectNative();

    get isOwn() {
        return this.element.hasAttribute('data-own');
    }

    get userId() {
        let idStr = this.element.getAttribute('data-id');

        if (!idStr) {
            idStr =
                this.element.parentElement?.parentElement
                    ?.querySelector('[data-id]')
                    ?.getAttribute('data-id') ?? null;
        }

        return idStr ? Number(idStr) : null;
    }

    get username(): string {
        const name = this.getRawName();

        if (!isSet(name) || name === 'DELETED' || name === 'Аноним') {
            return '';
        }

        return name;
    }

    get hasNote() {
        return !!this.element.querySelector(HAS_NOTE_SELECTOR);
    }

    readonly slotLeft$ = deferShareReplay(() => this.createSlotAppend());

    readonly slotRight$ = deferShareReplay(() => this.createSlotAfter());

    private getRawName(): string | null {
        return (
            this.element.getAttribute('data-name') ||
            this.element.textContent?.trim() ||
            null
        );
    }

    private createSlotAppend(): Observable<Slot> {
        const slot = new Slot(this.element, 'append');

        return whileSubscribed(() => {
            this.element.classList.add(WITH_SLOT_APPEND_CLASS);

            return () => {
                slot.unmountAll();
                this.element.classList.remove(WITH_SLOT_APPEND_CLASS);
            };
        }).pipe(startWith(slot));
    }

    private createSlotAfter(): Observable<Slot> {
        const slot = new Slot(this.element, 'after');

        return whileSubscribed(() => {
            this.element.classList.add(WITH_SLOT_AFTER_CLASS);

            return () => {
                slot.unmountAll();
                this.element.classList.remove(WITH_SLOT_AFTER_CLASS);
            };
        }).pipe(startWith(slot));
    }
}
