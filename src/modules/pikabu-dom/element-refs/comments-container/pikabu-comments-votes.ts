import {
    BehaviorSubject,
    distinctUntilChanged,
    Observable,
    shareReplay,
} from 'rxjs';
import { map } from 'rxjs/operators';

import { isObjectsEqual } from '@des-kit/core';

import { PikabuVoteEnum } from '../../enums/pikabu-vote.enum';

export class PikabuCommentsVotes {
    private readonly votes$ = new BehaviorSubject<
        ReadonlyMap<number, ReadonlyMap<number, PikabuVoteEnum>>
    >(new Map());

    readonly signedAuthorIds$: Observable<readonly number[]> = this.votes$.pipe(
        map((votes) => this.calcAvgVotes(votes)),
        distinctUntilChanged(isObjectsEqual),
        shareReplay({
            refCount: true,
            bufferSize: 1,
        }),
    );

    setVote(authorId: number, commentId: number, vote: PikabuVoteEnum): void {
        const map = new Map(this.votes$.value);

        const authorMap = new Map(map.get(authorId));

        if (vote === PikabuVoteEnum.None) {
            authorMap.delete(commentId);
        } else {
            authorMap.set(commentId, vote);
        }

        if (!authorMap.size) {
            map.delete(authorId);
        } else {
            map.set(authorId, authorMap);
        }

        this.votes$.next(map);
    }

    private calcAvgVotes(
        map: ReadonlyMap<number, ReadonlyMap<number, PikabuVoteEnum>>,
    ): readonly number[] {
        const votes: number[] = [];

        for (const [authorId, commentMap] of map) {
            let commonVote = PikabuVoteEnum.None;

            for (const vote of commentMap.values()) {
                if (commonVote === PikabuVoteEnum.None) {
                    commonVote = vote;
                    continue;
                }

                if (commonVote !== vote) {
                    commonVote = PikabuVoteEnum.None;
                    break;
                }
            }

            if (commonVote !== PikabuVoteEnum.None) {
                votes.push(
                    commonVote === PikabuVoteEnum.Up ? authorId : -authorId,
                );
            }
        }

        return votes;
    }
}
