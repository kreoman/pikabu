import './pikabu-comments-container.scss';

import {
    combineLatest,
    concatMap,
    debounceTime,
    merge,
    Observable,
    of,
    shareReplay,
    startWith,
    switchMap,
} from 'rxjs';
import { map } from 'rxjs/operators';

import { ElementRef, injectNative, many } from '@des-kit/element-ref';
import { whileSubscribed } from '@des-kit/rxjs';
import { deferShareReplay } from '@des-kit/rxjs';
import { never } from '@des-kit/rxjs';
import { switchEvery } from '@des-kit/rxjs';
import { Slot } from '@des-kit/slot';

import { PikabuCommentRef } from '../comment/pikabu-comment.ref';
import { PikabuCommentsExpanderRef } from '../comments-expander/pikabu-comments-expander.ref';
import { PikabuCommentsExpanderButtonRef } from '../comments-expander/pikabu-comments-expander-button.ref';
import { PikabuCommentsExpanderHiddenGroupRef } from '../comments-expander/pikabu-comments-expander-hidden-group.ref';
import { PikabuCommentsExpanderLinkRef } from '../comments-expander/pikabu-comments-expander-link.ref';
import { PikabuCommentsVotes } from './pikabu-comments-votes';

const WITH_EXPANDER_SLOT_CLASS = 'des--comments-container-with-expander-slot';

@ElementRef({
    selector: '.comments__container',
})
export class PikabuCommentsContainerRef {
    readonly element = injectNative();
    readonly expanderSlot$ = deferShareReplay(() => this.createSlotAfter());

    readonly votes = new PikabuCommentsVotes();

    readonly comments$ = many(PikabuCommentRef, this.element);

    getCommentsByAuthorId(
        authorId: number,
    ): Observable<readonly PikabuCommentRef[]> {
        return this.comments$.pipe(
            map((comments) =>
                comments.filter((comment) => comment.authorId === authorId),
            ),
        );
    }

    getCommentsExpanders(): Observable<readonly PikabuCommentsExpanderRef[]> {
        return combineLatest([
            many(PikabuCommentsExpanderLinkRef, this.element),
            many(PikabuCommentsExpanderHiddenGroupRef, this.element),
            many(PikabuCommentsExpanderButtonRef, this.element),
        ]).pipe(
            map((groups) => groups.flat()),
            shareReplay({
                refCount: true,
                bufferSize: 1,
            }),
        );
    }

    expandAllComments() {
        const expanders$ = this.getCommentsExpanders();

        const userClickListen$ = expanders$.pipe(
            switchEvery((expander) => expander.click$),
            never(),
        );

        const expandAll$ = expanders$.pipe(
            debounceTime(100),
            switchMap((expanders) =>
                of(...expanders).pipe(
                    concatMap((expander) => expander.expand()),
                ),
            ),
            never(),
        );

        return merge(userClickListen$, expandAll$);
    }

    private createSlotAfter() {
        const slot = new Slot(this.element, 'after');

        return whileSubscribed(() => {
            this.element.classList.add(WITH_EXPANDER_SLOT_CLASS);

            return () => {
                this.element.classList.remove(WITH_EXPANDER_SLOT_CLASS);

                slot.unmountAll();
            };
        }).pipe(startWith(slot));
    }
}
