import './pikabu-page.scss';

import { ElementRef, injectNative } from '@des-kit/element-ref';
import { whileSubscribed } from '@des-kit/rxjs';

const HIDE_DONATE_CLASS = 'des--page-hide-donate';
const HIDE_SHORT_STORY_DESIGN_CLASS = 'des--page-hide-short-story-design';

@ElementRef({
    selector: 'body > .app',
})
export class PikabuPageRef {
    readonly element = injectNative();

    hideDonate() {
        return whileSubscribed(() => {
            this.element.classList.add(HIDE_DONATE_CLASS);
            return () => this.element.classList.remove(HIDE_DONATE_CLASS);
        });
    }

    hideStoryShortDesign() {
        return whileSubscribed(() => {
            this.element.classList.add(HIDE_SHORT_STORY_DESIGN_CLASS);
            return () =>
                this.element.classList.remove(HIDE_SHORT_STORY_DESIGN_CLASS);
        });
    }
}
