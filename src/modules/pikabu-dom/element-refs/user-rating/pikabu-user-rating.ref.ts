import './pikabu-user-rating.scss';

import { Observable, startWith } from 'rxjs';

import { getElementText } from '@des-kit/dom';
import { ElementRef, injectNative } from '@des-kit/element-ref';
import { whileSubscribed } from '@des-kit/rxjs';
import { Slot } from '@des-kit/slot';

const WITH_SLOT_CLASS = 'des--profile-rating-with-slot';

@ElementRef({
    selector: '.profile__digital b progress.rating-progress',
})
export class PikabuUserRatingRef {
    readonly element = injectNative();

    get bElement() {
        return this.element.parentElement!;
    }

    get nicknameElement() {
        return document.querySelector('.section-group .profile__nick');
    }

    get nickname() {
        const element = this.nicknameElement;

        if (!element) {
            return null;
        }

        return getElementText(element);
    }

    readonly slot$ = this.createSlot();

    private createSlot(): Observable<Slot> {
        const slot = new Slot(this.bElement, 'append');

        return whileSubscribed(() => {
            this.bElement.classList.add(WITH_SLOT_CLASS);

            return () => {
                slot.unmountAll();
                this.bElement.classList.remove(WITH_SLOT_CLASS);
            };
        }).pipe(startWith(slot));
    }
}
