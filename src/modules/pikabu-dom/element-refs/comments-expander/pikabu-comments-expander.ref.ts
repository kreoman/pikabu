import './pikabu-comments-expander.scss';

import { defer, fromEvent, share } from 'rxjs';
import { tap } from 'rxjs/operators';

import { injectNative } from '@des-kit/element-ref';

const EXPANDED_CLASS = 'des--pikabu-comments-expander_expanded';
const USER_CLICKED_CLASS = 'des--pikabu-comments-expander_user-clicked';

export abstract class PikabuCommentsExpanderRef {
    readonly element = injectNative<HTMLElement>();

    readonly click$ = defer(() => fromEvent(this.expandElement, 'click')).pipe(
        tap((event) => this.markAsClicked(event)),
        share(),
    );

    get collapsed() {
        return true;
    }

    get canExpand(): boolean {
        return (
            this.collapsed &&
            !this.element.classList.contains(USER_CLICKED_CLASS)
        );
    }

    get expandElement(): HTMLElement {
        return this.element;
    }

    async expand() {
        if (!this.canExpand) {
            return;
        }

        this.expandElement.classList.add(EXPANDED_CLASS);
        this.expandElement.click();
    }

    private markAsClicked(event: Event) {
        if (event.isTrusted) {
            this.expandElement.classList.add(USER_CLICKED_CLASS);
        }
    }
}
