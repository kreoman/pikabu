import { listenElementHasChildren } from '@des-kit/dom';
import { ElementRef } from '@des-kit/element-ref';

import { PikabuCommentsExpanderRef } from './pikabu-comments-expander.ref';

@ElementRef({
    selector: 'button.comment__more',
    validator: (element) =>
        listenElementHasChildren(element, '.comments__more-count'),
})
export class PikabuCommentsExpanderButtonRef extends PikabuCommentsExpanderRef {}
