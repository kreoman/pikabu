import { ElementRef } from '@des-kit/element-ref';

import { PikabuCommentsExpanderRef } from './pikabu-comments-expander.ref';

@ElementRef({
    selector: ':not(.comment_hidden-body) > .comment-toggle-children',
})
export class PikabuCommentsExpanderLinkRef extends PikabuCommentsExpanderRef {
    get collapsed(): boolean {
        return this.element.matches('.comment-toggle-children_collapse');
    }
}
