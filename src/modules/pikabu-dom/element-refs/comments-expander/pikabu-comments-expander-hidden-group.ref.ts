import { map } from 'rxjs/operators';

import { listenElementMatches } from '@des-kit/dom';
import { ElementRef } from '@des-kit/element-ref';

import { PikabuCommentsExpanderRef } from './pikabu-comments-expander.ref';

@ElementRef({
    selector: '.comment-hidden-group .comment-hidden-group__toggle',
    validator: (element) =>
        listenElementMatches(element, '[hidden]').pipe(
            map((hidden) => !hidden),
        ),
})
export class PikabuCommentsExpanderHiddenGroupRef extends PikabuCommentsExpanderRef {}
