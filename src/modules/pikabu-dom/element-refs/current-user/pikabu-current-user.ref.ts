import { defer, fromEvent } from 'rxjs';

import { getElementText, requireElementChild } from '@des-kit/dom';
import { ElementRef, injectNative } from '@des-kit/element-ref';

@ElementRef({
    selector: '.sidebar__inner .user_right-sidebar',
})
export class PikabuCurrentUserRef {
    readonly element = injectNative();

    readonly listenLogoutClick$ = defer(() =>
        fromEvent(this.exitElement, 'click'),
    );

    get nicknameElement() {
        return requireElementChild(this.element, '.user__nick.user__nick_big');
    }

    get nickname() {
        return getElementText(this.nicknameElement);
    }

    get exitElement() {
        return requireElementChild(this.element, '.user__exit');
    }
}
