import './pikabu-user-popup.scss';

import { Observable, startWith } from 'rxjs';
import { map } from 'rxjs/operators';

import { isSet } from '@des-kit/core';
import {
    getElementText,
    listenElementHasChildren,
    requireElementChild,
} from '@des-kit/dom';
import { ElementRef, injectNative } from '@des-kit/element-ref';
import { whileSubscribed } from '@des-kit/rxjs';
import { deferShareReplay } from '@des-kit/rxjs';
import { Slot } from '@des-kit/slot';

const WITH_SLOT_CLASS = 'des--pikabu-user-popup-with-slot';

@ElementRef({
    selector: '.popup__wrapper .profile_short',
    validator: (element) =>
        listenElementHasChildren(element, '.profile__user').pipe(
            map((containElement) => containElement),
        ),
})
export class PikabuUserPopupRef {
    readonly element = injectNative();

    get profileNickElement() {
        return requireElementChild(this.element, '.profile__nick a');
    }

    get profileUserElement() {
        return requireElementChild(this.element, '.profile__user');
    }

    get username(): string {
        const name = this.getRawName();

        if (!isSet(name) || name === 'DELETED' || name === 'Аноним') {
            return '';
        }

        return name;
    }

    readonly slot$ = deferShareReplay(() => this.createSlot());

    private getRawName(): string | null {
        return getElementText(this.profileNickElement) || '';
    }

    private createSlot(): Observable<Slot> {
        const slot = new Slot(this.profileUserElement, 'after');

        return whileSubscribed(() => {
            this.element.classList.add(WITH_SLOT_CLASS);

            return () => {
                slot.unmountAll();
                this.element.classList.remove(WITH_SLOT_CLASS);
            };
        }).pipe(startWith(slot));
    }
}
