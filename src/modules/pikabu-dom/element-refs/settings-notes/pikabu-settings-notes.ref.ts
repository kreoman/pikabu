import './pikabu-settings-notes.scss';

import { Observable, startWith } from 'rxjs';

import { ElementRef, injectNative } from '@des-kit/element-ref';
import { deferShareReplay, whileSubscribed } from '@des-kit/rxjs';
import { Slot } from '@des-kit/slot';

const WITH_SLOT_CLASS = 'des--notes-with-slot';

@ElementRef({
    selector: '.settings-notes',
})
export class PikabuSettingsNotesRef {
    readonly element = injectNative();

    readonly topSlot$ = deferShareReplay(() => this.createTopSlot());

    private createTopSlot(): Observable<Slot> {
        const slot = new Slot(this.element, 'prepend');

        return whileSubscribed(() => {
            this.element.classList.add(WITH_SLOT_CLASS);

            return () => {
                slot.unmountAll();
                this.element.classList.remove(WITH_SLOT_CLASS);
            };
        }).pipe(startWith(slot));
    }
}
