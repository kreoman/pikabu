import { defer, fromEvent } from 'rxjs';

import { requireElementChild } from '@des-kit/dom';
import { ElementRef, injectNative } from '@des-kit/element-ref';

@ElementRef({
    selector: '.sidebar-block .sidebar-auth',
})
export class PikabuLoginFormRef {
    private readonly element = injectNative();

    get formElement() {
        return requireElementChild(
            this.element,
            '#signin-form',
        ) as HTMLFormElement;
    }

    get usernameInputElement() {
        return requireElementChild(
            this.formElement,
            'input[name="username"]',
        ) as HTMLInputElement;
    }

    get passwordInputElement() {
        return requireElementChild(
            this.formElement,
            'input[name="password"]',
        ) as HTMLInputElement;
    }

    get username() {
        return this.usernameInputElement.value;
    }

    get password() {
        return this.passwordInputElement.value;
    }

    readonly listenSubmit$ = defer(() => fromEvent(this.formElement, 'submit'));
}
