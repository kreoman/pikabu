import './pikabu-sidebar-ref.scss';

import { Observable, startWith } from 'rxjs';

import { ElementRef, injectNative } from '@des-kit/element-ref';
import { whileSubscribed } from '@des-kit/rxjs';
import { deferShareReplay } from '@des-kit/rxjs';
import { Slot } from '@des-kit/slot';

const SIDEBAR_WITH_SLOT_CLASS = 'des--sidebar-with-slot';

@ElementRef({
    selector: '.sidebar__inner',
})
export class PikabuSidebarRef {
    readonly element = injectNative();

    readonly beforeUserBlockSlot$ = deferShareReplay(() =>
        this.createBeforeUserBlockSlot(),
    );

    private createBeforeUserBlockSlot(): Observable<Slot> {
        const slot = new Slot(this.element, 'prepend');

        return whileSubscribed(() => {
            this.element.classList.add(SIDEBAR_WITH_SLOT_CLASS);

            return () => {
                slot.unmountAll();
                this.element.classList.remove(SIDEBAR_WITH_SLOT_CLASS);
            };
        }).pipe(startWith(slot));
    }
}
