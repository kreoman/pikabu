import { defer, fromEvent } from 'rxjs';

import { ElementRef, injectNative } from '@des-kit/element-ref';

@ElementRef({
    selector: '.popup__wrapper .menu.menu_vertical [data-role="logout"]',
})
export class CurrentUserLogoutMenuItemRef {
    private readonly element = injectNative();

    readonly logoutElementClick$ = defer(() =>
        fromEvent(this.element, 'click'),
    );
}
