import './pikabu-story-video.scss';

import { startWith } from 'rxjs';

import {
    requireElementAttribute,
    requireElementAttributeNumber,
} from '@des-kit/dom';
import { ElementRef, injectNative } from '@des-kit/element-ref';
import { whileSubscribed } from '@des-kit/rxjs';
import { deferShareReplay } from '@des-kit/rxjs';
import { Slot } from '@des-kit/slot';

const WITH_SLOT_AFTER_CLASS = 'des--pikabu-story-video-with-slot-after';

@ElementRef({
    selector: '.story-block_type_video .player[data-story-id][data-webm]',
})
export class PikabuStoryVideoRef {
    readonly element = injectNative();

    get storyId() {
        return requireElementAttributeNumber(this.element, 'data-story-id');
    }

    get webmUrl() {
        return requireElementAttribute(this.element, 'data-webm');
    }

    get av1Url(): string | null {
        return this.element.getAttribute('data-av1');
    }

    readonly slotAfter$ = deferShareReplay(() => this.createSlotAfter());

    private createSlotAfter() {
        const slot = new Slot(this.element, 'after');

        return whileSubscribed(() => {
            this.element.classList.add(WITH_SLOT_AFTER_CLASS);

            return () => {
                slot.unmountAll();
                this.element.classList.remove(WITH_SLOT_AFTER_CLASS);
            };
        }).pipe(startWith(slot));
    }
}
