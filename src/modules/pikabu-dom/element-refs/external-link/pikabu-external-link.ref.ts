import { defer, fromEvent } from 'rxjs';

import { requireElementAttribute } from '@des-kit/dom';
import { ElementRef, injectNative } from '@des-kit/element-ref';

@ElementRef({
    selector: 'a[href^="https"]:not([href*="pikabu.ru"])',
})
export class PikabuExternalLinkRef {
    readonly element = injectNative();

    get href() {
        return requireElementAttribute(this.element, 'href');
    }

    readonly click$ = defer(() => fromEvent(this.element, 'click'));
}
