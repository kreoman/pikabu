import './pikabu-user-popup-v2.scss';

import { Observable, startWith } from 'rxjs';
import { map } from 'rxjs/operators';

import { isSet } from '@des-kit/core';
import {
    getElementText,
    listenElementHasChildren,
    requireElementChild,
} from '@des-kit/dom';
import { ElementRef, injectNative } from '@des-kit/element-ref';
import { whileSubscribed } from '@des-kit/rxjs';
import { deferShareReplay } from '@des-kit/rxjs';
import { Slot } from '@des-kit/slot';

const WITH_SLOT_CLASS = 'des--pikabu-user-popup-v2-with-slot';

@ElementRef({
    selector: '.popup__wrapper .mini-profile',
    validator: (element) =>
        listenElementHasChildren(element, '.mini-profile__btns').pipe(
            map((containElement) => containElement),
        ),
})
export class PikabuUserPopupV2Ref {
    readonly element = injectNative();

    get profileNickElement() {
        return requireElementChild(this.element, 'h2 a');
    }

    get profileButtonsElement() {
        return requireElementChild(this.element, '.mini-profile__btns');
    }

    get username(): string {
        const name = this.getRawName();

        if (!isSet(name) || name === 'DELETED' || name === 'Аноним') {
            return '';
        }

        return name;
    }

    readonly slot$ = deferShareReplay(() => this.createSlot());

    private getRawName(): string | null {
        return getElementText(this.profileNickElement);
    }

    private createSlot(): Observable<Slot> {
        const slot = new Slot(this.profileButtonsElement, 'after');

        return whileSubscribed(() => {
            this.element.classList.add(WITH_SLOT_CLASS);

            return () => {
                slot.unmountAll();
                this.element.classList.remove(WITH_SLOT_CLASS);
            };
        }).pipe(startWith(slot));
    }

    protected isValid(): Observable<boolean> {
        return listenElementHasChildren(
            this.element,
            '.mini-profile__btns',
        ).pipe(map((containElement) => containElement && !!this.username));
    }
}
