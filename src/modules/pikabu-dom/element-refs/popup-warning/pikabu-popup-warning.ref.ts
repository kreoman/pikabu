import { defer, fromEvent } from 'rxjs';

import { requireElementChild } from '@des-kit/dom';
import { ElementRef, injectNative } from '@des-kit/element-ref';

@ElementRef({
    selector: '.popup_warning',
})
export class PikabuPopupWarningRef {
    private readonly element = injectNative();

    get footerElement() {
        return requireElementChild(this.element, '.popup__footer');
    }

    get confirmButtonElement() {
        return requireElementChild(this.footerElement, 'button.button_danger');
    }

    get cancelButtonElement() {
        return requireElementChild(
            this.footerElement,
            'button:not(.button_danger)',
        );
    }

    readonly listenConfirmClick$ = defer(() =>
        fromEvent(this.confirmButtonElement, 'click'),
    );

    readonly listenCancelClick$ = defer(() =>
        fromEvent(this.cancelButtonElement, 'click'),
    );
}
