import './pikabu-story-ref.scss';

import { filter, firstValueFrom, Observable } from 'rxjs';

import { delay } from '@des-kit/core';
import {
    getElementText,
    listenElementHasChildren,
    listenElementSomeClass,
    listenElementTextNumber,
    requireElementAttributeNumber,
    requireElementChild,
} from '@des-kit/dom';
import {
    ElementRef,
    injectNative,
    many,
    oneOrNever,
} from '@des-kit/element-ref';
import { deferShareReplay } from '@des-kit/rxjs';
import { listenSlot } from '@des-kit/slot';

import { PikabuCommentsContainerRef } from '../comments-container/pikabu-comments-container.ref';
import { PikabuStoryVideoRef } from '../story-video/pikabu-story-video.ref';
import { pikabuStoryIsAdult } from './functions/pikabu-story-is-adult';

const CLASSES = {
    withSlot: {
        upvote: 'des--pikabu-story-with-slot-upvote',
        rating: 'des--pikabu-story-with-slot-rating',
        toolbarLeft: 'des--pikabu-story-with-slot-toolbar-left',
        toolbarRight: 'des--pikabu-story-with-slot-toolbar-right',
    },
};

const PIKABU_ATTRIBUTES = {
    storyId: 'data-story-id',
    authorId: 'data-author-id',
    initialPluses: 'data-pluses',
    initialMinuses: 'data-minuses',
};

const PIKABU_CLASSES = {
    story: {
        rating: {
            up: {
                active: 'story__rating-up_active',
            },
            down: {
                active: 'story__rating-down_active',
            },
        },
        tool: {
            active: 'tool_active',
        },
    },
};

const PIKABU_SELECTORS = {
    story: {
        ref: 'article.story[data-story-id][data-author-id]',
        title: '.story__title',
        tools: {
            left: '.story__tools',
            right: '.story__additional-tools',
        },
        rating: {
            block: '.story__rating-block',
            up: '.story__rating-up',
            down: '.story__rating-down',
            count: '.story__rating-count',
        },
        tag: '.story__tags .tags__tag',
    },
};

@ElementRef({
    selector: PIKABU_SELECTORS.story.ref,
    validator: (el) =>
        listenElementHasChildren(
            el,
            PIKABU_SELECTORS.story.rating.up,
            PIKABU_SELECTORS.story.rating.count,
        ),
})
export class PikabuStoryRef {
    readonly element = injectNative();

    get title() {
        return getElementText(this.titleElement);
    }

    get titleElement() {
        return requireElementChild(this.element, PIKABU_SELECTORS.story.title);
    }

    get isAdult() {
        return pikabuStoryIsAdult(this.tags);
    }

    get isAuthors() {
        return this.tags.includes('[моё]');
    }

    get tags(): string[] {
        const tags: string[] = [];

        const tagElements = this.element.querySelectorAll(
            PIKABU_SELECTORS.story.tag,
        );

        for (const tagElement of tagElements) {
            tags.push(getElementText(tagElement));
        }

        return tags;
    }

    get storyRatingBlockElement() {
        return requireElementChild(
            this.element,
            PIKABU_SELECTORS.story.rating.block,
        ) as HTMLElement;
    }

    get storyRatingUpElement() {
        return requireElementChild(
            this.element,
            PIKABU_SELECTORS.story.rating.up,
        ) as HTMLElement;
    }

    get storyRatingCountElement() {
        return requireElementChild(
            this.element,
            PIKABU_SELECTORS.story.rating.count,
        );
    }

    get storyRatingDownButtonElement() {
        return requireElementChild(
            this.element,
            PIKABU_SELECTORS.story.rating.down,
        ) as HTMLElement;
    }

    get commentsElement() {
        return (
            document.querySelector(
                `.comments[data-story-id="${this.storyId}"]`,
            ) ?? this.element
        );
    }

    get storyId() {
        return requireElementAttributeNumber(
            this.element,
            PIKABU_ATTRIBUTES.storyId,
        );
    }

    get initialPluses() {
        try {
            return requireElementAttributeNumber(
                this.storyRatingBlockElement,
                PIKABU_ATTRIBUTES.initialPluses,
            );
        } catch (e) {
            return null;
        }
    }

    get initialMinuses() {
        try {
            return requireElementAttributeNumber(
                this.storyRatingBlockElement,
                PIKABU_ATTRIBUTES.initialMinuses,
            );
        } catch (e) {
            return null;
        }
    }

    get authorId() {
        return requireElementAttributeNumber(
            this.element,
            PIKABU_ATTRIBUTES.authorId,
        );
    }

    get upVoted() {
        const list = this.storyRatingUpElement.classList;

        return (
            list.contains(PIKABU_CLASSES.story.rating.up.active) ||
            list.contains(PIKABU_CLASSES.story.tool.active)
        );
    }

    get downVoted() {
        const list = this.storyRatingUpElement.classList;

        return (
            list.contains(PIKABU_CLASSES.story.rating.down.active) ||
            list.contains(PIKABU_CLASSES.story.tool.active)
        );
    }

    get ratingCount$() {
        return listenElementTextNumber(this.storyRatingCountElement);
    }

    get upVoted$(): Observable<boolean> {
        return listenElementSomeClass(
            this.storyRatingUpElement,
            PIKABU_CLASSES.story.rating.up.active,
            PIKABU_CLASSES.story.tool.active,
        );
    }

    get downVoted$(): Observable<boolean> {
        return listenElementSomeClass(
            this.storyRatingDownButtonElement,
            PIKABU_CLASSES.story.rating.down.active,
            PIKABU_CLASSES.story.tool.active,
        );
    }

    readonly videos$ = many(PikabuStoryVideoRef, this.element);
    readonly commentsContainer$ = oneOrNever(
        PikabuCommentsContainerRef,
        this.commentsElement,
    );

    readonly slotUpvote$ = deferShareReplay(() => this.createSlotUpvote());
    readonly slotRating$ = deferShareReplay(() => this.createSlotRating());
    readonly slotToolbarLeft$ = deferShareReplay(() =>
        this.createSlotToolbarLeft(),
    );
    readonly slotToolbarRight$ = deferShareReplay(() =>
        this.createSlotToolbarRight(),
    );

    async upvote(): Promise<void> {
        if (this.upVoted) {
            return;
        }

        if (this.downVoted) {
            this.storyRatingUpElement.click();
            await firstValueFrom(
                this.downVoted$.pipe(filter((downVoted) => !downVoted)),
            );
        }

        this.storyRatingUpElement.click();

        await firstValueFrom(this.upVoted$.pipe(filter(Boolean)));

        await delay(1000);
    }

    private createSlotUpvote() {
        return listenSlot(
            this.element,
            PIKABU_SELECTORS.story.rating.up,
            'append',
            CLASSES.withSlot.upvote,
        );
    }

    private createSlotRating() {
        return listenSlot(
            this.element,
            PIKABU_SELECTORS.story.rating.count,
            'after',
            CLASSES.withSlot.rating,
        );
    }

    private createSlotToolbarLeft() {
        return listenSlot(
            this.element,
            PIKABU_SELECTORS.story.tools.left,
            'append',
            CLASSES.withSlot.toolbarLeft,
        );
    }

    private createSlotToolbarRight() {
        return listenSlot(
            this.element,
            PIKABU_SELECTORS.story.tools.right,
            'append',
            CLASSES.withSlot.toolbarRight,
        );
    }
}
