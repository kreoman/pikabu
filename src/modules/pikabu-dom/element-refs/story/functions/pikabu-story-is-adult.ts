const GROUPS = [['18+'], ['Эротика'], ['Попа']];

export function pikabuStoryIsAdult(tags: readonly string[]): boolean {
    const tagsSet = new Set(tags);
    return GROUPS.some((group) => group.every((tag) => tagsSet.has(tag)));
}
