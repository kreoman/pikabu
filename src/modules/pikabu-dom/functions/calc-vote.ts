import { PikabuVoteEnum } from '../enums/pikabu-vote.enum';

export function calcVote(upVoted: boolean, downVoted: boolean): PikabuVoteEnum {
    if (upVoted) {
        return PikabuVoteEnum.Up;
    }

    if (downVoted) {
        return PikabuVoteEnum.Down;
    }

    return PikabuVoteEnum.None;
}
