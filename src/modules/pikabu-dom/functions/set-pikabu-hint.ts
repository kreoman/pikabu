export function setPikabuHint(element: Element, hint: string | null): void {
    element.classList.toggle('hint', !!hint);
    element.setAttribute('aria-label', hint ?? '');
}
