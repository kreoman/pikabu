import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { listenElementAttribute } from '@des-kit/dom';
import { ThemeEnum } from '@des-kit/styles';

export function listenTheme(): Observable<ThemeEnum> {
    return listenElementAttribute(
        document.documentElement,
        'data-theme-dark',
    ).pipe(
        map((value) => (value === 'true' ? ThemeEnum.Dark : ThemeEnum.Light)),
    );
}
