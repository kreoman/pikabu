import { FeatureModel } from '../../pikabu-features-ui/models/feature.model';
import { SendStatisticFeature } from '../features/send-statistic.feature';

export const BackgroundFeaturesConst: FeatureModel[] = [
    {
        enabledOn: 'sendStatistic',
        featureFactory: SendStatisticFeature,
    },
];
