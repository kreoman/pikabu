import { Container } from 'typedi';

import { StatisticSenderService } from '../../statistic/statistic-sender.service';

export function SendStatisticFeature() {
    const statisticSenderService = Container.get(StatisticSenderService);

    return statisticSenderService.workOnSubscribe$;
}
