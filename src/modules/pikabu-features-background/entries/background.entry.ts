import { map } from 'rxjs/operators';
import { Container } from 'typedi';

import { deferShareReplay, switchEvery } from '@des-kit/rxjs';

import { SettingsService } from '../../settings/settings.service';
import { UsageErrorService } from '../../statistic/usage-error.service';
import { BackgroundFeaturesConst } from '../consts/background-features.const';

const settingsService = Container.get(SettingsService);
const usageErrorService = Container.get(UsageErrorService);

settingsService.settings$
    .pipe(
        map((settings) =>
            BackgroundFeaturesConst.filter((f) =>
                f.enabledOn ? settings[f.enabledOn] : true,
            ),
        ),
        switchEvery((feature) =>
            deferShareReplay(feature.featureFactory).pipe(
                usageErrorService.catchError(),
            ),
        ),
    )
    .subscribe();
