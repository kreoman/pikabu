import { firstValueFrom } from 'rxjs';
import { Service } from 'typedi';

import { PikabuAjaxApiService } from '../../../pikabu-ajax-api/pikabu-ajax-api.service';
import { PikabuIgnoreListAddRuleRequestModel } from '../models/pikabu-ignore-list-add-rule-request.model';

@Service()
export class PikabuIgnoreListApiService {
    constructor(private readonly pikabuAjaxApiService: PikabuAjaxApiService) {}

    async addForever(authorId: number): Promise<void> {
        const response$ = this.pikabuAjaxApiService.ajax<
            unknown,
            PikabuIgnoreListAddRuleRequestModel
        >('ignore_actions.php', {
            action: 'add_rule',
            period: 'forever',
            story_id: 0,
            keywords: '',
            tags: '',
            communities: '',
            authors: authorId,
        });

        await firstValueFrom(response$);
    }
}
