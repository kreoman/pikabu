import { PikabuAjaxActionRequestModel } from '../../../pikabu-ajax-api/models/pikabu-ajax-action-request.model';

export interface PikabuIgnoreListAddRuleRequestModel
    extends PikabuAjaxActionRequestModel {
    readonly action: 'add_rule';
    readonly period: string; // 'forever'
    readonly story_id: number; // 0
    readonly keywords: string; //
    readonly tags: string; //
    readonly communities: string; //
    readonly authors: number; //
}
