import { PikabuAjaxActionRequestModel } from '../../../pikabu-ajax-api/models/pikabu-ajax-action-request.model';

export interface PikabuDeleteCommentRequestModel
    extends PikabuAjaxActionRequestModel {
    readonly action: 'delete'; // create
    readonly id: number;
}
