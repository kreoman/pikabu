import { PikabuAjaxActionRequestModel } from '../../../pikabu-ajax-api/models/pikabu-ajax-action-request.model';

export interface PikabuPostCommentRequestModel
    extends PikabuAjaxActionRequestModel {
    readonly action: 'create'; // create
    readonly images: [];
    readonly desc: string; // <p>Тест</p>
    readonly parent_id: number; // 0;
    readonly story_id: number; // 11506444;
    readonly is_official: boolean; // false;
    readonly by_moderator: boolean; // false
    readonly ad_caption: string; //
}
