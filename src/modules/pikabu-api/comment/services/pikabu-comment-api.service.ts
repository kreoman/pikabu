import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Service } from 'typedi';

import { PikabuAjaxApiService } from '../../../pikabu-ajax-api/pikabu-ajax-api.service';
import { PikabuDeleteCommentRequestModel } from '../models/pikabu-delete-comment-request.model';
import { PikabuPostCommentRequestModel } from '../models/pikabu-post-comment-request.model';
import { PikabuPostCommentResponseDataModel } from '../models/pikabu-post-comment-response-data.model';

const ACTION_SCRIPT = 'comments_actions.php';

@Service()
export class PikabuCommentApiService {
    constructor(private readonly pikabuAjaxApiService: PikabuAjaxApiService) {}

    postText(
        storyId: number,
        text: string,
        parentCommentId: number = 0,
    ): Observable<number> {
        return this.post({
            action: 'create',
            story_id: storyId,
            parent_id: parentCommentId,
            desc: `<p>${text}</p>`,
            images: [],
            by_moderator: false,
            is_official: false,
            ad_caption: '',
        }).pipe(map((response) => response.comment_id));
    }

    delete(id: number): Observable<void> {
        return this.pikabuAjaxApiService
            .ajax<[], PikabuDeleteCommentRequestModel>(ACTION_SCRIPT, {
                action: 'delete',
                id,
            })
            .pipe(map(() => void 0));
    }

    post(
        request: PikabuPostCommentRequestModel,
    ): Observable<PikabuPostCommentResponseDataModel> {
        return this.pikabuAjaxApiService.ajax<
            PikabuPostCommentResponseDataModel,
            PikabuPostCommentRequestModel
        >(ACTION_SCRIPT, request);
    }
}
