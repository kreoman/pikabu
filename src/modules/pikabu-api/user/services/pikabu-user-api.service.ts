import { firstValueFrom } from 'rxjs';
import { Container, Service } from 'typedi';

import { Cache } from '@des-kit/cache';
import { IntervalMsEnum, isSet } from '@des-kit/core';
import { ExtensionRuntimeTypeEnum } from '@des-kit/extension';
import { BackgroundPromise } from '@des-kit/extension-bg';

import { PikabuAjaxApiService } from '../../../pikabu-ajax-api/pikabu-ajax-api.service';
import { PikabuMobileApiService } from '../../../pikabu-mobile-api/common/pikabu-mobile-api.service';
import { pikabuUserShortParse } from '../functions/user-short-parser/pikabu-user-short-parse';
import { PikabuMobileApiUserModel } from '../models/pikabu-mobile-api-user.model';
import { PikabuUserInfoRequestModel } from '../models/pikabu-user-info-request.model';
import { PikabuUserInfoResponseDataModel } from '../models/pikabu-user-info-response-data.model';
import { PikabuUserShortModel } from '../models/pikabu-user-short.model';

@Service()
export class PikabuUserApiService {
    private readonly pikabuMobileApiService = Container.get(
        PikabuMobileApiService,
    );
    private readonly pikabuAjaxApiService = Container.get(PikabuAjaxApiService);

    @Cache(IntervalMsEnum.Week, ExtensionRuntimeTypeEnum.Background)
    @BackgroundPromise('PikabuUserApiService.getIdByUsername')
    async getIdByUsername(username: string): Promise<number> {
        const data = await this.getFromPikabuByNameLongCache(username);

        if (!isSet(data?.id)) {
            throw new Error(`Не удалось определить ID пользователя`);
        }

        return data.id;
    }

    @Cache(IntervalMsEnum.Week, ExtensionRuntimeTypeEnum.Background)
    @BackgroundPromise('PikabuUserApiService.getFromPikabuByNameLongCache')
    async getFromPikabuByNameLongCache(
        username: string,
    ): Promise<PikabuUserShortModel | null> {
        return this.getFromPikabuByName(username);
    }

    @BackgroundPromise('PikabuUserApiService.getFromPikabuByName')
    async getFromPikabuByName(
        username: string,
    ): Promise<PikabuUserShortModel | null> {
        if (!username) {
            return null;
        }

        const html = await this.getShortUserInfoHtmlOrFailBg(username);

        return html ? pikabuUserShortParse(html) : null;
    }

    private async getShortUserInfoHtmlOrFailBg(
        username: string,
    ): Promise<string | null> {
        const response$ = this.pikabuAjaxApiService.ajax<
            PikabuUserInfoResponseDataModel,
            PikabuUserInfoRequestModel
        >(`user_info.php?${username}`, {
            action: 'get_short_profile',
            user_name: username,
        });

        const response = await firstValueFrom(response$);

        if (!response.html) {
            return null;
        }

        return response.html;
    }

    @Cache(IntervalMsEnum.Hour, ExtensionRuntimeTypeEnum.Background)
    @BackgroundPromise('PikabuUserApiService.fetchMobileApiUser')
    async fetchMobileApiUser(
        username: string,
    ): Promise<PikabuMobileApiUserModel> {
        const response = await this.pikabuMobileApiService.fetchData<{
            user: PikabuMobileApiUserModel;
        }>({
            controller: 'user.profile.get',
            body: {
                user_name: username,
            },
            queries: `${username}`,
        });

        return response.user;
    }
}
