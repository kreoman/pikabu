export const PikabuUserShortParseBaseMock = `
			<div class="profile profile_short" data-user-id="{id}">
				<div class="profile__header"><div class="background"  data-default="true"><div class="background__placeholder" ></div></div><div class="avatar avatar_big"><span class="avatar__default"></span></div>
				</div>
				<div class="profile__user">
					<div class="profile__nick-wrap">
						<h1 class="profile__nick">
							
							<a href="https://pikabu.ru/@{nickname}" itemprop="additionalName" title="{nickname}">{nickname}</a> 
		</h1>
						<div class="profile__user-information">на Пикабу</span> <span>{duration}</div>
			
					</div>  <div class="profile__button-group-wrap">
			<span class="button-group button-group_little  button-group_success" data-role="subscribe" data-type="user" data-full-ban="" data-avatar="" data-name="{nickname}" data-id="2574926" data-ignore-actions="ignore,ignore-comments" data-actions="">
				<button>Подписаться</button><button class="button_dot"></button>
			</span> </div>
				</div>
		
				<div class="profile__section profile__section_center profile__section_short">
					<span class="profile__digital hint"  aria-label="1&emsp14;549"><b>1549</b> <span>рейтинг</span></span>
					<span class="profile__digital" ><b>227</b> <span>комментариев</span></span>
					<span class="profile__digital" ><b>2</b> <span>подписчика</span></span>
					<span class="profile__digital" ><b>4</b> <span>поста</span></span>
				</div>
			
			</div>
		`;
