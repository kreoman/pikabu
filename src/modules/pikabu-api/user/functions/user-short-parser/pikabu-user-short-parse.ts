import { Duration, sub } from 'date-fns';
import DomParser from 'dom-parser';

import { PikabuUserShortModel } from '../../models/pikabu-user-short.model';

function parseDuration(durationStr: string): Duration {
    const duration: Duration = {};
    const units: { [key in keyof Duration]: string[] } = {
        years: ['год', 'лет'],
        months: ['мес'],
        weeks: ['нед'],
        days: ['д', 'сут'],
        hours: ['час'],
    };
    const tokens = durationStr.split(' ');

    for (let i = 0; i < tokens.length; i += 2) {
        const value = tokens[i] ?? '';
        const unitFull = tokens[i + 1] ?? '';

        for (const unitKey in units) {
            const k = unitKey as keyof Duration;
            const unitSubstrings = units[k]!;

            if (unitSubstrings.some((start) => unitFull.startsWith(start))) {
                duration[k] = Number(value);
            }
        }
    }

    return duration;
}

function getElementByClassName(
    domSearchable: DomParser.DOMSearchable,
    className: string,
): DomParser.Node | null {
    const result = domSearchable.getElementsByClassName(className);

    return result ? result[0] : null;
}

function getElementByTagName(
    domSearchable: DomParser.DOMSearchable,
    tagName: string,
): DomParser.Node | null {
    const result = domSearchable.getElementsByTagName(tagName);

    return result ? result[0] : null;
}

export function pikabuUserShortParse(html: string): PikabuUserShortModel {
    const parser = new DomParser();

    const domFragment = parser.parseFromString(html);
    const profileElement =
        getElementByClassName(domFragment, 'profile_short') ??
        getElementByClassName(domFragment, 'mini-profile');

    if (!profileElement) {
        throw new Error('Не удалось получить элемент профиля');
    }

    const idStr = profileElement.getAttribute('data-user-id');

    if (!idStr) {
        throw new Error('Не удалось получить идентификатор пользователя');
    }

    const id = Number(idStr);

    const userInformationElement =
        getElementByClassName(profileElement, 'profile__user-information') ??
        getElementByClassName(profileElement, 'mini-profile__signup-info');

    if (!userInformationElement) {
        throw new Error('Не удалось получить элемент срока регистрации');
    }

    const [durationElement] =
        userInformationElement.getElementsByTagName('span') ?? [];

    const durationStr = durationElement?.textContent ?? '';
    const regDuration = parseDuration(durationStr);

    const profileNickElement =
        getElementByClassName(profileElement, 'profile__nick') ??
        getElementByTagName(profileElement, 'h2');

    if (!profileNickElement) {
        throw new Error(
            'Не удалось получить элемент с никнеймом пользователя (блок)',
        );
    }

    const nicknameElement = getElementByTagName(profileNickElement, 'a');

    if (!nicknameElement) {
        throw new Error(
            'Не удалось получить элемент с никнеймом пользователя (ссылка)',
        );
    }

    const nickname =
        nicknameElement.textContent ?? nicknameElement.getAttribute('title');

    if (!nickname) {
        throw new Error('Не удалось получить никнейм пользователя');
    }

    const registrationDate = sub(new Date(), regDuration);

    let note;

    const noteElement = getElementByClassName(
        profileElement,
        'mini-profile__note-inner',
    );

    if (noteElement) {
        note = noteElement.textContent;
    }

    return {
        id,
        nickname,
        registrationDateMs: registrationDate.getTime(),
        note,
    };
}
