import { faker } from '@faker-js/faker';
import { addHours, subHours } from 'date-fns';

import { pikabuUserShortParse } from './pikabu-user-short-parse';
import { PikabuUserShortParseBaseMock } from './pikabu-user-short-parse-base.mock';
import { PikabuUserShortParseBaseV2Mock } from './pikabu-user-short-parse-base-v2.mock';

describe(pikabuUserShortParse.name, () => {
    jest.useFakeTimers().setSystemTime(new Date('2020-01-01'));
    const durationStr = '5 лет 4 месяца 3 недели 2 дня';
    const timeMs = 1407538800000;
    const timeMin = subHours(timeMs, 1).getTime();
    const timeMax = addHours(timeMs, 1).getTime();

    describe('Обычный ответ', () => {
        const id = faker.number.int();
        const nickname = faker.internet.userName();

        const response = PikabuUserShortParseBaseMock.replaceAll(
            '{id}',
            String(id),
        )
            .replaceAll('{nickname}', nickname)
            .replaceAll('{duration}', durationStr);

        it('Ожидаем корректный парсинг пользователя', () => {
            const user = pikabuUserShortParse(response);

            expect(user).toMatchObject({
                id: id,
                nickname: nickname,
            });

            expect(user.registrationDateMs).toBeGreaterThanOrEqual(timeMin);
            expect(user.registrationDateMs).toBeLessThanOrEqual(timeMax);
        });
    });

    describe('Обычный ответ v2', () => {
        const id = faker.number.int();
        const nickname = faker.internet.userName();
        const note = faker.lorem.paragraph();

        const response = PikabuUserShortParseBaseV2Mock.replaceAll(
            '{id}',
            String(id),
        )
            .replaceAll('{nickname}', nickname)
            .replaceAll('{duration}', durationStr)
            .replaceAll('{note}', note);

        it('Ожидаем корректный парсинг пользователя', () => {
            const user = pikabuUserShortParse(response);

            expect(user).toMatchObject({
                id,
                nickname,
                note,
            });

            expect(user.registrationDateMs).toBeGreaterThanOrEqual(timeMin);
            expect(user.registrationDateMs).toBeLessThanOrEqual(timeMax);
        });
    });
});
