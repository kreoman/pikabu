export const PikabuUserShortParseBaseV2Mock = `<div class="mini-profile" data-user-id="{id}">
    <div class="mini-profile__main">
        <div class="avatar avatar_big mini-profile__avatar"><img
            src="https://cs13.pikabu.ru/avatars/2399/v{id}-690744288.png" alt="{nickname}"></div>
        <div class="pkb-profile-username">
<span
    class="pkb-profile-rating-counter hint"
    aria-label="37&emsp14;685"
>
37.7K
</span>

            <h2>
                <a href="https://pikabu.ru/@{nickname}" title="{nickname}">{nickname}</a>
            </h2>
            <div class="pkb-profile-info-icons"></div>
        </div>

        <div class="mini-profile__signup-info">пикабушник</span> <span>{duration}</div>
        <div class="mini-profile__note"><pre class="mini-profile__note-inner">{note}</pre></div>
    </div>

    <div class="mini-profile__btns">
        <button class="pkb-normal-btn pkb-normal-btn_border pkb-normal-btn_with-icon mini-profile__support-btn"
                data-subs-count="0">
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon--ui__ruble_16">
                <use xlink:href="#icon--ui__ruble_16"></use>
            </svg>
            Поддержать
        </button>
        <div
            class="button-group button-group_success"
            data-role="subscribe"
            data-type="user"
            data-full-ban=""
            data-avatar="https://cs13.pikabu.ru/avatars/2399/x{id}-690744288.png"
            data-name="{nickname}"
            data-id="{id}"
            data-ignore-actions="ignore,ignore-comments"
            data-actions=""
        >
            <button>Подписаться</button>
            <button class="button_dot"></button>
        </div>
        <span class="mini-profile__user-notification-bell icon-selector-btn" data-subscribed="0"
              data-notification-subs="0"><svg xmlns="http://www.w3.org/2000/svg"
                                              class="icon icon--ui__notification-stroke"><use
            xlink:href="#icon--ui__notification-stroke"></use></svg></span>
    </div>

    <div class="mini-profile__stats">
        <span class="mini-profile__stat hint" aria-label="65"><b>65</b> <span>подписчиков</span></span>
        <span class="mini-profile__stat hint" aria-label="24"><b>24</b> <span>постов</span></span>
        <span class="mini-profile__stat hint" aria-label="13"><b>13</b> <span>в горячем</span></span>
    </div>

</div>
`;
