export interface PikabuUserShortModel {
    readonly id: number;
    readonly nickname: string;
    readonly registrationDateMs: number;
    readonly note?: string;
}
