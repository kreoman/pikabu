export interface PikabuMobileApiUserModel {
    readonly current_user_id: number; // 0
    readonly user_id: string; // 7447432
    readonly user_name: string; // Pikabu.Fixes
    readonly rating: string; // 515
    readonly gender: string; // 0
    readonly comments_count: number; // 106
    readonly stories_count: number; // 6
    readonly stories_hot_count: number; // 1
    readonly pluses_count: number; // 1647
    readonly minuses_count: number; // 2765
    readonly signup_date: string; // 1692201173
    readonly is_rating_ban: boolean; // false
    readonly avatar: string; // https://cs15.pikabu.ru/avatars/7447/x7447432-1320228063.png
    readonly awards: unknown[];
    readonly is_subscribed: boolean; // false
    readonly is_subscribed_to_notifications: boolean; // false
    readonly is_ignored: boolean; // false
    readonly is_ignored_in_stories: boolean; // false
    readonly is_ignored_in_comments: boolean; // false
    readonly note: string; //
    readonly approved: string; //
    readonly user_has_paid_options: boolean; // false
    readonly communities: unknown[];
    readonly subscribers_count: number; // 8
    readonly is_user_banned: boolean; // false
    readonly is_user_fully_banned: boolean; // false
    readonly public_ban_history: unknown[];
    readonly user_ban_time: null;
    readonly about: string; // Расширение для отображения минусов: https://chromewebstore.google.com/detail/nochogffendelpmbpajbdpaodbibgdmk\nКанал расширения: https://t.me/pikabu_fixes
    readonly is_slow_mode_enabled: boolean; // false
    readonly is_show_extended_profile: boolean; // true
    readonly slow_mode_text: null;
    readonly registration_duration: string; // 1 год 4 месяца 3 недели 1 день
    readonly can_block_author: boolean; // true
    readonly is_advert_blogs_user: boolean; // false
    readonly cedit_changes_count: null;
    readonly cedit_votes_count: null;
    readonly header_bg_url: string; //
    readonly header_bg_forbidden: boolean; // false
    readonly stories_queue_count: number; // 0
    readonly series_count: number; // 1
    readonly rkn_blogger_url: string; //
    readonly is_rkn_banned: boolean; // false
}
