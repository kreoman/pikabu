import { PikabuAjaxActionRequestModel } from '../../../pikabu-ajax-api/models/pikabu-ajax-action-request.model';

export interface PikabuUserInfoRequestModel
    extends PikabuAjaxActionRequestModel {
    action: 'get_short_profile';
    user_name: string;
}
