import { firstValueFrom } from 'rxjs';
import { Container, Service } from 'typedi';

import { Cache } from '@des-kit/cache';
import { IntervalMsEnum } from '@des-kit/core';
import { ExtensionRuntimeTypeEnum } from '@des-kit/extension';
import { BackgroundPromise } from '@des-kit/extension-bg';

import { PikabuMobileAuthApiFetchStrategyEnum } from '../../../pikabu-mobile-api/auth/pikabu-mobile-auth-api-fetch-strategy.enum';
import { PikabuMobileAuthApiService } from '../../../pikabu-mobile-api/auth/services/pikabu-mobile-auth-api.service';
import { PikabuMobileApiRequestModel } from '../../../pikabu-mobile-api/common/models/pikabu-mobile-api-request.model';
import { PikabuMobileApiService } from '../../../pikabu-mobile-api/common/pikabu-mobile-api.service';
import { PikabuStoryApiError } from '../errors/pikabu-story-api.error';
import { MobileStoryDataItemModel } from '../models/data/mobile-story-data-item.model';
import { MobileStoryDataVideoModel } from '../models/data/mobile-story-data-video.model';
import { MobileCommentModel } from '../models/mobile-comment.model';
import { MobileStoryModel } from '../models/mobile-story.model';
import { MobileStoryDataType } from '../models/mobile-story-data.type';
import { MobileStoryFirstPageResponseModel } from '../models/mobile-story-first-page-response.model';
import { MobileStoryPageResponseModel } from '../models/mobile-story-page-response.model';

@Service()
export class PikabuStoryApiService {
    private readonly pikabuMobileApiService = Container.get(
        PikabuMobileApiService,
    );
    private readonly pikabuMobileAuthApiService = Container.get(
        PikabuMobileAuthApiService,
    );
    private readonly authEnabled$ =
        this.pikabuMobileAuthApiService.authEnabled$;

    async getStoryComment(
        storyId: number,
        commentId: number,
        adult?: boolean,
    ): Promise<MobileCommentModel> {
        const storyComments = await this.getStoryComments(storyId, adult);

        const comment = storyComments.get(commentId);

        if (!comment) {
            throw new PikabuStoryApiError(`Данные комментария не найдены`);
        }

        return comment;
    }

    @Cache(IntervalMsEnum.Second * 30)
    async getStoryVideoByWebm(
        storyId: number,
        webmUrl: string,
        adult?: boolean,
    ): Promise<MobileStoryDataVideoModel | null> {
        const map = await this.getStoryVideosMapByWebm(storyId, adult);

        const video = map.get(webmUrl);

        if (!video) {
            throw new PikabuStoryApiError(`Видео не найдено`);
        }

        return video;
    }

    @Cache(IntervalMsEnum.Second * 30)
    async getStoryVideosMapByWebm(
        storyId: number,
        adult?: boolean,
    ): Promise<ReadonlyMap<string, MobileStoryDataVideoModel>> {
        const videos = await this.getStoryVideos(storyId, adult);

        return new Map<string, MobileStoryDataVideoModel>(
            videos.map((item) => [item.data.webm.url, item.data]),
        );
    }

    @Cache(IntervalMsEnum.Second * 30)
    async getStoryVideos(
        storyId: number,
        adult?: boolean,
    ): Promise<MobileStoryDataItemModel<'vf', MobileStoryDataVideoModel>[]> {
        const story = await this.getStory(storyId, adult);

        function isVideo(
            item: MobileStoryDataType,
        ): item is MobileStoryDataItemModel<'vf', MobileStoryDataVideoModel> {
            return item.type === 'vf';
        }

        return story.story_data.filter(isVideo);
    }

    @Cache(IntervalMsEnum.Second * 5)
    @BackgroundPromise('PikabuStoryApiService.getStory')
    async getStory(
        storyId: number,
        adult?: boolean,
    ): Promise<MobileStoryModel> {
        const useAuth = await firstValueFrom(this.authEnabled$);
        const firstPage = await this.getStoryPage(storyId, 1, useAuth, adult);

        return firstPage.story;
    }

    @Cache(IntervalMsEnum.Second * 5)
    @BackgroundPromise('PikabuStoryApiService.getStoryComments')
    private async getStoryComments(
        storyId: number,
        adult?: boolean,
    ): Promise<ReadonlyMap<number, MobileCommentModel>> {
        const allPages = await this.getStoryAllPagesBg(storyId, adult);

        return new Map<number, MobileCommentModel>(
            allPages?.comments.map((comment) => [comment.comment_id, comment]),
        );
    }

    @Cache(IntervalMsEnum.Second * 30, ExtensionRuntimeTypeEnum.Background)
    private async getStoryAllPagesBg(
        storyId: number,
        adult?: boolean,
    ): Promise<MobileStoryFirstPageResponseModel | null> {
        const authEnabled = await firstValueFrom(this.authEnabled$);
        const firstPage = await this.getStoryPage(
            storyId,
            1,
            authEnabled,
            adult,
        );

        if (firstPage.story.is_adult) {
            adult = true;
        }

        const commentsCount = firstPage.story.comments_count ?? 0;
        const pagesCount = Math.ceil(commentsCount / 100);

        if (pagesCount === 1) {
            return firstPage;
        }

        let allComments: readonly MobileCommentModel[] = firstPage.comments;
        const otherPagesRequests: Promise<MobileStoryPageResponseModel>[] = [];

        for (let page = 2; page <= pagesCount; page++) {
            otherPagesRequests.push(
                this.getStoryPage(storyId, page, authEnabled, adult),
            );
        }

        const otherPagesResponses = await Promise.all(otherPagesRequests);

        for (const response of otherPagesResponses) {
            allComments = allComments.concat(response.comments);
        }

        return {
            story: firstPage.story,
            comments: allComments,
        };
    }

    async getStoryPage(
        storyId: number,
        page: 1,
        auth: boolean,
        adult?: boolean,
    ): Promise<MobileStoryFirstPageResponseModel>;
    async getStoryPage(
        storyId: number,
        page: number,
        auth: boolean,
        adult?: boolean,
    ): Promise<MobileStoryPageResponseModel>;
    @Cache(IntervalMsEnum.Second * 30, ExtensionRuntimeTypeEnum.Background)
    @BackgroundPromise('PikabuStoryApiService.getStoryPage')
    async getStoryPage(
        storyId: number,
        page: 1 | number,
        auth: boolean, // используется исключительно для разделения кэша
        adult?: boolean,
    ): Promise<any> {
        return this.fetchStoryPageWithoutCache(storyId, page, adult);
    }

    async fetchStoryPageWithoutCache(
        storyId: number,
        page: 1,
        adult?: boolean,
    ): Promise<MobileStoryFirstPageResponseModel>;
    async fetchStoryPageWithoutCache(
        storyId: number,
        page: number,
        adult?: boolean,
    ): Promise<MobileStoryPageResponseModel>;
    @BackgroundPromise('PikabuStoryApiService.fetchStoryPageWithoutCache')
    async fetchStoryPageWithoutCache(
        storyId: number,
        page: 1 | number,
        adult = false,
    ): Promise<any> {
        const authEnabled = await firstValueFrom(this.authEnabled$);

        const request: PikabuMobileApiRequestModel = {
            controller: `story.get`,
            queries: `_story_id=${storyId}&_page=${page}`,
            body: {
                story_id: storyId,
                page: page,
            },
        };

        if (!authEnabled) {
            if (adult) {
                throw new PikabuStoryApiError(
                    `Невозможно получить данные этого поста без авторизации`,
                );
            }

            return this.pikabuMobileApiService.fetchData(request);
        }

        const fetchStrategy = adult
            ? PikabuMobileAuthApiFetchStrategyEnum.AuthThenCommon
            : PikabuMobileAuthApiFetchStrategyEnum.CommonThenAuth;

        return this.pikabuMobileAuthApiService.fetchData<MobileStoryFirstPageResponseModel>(
            request,
            fetchStrategy,
        );
    }
}
