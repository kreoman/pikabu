import { MobileImageAnimationModel } from './mobile-image-animation.model';

export interface MobileImageModel {
    readonly small: string;
    readonly large?: string;
    readonly animation?: MobileImageAnimationModel;
    readonly img_size: readonly [number | null, number | null] | null;
}
