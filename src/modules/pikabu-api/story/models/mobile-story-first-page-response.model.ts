import { MobileStoryModel } from './mobile-story.model';
import { MobileStoryPageResponseModel } from './mobile-story-page-response.model';

export interface MobileStoryFirstPageResponseModel
    extends MobileStoryPageResponseModel {
    readonly story: MobileStoryModel;
}
