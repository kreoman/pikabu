export interface MobileStoryDataVideoEmbedModel {
    readonly duration: number; // 142;
    readonly img_size: [number, number]; // [480, 360];
    readonly ratio: number; // 1.78;
    readonly thumb: string; // 'https://i.ytimg.com/vi/r7ewrU9kySg/hqdefault.jpg';
    readonly url: string; // 'https://www.youtube.com/embed/r7ewrU9kySg';
}
