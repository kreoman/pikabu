export interface MobileStoryDataVideoFileModel {
    readonly url: string;
    readonly size: number;
}

export interface MobileStoryDataVideoModel {
    readonly fid: number;
    readonly width: number;
    readonly height: number;
    readonly duration: number; // sec
    readonly is_muted: boolean;
    readonly av1: MobileStoryDataVideoFileModel;
    readonly dash_av1: MobileStoryDataVideoFileModel;
    readonly hls: MobileStoryDataVideoFileModel;
    readonly mp4: MobileStoryDataVideoFileModel;
    readonly thumb: string; // 'https://cs14.pikabu.ru/video/2024/05/13/1715621459287134057_61c0ded6_1080x1920.jpg';
    readonly webm: MobileStoryDataVideoFileModel;
}
