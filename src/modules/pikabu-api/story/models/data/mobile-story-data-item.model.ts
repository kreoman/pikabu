export interface MobileStoryDataItemModel<T extends string, TData> {
    id: string;
    type: T;
    data: TData;
}
