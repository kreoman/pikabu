export interface MobileStoryDataImageAnimationModel {
    readonly formats: Record<string, unknown>;
    readonly preview: string; // '';
    readonly raw_url: string; // 'https://cs13.pikabu.ru/post_img/2024/06/14/10/1718383567175559526.jpg';
}

export interface MobileStoryDataImageModel {
    readonly animations: MobileStoryDataImageAnimationModel;
    readonly carousel_id: string; // '';
    readonly img_size: [number, number]; // [700, 438];
    readonly large: string; // 'https://cs13.pikabu.ru/post_img/big/2024/06/14/10/1718383567175559526.jpg';
    readonly small: string; // 'https://cs13.pikabu.ru/post_img/2024/06/14/10/1718383567175559526.webp';
    readonly title: string; // 'Будь как Рон!';
}
