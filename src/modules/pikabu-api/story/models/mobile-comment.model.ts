import { MobileCommentDescModel } from './mobile-comment-desc.model';

export interface MobileCommentModel {
    readonly comment_id: number;
    readonly comment_rating: number | null;
    readonly comment_pluses: number | null;
    readonly comment_minuses: number | null;
    readonly comment_platform: number;
    readonly curr_user_vote: number;

    readonly comment_time: number;

    readonly story_id: number;
    readonly parent_id: number;
    readonly is_deleted: boolean;

    readonly comment_desc: MobileCommentDescModel;

    readonly user_id: number;
    readonly user_name: string;
}
