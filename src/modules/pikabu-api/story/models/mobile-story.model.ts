import { MobileStoryDataType } from './mobile-story-data.type';

export interface MobileStoryModel {
    readonly story_id: number;
    readonly parent_story_id: number | null;

    readonly story_time: number;
    readonly story_title: string;
    readonly story_url: string;
    readonly story_data: MobileStoryDataType[];

    readonly story_pluses: number | null;
    readonly story_minuses: number | null;
    readonly story_digs: number | null;

    readonly tags: string[];

    readonly community_id: number | null;

    readonly comments_count: number;

    readonly user_id: number;
    readonly user_name: string;

    readonly is_adult: boolean;
    readonly is_authors: boolean;
}
