export interface MobileImageAnimationFormatsModel {
    readonly gif: number; // 941;
    readonly mp4: number; // 144;
    readonly webm: number; // 587;
}
