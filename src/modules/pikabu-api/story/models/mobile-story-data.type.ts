import { MobileStoryDataImageModel } from './data/mobile-story-data-image.model';
import { MobileStoryDataItemModel } from './data/mobile-story-data-item.model';
import { MobileStoryDataVideoModel } from './data/mobile-story-data-video.model';
import { MobileStoryDataVideoEmbedModel } from './data/mobile-story-data-video-embed.model';

export type MobileStoryDataType =
    | MobileStoryDataItemModel<'vf', MobileStoryDataVideoModel>
    | MobileStoryDataItemModel<'v', MobileStoryDataVideoEmbedModel>
    | MobileStoryDataItemModel<'i', MobileStoryDataImageModel>
    | MobileStoryDataItemModel<'t', string>;
