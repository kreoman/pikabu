export interface MobileCommentVideoEmbedModel {
    duration: number; // 10;
    height: number; // 360;
    ratio: number; // 1.7777777778;
    thumb: string; // 'https://cs12.pikabu.ru/video/2022/04/12/9/1649778074274172331.jpg';
    url: string; // 'https://www.youtube.com/watch?v=iTJ4yea3q78';
    width: number; // 480;
}
