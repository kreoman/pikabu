import { MobileCommentModel } from './mobile-comment.model';

export interface MobileStoryPageResponseModel {
    readonly comments: readonly MobileCommentModel[];
}
