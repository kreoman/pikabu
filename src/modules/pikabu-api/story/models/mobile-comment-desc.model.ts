import { MobileCommentVideoEmbedModel } from './mobile-comment-video-embed.model';
import { MobileImageModel } from './mobile-image.model';

export interface MobileCommentDescModel {
    readonly images: readonly MobileImageModel[];
    readonly text: string;
    readonly videos: readonly MobileCommentVideoEmbedModel[];
}
