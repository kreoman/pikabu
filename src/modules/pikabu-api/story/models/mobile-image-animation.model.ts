import { MobileImageAnimationFormatsModel } from './mobile-image-animation-formats.model';

export interface MobileImageAnimationModel {
    readonly formats: MobileImageAnimationFormatsModel;
    readonly preview: string;
    readonly raw_url: string;
}
