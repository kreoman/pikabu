import { defer, filter, Observable, of, repeat, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Container, Service } from 'typedi';

import { Cache } from '@des-kit/cache';
import { IntervalMsEnum } from '@des-kit/core';

import { UsageEventService } from '../../../statistic/usage-event.service';
import { PikabuUserNoteModel } from '../models/pikabu-user-note.model';
import { PikabuUserNoteDataService } from './pikabu-user-note-data.service';

@Service()
export class PikabuUserNoteTagsService {
    private readonly usageEventService = Container.get(UsageEventService);
    private readonly pikabuUserNoteDataService = Container.get(
        PikabuUserNoteDataService,
    );

    private readonly updatedUsername$ = new Subject<string>();

    @Cache(IntervalMsEnum.Second * 5)
    getTags(username: string, hasNote: boolean): Observable<string[]> {
        let firstGet = true;
        const updated$ = this.updatedUsername$.pipe(
            filter((updatedUsername) => updatedUsername === username),
        );

        const getTags$ = defer(() =>
            this.pikabuUserNoteDataService.getNote(username),
        ).pipe(map((note) => Array.from(note.data.tags)));

        return defer(() => {
            if (firstGet && !hasNote) {
                firstGet = false;
                return of([]);
            }

            return getTags$;
        }).pipe(
            repeat({
                delay: () => updated$,
            }),
        );
    }

    async addTag(username: string, tag: string): Promise<void> {
        this.usageEventService.register('extensionEventNoteTagModify', {
            adding: true,
            removing: false,
        });

        await this.reduceTags(username, async (tags) => {
            tags.add(tag);

            return tags;
        });
    }

    async removeTag(username: string, tag: string): Promise<void> {
        this.usageEventService.register('extensionEventNoteTagModify', {
            adding: false,
            removing: true,
        });

        await this.reduceTags(username, async (tags) => {
            tags.delete(tag);

            return tags;
        });
    }

    private async reduceTags(
        username: string,
        reducer: (tags: Set<string>) => Promise<ReadonlySet<string>>,
    ): Promise<void> {
        const note = await this.pikabuUserNoteDataService.getNote(username);

        const tags = new Set(note.data.tags);

        const tagsToUpdate = await reducer(tags);

        const noteToUpdate: PikabuUserNoteModel = {
            ...note,
            data: {
                ...note.data,
                tags: [...tagsToUpdate],
            },
        };

        await this.pikabuUserNoteDataService.setNote(username, noteToUpdate);

        this.updatedUsername$.next(username);
    }
}
