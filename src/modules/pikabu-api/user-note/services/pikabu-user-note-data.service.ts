import { Container, Service } from 'typedi';

import { PikabuUserApiService } from '../../user/services/pikabu-user-api.service';
import { PikabuUserNoteMapper } from '../mappers/pikabu-user-note.mapper';
import { PikabuUserNoteModel } from '../models/pikabu-user-note.model';
import { PikabuUserNoteApiService } from './pikabu-user-note-api.service';

@Service()
export class PikabuUserNoteDataService {
    private readonly pikabuUserApiService = Container.get(PikabuUserApiService);
    private readonly pikabuUserNoteApiService = Container.get(
        PikabuUserNoteApiService,
    );

    async getNote(username: string): Promise<PikabuUserNoteModel> {
        const userShort = await this.pikabuUserApiService.getFromPikabuByName(
            username,
        );

        return PikabuUserNoteMapper.parse(userShort?.note ?? '');
    }

    async setNote(username: string, note: PikabuUserNoteModel): Promise<void> {
        const userId = await this.pikabuUserApiService.getIdByUsername(
            username,
        );
        const rawNote = PikabuUserNoteMapper.stringify(note);

        await this.pikabuUserNoteApiService.addNote(userId, rawNote);
    }
}
