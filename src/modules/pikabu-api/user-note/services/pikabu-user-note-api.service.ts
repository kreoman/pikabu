import { firstValueFrom } from 'rxjs';
import { Container, Service } from 'typedi';

import { isSet } from '@des-kit/core';
import { HttpService } from '@des-kit/extension-bg-http';

import { PikabuAjaxApiService } from '../../../pikabu-ajax-api/pikabu-ajax-api.service';
import { parsePikabuUserNotes } from '../functions/parse-pikabu-user-notes';
import { parsePikabuUserNotesPages } from '../functions/parse-pikabu-user-notes-pages';

type FetchProgress = (currentPage: number, found: number) => void;

@Service()
export class PikabuUserNoteApiService {
    private readonly pikabuCommonApiService =
        Container.get(PikabuAjaxApiService);

    async fetchNotes(
        progress: FetchProgress = () => void 0,
    ): Promise<Map<number, string>> {
        return this.fetchNotesStartAt(1, progress);
    }

    async addNote(userId: number, text: string): Promise<void> {
        const result = await firstValueFrom(
            this.pikabuCommonApiService.ajax('users_actions.php', {
                action: 'note+',
                message: text,
                id: userId,
            }),
        );
    }

    private async fetchNotesStartAt(
        page: number,
        progress: FetchProgress,
    ): Promise<Map<number, string>> {
        const httpService = Container.get(HttpService);
        const url = `https://pikabu.ru/notes${page > 1 ? `?page=${page}` : ''}`;
        const html = await firstValueFrom(
            httpService.fetchText({
                method: 'GET',
                url,
                parseResponseAsText: 'windows-1251',
            }),
        );

        let notes = parsePikabuUserNotes(html);

        const pages = parsePikabuUserNotesPages(html);

        if (isSet(pages) && pages > page) {
            progress(page, pages);

            const nextPageNotes = await this.fetchNotesStartAt(
                page + 1,
                progress,
            );
            notes = new Map([...notes, ...nextPageNotes]);
        }

        return notes;
    }
}
