import { PikabuUserNoteDataModel } from './pikabu-user-note-data.model';

export interface PikabuUserNoteModel {
    readonly noteBefore: string;
    readonly data: PikabuUserNoteDataModel;
    readonly noteAfter: string;
}
