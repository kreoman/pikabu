import DomParser from 'dom-parser';

export function parsePikabuUserNotes(html: string): Map<number, string> {
    const notes = new Map<number, string>();

    const parser = new DomParser();

    const dom = parser.parseFromString(html);

    const noteNodes = dom.getElementsByClassName('settings-note') ?? [];

    for (const noteNode of noteNodes) {
        const userIdStr = noteNode.getAttribute('data-id');
        const userId = Number(userIdStr);

        if (Number.isNaN(userId)) {
            continue;
        }

        const noteContent = noteNode.getElementsByClassName(
            'settings-note__content',
        );

        if (!noteContent || noteContent.length === 0) {
            continue;
        }

        const noteText = noteContent[0].textContent;

        notes.set(userId, noteText);
    }

    return notes;
}
