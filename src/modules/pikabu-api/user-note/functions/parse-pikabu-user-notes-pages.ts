import DomParser from 'dom-parser';

export function parsePikabuUserNotesPages(html: string): number | null {
    const domParser = new DomParser();
    const dom = domParser.parseFromString(html);

    const pageNodes = dom.getElementsByClassName('pagination__page');

    if (!pageNodes) {
        return null;
    }

    let maxPage = 0;

    for (const pageNode of pageNodes) {
        const pageLink = pageNode.getAttribute('href');
        const regexResult = /page=([0-9]+)/.exec(pageLink ?? '');
        if (!regexResult) {
            continue;
        }

        const pageNumber = Number(regexResult[1]);

        maxPage = Math.max(maxPage, pageNumber);
    }

    return maxPage || null;
}
