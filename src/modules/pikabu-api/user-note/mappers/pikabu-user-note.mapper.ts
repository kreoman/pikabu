import { PikabuUserNoteModel } from '../models/pikabu-user-note.model';
import { PikabuUserNoteDataMapper } from './pikabu-user-note-data.mapper';

const SEPARATOR = 'PF';

export abstract class PikabuUserNoteMapper {
    static parse(data: string): PikabuUserNoteModel {
        const segments = PikabuUserNoteMapper.parseSegments(data);

        return {
            noteBefore: segments[0],
            data: PikabuUserNoteDataMapper.parse(segments[1]),
            noteAfter: segments[2],
        };
    }

    static stringify(model: PikabuUserNoteModel): string {
        return PikabuUserNoteMapper.stringifySegments([
            model.noteBefore,
            PikabuUserNoteDataMapper.stringify(model.data),
            model.noteAfter,
        ]);
    }

    private static parseSegments(note: string): [string, string, string] {
        const regexp = new RegExp(
            `\n?\\[${SEPARATOR}\](.*?)\\[\/${SEPARATOR}\]\n?`,
            'gms',
        );

        const [noteBefore, dataWrapped, noteAfter] = note.split(regexp);

        return [noteBefore ?? '', dataWrapped ?? '', noteAfter ?? ''];
    }

    private static stringifySegments([
        noteBefore,
        data,
        noteAfter,
    ]: string[]): string {
        let dataWrapped = '';

        if (data) {
            dataWrapped = `\n[${SEPARATOR}]${data}[/${SEPARATOR}]\n`;
        }

        return `${noteBefore}${dataWrapped}${noteAfter}`;
    }
}
