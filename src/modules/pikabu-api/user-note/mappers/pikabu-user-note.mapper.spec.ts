import { faker } from '@faker-js/faker';

import { PikabuUserNoteModel } from '../models/pikabu-user-note.model';
import { PikabuUserNoteMapper } from './pikabu-user-note.mapper';
import { PikabuUserNoteDataMapper } from './pikabu-user-note-data.mapper';

function createTags() {
    return faker.helpers.multiple(() => faker.lorem.word(), {
        count: {
            min: 1,
            max: 10,
        },
    });
}

describe('PikabuUserNoteModel', () => {
    it('Пустая строка должна парситься как объект с пустыми данными', () => {
        const obj = PikabuUserNoteMapper.parse('');

        expect(obj).toMatchObject({
            noteBefore: '',
            data: PikabuUserNoteDataMapper.parse(''),
            noteAfter: '',
        });
    });

    it('Строка с простым комментом отображается в noteBefore', () => {
        const sample = faker.lorem.paragraph();
        const obj = PikabuUserNoteMapper.parse(sample);

        expect(obj).toMatchObject({
            noteBefore: sample,
            data: PikabuUserNoteDataMapper.parse(''),
            noteAfter: '',
        });
    });

    it('Объект с пустыми данными должен сериализоваться как пустая строка', () => {
        const string = PikabuUserNoteMapper.stringify({
            noteBefore: '',
            data: PikabuUserNoteDataMapper.parse(''),
            noteAfter: '',
        });

        expect(string).toBe('');
    });

    describe('Корректная сериализация/десериализация', () => {
        it('noteBefore: true, data: true, noteAfter: true', () => {
            const initialNote: PikabuUserNoteModel = {
                noteBefore: faker.lorem.paragraph(),
                data: {
                    tags: createTags(),
                },
                noteAfter: faker.lorem.paragraph(),
            };

            const string = PikabuUserNoteMapper.stringify(initialNote);

            const parsedNote = PikabuUserNoteMapper.parse(string);

            expect(initialNote).toEqual(parsedNote);
        });

        it('noteBefore: false, data: true, noteAfter: false', () => {
            const initialNote: PikabuUserNoteModel = {
                noteBefore: '',
                data: {
                    tags: createTags(),
                },
                noteAfter: '',
            };

            const string = PikabuUserNoteMapper.stringify(initialNote);

            const parsedNote = PikabuUserNoteMapper.parse(string);

            expect(initialNote).toEqual(parsedNote);
        });

        it('noteBefore: true, data: false, noteAfter: false', () => {
            const initialNote: PikabuUserNoteModel = {
                noteBefore: faker.lorem.paragraph(),
                data: {
                    tags: [],
                },
                noteAfter: '',
            };

            const string = PikabuUserNoteMapper.stringify(initialNote);

            const parsedNote = PikabuUserNoteMapper.parse(string);

            expect(initialNote).toEqual(parsedNote);
        });
    });
});
