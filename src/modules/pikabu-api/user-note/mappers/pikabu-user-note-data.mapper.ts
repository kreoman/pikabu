import { PikabuUserNoteDataModel } from '../models/pikabu-user-note-data.model';

const TAGS_KEY = 't';

export abstract class PikabuUserNoteDataMapper {
    static parse(data: string): PikabuUserNoteDataModel {
        if (!data) {
            return {
                tags: [],
            };
        }

        const tagsString = PikabuUserNoteDataMapper.parseKey(data, TAGS_KEY);

        const tags = tagsString.split(',').map((tag) => tag.trim());

        return {
            tags,
        };
    }

    static stringify(model: PikabuUserNoteDataModel): string {
        if (!model.tags.length) {
            return '';
        }

        const tagsString = model.tags.map((tag) => tag.trim()).join(', ');

        return PikabuUserNoteDataMapper.stringifyKey(TAGS_KEY, tagsString);
    }

    private static parseKey(data: string, key: string): string {
        const regexp = new RegExp(`\\[${key}\](.*?)\\[\/${key}\]`, 'i');

        const result = regexp.exec(data);

        if (!result) {
            return '';
        }

        return result[1];
    }

    private static stringifyKey(key: string, data: string): string {
        return `[${key}]${data}[/${key}]`;
    }
}
