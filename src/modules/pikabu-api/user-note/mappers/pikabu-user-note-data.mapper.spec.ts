import { faker } from '@faker-js/faker';

import { PikabuUserNoteDataModel } from '../models/pikabu-user-note-data.model';
import { PikabuUserNoteDataMapper } from './pikabu-user-note-data.mapper';

describe('PikabuUserNoteDataMapper', () => {
    it('Пустая строка должна парситься как объект с пустым списком тегов', () => {
        const obj = PikabuUserNoteDataMapper.parse('');

        expect(obj).toMatchObject({
            tags: [],
        });
    });

    it('Объект с пустым списком тегов должен сериализоваться как пустая строка', () => {
        const string = PikabuUserNoteDataMapper.stringify({
            tags: [],
        });

        expect(string).toBe('');
    });

    it('Объект со списком тегов должен сериализоваться и десериализовываться в идентичный объект', () => {
        const initialData: PikabuUserNoteDataModel = {
            tags: faker.helpers.multiple(() => faker.lorem.word(), {
                count: {
                    min: 1,
                    max: 10,
                },
            }),
        };

        const string = PikabuUserNoteDataMapper.stringify(initialData);

        const parsedData = PikabuUserNoteDataMapper.parse(string);

        expect(initialData).toEqual(parsedData);
    });
});
