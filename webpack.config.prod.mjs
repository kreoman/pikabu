import TerserPlugin from 'terser-webpack-plugin';
import { merge } from 'webpack-merge';

import common from './webpack.config.mjs';

export default merge(common, {
    mode: 'production',
    devtool: false,
    optimization: {
        minimizer: [new TerserPlugin({})],
    },
    performance: {
        maxAssetSize: 800_000,
        maxEntrypointSize: 800_000,
    },
});
