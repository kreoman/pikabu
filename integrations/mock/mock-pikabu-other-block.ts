import { Page } from '@playwright/test';

import { mockBlock } from '../helpers/mock-block';

export async function mockPikabuOtherBlock(page: Page) {
    const urls: string[] = [
        'https://*pikabu.ru/**',
        'https://*yandex.ru/**',
        'https://*yastatic.net/**',
        'https://*vk.com/**',
        'https://*google*/**',
        'https://*counter*/**',
    ];

    for (const url of urls) {
        await mockBlock(page, url);
    }
}
