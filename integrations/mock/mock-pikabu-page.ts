import { Page } from '@playwright/test';

import { mockStatic } from '../helpers/mock-static';

export async function mockPikabuPage(page: Page, path: string) {
    await page.route(
        `https://pikabu.ru/${path}`,
        mockStatic(`pikabu/pikabu.ru/${path}`),
    );
}
