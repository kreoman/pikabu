import { Page } from '@playwright/test';

import { mockStatic } from '../helpers/mock-static';

export async function mockPikabuStaticByPath(page: Page, path: string) {
    await page.route(
        `https://cs.pikabu.ru/${path}`,
        mockStatic(`pikabu/cs.pikabu.ru/${path}`),
    );
}

export async function mockPikabuStatic(page: Page) {
    const files: string[] = [
        'apps/ub/5.3.530/desktop/app.a1730406d28e.mo.js',
        'apps/ub/5.3.535/desktop/vendors-editor.9be40aef2006.mo.js',
        'apps/ub/5.3.569/desktop/vendors.ac3b7190b365.mo.js',
        'apps/ub/5.3.600/desktop/app.mo.css',
        'apps/ub/5.3.601/desktop/app.372ffdfb6d43.mo.js',
        'apps/ub/5.3.601/desktop/ui.7519231405c6.mo.js',
    ];

    for (const file of files) {
        await mockPikabuStaticByPath(page, file);
    }
}
