import { firstValueFrom, timer } from 'rxjs';

import { testWithExtension } from '../helpers/test-with-extension';
import { mockPikabuOtherBlock } from '../mock/mock-pikabu-other-block';
import { mockPikabuPage } from '../mock/mock-pikabu-page';
import { mockPikabuStatic } from '../mock/mock-pikabu-static';

testWithExtension.describe.skip('Example', () => {
    testWithExtension.beforeEach(async ({ page }) => {
        await mockPikabuOtherBlock(page);
        await mockPikabuStatic(page);
    });

    testWithExtension('First test', async ({ page }) => {
        const pagePath = 'story/komuto_pridyotsya_opustit_planku_11563909';

        await mockPikabuPage(page, pagePath);

        await page.goto(`https://pikabu.ru/${pagePath}`);

        await firstValueFrom(timer(2000));
    });
});
