import { expect } from '@playwright/test';

import { testWithExtension } from '../../helpers/test-with-extension';

testWithExtension.describe('Popup', () => {
    testWithExtension('Отображение меню', async ({ page, extensionId }) => {
        await page.goto(`chrome-extension://${extensionId}/popup.html`);

        await expect(page.locator('.des--island').first()).toBeVisible();
    });
});
