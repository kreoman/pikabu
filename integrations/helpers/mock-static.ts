import { Route } from '@playwright/test';
import * as fs from 'fs/promises';
import path from 'path';

export function mockStatic(localStaticPath: string) {
    return async (route: Route) => {
        const file = path.join(__dirname, '../static', localStaticPath);

        try {
            const content = await fs.readFile(file);

            await route.fulfill({
                body: content,
            });
        } catch (e) {
            await route.fulfill({
                status: 404,
            });
        }
    };
}
