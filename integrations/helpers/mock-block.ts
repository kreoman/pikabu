import { Page } from '@playwright/test';

export async function mockBlock(page: Page, url: string) {
    await page.route(url, async (route) => {
        await route.abort('blockedbyclient');
    });
}
