import fs from 'fs';
import { merge } from 'webpack-merge';
import ZipPlugin from 'zip-webpack-plugin';

import common from './webpack.config.prod.mjs';

const packageJsonString = fs.readFileSync('package.json', 'utf-8');
const packageJson = JSON.parse(packageJsonString);

export default merge(common, {
    plugins: [
        new ZipPlugin({
            filename: `pikabu-chrome-extension.${packageJson.version}.chrome.zip`,
        }),
    ],
});
