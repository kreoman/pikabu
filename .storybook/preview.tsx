import { Decorator, Preview } from '@storybook/react';

import '../src/common/ui/_core.scss';

const themeDecorator: Decorator = (Story, context) => {
    let { theme, accentColor } = context.globals;
    theme = theme ?? 'light';
    accentColor = accentColor ?? 'grey';

    return <div className={'des--app'} data-theme-auto data-color-scheme={accentColor}><Story /></div>;
};

const preview: Preview = {
    parameters: {
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/i,
            },
        },
    },
    decorators: [
        themeDecorator,
    ],
    globalTypes: {
        theme: {
            name: 'Theme',
            description: 'Global theme for components',
            defaultValue: 'light',
            toolbar: {
                icon: 'circlehollow',
                // Array of plain string values or MenuItem shape (see below)
                items: ['light', 'dark'],
                dynamicTitle: true,
                // Property that specifies if the name of the item will be displayed
                // showName: true,
            },
        },
    },

};

export default preview;
