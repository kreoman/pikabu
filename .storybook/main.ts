import type { StorybookConfig } from '@storybook/react-webpack5';

// @ts-ignore
import custom from '../webpack.config.mjs';

const config: StorybookConfig = {
    stories: ['../src/**/*.mdx', '../src/**/*.stories.@(js|jsx|mjs|ts|tsx)'],
    addons: [
        '@storybook/addon-webpack5-compiler-swc',
        '@storybook/addon-links',
        '@storybook/addon-essentials',
        '@storybook/addon-interactions',
    ],
    framework: {
        name: '@storybook/react-webpack5',
        options: {},
    },
    webpackFinal: async (config) => {
        return {
            ...config,
            // @ts-ignore
            module: { ...config.module, rules: [...config.module.rules, ...custom.module.rules] },
        };
    },
};
export default config;
