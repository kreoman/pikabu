# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [5.24.2](https://gitlab.com/kreoman/pikabu/compare/v5.24.1...v5.24.2) (2025-03-06)

### [5.24.1](https://gitlab.com/kreoman/pikabu/compare/v5.24.0...v5.24.1) (2025-03-06)


### Bug Fixes

* После нажатия на upvote не сразу отображался waiter ([010176c](https://gitlab.com/kreoman/pikabu/commit/010176cbec7d2ee9ca78171bdedaef2c3a4d95cb))

## [5.24.0](https://gitlab.com/kreoman/pikabu/compare/v5.23.0...v5.24.0) (2025-03-02)


### Features

* **booster:** Возможность указывать стоимость одного Upvote ([f06c9b2](https://gitlab.com/kreoman/pikabu/commit/f06c9b2c41754a0610402253aa60bfbcdf9abfbb))

## [5.23.0](https://gitlab.com/kreoman/pikabu/compare/v5.22.1...v5.23.0) (2025-02-23)


### Features

* **note-tags:** Возможность добавлять теги к пользователям ([573baaa](https://gitlab.com/kreoman/pikabu/commit/573baaa7b2a150b607cbe217dc109b87f7c4a5d1))

### [5.22.1](https://gitlab.com/kreoman/pikabu/compare/v5.22.0...v5.22.1) (2025-02-17)


### Bug Fixes

* **owner-comment-rating:** Некорректное отображение рейтинга своих комментариев ([bd471f5](https://gitlab.com/kreoman/pikabu/commit/bd471f50b30aead054707d13a3451ea4d7a056e5))

## [5.22.0](https://gitlab.com/kreoman/pikabu/compare/v5.21.0...v5.22.0) (2025-02-17)


### Features

* **user-rating:** Отображение значения отрицательного рейтинга пользователя ([7f6f298](https://gitlab.com/kreoman/pikabu/commit/7f6f2981c99fed85724677e5c0cde2ae1c4dfc60))


### Bug Fixes

* **user-rating:** Опечатка ([6e33fea](https://gitlab.com/kreoman/pikabu/commit/6e33feae35dbe706ba7165d5eed2b3919dafd8bd))

## [5.21.0](https://gitlab.com/kreoman/pikabu/compare/v5.20.2...v5.21.0) (2025-02-15)


### Features

* Ссылка на телеграм-канал в карточке расширения в сайдбаре ([2a3c4b9](https://gitlab.com/kreoman/pikabu/commit/2a3c4b910b987b0684a193c8a764cc3c64016f13))

### [5.20.2](https://gitlab.com/kreoman/pikabu/compare/v5.20.1...v5.20.2) (2025-02-14)


### Bug Fixes

* **comments-rating:** Не отображался рейтинг в срезе комментариев ([ea76905](https://gitlab.com/kreoman/pikabu/commit/ea769059c26f1556ca8f64178e773a47d5d7617b))
* **теги пользователя:** не всегда парсился идентификатор pikabu.monster ([2560656](https://gitlab.com/kreoman/pikabu/commit/25606569087c4b00d0866bbe71291365d4983a4a))

### [5.20.1](https://gitlab.com/kreoman/pikabu/compare/v5.20.0...v5.20.1) (2025-02-14)


### Bug Fixes

* **RatingV2:** не учитывался upvote ([72a7dfb](https://gitlab.com/kreoman/pikabu/commit/72a7dfb2b6708bfa3daf4a3ac917d4d1376c89d6))

## [5.20.0](https://gitlab.com/kreoman/pikabu/compare/v5.18.1...v5.20.0) (2025-02-14)


### Features

* Доработка под обновленный рейтинг ([7f24133](https://gitlab.com/kreoman/pikabu/commit/7f24133dc62f0bc445278525831086ef17889841))
* Использование data-атрибутов для рейтинга ([c089efd](https://gitlab.com/kreoman/pikabu/commit/c089efdc17308a994974ad4b8fddbdd1b725ea1a))


### Bug Fixes

* Отображение меню в мобильной версии ([e26c5b3](https://gitlab.com/kreoman/pikabu/commit/e26c5b39513d0d71398aa63992019d4d52f1b5b3))
* Размер кнопки upvote в мобильной версии ([dc00922](https://gitlab.com/kreoman/pikabu/commit/dc00922206076d08ec8f8211f23409e4c7a27f81))
* Цвет иконки ([8851a1e](https://gitlab.com/kreoman/pikabu/commit/8851a1e28601881734fab9baa8739f098d8fa7e8))

### [5.18.1](https://gitlab.com/kreoman/pikabu/compare/v5.18.0...v5.18.1) (2025-02-05)


### Bug Fixes

* Дублирование рейтинга ([c84e3cf](https://gitlab.com/kreoman/pikabu/commit/c84e3cf85fdf994f8bbb8875f6cef4d7008985b3))

## [5.18.0](https://gitlab.com/kreoman/pikabu/compare/v5.17.0...v5.18.0) (2025-02-04)


### Features

* **booster:** promisify ([d7c8aa5](https://gitlab.com/kreoman/pikabu/commit/d7c8aa56132c32a621f1710bbfb14f599115c83f))
* **mobile-api:** Автоматический выбор между общим API и авторизованным. ([ecc5653](https://gitlab.com/kreoman/pikabu/commit/ecc5653d13b1a7db1bf421d2ba8e19658ab3c72d))
* **mobile-api:** Автоматический выбор между общим API и авторизованным. ([0c5931e](https://gitlab.com/kreoman/pikabu/commit/0c5931ed043fe14851e87cac01e74c9593b11b6a))
* **mobile-api:** Автоматический выбор между общим API и авторизованным. ([df5d35c](https://gitlab.com/kreoman/pikabu/commit/df5d35c5bde32aed0b7e13e39ff06362c57408ae))
* **mobile-api:** Автоматический выбор между общим API и авторизованным. ([c031bfd](https://gitlab.com/kreoman/pikabu/commit/c031bfd01617b535dd16e06a09d2c35416567fa6))
* **mobile-api:** Автоматический выбор между общим API и авторизованным. ([9f92d39](https://gitlab.com/kreoman/pikabu/commit/9f92d398382b2a48f8a8a4ae58e2e13ff5c50eb2))
* **storage-service:** move to kit ([ca172d0](https://gitlab.com/kreoman/pikabu/commit/ca172d026d025ea3cc91b1b78c62f9cbfc421d77))
* **user-tags:** Счетчик ([ba75c78](https://gitlab.com/kreoman/pikabu/commit/ba75c7897b70d8296dfe412fd3abbe4f8b8c7301))
* Отображение ошибки 429 ([db47623](https://gitlab.com/kreoman/pikabu/commit/db47623231fcfe6abcc5fc7374aca0a2aafd61a2))


### Bug Fixes

* **booster-tool:** size ([65b617a](https://gitlab.com/kreoman/pikabu/commit/65b617a8b1176d406d361d4fdcc8286bb8657049))
* **booster:** бесконечная загрузка в списке кандидатов ([1d5a740](https://gitlab.com/kreoman/pikabu/commit/1d5a740ef5ed502b9a4bff02fb95ff5ba05630d2))
* **booster:** опечатка ([ab7d484](https://gitlab.com/kreoman/pikabu/commit/ab7d484661ce81a010bd9c05680c5d777244561b))
* **user-indicator:** size ([9e41ebe](https://gitlab.com/kreoman/pikabu/commit/9e41ebe11f041b77abf2e6edf9a65c6705ec803e))

## [5.17.0](https://gitlab.com/kreoman/pikabu/compare/v5.16.2...v5.17.0) (2025-01-09)


### Features

* **booster:** Отклонение кандидата ([80d49f5](https://gitlab.com/kreoman/pikabu/commit/80d49f56a516b435f0d80015e0a9d21b3ea74567))


### Bug Fixes

* **tooltip:** z-index ([d2418ab](https://gitlab.com/kreoman/pikabu/commit/d2418abbbd260f0094f8c440494550fcbef3284a))

### [5.16.2](https://gitlab.com/kreoman/pikabu/compare/v5.16.1...v5.16.2) (2025-01-09)


### Bug Fixes

* Применение новых правил запросов ([6f372e0](https://gitlab.com/kreoman/pikabu/commit/6f372e0ab932f8e4045521a58e5594de14567cef))

### [5.16.1](https://gitlab.com/kreoman/pikabu/compare/v5.16.0...v5.16.1) (2025-01-09)


### Bug Fixes

* Не корректно определялось всплывающее меню пользователя ([099898c](https://gitlab.com/kreoman/pikabu/commit/099898c735a1c26da2da0c3562f1cce5a658b0c1))

## [5.16.0](https://gitlab.com/kreoman/pikabu/compare/v5.15.2...v5.16.0) (2025-01-09)


### Features

* update icon ([1355787](https://gitlab.com/kreoman/pikabu/commit/1355787bb1e337daedc3e0e1ef809fadc9f38b4e))
* Авторизация в мобильном API для получения минусов в постах 18+ ([1ce3da7](https://gitlab.com/kreoman/pikabu/commit/1ce3da75aba073e2affd1b5b8524bf5c41b26ea2))
* Авторизация в мобильном API для получения минусов в постах 18+ ([cd3d924](https://gitlab.com/kreoman/pikabu/commit/cd3d924628b304aea60dc28287a9085504915f29))
* Вернул запрос информации о пользователя через ajax. Мобильный API отваливается из-за большого кол-ва запросов ([3b05641](https://gitlab.com/kreoman/pikabu/commit/3b056415495195ebda09343266c6b346c97df155))
* Мобильное API для получения информации о пользователе ([2345dfb](https://gitlab.com/kreoman/pikabu/commit/2345dfbb5d00640c9f5fa48902666deb3fd2fe9c))
* Подсветка ника автора вместо подсветки комментария ([e89cf62](https://gitlab.com/kreoman/pikabu/commit/e89cf628bb34846b85cba38d08db5e26016f05f8))


### Bug Fixes

* build ([e1c98c8](https://gitlab.com/kreoman/pikabu/commit/e1c98c813145df4fa70c7deed56d54a085acd8ab))

### [5.15.2](https://gitlab.com/kreoman/pikabu/compare/v5.15.1...v5.15.2) (2025-01-07)


### Bug Fixes

* import user notes delay ([7dbee46](https://gitlab.com/kreoman/pikabu/commit/7dbee46dfbc78ae57c672084fbb3bb202385dd71))

### [5.15.1](https://gitlab.com/kreoman/pikabu/compare/v5.15.0...v5.15.1) (2025-01-07)


### Bug Fixes

* disabled button style ([d787832](https://gitlab.com/kreoman/pikabu/commit/d787832851daf8046ec2fa9639ed784d4986659c))
* windows build ([e52b95f](https://gitlab.com/kreoman/pikabu/commit/e52b95f16b1ba59c09726da13676dc6df5e7d04b))

## [5.15.0](https://gitlab.com/kreoman/pikabu/compare/v5.14.0...v5.15.0) (2025-01-05)


### Features

* booster upvote button ([eac308b](https://gitlab.com/kreoman/pikabu/commit/eac308b065b20721219f83a94293629adb29abb2))
* Обновленные иконки ([5472f56](https://gitlab.com/kreoman/pikabu/commit/5472f5613feb5fcc7b267effa7293913acab95af))
* Сохранение заметок ([2622fdc](https://gitlab.com/kreoman/pikabu/commit/2622fdc9b3d9dc23c3f5988c72fd06abe79b4de1))
* Сохранение заметок ([8d57755](https://gitlab.com/kreoman/pikabu/commit/8d57755dd2e0582efda5f151b9c7dcfd8ddc3367))
* Убрана текущая дата из названия экспортируемого файла ([6234c4e](https://gitlab.com/kreoman/pikabu/commit/6234c4e81c6e5ba16127e5bdf2a627c05dc86931))


### Bug Fixes

* zip build ([f52e7dc](https://gitlab.com/kreoman/pikabu/commit/f52e7dc655b57fa86c892a8ede13e9963af6380f))

## [5.14.0](https://gitlab.com/kreoman/pikabu/compare/v5.13.0...v5.14.0) (2025-01-02)


### Features

* **core:** page-script-load from des-kit ([4d0789b](https://gitlab.com/kreoman/pikabu/commit/4d0789ba1707d844157966ed2129303e8416ce30))
* **story-video-download:** link ([b81aa23](https://gitlab.com/kreoman/pikabu/commit/b81aa23ffa548e1a321cbd29a06708774f4c5779))
* **styles:** label margins ([96b28d6](https://gitlab.com/kreoman/pikabu/commit/96b28d62c4f43badea54946f36ef98a4cea938c3))
* **style:** Обновление стилей ([662eed1](https://gitlab.com/kreoman/pikabu/commit/662eed151475edfd4917be175b514a157d9030ce))


### Bug Fixes

* **style:** Общий стиль подсветки комментов ([e99ec4f](https://gitlab.com/kreoman/pikabu/commit/e99ec4f55451aaabcf790bb9a880075424b8bb01))

## [5.13.0](https://gitlab.com/kreoman/pikabu/compare/v5.12.0...v5.13.0) (2024-12-30)


### Features

* **BackgroundConnectorService:** move to kit ([c852d4d](https://gitlab.com/kreoman/pikabu/commit/c852d4d79b7e5fa63b5f96fd0f332ff9f68b233f))
* **booster:** Окно ключа ([e842d4a](https://gitlab.com/kreoman/pikabu/commit/e842d4a53f9b2f8b271344c3a75a8c5ed12c2547))
* **cache:** from kit ([7d35f28](https://gitlab.com/kreoman/pikabu/commit/7d35f2873f6fd563754d65c5deb1a970c6f5df9d))
* **serializer:** from kit ([9e43221](https://gitlab.com/kreoman/pikabu/commit/9e43221ae5a5c010a2b1086324ce64213977a434))
* **settings:** Сохранение последних параметров заказа ([d30ca24](https://gitlab.com/kreoman/pikabu/commit/d30ca24e6d16e4593ea8461ba2a0426f5982ab2f))


### Bug Fixes

* **Booster:** При отключении бустера в настройках все равно шли запросы к бэку ([52d094a](https://gitlab.com/kreoman/pikabu/commit/52d094a6449d28aecaf7c227e7a910228257f6f0))

## [5.12.0](https://gitlab.com/kreoman/pikabu/compare/v5.11.1...v5.12.0) (2024-12-24)


### Features

* **booster:** Полная ссылка на пост ([c7b15fa](https://gitlab.com/kreoman/pikabu/commit/c7b15fa638108e138120bb6fc406f2522a25c9d3))


### Bug Fixes

* **tooltip:** style ([16400f0](https://gitlab.com/kreoman/pikabu/commit/16400f0a32b28f3d55d5ad90b4a562b408c25afe))

### [5.11.1](https://gitlab.com/kreoman/pikabu/compare/v5.11.0...v5.11.1) (2024-12-24)


### Bug Fixes

* **story-video-download:** Подстановка названия поста в название скачиваемого файла ([2d7ef1e](https://gitlab.com/kreoman/pikabu/commit/2d7ef1e3e159b9c61332023fe387bfe3625d1edb))

## [5.11.0](https://gitlab.com/kreoman/pikabu/compare/v5.10.1...v5.11.0) (2024-12-23)


### Features

* **booster:** Ссылки для открытия постов ([6c45603](https://gitlab.com/kreoman/pikabu/commit/6c4560381745511042291eaee34d0210e0ca5ae6))

### [5.10.1](https://gitlab.com/kreoman/pikabu/compare/v5.10.0...v5.10.1) (2024-12-23)


### Bug Fixes

* **booster:** API ошибки ломали поток ([bf27899](https://gitlab.com/kreoman/pikabu/commit/bf27899c567de8efd31dc2e75eff2505c47033e2))

## [5.10.0](https://gitlab.com/kreoman/pikabu/compare/v5.9.1...v5.10.0) (2024-12-23)


### Features

* **style:** Корректировка стилей ([5f7e59a](https://gitlab.com/kreoman/pikabu/commit/5f7e59ae98f00994a0d81ea26abcfa15a72421d2))
* **style:** Обновлена светлая тема ([4e7b277](https://gitlab.com/kreoman/pikabu/commit/4e7b2777079f7509d21eb3f839df58268a1af97d))
* **style:** Оформление в мобильном разрешении ([f7aab93](https://gitlab.com/kreoman/pikabu/commit/f7aab93d6f8322b2c9886735804e5b5867e89454))


### Bug Fixes

* **style:** Подсветка комментариев ([63d6c06](https://gitlab.com/kreoman/pikabu/commit/63d6c0623fb60c8504eaef2b729e90509f95eaba))

### [5.9.1](https://gitlab.com/kreoman/pikabu/compare/v5.9.0...v5.9.1) (2024-12-23)


### Bug Fixes

* **style:** Вернул палитру pikabu для рейтинга ([1e72418](https://gitlab.com/kreoman/pikabu/commit/1e724181613102dd30c378512af2f1c1ef7b6bf3))

## [5.9.0](https://gitlab.com/kreoman/pikabu/compare/v5.8.0...v5.9.0) (2024-12-22)


### Features

* **BackgroundConnection:** retry connection ([7eb7450](https://gitlab.com/kreoman/pikabu/commit/7eb7450e1e10f0de61258b719f28951e47e66de5))

## [5.8.0](https://gitlab.com/kreoman/pikabu/compare/v5.7.0...v5.8.0) (2024-12-22)


### Features

* **BackgroundConnection:** retry connection ([dfc18c4](https://gitlab.com/kreoman/pikabu/commit/dfc18c4588f352108c3b4be930fbdbe66b310cb1))


### Bug Fixes

* **booster:** Список кандидатов ([452da27](https://gitlab.com/kreoman/pikabu/commit/452da27453421522a47dfad1d921abc04cb9c3ef))

## [5.7.0](https://gitlab.com/kreoman/pikabu/compare/v5.6.0...v5.7.0) (2024-12-22)


### Features

* **booster:** Список заказов ([50fbcc0](https://gitlab.com/kreoman/pikabu/commit/50fbcc0c6891330dbfeade8a36ee9f2d59dfc48f))

## [5.6.0](https://gitlab.com/kreoman/pikabu/compare/v5.5.0...v5.6.0) (2024-12-22)


### Features

* **booster:** Список заказов ([8a96e7f](https://gitlab.com/kreoman/pikabu/commit/8a96e7fb11a3c7a03efac1ded18c554610254dcf))

## [5.5.0](https://gitlab.com/kreoman/pikabu/compare/v5.4.1...v5.5.0) (2024-12-22)


### Features

* **booster:** Список кандидатов ([fba529d](https://gitlab.com/kreoman/pikabu/commit/fba529df8974fd048d5f7732c56aaefeac4b9879))
* **booster:** Список транзакций ([86956db](https://gitlab.com/kreoman/pikabu/commit/86956db965038bfb46433421629ce4b0dc73befc))
* **booster:** Список транзакций ([8826251](https://gitlab.com/kreoman/pikabu/commit/88262517907482dfeae2feacf135d91bf599adc4))
* **styles:** remove color-scheme enum ([fac7e8b](https://gitlab.com/kreoman/pikabu/commit/fac7e8bd62d72b62579432fa2cb5910ea47e1311))
* **styles:** remove color-scheme enum ([d3e2a9e](https://gitlab.com/kreoman/pikabu/commit/d3e2a9eec1f3f68439b3f13e0e8ae9ee62b013cd))
* **styles:** remove old styles ([2846f25](https://gitlab.com/kreoman/pikabu/commit/2846f25de65fc3e714fffe55fe69e210864519f7))
* **styles:** remove old styles ([d771507](https://gitlab.com/kreoman/pikabu/commit/d771507fcaa86ea190637df03d2594b10de94331))
* **styles:** tooltip theme auto ([9fae90a](https://gitlab.com/kreoman/pikabu/commit/9fae90a6869d10f89cbc2991109f86853a3879b3))
* **styles:** update ([f573240](https://gitlab.com/kreoman/pikabu/commit/f5732409c712c8d5a961ec123af156316558de73))


### Bug Fixes

* **rating:** waiter size ([dd43242](https://gitlab.com/kreoman/pikabu/commit/dd4324281e980282910af586b4bedea3f01f1e89))

### [5.4.1](https://gitlab.com/kreoman/pikabu/compare/v5.4.0...v5.4.1) (2024-12-20)


### Bug Fixes

* **firstInitializeBg:** Зацикливание ([fb31fd0](https://gitlab.com/kreoman/pikabu/commit/fb31fd0e79d05435c547fbffc1b0c26baf7461a0))

## [5.4.0](https://gitlab.com/kreoman/pikabu/compare/v5.3.1...v5.4.0) (2024-12-20)


### Features

* **booster:** Настройка ограничения по времени ([f073f10](https://gitlab.com/kreoman/pikabu/commit/f073f10c5b34c20e0c8cece670670ea97b01639e))

### [5.3.1](https://gitlab.com/kreoman/pikabu/compare/v5.3.0...v5.3.1) (2024-12-17)


### Bug Fixes

* **booster:** Форматирование тултипа с запрещенным тегом ([82b0c2b](https://gitlab.com/kreoman/pikabu/commit/82b0c2b3eb688f55c08956ccdeef35a95266f945))

## [5.3.0](https://gitlab.com/kreoman/pikabu/compare/v4.23.0...v5.3.0) (2024-12-17)


### Features

* **background-connector:** Удаление лишних слоев ([34541db](https://gitlab.com/kreoman/pikabu/commit/34541dbf013364c5cd2453efdbb8c997b42a5292))
* **background:** Удаление ненужного ProxyRuntime ([9daf4ea](https://gitlab.com/kreoman/pikabu/commit/9daf4ea6756facf43c196157c7067040a236babc))
* **booster-client:** непосредственное обновление статуса ([cf2a29a](https://gitlab.com/kreoman/pikabu/commit/cf2a29a32705067854b758dc530cc535a85e28dd))
* booster-order-storage.service.ts ([0e3f8f0](https://gitlab.com/kreoman/pikabu/commit/0e3f8f02d352dec1aead5f4603fd7178f73b8878))
* **booster:** auto open fix ([da90ab2](https://gitlab.com/kreoman/pikabu/commit/da90ab29be832c63a35eb44b6562486d57cc6c7c))
* **booster:** auto open fix ([acd087d](https://gitlab.com/kreoman/pikabu/commit/acd087d3d587b056435a88a07be9bab72e00c77f))
* **booster:** Автоматическая регистрация ([406a84c](https://gitlab.com/kreoman/pikabu/commit/406a84c33ed2416a68c70a0d48d80f24f83fbf2b))
* **booster:** Завершенный заказ ([59297d3](https://gitlab.com/kreoman/pikabu/commit/59297d34dfe6b87dc6506182ca3ccadbed6eff32))
* **booster:** Запрещенные теги ([0a9495a](https://gitlab.com/kreoman/pikabu/commit/0a9495a33e106af3d58f749de82052d5e1203db3))
* **booster:** Подробнее о работе Booster ([ca891e9](https://gitlab.com/kreoman/pikabu/commit/ca891e9b90682cb00618457d2cc76374026be116))
* **communicator:** Единое постоянное подключение к фоновому скрипту расширения ([be21a27](https://gitlab.com/kreoman/pikabu/commit/be21a275f5b42de73dbf46db06f76782f5cc92d9))
* **rating:** Вернул привычный компонент рейтинга ([c5dfb03](https://gitlab.com/kreoman/pikabu/commit/c5dfb039a3eb208107f8562314aae6894f27f113))
* **rating:** Вернул привычный компонент рейтинга ([2e1754f](https://gitlab.com/kreoman/pikabu/commit/2e1754ff7fc341a5eb44393b1f48e68b2529d8b2))
* **settings:** Настройка отправки статистики ([3835b27](https://gitlab.com/kreoman/pikabu/commit/3835b27d187396f67b5cb799378a8e498daee28f))
* storybook ([d36b34b](https://gitlab.com/kreoman/pikabu/commit/d36b34b304f938c5e1219df449edea0834ed1c32))
* storybook ([1d71c9c](https://gitlab.com/kreoman/pikabu/commit/1d71c9c03c3f50c95ab6dfffd0656cf2f9a07011))
* задержка при закрытии вкладки, а также задержка после установки плюса посту ([24d49cc](https://gitlab.com/kreoman/pikabu/commit/24d49ccb147bb7ec42d2777d215fa1533823ea8a))
* Иконки через импорт ([0a24652](https://gitlab.com/kreoman/pikabu/commit/0a246525ffde8df980a8ed7f67fe74c3b8113b41))
* Удаление логов ([076259f](https://gitlab.com/kreoman/pikabu/commit/076259fbb3d27379a5555772d4653916f7914573))


### Bug Fixes

* **PikabuStoryToolsBooster:** highlight ([6967759](https://gitlab.com/kreoman/pikabu/commit/6967759bdaec4d0e2f59ada5858260f569dd81b7))
* **pluralize:** 0 плюсов ([d227deb](https://gitlab.com/kreoman/pikabu/commit/d227deb6ad43166d87d1d867d5b96584be9988b0))

## [5.2.0](https://gitlab.com/kreoman/pikabu/compare/v5.1.1...v5.2.0) (2024-12-17)


### Features

* **booster:** Скрытие опции "автоматическая установка плюса" ([b9a0714](https://gitlab.com/kreoman/pikabu/commit/b9a071407b995120792d252df3e1e101adb1ad93))

### [5.1.1](https://gitlab.com/kreoman/pikabu/compare/v5.1.0...v5.1.1) (2024-12-17)


### Bug Fixes

* **layout:** Обновление расположения блока в сайдбаре ([9a89349](https://gitlab.com/kreoman/pikabu/commit/9a8934928ffbdb37c67a5a3daed6e1910f261ea1))
* **modal:** Скрытие глобального скролла ([fcf3ca9](https://gitlab.com/kreoman/pikabu/commit/fcf3ca994e38d0bfcafea864faf10b3b7411d554))

## [5.1.0](https://gitlab.com/kreoman/pikabu/compare/v5.0.0...v5.1.0) (2024-12-17)


### Features

* **booster:** Пометка о том что рейтинг уже был изменен ([2bee856](https://gitlab.com/kreoman/pikabu/commit/2bee856ce53d5b53b7d6ad64a765e5f23adae965))
* **booster:** Пометка о том что рейтинг уже был изменен ([ae86e31](https://gitlab.com/kreoman/pikabu/commit/ae86e312585b79bd89c034a8a8516306fa16f751))


### Bug Fixes

* **assets:** svg assets/inline (специально для Adblock) ([55f01d9](https://gitlab.com/kreoman/pikabu/commit/55f01d960bd45424860fe8c715e8fc8e508d9a95))
* **booster:** Не открывать вкладку на собственном заказе ([29a5857](https://gitlab.com/kreoman/pikabu/commit/29a5857f8fdee52d799e22ad203dde698cc9cceb))

## [5.0.0](https://gitlab.com/kreoman/pikabu/compare/v4.23.0...v5.0.0) (2024-12-16)


### Features

* **background-connector:** Дополнительная абстракция над Port, так как необходимо будет реализовать работу в обход прямого подключения для firefox ([113f681](https://gitlab.com/kreoman/pikabu/commit/113f6814ddebfb75707aad27b04ee8a69cbaccf7))
* **background-connector:** работа в обход прямого подключения для firefox ([b9ca92e](https://gitlab.com/kreoman/pikabu/commit/b9ca92e754b0aec774ec35074c407745824722f0))
* **background-connector:** Удаление лишних слоев ([34541db](https://gitlab.com/kreoman/pikabu/commit/34541dbf013364c5cd2453efdbb8c997b42a5292))
* **background:** Удаление ненужного ProxyRuntime ([9daf4ea](https://gitlab.com/kreoman/pikabu/commit/9daf4ea6756facf43c196157c7067040a236babc))
* **booster-client:** непосредственное обновление статуса ([cf2a29a](https://gitlab.com/kreoman/pikabu/commit/cf2a29a32705067854b758dc530cc535a85e28dd))
* **booster:** auto open fix ([da90ab2](https://gitlab.com/kreoman/pikabu/commit/da90ab29be832c63a35eb44b6562486d57cc6c7c))
* **booster:** auto open fix ([acd087d](https://gitlab.com/kreoman/pikabu/commit/acd087d3d587b056435a88a07be9bab72e00c77f))
* **booster:** Автоматическая регистрация ([406a84c](https://gitlab.com/kreoman/pikabu/commit/406a84c33ed2416a68c70a0d48d80f24f83fbf2b))
* **booster:** Завершенный заказ ([59297d3](https://gitlab.com/kreoman/pikabu/commit/59297d34dfe6b87dc6506182ca3ccadbed6eff32))
* **booster:** Подробнее о работе Booster ([ca891e9](https://gitlab.com/kreoman/pikabu/commit/ca891e9b90682cb00618457d2cc76374026be116))
* **communicator:** Единое постоянное подключение к фоновому скрипту расширения ([be21a27](https://gitlab.com/kreoman/pikabu/commit/be21a275f5b42de73dbf46db06f76782f5cc92d9))
* **rating:** Вернул привычный компонент рейтинга ([c5dfb03](https://gitlab.com/kreoman/pikabu/commit/c5dfb039a3eb208107f8562314aae6894f27f113))
* **rating:** Вернул привычный компонент рейтинга ([2e1754f](https://gitlab.com/kreoman/pikabu/commit/2e1754ff7fc341a5eb44393b1f48e68b2529d8b2))
* **settings:** Настройка отправки статистики ([3835b27](https://gitlab.com/kreoman/pikabu/commit/3835b27d187396f67b5cb799378a8e498daee28f))
* storybook ([d36b34b](https://gitlab.com/kreoman/pikabu/commit/d36b34b304f938c5e1219df449edea0834ed1c32))
* storybook ([1d71c9c](https://gitlab.com/kreoman/pikabu/commit/1d71c9c03c3f50c95ab6dfffd0656cf2f9a07011))
* задержка при закрытии вкладки, а также задержка после установки плюса посту ([24d49cc](https://gitlab.com/kreoman/pikabu/commit/24d49ccb147bb7ec42d2777d215fa1533823ea8a))
* Иконки через импорт ([0a24652](https://gitlab.com/kreoman/pikabu/commit/0a246525ffde8df980a8ed7f67fe74c3b8113b41))
* Удаление логов ([076259f](https://gitlab.com/kreoman/pikabu/commit/076259fbb3d27379a5555772d4653916f7914573))


### Bug Fixes

* **PikabuStoryToolsBooster:** highlight ([6967759](https://gitlab.com/kreoman/pikabu/commit/6967759bdaec4d0e2f59ada5858260f569dd81b7))
* **pluralize:** 0 плюсов ([d227deb](https://gitlab.com/kreoman/pikabu/commit/d227deb6ad43166d87d1d867d5b96584be9988b0))
* **settings:** Фикс сохранения настроек ([6ee5bb0](https://gitlab.com/kreoman/pikabu/commit/6ee5bb0d607591b81ef21775e2acf29b13c87f10))

## [4.23.0](https://gitlab.com/kreoman/pikabu/compare/v4.22.0...v4.23.0) (2024-09-08)


### Features

* другое отображение подсветки комментария в темной теме ([ff1289a](https://gitlab.com/kreoman/pikabu/commit/ff1289a20cc5c7ca63448c1ab080543c88235d51))
* Рефакторинг + сервис для работы с комментариями ([18a34ac](https://gitlab.com/kreoman/pikabu/commit/18a34acf5cdec2bad0625f048b53de272e2898f1))

## [4.22.0](https://gitlab.com/kreoman/pikabu/compare/v4.21.2...v4.22.0) (2024-08-17)


### Features

* Скорректированы цвета темной темы в соответствии с темой pikabu ([613ef24](https://gitlab.com/kreoman/pikabu/commit/613ef24313dd114ee4e54bc0d8be6955bc292067))


### Bug Fixes

* actual pikabu.monster domain ([f5af4ca](https://gitlab.com/kreoman/pikabu/commit/f5af4ca0e74194036edcabda988e625629923eac))

### [4.21.2](https://gitlab.com/kreoman/pikabu/compare/v4.21.1...v4.21.2) (2024-08-13)


### Bug Fixes

* отступ онбординга в мобильной версии ([5c898c9](https://gitlab.com/kreoman/pikabu/commit/5c898c9e920a0fb575179904d8e3665564e43653))

### [4.21.1](https://gitlab.com/kreoman/pikabu/compare/v4.21.0...v4.21.1) (2024-08-12)


### Bug Fixes

* отступы в мобильной версии ([a12488b](https://gitlab.com/kreoman/pikabu/commit/a12488bbf93f91f80b2b21ddad56a500c3bcd9c8))

## [4.21.0](https://gitlab.com/kreoman/pikabu/compare/v4.20.1...v4.21.0) (2024-08-10)


### Features

* рейтинг комментариев в мобильной версии ([24ba2e9](https://gitlab.com/kreoman/pikabu/commit/24ba2e9c229481c885ab4fa7434af9bff1626c8e))
* рейтинг поста в мобильной версии ([9e89628](https://gitlab.com/kreoman/pikabu/commit/9e896285270d458ef6c76492ef3db76eb8811381))

### [4.20.1](https://gitlab.com/kreoman/pikabu/compare/v4.20.0...v4.20.1) (2024-08-07)

## [4.20.0](https://gitlab.com/kreoman/pikabu/compare/v4.19.3...v4.20.0) (2024-08-07)


### Features

* TabsMessageSender unification ([a8fe29e](https://gitlab.com/kreoman/pikabu/commit/a8fe29e6ecbc00e783f817a3cc296a0eee802944))
* Подсветка комментариев автора, которому поставлен upvote или downvote ([c59cd9f](https://gitlab.com/kreoman/pikabu/commit/c59cd9f06e9879b9b820224ab5d897aae1d39de0))

### [4.19.3](https://gitlab.com/kreoman/pikabu/compare/v4.19.2...v4.19.3) (2024-07-14)


### Bug Fixes

* проверка наличия элементов комментария ([c5a2878](https://gitlab.com/kreoman/pikabu/commit/c5a2878411b2ff6603a2f2bad1f2ac6ec8855c3f))

### [4.19.2](https://gitlab.com/kreoman/pikabu/compare/v4.19.1...v4.19.2) (2024-07-13)


### Bug Fixes

* проверка наличия элементов комментария ([1348f5e](https://gitlab.com/kreoman/pikabu/commit/1348f5e8562057f78fab94a0e0c00d7705606649))

### [4.19.1](https://gitlab.com/kreoman/pikabu/compare/v4.19.0...v4.19.1) (2024-07-13)


### Bug Fixes

* Дополнительные проверки на наличие требуемых элементов в посте ([743f088](https://gitlab.com/kreoman/pikabu/commit/743f088de190cf96238df417d5f8e8e3c7bc564b))

## [4.19.0](https://gitlab.com/kreoman/pikabu/compare/v4.18.3...v4.19.0) (2024-07-13)


### Features

* Автоматическое переключение между светлым и темным режимом ([8b90583](https://gitlab.com/kreoman/pikabu/commit/8b90583f07905c832ac124e3d35d14ba93da60d9))
* Автоматическое переключение между светлым и темным режимом ([a5293fe](https://gitlab.com/kreoman/pikabu/commit/a5293fe69312ffb0cf08bbb6e281aaa07a076ebf))


### Bug Fixes

* Неправильное условие для валидности экспандера при рефакторинге ([8132adc](https://gitlab.com/kreoman/pikabu/commit/8132adc1eaea32cedb2bb126f5fd857105b479b8))
* Неправильное условие для валидности экспандера при рефакторинге ([459e691](https://gitlab.com/kreoman/pikabu/commit/459e69171138cd6002130b36d68fa125fc82450a))

### [4.18.3](https://gitlab.com/kreoman/pikabu/compare/v4.18.2...v4.18.3) (2024-07-12)

### [4.18.2](https://gitlab.com/kreoman/pikabu/compare/v4.18.1...v4.18.2) (2024-07-12)


### Bug Fixes

* Комментарии в контексте поста ([6d1a1c8](https://gitlab.com/kreoman/pikabu/commit/6d1a1c87609e4717b0698cff39e664943287607d))
* Комментарии в контексте поста (remove console.log) ([42c1ce9](https://gitlab.com/kreoman/pikabu/commit/42c1ce9c18e07d366cab157f5999383787e11284))
* Комментарии в контексте поста (remove console.log) ([374275d](https://gitlab.com/kreoman/pikabu/commit/374275d65ce69b30cbaeef9117825eb8bdb65bb7))
* Не все группы комментариев раскрывались ([eeb7c72](https://gitlab.com/kreoman/pikabu/commit/eeb7c7260f93a008d8d104abf9f78144f2489785))
* Не всегда раскрывались все комментарии ([a4119c2](https://gitlab.com/kreoman/pikabu/commit/a4119c2e5e8beb1e6434646fddffbc14bc7caa2d))
* Не всегда раскрывались все комментарии ([fd03bd0](https://gitlab.com/kreoman/pikabu/commit/fd03bd0c028812dc33dda96d5fd4c6e80e8ff503))
* Удалил неиспользуемы getter ([47bd20a](https://gitlab.com/kreoman/pikabu/commit/47bd20a532cc938f4d00030c8880b31a4f180155))

### [4.18.1](https://gitlab.com/kreoman/pikabu/compare/v4.18.0...v4.18.1) (2024-07-12)


### Bug Fixes

* Перестаем пытаться раскрыть ветки комментариев, если в течение 1 секунды не появилось ни одной новой ветки для раскрытия ([93c240a](https://gitlab.com/kreoman/pikabu/commit/93c240ae36adf6ef11c523ab0290c3d5a7c80270))

## [4.18.0](https://gitlab.com/kreoman/pikabu/compare/v4.17.1...v4.18.0) (2024-07-11)


### Features

* Раскрытие всех комментариев ([beabde0](https://gitlab.com/kreoman/pikabu/commit/beabde08c9e9e701f5a512461c406bf322331378))

### [4.17.1](https://gitlab.com/kreoman/pikabu/compare/v4.17.0...v4.17.1) (2024-07-11)

## [4.17.0](https://gitlab.com/kreoman/pikabu/compare/v4.16.0...v4.17.0) (2024-07-10)


### Features

* Отображение рейтинга собственных комментариев ([d1ffd4b](https://gitlab.com/kreoman/pikabu/commit/d1ffd4bb657a999ba38afdaef04d5d29bc4a560c))


### Bug Fixes

* предположительно, исправление ошибки EmptyError из firstValueFrom ([edc38b5](https://gitlab.com/kreoman/pikabu/commit/edc38b5e307c9df05825a7eedbaf96d360decada))

## [4.16.0](https://gitlab.com/kreoman/pikabu/compare/v4.15.0...v4.16.0) (2024-07-08)


### Features

* Использование source-map для более точного определения стека ошибки ([7c57f11](https://gitlab.com/kreoman/pikabu/commit/7c57f119b04a86200ace2dbe4d7f31d7b56c736d))


### Bug Fixes

* Отсутствует элемент mini-profile__btns ([cf42c11](https://gitlab.com/kreoman/pikabu/commit/cf42c11f625275b345436e4c7b5f8f043ffe032c))

## [4.15.0](https://gitlab.com/kreoman/pikabu/compare/v4.14.0...v4.15.0) (2024-07-08)


### Features

* Поддержка нового формата всплывающей информации о пользователе ([05eff8d](https://gitlab.com/kreoman/pikabu/commit/05eff8db4bada3f147e10cf550e215effde4d123))


### Bug Fixes

* Поддержка нового формата всплывающей информации о пользователе (test) ([74d7789](https://gitlab.com/kreoman/pikabu/commit/74d778988fb6fc9873dde40abc53448511927482))

## [4.14.0](https://gitlab.com/kreoman/pikabu/compare/v4.13.0...v4.14.0) (2024-07-08)


### Features

* **video-saver:** Копирование ссылки на видео ([aab703f](https://gitlab.com/kreoman/pikabu/commit/aab703f0f7ffd59caf293325189d79d32e882138))

## [4.13.0](https://gitlab.com/kreoman/pikabu/compare/v4.12.1...v4.13.0) (2024-07-08)


### Features

* Отслеживание загрузки скрипта ([f6e6d60](https://gitlab.com/kreoman/pikabu/commit/f6e6d604d9cda94f3c82b560839e437acf1d7424))


### Bug Fixes

* **video-saver:** Исправлено получение заголовка поста для имени файла при сохранении видео ([4d5ddbe](https://gitlab.com/kreoman/pikabu/commit/4d5ddbe0b12a3ddcfcd79543a1a773aea37aaa2a))

### [4.12.1](https://gitlab.com/kreoman/pikabu/compare/v4.12.0...v4.12.1) (2024-07-07)


### Bug Fixes

* **ui:** Добавлена проверка на валидность поста ([1fca3c6](https://gitlab.com/kreoman/pikabu/commit/1fca3c6fab573a7091549f4745b212b3ca550e06))

## [4.12.0](https://gitlab.com/kreoman/pikabu/compare/v4.11.2...v4.12.0) (2024-07-07)


### Features

* **booster:** Отображение кол-ва клиентов в сети ([563237c](https://gitlab.com/kreoman/pikabu/commit/563237cabeff7f428880ee034fddb092287a1949))

### [4.11.2](https://gitlab.com/kreoman/pikabu/compare/v4.11.1...v4.11.2) (2024-07-05)


### Bug Fixes

* Работа со старыми версиями браузеров, в котрорых не поддерживается :has в селекторах ([c85767a](https://gitlab.com/kreoman/pikabu/commit/c85767a6b725d79f636174935bfd13811541e8c8))

### [4.11.1](https://gitlab.com/kreoman/pikabu/compare/v4.11.0...v4.11.1) (2024-07-05)


### Bug Fixes

* Пропускаем посты без нужных элементов ([504efe5](https://gitlab.com/kreoman/pikabu/commit/504efe59d5c3f16487faeffe748649104d7e7071))

## [4.11.0](https://gitlab.com/kreoman/pikabu/compare/v4.10.0...v4.11.0) (2024-07-05)


### Features

* кэширование карты комментариев на странице, чтоб сократить кол-во запросов к background ([d77b0e0](https://gitlab.com/kreoman/pikabu/commit/d77b0e0429d02dbdfc58f4776ce2b877e598d4ac))
* Улучшена обработка ошибок ([1abe0af](https://gitlab.com/kreoman/pikabu/commit/1abe0afa0b3deb3f6d03d17df23ccb8facc6645c))

## [4.10.0](https://gitlab.com/kreoman/pikabu/compare/v4.9.1...v4.10.0) (2024-07-04)


### Features

* События отображения вкладок "Booster" и "Справка" ([3df1712](https://gitlab.com/kreoman/pikabu/commit/3df1712a4c09fc5429b4b7a17163bcfb6ef34345))


### Bug Fixes

* пропуск записи и очистка базы событий при отключении сбора статистики ([e28cf1f](https://gitlab.com/kreoman/pikabu/commit/e28cf1ffd6112d86f3337f88da6a4a98d31c3d87))

### [4.9.1](https://gitlab.com/kreoman/pikabu/compare/v4.9.0...v4.9.1) (2024-07-04)


### Bug Fixes

* Иллюстрация на темном фоне ([276bb76](https://gitlab.com/kreoman/pikabu/commit/276bb765d6d3defa5bf5a5ab23224897ba338bc7))

## [4.9.0](https://gitlab.com/kreoman/pikabu/compare/v4.8.1...v4.9.0) (2024-07-04)


### Features

* Онбординг в меню настроек расширения ([ddebac4](https://gitlab.com/kreoman/pikabu/commit/ddebac4edd89dbdb66c045133fba0ac009ce404e))
* Справка ([2f4d9e5](https://gitlab.com/kreoman/pikabu/commit/2f4d9e5a29ea708b783ebf063f8afa5e58d8a9bb))

### [4.8.1](https://gitlab.com/kreoman/pikabu/compare/v4.8.0...v4.8.1) (2024-07-04)


### Bug Fixes

* Во внешних ссылках не должен присутствовать домен pikabu.ru ([43c7c45](https://gitlab.com/kreoman/pikabu/commit/43c7c459d9240cb196ec8b7458cf694d00103159))
* Лишний дебаг ([63e5f6d](https://gitlab.com/kreoman/pikabu/commit/63e5f6d9ea5990497199ac4a22e583b427c5cb2b))

## [4.8.0](https://gitlab.com/kreoman/pikabu/compare/v4.7.0...v4.8.0) (2024-07-04)


### Features

* Событие открытия попапа с настройками ([8f7cf29](https://gitlab.com/kreoman/pikabu/commit/8f7cf291ed129fa3f7378d723c7922ae5e06e7ad))

## [4.7.0](https://gitlab.com/kreoman/pikabu/compare/v4.6.1...v4.7.0) (2024-07-04)


### Features

* Сбор статистики ([13f337e](https://gitlab.com/kreoman/pikabu/commit/13f337ebc49b19e368fe5be7aaa5d61db221bd08))


### Bug Fixes

* Более редкая отправка статистики ([f5df04e](https://gitlab.com/kreoman/pikabu/commit/f5df04ecfbec7b7e6208e02e65541a07ee75c056))

### [4.6.1](https://gitlab.com/kreoman/pikabu/compare/v4.6.0...v4.6.1) (2024-07-03)


### Bug Fixes

* работа в firefox ([1af86b4](https://gitlab.com/kreoman/pikabu/commit/1af86b48cd213d13caf0d28b0acd7dd9369e86cd))

## [4.6.0](https://gitlab.com/kreoman/pikabu/compare/v4.4.5...v4.6.0) (2024-07-02)


### Features

* Название поста в названии видеофайла при сохранении ([2d821a5](https://gitlab.com/kreoman/pikabu/commit/2d821a57fc297bbb0836b9ce91b4ee4a1bce5259))


### Bug Fixes

* pikabu переопределял стиль кнопки в статусе disabled ([4467a11](https://gitlab.com/kreoman/pikabu/commit/4467a116a41305e9bf0bb0a9cb213f417fb76c2f))
* проброс пустой ошибки ([f3f0876](https://gitlab.com/kreoman/pikabu/commit/f3f0876f5caab69c32b60fb98f3a3fd27777a578))
* проброс пустой ошибки ([651266a](https://gitlab.com/kreoman/pikabu/commit/651266ad1d629f29ede713f58e2bb67e296829c6))

## [4.5.0](https://gitlab.com/kreoman/pikabu/compare/v4.4.5...v4.5.0) (2024-07-02)


### Features

* Название поста в названии видеофайла при сохранении ([2d821a5](https://gitlab.com/kreoman/pikabu/commit/2d821a57fc297bbb0836b9ce91b4ee4a1bce5259))


### Bug Fixes

* проброс пустой ошибки ([f3f0876](https://gitlab.com/kreoman/pikabu/commit/f3f0876f5caab69c32b60fb98f3a3fd27777a578))
* проброс пустой ошибки ([651266a](https://gitlab.com/kreoman/pikabu/commit/651266ad1d629f29ede713f58e2bb67e296829c6))

### [4.4.5](https://gitlab.com/kreoman/pikabu/compare/v4.4.4...v4.4.5) (2024-07-02)

### [4.4.4](https://gitlab.com/kreoman/pikabu/compare/v4.4.3...v4.4.4) (2024-07-02)

### [4.4.3](https://gitlab.com/kreoman/pikabu/compare/v4.4.2...v4.4.3) (2024-07-02)


### Bug Fixes

* performance ([f4bcf2a](https://gitlab.com/kreoman/pikabu/commit/f4bcf2ac3500d043864c6ab6fd4e414951afec56))

### [4.4.2](https://gitlab.com/kreoman/pikabu/compare/v4.4.1...v4.4.2) (2024-07-01)


### Bug Fixes

* исправление цвета тени у инпута ([6fd5505](https://gitlab.com/kreoman/pikabu/commit/6fd5505ab2f6cb7b921743e7f8134c8760f2df63))
* исправление цвета тени у кнопки ([4417932](https://gitlab.com/kreoman/pikabu/commit/44179322a945afc158523811d32aa9be3ff08e52))

### [4.4.1](https://gitlab.com/kreoman/pikabu/compare/v4.4.0...v4.4.1) (2024-07-01)


### Bug Fixes

* performance ([10d0c1a](https://gitlab.com/kreoman/pikabu/commit/10d0c1a6870e9ab1ad9ae166690b412ede752916))

## [4.4.0](https://gitlab.com/kreoman/pikabu/compare/v4.3.0...v4.4.0) (2024-07-01)


### Features

* Анимация шкалы при изменении рейтинга ([20b36ff](https://gitlab.com/kreoman/pikabu/commit/20b36fffb0e2fd912c7dffa7d26c7b6da654c607))

## [4.3.0](https://gitlab.com/kreoman/pikabu/compare/v4.2.0...v4.3.0) (2024-07-01)


### Features

* подсветка голоса на шкале ([d222225](https://gitlab.com/kreoman/pikabu/commit/d22222594c4b55761a97aff77e27609be7cfc500))
* подсветка голоса на шкале ([0b71717](https://gitlab.com/kreoman/pikabu/commit/0b71717351d23259215750400c94992217ac7a75))
* подсветка голоса на шкале ([af4b8bd](https://gitlab.com/kreoman/pikabu/commit/af4b8bdd24c7cfeac834ebfcbb7385f50d8addae))


### Bug Fixes

* лишние импорты ([cc523ff](https://gitlab.com/kreoman/pikabu/commit/cc523ff279bb350095677842fe89c7510a8647f1))
* лишние импорты ([2308e44](https://gitlab.com/kreoman/pikabu/commit/2308e4419832e65053002f8415a6fea4037417ce))

## [4.2.0](https://gitlab.com/kreoman/pikabu/compare/v4.1.3...v4.2.0) (2024-07-01)


### Features

* Убран запрос пермишенов в манифесте ([b66ec91](https://gitlab.com/kreoman/pikabu/commit/b66ec91f3e11bfffb582f27d4aca2370bb0ea8d2))

### [4.1.3](https://gitlab.com/kreoman/pikabu/compare/v4.1.2...v4.1.3) (2024-06-30)


### Bug Fixes

* stability ([8cd3faa](https://gitlab.com/kreoman/pikabu/commit/8cd3faab967063814b056e81a74438191fd11298))

### [4.1.2](https://gitlab.com/kreoman/pikabu/compare/v4.1.1...v4.1.2) (2024-06-30)


### Bug Fixes

* --- ([dc079db](https://gitlab.com/kreoman/pikabu/commit/dc079dbf23b3e2bcf1590a51bd18d3e23ca756d7))
* --- ([a4c785b](https://gitlab.com/kreoman/pikabu/commit/a4c785b8bd7e6fadf772759b5924a85b521ad184))
* --- ([8937684](https://gitlab.com/kreoman/pikabu/commit/8937684ac20c309684ea74ba8baa2e925cdee240))

### [4.1.1](https://gitlab.com/kreoman/pikabu/compare/v4.1.0...v4.1.1) (2024-06-30)


### Bug Fixes

* Автоматическое открытие страницы поста для голосования ([23d1197](https://gitlab.com/kreoman/pikabu/commit/23d119723ad8f995689cdfd11d8f51f8da46722f))

## [4.1.0](https://gitlab.com/kreoman/pikabu/compare/v4.0.0...v4.1.0) (2024-06-29)


### Features

* resolve origin links ([e93f2d5](https://gitlab.com/kreoman/pikabu/commit/e93f2d5312f98139ffbf5455ded86b3b32dae4a1))
* не отправляем куки ([fa00844](https://gitlab.com/kreoman/pikabu/commit/fa00844e97e26bf7414781c27d52461386d73c87))
* Подмена реферальной ссылки ([1c60a6d](https://gitlab.com/kreoman/pikabu/commit/1c60a6dff8a1b062bd48af29e355566df5ec9fb2))
* Подмена реферальной ссылки для aliexpress ([7b758c5](https://gitlab.com/kreoman/pikabu/commit/7b758c595d78982c09af3f2f7f48d8fa626a2694))
* Подмена реферальной ссылки для aliexpress ([f4e7ea2](https://gitlab.com/kreoman/pikabu/commit/f4e7ea2193538d17656bebde0fc44cc18adbd1d5))


### Bug Fixes

* Парсинг aliclick.shop ([ff6771c](https://gitlab.com/kreoman/pikabu/commit/ff6771c1536e4691b454c588f8d848318ed3ae47))
* Проверка авторизации ([43dc047](https://gitlab.com/kreoman/pikabu/commit/43dc04776cbc2e9717a83975928ca7d04e38a9da))

## [4.0.0](https://gitlab.com/kreoman/pikabu/compare/v3.3.1...v4.0.0) (2024-06-29)


### ⚠ BREAKING CHANGES

* booster

### Features

* booster ([35d3f11](https://gitlab.com/kreoman/pikabu/commit/35d3f11a58594cfa78bd4aa11976e5258370e5a5))

### [3.3.1](https://gitlab.com/kreoman/pikabu/compare/v3.3.0...v3.3.1) (2024-03-30)


### Bug Fixes

* Косметические фиксы ([e70f1d0](https://gitlab.com/kreoman/pikabu/commit/e70f1d028dacbd75f8151e0b597b949b34959b8a))
* Косметические фиксы ([c3a5110](https://gitlab.com/kreoman/pikabu/commit/c3a5110c7ba24d7772b78b2108f196b081f9b76c))

## [3.3.0](https://gitlab.com/kreoman/pikabu/compare/v3.2.0...v3.3.0) (2024-03-14)


### Features

* Отображение топа тегов среди созданных постов пользователя и топа тегов среди комментируемых постов ([edca3eb](https://gitlab.com/kreoman/pikabu/commit/edca3ebe5d476daa7e1a5250eb8d00555b00f72d))

## [3.2.0](https://gitlab.com/kreoman/pikabu/compare/v3.1.0...v3.2.0) (2024-03-09)


### Features

* Добавлена шкала соотношения плюсов и минусов для рейтинга комментариев ([9096db7](https://gitlab.com/kreoman/pikabu/commit/9096db70ed8462812c0339d63e5b910ddcee19f8))

## [3.1.0](https://gitlab.com/kreoman/pikabu/compare/v3.0.0...v3.1.0) (2024-03-03)


### Features

* для получения ссылок на скачивание видео используется мобильное API, в котором присутствует mp4. ([433d90b](https://gitlab.com/kreoman/pikabu/commit/433d90b462ce29b331890e057d5d461fd6b6a384))

## [3.0.0](https://gitlab.com/kreoman/pikabu/compare/v2.6.2...v3.0.0) (2024-01-15)


### ⚠ BREAKING CHANGES

* Удален функционал, связанный с внешним источником данных
* Удален функционал, связанный с внешним источником данных

### Features

* Удален функционал, связанный с внешним источником данных ([b202aed](https://gitlab.com/kreoman/pikabu/commit/b202aed9dc19ffc769a842d432cf8f7571698657))
* Удален функционал, связанный с внешним источником данных ([3187b09](https://gitlab.com/kreoman/pikabu/commit/3187b0926a96c8dbff2f7e0f724943124c157a8c))


### Bug Fixes

* Вернул ts-md5 ([e03de94](https://gitlab.com/kreoman/pikabu/commit/e03de94059832e606eb2d2ad818ad0e8afc1afc0))

### [2.6.2](https://gitlab.com/kreoman/pikabu/compare/v2.6.1...v2.6.2) (2023-11-07)


### Bug Fixes

* Поправлен маппер из за которого не для всех публичных постов отображался рейтинг (Убрал console.log) ([a66e4e0](https://gitlab.com/kreoman/pikabu/commit/a66e4e05a3f2f1f12fc408bc2e2f83759bc4823e))

### [2.6.1](https://gitlab.com/kreoman/pikabu/compare/v2.6.0...v2.6.1) (2023-11-07)


### Bug Fixes

* Поправлен маппер из за которого не для всех публичных постов отображался рейтинг ([9925703](https://gitlab.com/kreoman/pikabu/commit/9925703cd9484b732e3529b37f7406b426e01b5a))

## [2.6.0](https://gitlab.com/kreoman/pikabu/compare/v2.5.0...v2.6.0) (2023-10-22)


### Features

* **story:** Поддержка новой версии постов ([49ee5ed](https://gitlab.com/kreoman/pikabu/commit/49ee5ed9440eec6c27ea30077fa63e6d963bdfe2))
* **story:** Поддержка новой версии постов ([01c3036](https://gitlab.com/kreoman/pikabu/commit/01c3036cb00b9646a66cb6a3a0407d4ec78209a4))


### Bug Fixes

* **hint:** title оказался лишним ) ([206a380](https://gitlab.com/kreoman/pikabu/commit/206a380758692d0ce48dfdc40201c8920a1c1ad2))

## [2.5.0](https://gitlab.com/kreoman/pikabu/compare/v2.4.2...v2.5.0) (2023-10-18)


### Features

* Скачивание видео ([a88f007](https://gitlab.com/kreoman/pikabu/commit/a88f0070bff850dd67b2dc1b33adfd2165843a30))

### [2.4.2](https://gitlab.com/kreoman/pikabu/compare/v2.4.1...v2.4.2) (2023-10-17)


### Bug Fixes

* Исправлено скрытие кнопки доната (в мобильной версии оставался пустой блок) ([5fbd8ee](https://gitlab.com/kreoman/pikabu/commit/5fbd8eeb4476f26492b3dd1556fc7d46f80e556f))

### [2.4.1](https://gitlab.com/kreoman/pikabu/compare/v2.4.0...v2.4.1) (2023-10-17)


### Bug Fixes

* Исправлено быстрое добавление в игнор-лист для FireFox ([4ccb75c](https://gitlab.com/kreoman/pikabu/commit/4ccb75c3f908f66c21f5e99dfe196e7317f8a1ec))

## [2.4.0](https://gitlab.com/kreoman/pikabu/compare/v2.3.1...v2.4.0) (2023-10-16)


### Features

* Добавлена ссылка на telegram канал (+ чат) ([0ef75ed](https://gitlab.com/kreoman/pikabu/commit/0ef75edd53178c8a579e55abf8ee0ae0b3d64771))

### [2.3.1](https://gitlab.com/kreoman/pikabu/compare/v2.3.0...v2.3.1) (2023-10-11)


### Bug Fixes

* Исправлено получение минусов (добавлен расчет хэша) ([9148e37](https://gitlab.com/kreoman/pikabu/commit/9148e3717b2c9aa3531bf50dcf415e83fbed78c2))

## [2.3.0](https://gitlab.com/kreoman/pikabu/compare/v2.2.3...v2.3.0) (2023-10-11)


### Features

* Всплывающее меню переписано без использования реакта ([426c6e8](https://gitlab.com/kreoman/pikabu/commit/426c6e8eff6f7c095b80814c300562d1e70d6785))

### [2.2.3](https://gitlab.com/kreoman/pikabu/compare/v2.2.2...v2.2.3) (2023-10-10)


### Bug Fixes

* Исправлено получение данных о регистрации пользователя. ([a6531e2](https://gitlab.com/kreoman/pikabu/commit/a6531e229d2756d7de2fa2718d4e2f6e1bb4b239))

### [2.2.2](https://gitlab.com/kreoman/pikabu/compare/v2.2.1...v2.2.2) (2023-10-10)


### Bug Fixes

* Не корректно определялся идентификатор удаленного поста по url страницы 404 ([ccc79e5](https://gitlab.com/kreoman/pikabu/commit/ccc79e55b2ea8b81b727af55c540d3820473bf3c))

### [2.2.1](https://gitlab.com/kreoman/pikabu/compare/v2.2.0...v2.2.1) (2023-10-09)


### Bug Fixes

* Cannot read properties of null (reading 'innerHTML') ([78c440d](https://gitlab.com/kreoman/pikabu/commit/78c440d64dd0a8989a8107c2e74b888ef13584b8))

## [2.2.0](https://gitlab.com/kreoman/pikabu/compare/v2.1.2...v2.2.0) (2023-10-09)


### Features

* Увеличен интервал передачи индексируемых данных ([2fabb87](https://gitlab.com/kreoman/pikabu/commit/2fabb87fe34db9dba88da6267a856710208416fa))

### [2.1.2](https://gitlab.com/kreoman/pikabu/compare/v2.1.1...v2.1.2) (2023-10-09)


### Bug Fixes

* Убрана дополнительная передача запрашиваемых данных ([0490be3](https://gitlab.com/kreoman/pikabu/commit/0490be33894c8c1ea2f6865a5fdae8801c694061))

### [2.1.1](https://gitlab.com/kreoman/pikabu/compare/v2.1.0...v2.1.1) (2023-10-08)


### Bug Fixes

* Повышена стабильность работы расширения. Уменьшено взаимодействие content <-> background ([d5b1511](https://gitlab.com/kreoman/pikabu/commit/d5b151103a25d2b710440309b203fdbddf666bfd))

## [2.1.0](https://gitlab.com/kreoman/pikabu/compare/v2.0.1...v2.1.0) (2023-10-07)


### Features

* Возможность отключения сжатия передаваемых для индексации данных ([2438713](https://gitlab.com/kreoman/pikabu/commit/24387136f8a95a6b8b84d0a46fb4ce6e04b0a3f2))
* Сжатие передаваемых для индексации данных ([cc09ce6](https://gitlab.com/kreoman/pikabu/commit/cc09ce6ebd299b0375019cdbee46c7294a2a5792))


### Bug Fixes

* Не применялись настройки dev/prod окружения ([dcdf887](https://gitlab.com/kreoman/pikabu/commit/dcdf887b95bd56970aac0672e01f1991048ba4a0))
* При выключении опции "Отображать содержимое удаленных постов" удалялись заголовки постов ([cb2e094](https://gitlab.com/kreoman/pikabu/commit/cb2e094ce3d725267452be5d351db0a3dac1dde8))

### [2.0.1](https://gitlab.com/kreoman/pikabu/compare/v2.0.0...v2.0.1) (2023-10-05)


### Bug Fixes

* Исправлено определение удаленного поста ([13658c8](https://gitlab.com/kreoman/pikabu/commit/13658c8a8d4cb784219697d29717eadb8f02cc2a))

## [2.0.0](https://gitlab.com/kreoman/pikabu/compare/v1.8.0...v2.0.0) (2023-10-05)


### Features

* Возможность форсить мажорный релиз ([a0879f7](https://gitlab.com/kreoman/pikabu/commit/a0879f75d536f9d9b8b3e9382b1a5da36ceb79a2))

## [1.8.0](https://gitlab.com/kreoman/pikabu/compare/v1.7.4...v1.8.0) (2023-10-05)


### Features

* Кэширование данных авторов постов и комментариев ([fb383a9](https://gitlab.com/kreoman/pikabu/commit/fb383a9158d88c1eb77df0b682f9fb73620e0bec))
* Передача информации о версии манифеста ([fb98f54](https://gitlab.com/kreoman/pikabu/commit/fb98f541196393e7d7aa668fc7f262cef2612d53))

### [1.7.4](https://gitlab.com/kreoman/pikabu/compare/v1.7.3...v1.7.4) (2023-09-22)


### Bug Fixes

* HtmlSanitizer as vendor script ([ceb8321](https://gitlab.com/kreoman/pikabu/commit/ceb8321b88d171c43028897a8978ca9ecf4461f0))
* пакет sanitize-html приводил к ошибке в Vivaldi. Заменен на @jitbit/htmlsanitizer ([e46490e](https://gitlab.com/kreoman/pikabu/commit/e46490eaf19478eaa2315ff1c69b8f30962f80b4))

### [1.7.3](https://gitlab.com/kreoman/pikabu/compare/v1.7.2...v1.7.3) (2023-09-21)


### Bug Fixes

* Firefox не восстанавливает SSE соединение самостоятельно ([5fdefa9](https://gitlab.com/kreoman/pikabu/commit/5fdefa902bf3ed49d35a9e38d07c42b6201521dc))
* По ршибке был указан локальный адрес ([5174d68](https://gitlab.com/kreoman/pikabu/commit/5174d6851dccbb2e9baeb2827045cec354d444c6))

### [1.7.2](https://gitlab.com/kreoman/pikabu/compare/v1.7.1...v1.7.2) (2023-09-20)


### Bug Fixes

* Не корректно вставлялись изображения удаленного комментария (не учитывались оригинальные ограничения в 250px по высоте) ([0f58225](https://gitlab.com/kreoman/pikabu/commit/0f5822578393bcb18b119cb28e5a1036d3688a84))

### [1.7.1](https://gitlab.com/kreoman/pikabu/compare/v1.7.0...v1.7.1) (2023-09-20)


### Bug Fixes

* Firefox не умеет использовать :has в querySelector ([811c8be](https://gitlab.com/kreoman/pikabu/commit/811c8be347870d68fab985677c61b4203e6ff630))
* Пропускались удаленные комменты без ссылки на правила ([d3c0e9d](https://gitlab.com/kreoman/pikabu/commit/d3c0e9d97aeee3b9c567f003dfa74c6008ccf8de))

## [1.7.0](https://gitlab.com/kreoman/pikabu/compare/v1.6.0...v1.7.0) (2023-09-20)


### Features

* Добавлена возможность отображения удаленных комментариев ([5a0317d](https://gitlab.com/kreoman/pikabu/commit/5a0317dda02ab6de06e705c72f62ba7b585eef74))
* Добавлена возможность отображения удаленных комментариев ([d928336](https://gitlab.com/kreoman/pikabu/commit/d928336585a27b4dfcf3b3e947770672fd803b96))

## [1.6.0](https://gitlab.com/kreoman/pikabu/compare/v1.5.0...v1.6.0) (2023-08-26)


### Features

* Добавлена возможность получения ника удаленного пользователя, если он был удален до того как был проиндексирован. ([8f005b0](https://gitlab.com/kreoman/pikabu/commit/8f005b076f5c439b75d51bd41388a3cd930c22a6))

## [1.5.0](https://gitlab.com/kreoman/pikabu/compare/v1.4.1...v1.5.0) (2023-08-25)


### Features

* Сохранение комментов ([49c9751](https://gitlab.com/kreoman/pikabu/commit/49c9751bacf06a869d6b0e3a83fe072d929b887b))


### Bug Fixes

* Исправлена работа индикатора даты регистрации пользователя в firefox ([16a38a0](https://gitlab.com/kreoman/pikabu/commit/16a38a08de2353da497356f2b8555cb21cbad4fe))

### [1.4.1](https://gitlab.com/kreoman/pikabu/compare/v1.4.0...v1.4.1) (2023-08-25)


### Bug Fixes

* Отображение индикатора пользователя в мобильной версии ([29fcd01](https://gitlab.com/kreoman/pikabu/commit/29fcd017da2d6ac3adeb6a4f30551f263e15ba83))

## [1.4.0](https://gitlab.com/kreoman/pikabu/compare/v1.3.1...v1.4.0) (2023-08-22)


### Features

* Сбор информации о постах и комментариях ([bc6ecff](https://gitlab.com/kreoman/pikabu/commit/bc6ecff0b4aedee897c02afb46b2d3ff6121a494))
* Сбор информации о рейтинге комментариев ([e7fef02](https://gitlab.com/kreoman/pikabu/commit/e7fef020d590665e6c5d94af344e436f4f395ca6))
* Сбор информации о рейтинге комментариев ([982472d](https://gitlab.com/kreoman/pikabu/commit/982472d848db3b288f8796c175237ae1ee553f92))
* Сбор информации о рейтинге постов ([b93dad0](https://gitlab.com/kreoman/pikabu/commit/b93dad0e7feae664ed685dc51667040bff8164bc))


### Bug Fixes

* Локальный url ([2c7ae97](https://gitlab.com/kreoman/pikabu/commit/2c7ae97539820b49ad335b03817841a0876bcc66))

### [1.3.1](https://gitlab.com/kreoman/pikabu/compare/v1.3.0...v1.3.1) (2023-08-21)


### Bug Fixes

* Исправление получения данных пользователя для firefox ([d33b079](https://gitlab.com/kreoman/pikabu/commit/d33b079067f42135a51b5d8f48c33167273886ad))

## [1.3.0](https://gitlab.com/kreoman/pikabu/compare/v1.2.1...v1.3.0) (2023-08-21)


### Features

* Добавлено получение пользователей из персонального кэша ([70cd81d](https://gitlab.com/kreoman/pikabu/commit/70cd81d7e515fee63c8675b973dd66eb36da7bde))

### [1.2.1](https://gitlab.com/kreoman/pikabu/compare/v1.2.0...v1.2.1) (2023-08-21)


### Bug Fixes

* Удален console.log ([908a7d2](https://gitlab.com/kreoman/pikabu/commit/908a7d2c92fcb5c71cb6a8070e2eaa8fae1bfbc9))

## [1.2.0](https://gitlab.com/kreoman/pikabu/compare/v1.1.1...v1.2.0) (2023-08-21)


### Features

* Получение информации о пользователе ([f78177c](https://gitlab.com/kreoman/pikabu/commit/f78177cad416b0bcd1982eb64f927cbfbacd472d))
* Реализовано отображение индикатора, указывающего на дату регистрации пользователя ([b2423ef](https://gitlab.com/kreoman/pikabu/commit/b2423efccb61573dfdd2a6dca16becd98ebdea48))

### [1.1.1](https://gitlab.com/kreoman/pikabu/compare/v1.1.0...v1.1.1) (2023-08-16)


### Bug Fixes

* Некорректно отображается рейтинг комментариев, если включена опция "показывать дополнительные посты под комментариями" ([1647bc4](https://gitlab.com/kreoman/pikabu/commit/1647bc46bbbd95d26965193c3c3129fe78aac2b8)), closes [#9](https://gitlab.com/kreoman/pikabu/issues/9)

## [1.1.0](https://gitlab.com/kreoman/pikabu/compare/v1.0.3...v1.1.0) (2023-08-16)


### Features

* добавлено отображение загрузки рейтинга комментариев ([a191930](https://gitlab.com/kreoman/pikabu/commit/a1919302bd1ddc3e720b9a54020046ce44179f6f))

### [1.0.3](https://gitlab.com/kreoman/pikabu/compare/v1.0.2...v1.0.3) (2023-08-15)


### Bug Fixes

* remove console.log ([2fc32db](https://gitlab.com/kreoman/pikabu/commit/2fc32dbeb67151c6598fdda5de091757bc5d2a4c))

### [1.0.2](https://gitlab.com/kreoman/pikabu/compare/v1.0.1...v1.0.2) (2023-08-15)


### Bug Fixes

* Для списка постов запришивались данные по всем страницам комментариев, что было избыточно ([cabe9e9](https://gitlab.com/kreoman/pikabu/commit/cabe9e903a63237f86903df2d8b4902351977a8c))
* Запрашивалась только первая 100 комментариев. Сейчас запрашиваются все. ([25d803d](https://gitlab.com/kreoman/pikabu/commit/25d803d9284ecda4a89cba5ce27ac523adc34c88))

### [1.0.1](https://gitlab.com/kreoman/pikabu/compare/v1.0.0...v1.0.1) (2023-08-15)


### Bug Fixes

* Дублировался блок с кастомным рейтингом коммента ([2b5c313](https://gitlab.com/kreoman/pikabu/commit/2b5c3131c7b740bc48b1fa42319f3524ca0478f8))

## [1.0.0](https://gitlab.com/kreoman/pikabu/compare/v0.0.35...v1.0.0) (2023-08-15)


### Bug Fixes

* Исправлено отображение рейтинга у своих комментариев ([fb92d6d](https://gitlab.com/kreoman/pikabu/commit/fb92d6d5256a8430629d297a972c8136674b35d9))

### [0.0.35](https://gitlab.com/kreoman/pikabu/compare/v0.0.34...v0.0.35) (2023-08-13)


### Features

* Собираем user-agent для статистики по браузерам ([e39e29c](https://gitlab.com/kreoman/pikabu/commit/e39e29c4c5489d1694d4745d3813d779dde752ce))

### [0.0.34](https://gitlab.com/kreoman/pikabu/compare/v0.0.33...v0.0.34) (2023-08-13)


### Features

* Сборка под firefox (manifest v2) remove content_security_policy ([52ba202](https://gitlab.com/kreoman/pikabu/commit/52ba2027e32512e06ada5879ef40f89c00c06a57))

### [0.0.33](https://gitlab.com/kreoman/pikabu/compare/v0.0.32...v0.0.33) (2023-08-13)


### Features

* Сборка под firefox (manifest v2) ([e99b1c7](https://gitlab.com/kreoman/pikabu/commit/e99b1c726bd055354d578fb44172091f0b51e182))

### [0.0.32](https://gitlab.com/kreoman/pikabu/compare/v0.0.31...v0.0.32) (2023-08-13)


### Features

* Сборка под firefox (manifest v2) ([b7e2c44](https://gitlab.com/kreoman/pikabu/commit/b7e2c4457a292638b801b35d693b456fcc2d5552))

### [0.0.31](https://gitlab.com/kreoman/pikabu/compare/v0.0.30...v0.0.31) (2023-08-13)


### Features

* Сборка под firefox (manifest v2) ([eb3f98a](https://gitlab.com/kreoman/pikabu/commit/eb3f98ad81a8a7bb058c3dc525506d012aa14792))

### [0.0.30](https://gitlab.com/kreoman/pikabu/compare/v0.0.29...v0.0.30) (2023-08-13)


### Features

* Косметические изменения применяются глобально ко всей странице ([7d9b485](https://gitlab.com/kreoman/pikabu/commit/7d9b485a05382dbfbfdcf97fe86b19bec900bf09))

### [0.0.29](https://gitlab.com/kreoman/pikabu/compare/v0.0.28...v0.0.29) (2023-08-13)


### Features

* Добавлено динамическое включение/выключение рейтинга в мобильной версии ([f9f1616](https://gitlab.com/kreoman/pikabu/commit/f9f161699956b1cc3628938f0548a6f8a6f571f3))

### [0.0.28](https://gitlab.com/kreoman/pikabu/compare/v0.0.27...v0.0.28) (2023-08-13)


### Features

* **popup:** settings in popup ([dd9ec9f](https://gitlab.com/kreoman/pikabu/commit/dd9ec9f3cbcae40da323b59f4952e85bb1627b8f))
* Добавлено меню ([4a1ddb6](https://gitlab.com/kreoman/pikabu/commit/4a1ddb6be4ba0b51bfa907104d3391eb963130e1))
* Добавлены косметические опции: скрытие доната и скрытие дизайна коротких постов ([43ee5c2](https://gitlab.com/kreoman/pikabu/commit/43ee5c2584229c46de5d61ed5a889c4f4ca24b11))


### Bug Fixes

* Убрано мерцание рейтинга при подгрузке новых постов ([052c751](https://gitlab.com/kreoman/pikabu/commit/052c75107c1e5c57608c63d54929304064ecc20d))

### [0.0.27](https://gitlab.com/kreoman/pikabu/compare/v0.0.26...v0.0.27) (2023-08-13)

### [0.0.26](https://gitlab.com/kreoman/pikabu/compare/v0.0.25...v0.0.26) (2023-08-11)


### Bug Fixes

* Ловим ошибки по каждому потоку, чтоб не ломать общий ([c15cbc3](https://gitlab.com/kreoman/pikabu/commit/c15cbc346432178a1a5b4c54dc128fb1b484c050))

### [0.0.25](https://gitlab.com/kreoman/pikabu/compare/v0.0.24...v0.0.25) (2023-08-10)

### [0.0.24](https://gitlab.com/kreoman/pikabu/compare/v0.0.23...v0.0.24) (2023-08-10)


### Features

* Добавлен сбор статистики использования и сведений об ошибках ([8abbc7b](https://gitlab.com/kreoman/pikabu/commit/8abbc7bbb6c6f86d8ec4ab2beec4630c271b3b89))
* Убрал замену "На три месяца" на "Навсегда", так как разрабы соизволили сами вернуть этот пункт в меню ([bd752b1](https://gitlab.com/kreoman/pikabu/commit/bd752b1512585869bbe13bcdeb0a51e2f0be4983))

### [0.0.23](https://gitlab.com/kreoman/pikabu/compare/v0.0.22...v0.0.23) (2023-08-08)


### Features

* Добавлена поддержка мобильной версии ([b3770d5](https://gitlab.com/kreoman/pikabu/commit/b3770d575932ddc01f223d08a8fc27893756f2c8))
* Добавлена поддержка мобильной версии ([fe44f30](https://gitlab.com/kreoman/pikabu/commit/fe44f303555b5179a78071fdfccdc0ab988a6f87))

### [0.0.22](https://gitlab.com/kreoman/pikabu/compare/v0.0.21...v0.0.22) (2023-08-08)


### Bug Fixes

* Забыт стиль для story-vote ([4b41be3](https://gitlab.com/kreoman/pikabu/commit/4b41be39076f73ec3861f835a36e408d73572180))

### [0.0.21](https://gitlab.com/kreoman/pikabu/compare/v0.0.20...v0.0.21) (2023-08-08)


### Features

* Сборка дистрибутива в production mode ([a1848f9](https://gitlab.com/kreoman/pikabu/commit/a1848f9436d10ae72453cf110be665c75b3cc6ac))

### [0.0.20](https://gitlab.com/kreoman/pikabu/compare/v0.0.19...v0.0.20) (2023-08-08)


### Features

* Добавлена обработка ситуации со скрытым рейтингом, когда пост был создан менее часа назад. В таком случае отображается серая шкала с хинтом. ([dddceca](https://gitlab.com/kreoman/pikabu/commit/dddceca9cfbece062390715ce86f66904ba17eb6))

### [0.0.19](https://gitlab.com/kreoman/pikabu/compare/v0.0.18...v0.0.19) (2023-08-07)


### Features

* Изменено отображение минусов и плюсов поста. Теперь используется верстка сайта со шкалой. ([1d825a9](https://gitlab.com/kreoman/pikabu/commit/1d825a94bb3e614a1c41c4b5aa1b5df1c096eb9f))

### [0.0.18](https://gitlab.com/kreoman/pikabu/compare/v0.0.17...v0.0.18) (2023-08-07)


### Bug Fixes

* При обновлении (раскрытии) комментариев дублировался блок с рейтингом поста ([9546a7a](https://gitlab.com/kreoman/pikabu/commit/9546a7ac7b3b0f322b5f348de00c4ce2cbcd77b6))

### [0.0.17](https://gitlab.com/kreoman/pikabu/compare/v0.0.16...v0.0.17) (2023-08-07)


### Bug Fixes

* Добавлена обработка ошибок при получении информации о рейтинге ([1cc559f](https://gitlab.com/kreoman/pikabu/commit/1cc559fc8a6674c64bab974354cdd033c05c571a))

### [0.0.16](https://gitlab.com/kreoman/pikabu/compare/v0.0.15...v0.0.16) (2023-08-07)


### Bug Fixes

* Добавлена обработка ошибок при получении информации о рейтинге ([661503d](https://gitlab.com/kreoman/pikabu/commit/661503d590f807622ecd0a6722221ae5b7abfc6c))

### [0.0.15](https://gitlab.com/kreoman/pikabu/compare/v0.0.14...v0.0.15) (2023-08-07)

### [0.0.14](https://gitlab.com/kreoman/pikabu/compare/v0.0.13...v0.0.14) (2023-08-06)


### Bug Fixes

* Скрыл недоработанное меню ([2b97c16](https://gitlab.com/kreoman/pikabu/commit/2b97c1630337b9123ce70322742d435639dd499d))

### [0.0.13](https://gitlab.com/kreoman/pikabu/compare/v0.0.12...v0.0.13) (2023-08-06)


### Features

* Добавлены минусы ([82067b3](https://gitlab.com/kreoman/pikabu/commit/82067b360e490d33e7f3754e7414fb464e429e03))

### [0.0.12](https://gitlab.com/kreoman/pikabu/compare/v0.0.11...v0.0.12) (2023-08-03)

### [0.0.11](https://gitlab.com/kreoman/pikabu/compare/v0.0.10...v0.0.11) (2023-08-03)

### [0.0.10](https://gitlab.com/kreoman/pikabu/compare/v0.0.9...v0.0.10) (2023-08-03)

### [0.0.9](https://gitlab.com/kreoman/pikabu/compare/v0.0.8...v0.0.9) (2023-08-03)

### [0.0.8](https://gitlab.com/kreoman/pikabu/compare/v0.0.7...v0.0.8) (2023-08-03)

### [0.0.7](https://gitlab.com/kreoman/pikabu/compare/v0.0.6...v0.0.7) (2023-08-03)

### [0.0.6](https://gitlab.com/kreoman/pikabu/compare/v0.0.5...v0.0.6) (2023-08-03)

### [0.0.5](https://gitlab.com/kreoman/pikabu/compare/v0.0.4...v0.0.5) (2023-08-03)

### [0.0.4](https://gitlab.com/kreoman/pikabu/compare/v0.0.3...v0.0.4) (2023-08-03)

### 0.0.3 (2023-08-03)


### Features

* Добавлена кнопка быстрого бана автора поста ([27f8fff](https://gitlab.com/kreoman/pikabu/commit/27f8fff39f9dd8e68909b39bff82a24e150d0cba))


### Bug Fixes

* Модификация меню блокировки работает не только для пользователя, но и для тегов ([dd73ddc](https://gitlab.com/kreoman/pikabu/commit/dd73ddcfcebb697b61a6c0f53598dd808330e145))
