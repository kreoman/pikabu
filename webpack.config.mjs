import { dirname } from 'node:path';
import { fileURLToPath } from 'node:url';

import CopyPlugin from 'copy-webpack-plugin';
import { globSync } from 'glob';
import path from 'path';

import { ExtensionRuntimeTypeEnum } from '@des-kit/extension';
import { getKitEntryPointFiles } from '@des-kit/extension-build';

const __dirname = dirname(fileURLToPath(import.meta.url));

const FileNameMap = {
    [ExtensionRuntimeTypeEnum.Background]: 'background',
    [ExtensionRuntimeTypeEnum.Popup]: 'popup',
    [ExtensionRuntimeTypeEnum.ContentScript]: 'content-script',
    [ExtensionRuntimeTypeEnum.PageScript]: 'page-script',
};

/**
 *
 * @param runtime ExtensionRuntimeTypeEnum
 * @returns {string[]}
 */
function getEntryPoints(runtime) {
    const kitFiles = getKitEntryPointFiles(runtime);

    const filename = FileNameMap[runtime];

    let entries = globSync(`./src/**/{${filename},all}.entry.{ts,tsx,js}`);

    entries = entries.map(
        (point) => './' + point.split(path.sep).join(path.posix.sep),
    );

    entries.sort();

    return kitFiles.concat(entries);
}

export default {
    entry: {
        'content-script': getEntryPoints(
            ExtensionRuntimeTypeEnum.ContentScript,
        ),
        'page-script': getEntryPoints(ExtensionRuntimeTypeEnum.PageScript),
        background: getEntryPoints(ExtensionRuntimeTypeEnum.Background),
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.s[ac]ss$/i,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
            {
                test: /\.svg$/i,
                type: 'asset/inline',
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
        fallback: {
            crypto: false,
            buffer: false,
        },
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        clean: true,
    },
    mode: 'development',
    devtool: 'source-map',
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: 'src/assets',
                    to: '.',
                },
            ],
        }),
    ],
    performance: {
        maxAssetSize: 1500000,
        maxEntrypointSize: 1500000,
    },
};
