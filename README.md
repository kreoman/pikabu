## Расширение восстанавливает отображение рейтинга постов и добавляет возможности по быстрому добавлению пользователя в игнор лист.

### Основные возможности:

- Шкала рейтинга поста с раздельным отображением плюсов и минусов
- Кнопка в тулбаре поста для быстрого добавления автора в игнор-лист

<details>
<summary>Скриншот</summary>

![frame-1.png](docs/frame-1.png)
</details>


- Цветовой индикатор срока регистрации пользователя на пикабу

<details>
<summary>Скриншот</summary>

![frame-2.png](docs/frame-2.png)
</details>

- Теги, в которых пользователь наиболее активен


<details>
<summary>Скриншот</summary>

![frame-3.png](docs/frame-3.png)
</details>

- Настройки расширения

<details>
<summary>Скриншот</summary>

![frame-4.png](docs/frame-4.png)
</details>


- Скачивание видео для встроенного плеера пикабу

<details>
<summary>Скриншот</summary>

![frame-5.png](docs/frame-5.png)
</details>

## Установка:

---

### Из магазина расширений:

[Chrome](https://chrome.google.com/webstore/detail/pikabu-fixes/nochogffendelpmbpajbdpaodbibgdmk)
[Mozilla](https://addons.mozilla.org/ru/firefox/addon/pikabu-fixes/)

_Из-за наличия модерации, версия в магазине может сильно отставать по функционалу от последней сборки на странице релизов._

---

### Из репозитория:

#### Chrome
- Скачать **pikabu-chrome-extension.zip** со страницы релизов: https://gitlab.com/kreoman/pikabu/-/releases
- Перейти на вкладку "Расширения" (chrome://extensions/)
- Включить "Режим разработчика"
- Перетащить скачаный архив в список расширений

#### Яндекс.Браузер
- Скачать **pikabu-chrome-extension.zip** со страницы релизов: https://gitlab.com/kreoman/pikabu/-/releases
- Распаковать **pikabu-chrome-extension.zip**
- Перейти в "Дополнения" (browser://tune/)
- Перетащить папку распакованного архива в список дополнений

#### Opera One
- Скачать **pikabu-chrome-extension.zip** со страницы релизов: https://gitlab.com/kreoman/pikabu/-/releases
- Распаковать **pikabu-chrome-extension.zip**
- Открыть меню "Расширения" (Ctrl+Shift+E)
- В правом верхнем углу страницы включить "Режим разработчика"
- Вверху страницы нажать "Загрузить распакованное расширение"
- Выбираем папку с разархивированным расширением, нажимаем "Выбор папки"
- Перезапустить браузер

#### Firefox
- Скачать **pikabu-ff-extension.zip** со страницы релизов: https://gitlab.com/kreoman/pikabu/-/releases
- Распаковать **pikabu-ff-extension.zip**
- Открыть страницу "Управление дополнениями" (about:addons)
- Открыть меню управления расширениями (шестеренка)
- Отладка дополнений
- Загрузить временное дополнение
- Выбрать файл manifest.json из папки распакованного архива

_После закрытия браузера дополнение необходимо будет установить заново._ 
